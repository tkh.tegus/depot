﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class InventaireController : Controller
    {
        Services service = new Services();
        public class JqwAjax
        {
            public Int64 id { get; set; }
            public DateTime Date { get; set; }
            public string NUM_DOC { get; set; }
            public string Utilisateur { get; set; }
            public string Statut { get; set; }               
            public string Motif { get; set; }
        }

        public JsonResult ajax(byte id)
        {
            //id == 0 Inventaire liquide
            //id == 1 inventaire emballage
            StoreEntities db = new StoreEntities();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.TP_Inventaire
                           where c.Type == id && c.Create_Date >= filtre.DateDebut && c.Create_Date < filtre.CDateFin
                           orderby c.Etat, c.Create_Date descending
                           select new JqwAjax
                           {
                               id = c.ID_Inventaire,
                               Date = c.Create_Date,
                               Motif = c.Motif,
                               NUM_DOC = c.NUM_DOC,
                               Statut = (c.Etat == 0 ? "En attente de validation" : (c.Etat == 1 ? "Valider" : (c.Etat == 2 ? "Stock Réguler" : "Annuler"))),
                               Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == c.Create_Code_User).Select(p => p.Libelle).FirstOrDefault()
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public class JqwAjaxInventaire
        {
            public Int64 id { get; set; }
            public string Produit { get; set; }
            public string Famille { get; set; }
            public int Qte_Casier_Theorique { get; set; }
            public int Qte_Casier_Reel { get; set; }
            public int Qte_Plastique_Theorique { get; set; }
            public int Qte_Plastique_Reel { get; set; }
            public int Qte_Bouteille_Theorique { get; set; }
            public int Qte_Bouteille_Reel { get; set; }
            public int Ecart_Casier 
            {
                get
                {
                    return Qte_Casier_Reel - Qte_Casier_Theorique; 
                }
                set { }
            }
            public int Ecart_Plastique
            {
                get
                {
                    return Qte_Plastique_Reel - Qte_Plastique_Theorique;
                }
                set { }
            }
            public int Ecart_Bouteille 
            {
                get
                {
                    return Qte_Bouteille_Reel - Qte_Bouteille_Theorique;
                }
                set { } 
            }           
        }

        public JsonResult ajaxInventaire(int id)
        {
            
            StoreEntities db = new StoreEntities();
            var resultat = (from c in db.TP_Inventaire_Detail
                           let type = db.TP_Inventaire.Where(p=>p.ID_Inventaire == c.ID_Inventaire).FirstOrDefault().Type
                           where c.ID_Inventaire == id    
                           
                           select new JqwAjaxInventaire
                           {
                               id = c.ID_Inventaire_Detail,
                               Famille = (type == 0 ? db.TP_Produit.Where(p => p.ID_Produit == c.ID_Element).Select(p => p.TP_Famille.Libelle).FirstOrDefault() : ""),
                               Produit = (type == 0 ? db.TP_Produit.Where(p => p.ID_Produit == c.ID_Element).FirstOrDefault().Libelle : db.TP_Emballage.Where(p => p.ID_Emballage == c.ID_Element).FirstOrDefault().Code),
                               Qte_Casier_Reel = c.Quantite_Casier_Reel,
                               Qte_Casier_Theorique = c.Quantite_Casier_Theorique,
                               Qte_Plastique_Reel = c.Quantite_Plastique_Reel,
                               Qte_Plastique_Theorique = c.Quantite_Plastique_Theorique,
                               Qte_Bouteille_Reel = c.Quantite_Bouteille_Reel,
                               Qte_Bouteille_Theorique = c.Quantite_Bouteille_Theorique
                           }).OrderBy(p=>p.Famille);
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Index(byte id)
        {
            //id == 0 Inventaire liquide
            //id == 1 inventaire emballage
            string[] tab_Habillitation = new string[3];
            if ((id == 0 && !service.VerifierHabiliationIndex("Gestion_des_inventaire_liquide", this, ref tab_Habillitation)) || (id == 1 && !service.VerifierHabiliationIndex("Gestion_des_inventaire_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }

            DateTime date = new DateTime(DateTime.Now.Year, 01, 01);
            Session["DateDebut"] = date.ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            TempData["type"] = id.ToString();
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Inventaire/Index.cshtml", filtre);
            if(id == 0)
            {
                tab[1] = "GESTION DES INVENTAIRES LIQUIDES";
            }
            else
            {
                tab[1] = "GESTION DES INVENTAIRES EMBALLAGES";
            }
            
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Opération</a></li> <li class='active'>Liste des inventaires</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Index_Detail(string id)
        {
            byte type = byte.Parse(id.Split(';')[0].ToString());
            Int64 ID_Element = byte.Parse(id.Split(';')[1].ToString());
            //type == 0 Inventaire liquide
            //type == 1 inventaire emballage
            string[] tab_Habillitation = new string[3];
            if ((type == 0 && !service.VerifierHabiliationIndex("Gestion_des_inventaire_liquide", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationIndex("Gestion_des_inventaire_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            var lib = db.TP_Inventaire.Where(p => p.ID_Inventaire == ID_Element).FirstOrDefault();
            TempData["ID_Inventaire"] = ID_Element.ToString();
            TempData["Cloturer"] = (lib.Etat == 0 ? 0 : 1);
            TempData["type"] = type.ToString();
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Inventaire/Index_Detail.cshtml", null);
            tab[1] = "Détail Inventaire N° : "+lib.NUM_DOC+ " du "+lib.Create_Date.ToString("dd/MM/yyyy");
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Opération</a></li> <li class='active'>Inventaire</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Ajouter")]
        public JsonResult Ajouter(byte id)
        {
            //id == 0 Inventaire liquide
            //id == 1 inventaire emballage
            string[] tab_Habillitation = new string[1];
            if ((id == 0 && !service.VerifierHabiliationCRUD("Ajouter_inventaire_liquide", this, ref tab_Habillitation)) || (id == 1 && !service.VerifierHabiliationCRUD("Ajouter_inventaire_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }

            InventaireBM ObjetBM = new InventaireBM();
            InventaireVM Return_Var = new InventaireVM();
            Return_Var.Type = id;
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Inventaire/Ajouter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(InventaireVM Param_Var)
        {
            StoreEntities db = new StoreEntities();
            Ouverture_Cloture_CaisseBM caisse = new Ouverture_Cloture_CaisseBM();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            if (db.TP_Ouverture_Cloture_Caisse.Where(p=>p.Etat == 0).FirstOrDefault() != null)
            {
                TempData["messagewarning"] = "Veuillez fermer toutes les caisse avant de faire l'inventaire";
                string[] tab = new string[2];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    InventaireBM Local_Var = new InventaireBM();
                    Int64 ID_Inventaire = Local_Var.Save(Param_Var, Session["Code_User"].ToString());
                    TempData["messagesucess"] = "Ajout effectué avec succès";
                    string[] tab = new string[2];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = ID_Inventaire.ToString();
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string[] tab = new string[1];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Inventaire/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }
       
         [ActionName("Modifier")]
        public JsonResult Modifier(string id)
        {
            Int64 ID_Decaissement = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            //LocalVM.Type_Element = 0 => Liquide
            //LocalVM.Type_Element = 1 => Emballage
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Modifier_inventaire_liquide", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Modifier_inventaire_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(ID_Decaissement, "TP_Inventaire", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            InventaireBM ObjetBM = new InventaireBM();
            InventaireVM LocalVM = ObjetBM.GetByID(ID_Decaissement);
            if (LocalVM == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            
            

            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Inventaire/Modifier.cshtml", LocalVM);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(string id, InventaireVM Param_Var)
        {
            Int64 ID_ = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    InventaireBM ObjetBM = new InventaireBM();
                    ObjetBM.Edit(ID_, Param_Var, Session["Code_User"].ToString());
                    // libère le verrou précédemment pauser
                    Locked_RecordsBM veroux = new Locked_RecordsBM();
                    veroux.Liberer(ID_, "TP_Inventaire");
                    TempData["messagesucess"] = "Modification effectuée avec succès";
                    string[] tab = new string[2];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["messagewarning"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Inventaire/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }


        [ActionName("Valider")]
        public JsonResult Valider(string id)
        {
            Int64 ID_Decaissement = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Valider_inventaire_liquide", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Valider_inventaire_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                string messageRetour = string.Empty;
                InventaireBM Local_Var = new InventaireBM();
                Local_Var.Valider(ID_Decaissement, Session["Code_User"].ToString());
                TempData["messagesucess"] = "Valider effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Inventaire/Valider/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

        }

        [ActionName("Annuler")]
        public JsonResult Annuler(string id)
        {
            Int64 ID_Decaissement = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Annuler_inventaire_liquide", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Annuler_inventaire_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                InventaireBM Local_Var = new InventaireBM();
                Local_Var.Annuler(ID_Decaissement, Session["Code_User"].ToString());
                TempData["messagesucess"] = "Action effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Inventaire/Annuler/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

        }

        [ActionName("Reguler")]
        public JsonResult Reguler(string id)
        {
            Int64 ID_Decaissement = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Reguler_inventaire_liquide", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Reguler_inventaire_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                InventaireBM Local_Var = new InventaireBM();
                Local_Var.Reguler(ID_Decaissement, Session["Code_User"].ToString());
                TempData["messagesucess"] = "Action effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Inventaire/Reguler/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet, ActionName("Edit_Detail_Grille_Inventaire")]
        public JsonResult Edit_Detail_Grille_Inventaire(Int64 id, int key, int value)
        {
            InventaireBM ObjetBM = new InventaireBM();
            ObjetBM.Edit_Detail_Grille_Inventaire(id, key, value);
            string[] tab = new string[2];//2 pour savoir que c'est ok
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Consulter")]
        public JsonResult Consulter(Int64 id)
        {
            TempData["id"] = id;
            InventaireBM Local_Var = new InventaireBM();
            InventaireVM Return_Var = Local_Var.GetByID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Inventaire/Consulter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
    }
}
