﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class ProduitController : Controller
    {    
        Services service = new Services();
        public class JqwAjax
        {
            public int id { get; set; }
            public string Producteur { get; set; }
            public string Emballage { get; set; }
            public string Libelle { get; set; }
            public double PU_Achat { get; set; }
            public double PU_Vente { get; set; }
            public bool Valeur_Emballage { get; set; }
            public bool Produit_Decomposer { get; set; }
            public string Code { get; set; }
            public string statut { get; set; }
            public int Conditionnement { get; set; }
        }

        public class Element
        {
            public int ID_Element { get; set; }
            public string Libelle { get; set; }
        }

        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();
            var resultat = from c in db.TP_Produit
                           orderby c.Actif descending, c.Code, c.Libelle
                           select new JqwAjax
                           {
                               id = c.ID_Produit,
                               Producteur = c.TP_Famille.Libelle,
                               Emballage = c.TP_Emballage.Code,
                               Libelle = c.Libelle,
                               PU_Achat = c.PU_Achat,
                               PU_Vente = c.PU_Vente,
                               Code = c.Code,
                               Valeur_Emballage = c.Valeur_Emballage,
                               Produit_Decomposer = c.Produit_Decomposer,
                               statut = c.Actif == true ? "Actif" : "Suspendu"
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }
      
        public JsonResult Index()
        {
           
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_produits", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Produit/Index.cshtml", null);
            tab[1] = "GESTION DES PRODUITS";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de base </a></li> <li class='active'>Liste des produits</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
       
        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {          
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Ajouter_Produit", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            TempData["ID_Famille"] = new SelectList(from c in db.TP_Famille
                                                       orderby c.Libelle
                                                       where c.Actif
                                                       select new
                                                       {
                                                           ID_Famille = c.ID_Famille,
                                                           Libelle = c.Libelle
                                                       }, "ID_Famille", "Libelle");
            int ID_Famille = ((TempData["ID_Famille"] as SelectList).Count() == 0 ? 0 : int.Parse((TempData["ID_Famille"] as SelectList).FirstOrDefault().Value));
            TempData["ID_Emballage"] = new SelectList(from c in db.TP_Emballage
                                                        orderby c.Code
                                                        where c.Actif
                                                        select new
                                                        {
                                                            ID_Emballage = c.ID_Emballage,
                                                            Code = c.Code+ " : "+c.Description
                                                        }, "ID_Emballage", "Code");
            var Produit = (from c in db.TP_Produit
                           where c.ID_Famille == ID_Famille
                           select new Element
                           {
                               ID_Element = c.ID_Produit,
                               Libelle = c.Libelle
                           }).ToList();
           
            TempData["ID_Produit_Base"] = new SelectList(Produit, "ID_Element", "Libelle");
            var emballage = new List<Element>
            {
                new Element { ID_Element= 1, Libelle = "C12" },
                new Element { ID_Element= 2, Libelle = "C15" },
                new Element { ID_Element= 3, Libelle = "C24" }
            };
            TempData["ID_Plastique_Base"] = new SelectList(emballage, "ID_Element", "Libelle");
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Produit/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        
        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(ProduitVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    ProduitBM Local_Var = new ProduitBM();
                    if (Local_Var.IsNotExist(Param_Var, ref messageRetour))
                    {
                        Local_Var.Save(Param_Var, Session["Code_User"].ToString());
                        TempData["messagesucess"] = "Ajout effectué avec succès";
                        string[] tab = new string[2];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string[] tab = new string[1];
                    TempData["messageerror"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);                    
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Produit/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

      
        [ActionName("Modifier")]
        public JsonResult Modifier(int id)
        {
            
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Modifier_Produit", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(id, "TP_Produit", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }

            ProduitBM Local_Var = new ProduitBM();
            ProduitVM Return_Var = Local_Var.GetByID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            TempData["ID_Famille"] = new SelectList(from c in db.TP_Famille
                                                       orderby c.Libelle
                                                       where c.Actif
                                                       select new
                                                       {
                                                           ID_Famille = c.ID_Famille,
                                                           Libelle = c.Libelle
                                                       }, "ID_Famille", "Libelle");         
            TempData["ID_Emballage"] = new SelectList(from c in db.TP_Emballage
                                                      orderby c.Code
                                                      where c.Actif
                                                      select new
                                                      {
                                                          ID_Emballage = c.ID_Emballage,
                                                          Code = c.Code + " : " + c.Description
                                                      }, "ID_Emballage", "Code");
            var Produit = (from c in db.TP_Produit
                           where c.ID_Famille == Return_Var.ID_Famille && c.ID_Produit != id
                           select new Element
                           {
                               ID_Element = c.ID_Produit,
                               Libelle = c.Libelle
                           }).ToList();
            
            TempData["ID_Produit_Base"] = new SelectList(Produit, "ID_Element", "Libelle");
            var emballage = new List<Element>
            {
                new Element { ID_Element= 1, Libelle = "C12" },
                new Element { ID_Element= 2, Libelle = "C15" },
                new Element { ID_Element= 3, Libelle = "C24" }
            };
            TempData["ID_Plastique_Base"] = new SelectList(emballage, "ID_Element", "Libelle");
            Return_Var.ID_Produit = id;
            Return_Var.ID_Produit_Base = (Return_Var.ID_Produit_Base == null ? 0 : Return_Var.ID_Produit_Base);
            Return_Var.ID_Plastique_Base = (Return_Var.ID_Plastique_Base == null ? 0 : Return_Var.ID_Plastique_Base);
            Return_Var.ID_Bouteille_Base = (Return_Var.ID_Bouteille_Base == null ? 0 : Return_Var.ID_Bouteille_Base);
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Produit/Modifier.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(int id, ProduitVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    // TODO: Add update logic here
                    ProduitBM Return_Var = new ProduitBM();
                    if (Return_Var.IsNotExist(Param_Var, id, ref messageRetour)) //vérifie si le Type_Depenses existe déjà
                    {
                        Return_Var.Edit(id, Param_Var, Session["Code_User"].ToString());
                        // libère le verrou précédemment pauser
                        Locked_RecordsBM veroux = new Locked_RecordsBM();
                        veroux.Liberer(id, "TP_Produit");
                        TempData["messagesucess"] = "Modification effectuée avec succès";
                        string[] tab = new string[2];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    TempData["messagewarning"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Produit/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName("Activer_Suspendre")]
        public JsonResult Activer_Suspendre(string id)
        {
           
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Activer_suspendre_Produit", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                string[] tab = id.Split(';');
                for (int i = 0; i < tab.Length; i++)
                {
                    ProduitBM Local_Var = new ProduitBM();
                    Local_Var.Activer_Suspendre(int.Parse(tab[i]), Session["Code_User"].ToString());
                }
                TempData["messagesucess"] = "Action effectuée avec succès";
                string[] tab1 = new string[1];
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Emballage/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
           
        }

        public JsonResult ChangeFamille(string id)
        {
            int ID_Famille = int.Parse(id.Split(';')[0]);
            int ID_Produit = int.Parse(id.Split(';')[1]);
            StoreEntities db = new StoreEntities();
            var Produit = (from c in db.TP_Produit
                           where c.ID_Famille == ID_Famille && c.ID_Produit != ID_Produit
                           select new Element
                           {
                               ID_Element = c.ID_Produit,
                               Libelle = c.Libelle
                           }).ToList();
            
            TempData["ID_Produit_Base"] = new SelectList(Produit, "ID_Element", "Libelle");
            return this.Json(TempData["ID_Produit_Base"], JsonRequestBehavior.AllowGet);
        }

    }
}
