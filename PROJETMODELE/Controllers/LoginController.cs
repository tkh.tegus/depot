﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;
using System.Web.Security;
using System.Configuration;
using PROJETMODELE.Models.DAL;
using System.IO;
using System.Net;
using System.Text;

namespace PROJETMODELE.Controllers
{
    public class LoginController : Controller
    {
        Services service = new Services();      
        public JsonResult Compte()
        {
            string[] tab = new string[1];
            LoginBM login = new LoginBM();
         
            var comptes =  login.GetCompte(int.Parse(Session["Code_User"].ToString()));
            Compte comp = new Compte();
            comp.ville = comptes.ville;
            comp.ID_Personnel = comptes.ID_Personnel;
            comp.login = comptes.login;
            comp.Nom = comptes.Nom;
            comp.nationalite = comptes.nationalite;
            comp.poste = comptes.poste;
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Login/compte.cshtml", comp);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
         }
        [HttpPost, ActionName("Compte")]
        [ValidateAntiForgeryToken]
        public JsonResult Compte(Compte compte)
        {
            StoreEntities db = new StoreEntities();
            int id = int.Parse(Session["Code_User"].ToString());
             var utilisateur = db.TG_Utilisateurs.Where(p => p.ID_Personnel == id).FirstOrDefault();
           
            if (compte.newpass == compte.retapnewpass)
            {
                string messageRetour = string.Empty;
                LoginBM ObjetBM = new LoginBM();
                if (utilisateur.Somme_Ctrl == ObjetBM.cryptage(compte.pass))
                {
                    utilisateur.Somme_Ctrl = ObjetBM.cryptage(compte.newpass);
                    db.SaveChanges();
                    TempData["messagesucess"] = "Mot de passe changé avec succés";
                    string[] tab = new string[2];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string[] tab = new string[1];
                    TempData["messagewarning"] = "L'ancien mot de passe est incorrect ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                string[] tab = new string[1];
                TempData["messagewarning"] = "Le mot de passe est différent ";
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }
        [AllowAnonymous]
        public ActionResult Login()
        {
            StoreEntities db = new StoreEntities();
            try
            {
                 //var all1 = db.TG_Variables.ToList();

            }
            catch (Exception ex)
            {
                return RedirectToAction("parametre");
            }
            return View();
        }
         [AllowAnonymous]
        [HttpPost, ActionName ( "Login" )]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login Login)
        {
            StoreEntities db = new StoreEntities();
            string reference = "";
            try
            {
                if (ModelState.IsValid)
                {
                    string message = string.Empty;
                    Licence licence = new Licence();
                    //if (licence.VerificationLicence(ref message))
                    //{
                        LoginBM ObjetBMRetunr = new LoginBM();                     
                            int ID_Personnel = 0;
                            if (ObjetBMRetunr.UserLogin(Login.Libelle, Login.Passe, Login.check, ref message, ref reference, ref ID_Personnel))
                            {
                                ViewBag.Message = message;
                                Session["Code_User"] = reference;
                                Session["Format"] = "0";
                                Session["Langue"] = "0";
                                Session["ID_Caisse"] = db.TG_Personnels.Where(p => p.ID_Personnel == ID_Personnel).Select(p => p.ID_Caisse).FirstOrDefault();
                                int jour_restant = licence.JoursRestant();
                                Session["jours_restant"] = (jour_restant < 31? "Votre licence expire dans " + jour_restant + " jour(s). Veuillez contacter votre fournisseur" : "");
                                return RedirectToAction("index", "home");
                            }
                            else
                            {
                                Session["messages"] = message;
                                return View();
                            }
                    //}
                    //else
                    //{
                    //    Session["messages"] = message;
                    //    return View();
                    //}
                }
                else
                {
                    Session["messages"] = "Le login et mot de passe obligatoire";
                    return View ( );
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM ( );
                Log.Save(this, ex, "POST: /Connexion/Connect", Session["Code_User"].ToString());
                ViewBag.Message = "Exception rencontrée";
                return View ( );
            }
        }
         [AllowAnonymous]
        public ActionResult parametre()
        {
            
            ViewBag.Message = "Paramétre de connexion au gestionnaire de base de données Invalide !!";
            return View();
        }
        public ActionResult Logout()
        {
            Locked_RecordsBM logrecord = new Locked_RecordsBM();
            logrecord.LibererTous(int.Parse(Session["Code_User"].ToString()));
            FormsAuthentication.SignOut();
            Session["messages"] = "Déconnexion éffectuée avec succès ";
            return RedirectToAction("Login", "Login");
          
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult parametre(Parametre par)
        {
            Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            ConnectionStringsSection section = config.GetSection("connectionStrings") as ConnectionStringsSection;
            if (ModelState.IsValid)
            {
                string ancien = section.ConnectionStrings["StoreEntities"].ConnectionString;
                section.ConnectionStrings["StoreEntities"].ConnectionString = "metadata=res://*/Models.DAL.Store.csdl|res://*/Models.DAL.Store.ssdl|res://*/Models.DAL.Store.msl;provider=System.Data.SqlClient;provider connection string='data source="+par.server+";initial catalog="+par.bd+";user id="+par.user+";password="+par.pass+";MultipleActiveResultSets=True;App=EntityFramework'";
                config.Save();
                StoreEntities _entities = new PROJETMODELE.Models.DAL.StoreEntities();
                try
                {
                    if (service.ConnexionOk(par.bd, par.user, par.pass, par.server))
                    {
                        CreateInit(par.bd, par.server, par.user, par.pass);
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                     
                        ViewBag.Message = "Paramétre Incorrect";
                        return View(par);
                    }
                }
                catch (Exception ex)
                {
                  //  section.ConnectionStrings["StoreEntities"].ConnectionString = ancien;
                   // config.Save();
                    ViewBag.Erreur = ex.InnerException.Message;
                    ViewBag.Message  = "Paramétre Incorrect";
                    return View(par);
                }
            }
            else
            {
                ViewBag.Message = "Modele Incorrect" ;
                return View();
            }
        }
        private static string Chaine(string tpl_path)
        {
            string strLine;
            string nom = "";
            string var = "";
            int j = 0;
            bool valeur = false;
            StreamReader readtemplte;
            readtemplte = new StreamReader(tpl_path);
            while ((strLine = readtemplte.ReadLine()) != null)
            {
                for (int i = 0; i < strLine.Length; i++)
                {
                    if (strLine[i].Equals('=') && !valeur)
                        valeur = true;
                    if (valeur)
                        var = var + strLine[i];
                    else
                        nom = nom + strLine[i];
                }
                nom = nom + "#";
                var = var + "#";
                valeur = false;

            }
            readtemplte.Close();
            return var;
        }
        public  void CreateInit(string nbase, string nserver, string nutil, string pass)
        {
            string sourcePath = Server.MapPath("/APP_DATA/");
            string chemin = System.IO.Path.Combine(sourcePath, "ini.ini");
            string[] lines = { "ServerName=" + nserver, "DataBaseName=" + nbase, "LoginName=" + nutil, "Password=" + pass };
            System.IO.File.WriteAllLines(chemin, lines);
        }
         [AllowAnonymous]
        public ActionResult TimeoutRedirect()
        {
            return View();
        }
        public string CheckTimeout()
        {
           // if(!Request.IsAuthenticated || Session["Code_User"]== null)
            if (Session["Code_User"] == null || !Request.IsAuthenticated)
            {
                return "_Logon_";
            }
            return "Ok";
        }

        [AllowAnonymous]
        public ActionResult TimeZone()
        {
             return View();
        }
        public JsonResult Format()
        {
            string[] tab = new string[1];
            LoginBM login = new LoginBM();
            TempData["id"] = Session["Format"].ToString();
            TempData["idL"] = Session["Langue"].ToString();
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Login/Format.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
    }
}
