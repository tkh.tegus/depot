﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class VariableController : Controller
    {
        Services Service = new Services();
        public class UplaodeFile
        {
            public bool success { get; set; }
            public string file { get; set; }
            public string message { get; set; }
        }
        public class JqwAjax
        {
            public Int64 id { get; set; }
            public DateTime Date { get; set; }
            public string Numero_Assure { get; set; }
            public string Numero_Beneficiaire { get; set; }
            public string Nom { get; set; }
            public string Prenom { get; set; }

        }

        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();
            return this.Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult MAJVariables(VariableVM Param_Var)
        {
            VariableBM Local_Var = new VariableBM();
            string chemin = Path.Combine(Server.MapPath("~/Content/pictures"), "current" + Session["Code_User"].ToString() + ".png");
            if (System.IO.File.Exists(chemin))//le fichier current existe 
            {
                FileStream fs = new FileStream(chemin, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                fs.Close();
                Param_Var.Logo = bytes;
                System.IO.File.Delete(chemin);
            }
            Local_Var.Edit(Param_Var, int.Parse(Session["Code_User"].ToString()));
            TempData["messagesucess"] = "Mise a jour effectué avec succès";
            string message = Service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Index()
        {
            string[] tab_Habillitation = new string[3];
            if (!Service.VerifierHabiliationIndex("Gestion_des_variables", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            VariableBM Local_Var = new VariableBM();
            VariableVM Return_Var = Local_Var.GetByID();
            if (Session["Code_User"].ToString() == "1")
            {
                TempData["Password"] = "ok";
            }
            else
            {
                TempData["Password"] = "no";
            }

            if (Return_Var.Logo == null)
            {
                string path = Path.Combine(Server.MapPath("~/Content/pictures"), "user.png");
                byte[] imageByteData = System.IO.File.ReadAllBytes(path);
                string imageBase64Data = Convert.ToBase64String(imageByteData);
                string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                TempData["URL_Picture"] = imageDataURL;
            }
            else
            {
                string imageBase64Data = Convert.ToBase64String(Return_Var.Logo);
                string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                TempData["URL_Picture"] = imageDataURL;
            }

            string[] tab = new string[3];
            tab[0] = Service.GetHtmlByView(ControllerContext, TempData, "~/Views/Variable/Index.cshtml", Return_Var);
            tab[1] = "GESTION DES VARIABLES DE LA SOCIÉTÉ";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Administration</a></li> <li class='active'>Variables</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UploadFile(int? id)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            UplaodeFile up = new UplaodeFile();
            up.file = "current" + Session["Code_User"].ToString() + ".png";
            if (Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = Request.Files["UploadedImage"];

                if (httpPostedFile != null)
                {
                    bool valid = false;
                    string ext = Path.GetExtension(httpPostedFile.FileName), chemin = "";
                    ext = ext.ToLower();
                    switch (ext)
                    {
                        case ".jpg":
                            valid = true;
                            break;
                        case ".png":
                            valid = true;
                            break;
                        case ".gif":
                            valid = true;
                            break;
                        case ".jpeg":
                            valid = true;
                            break;
                        case ".ico":
                            valid = true;
                            break;
                    }
                    if (!valid)
                    {
                        up.message = "Les formats de photo sont .jpg,.png,.gif,.jpeg,.ico";
                        up.success = false;
                        return this.Json(serializer.Serialize(up), JsonRequestBehavior.AllowGet);

                    }
                    chemin = Path.Combine(Server.MapPath("~/Content/pictures"), "current" + Session["Code_User"].ToString() + ".png");
                    System.IO.File.Delete(chemin);
                    httpPostedFile.SaveAs(chemin);
                    FileStream file = new FileStream(chemin, FileMode.Open, FileAccess.Read);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(file, false, false);

                    if ((id == null) && (img.Height < 80 || img.Width < 80 || img.Height > 300 || img.Width > 300))
                    {
                        img.Dispose();
                        file.Close();
                        System.IO.File.Delete(chemin);
                        up.message = "La hauteur et la largeur des images doivent être compris entre 80 et 300 px";
                        up.success = false;
                        return this.Json(serializer.Serialize(up), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        img.Dispose();
                        file.Close();
                    }
                    // photo = Session["Code_User"].ToString() + ".png";
                    up.message = "Image uploadé avec succés";
                    up.success = true;
                    return this.Json(serializer.Serialize(up), JsonRequestBehavior.AllowGet);
                }
            }
            up.message = "Aucune image sélectionnée";
            up.success = false;
            return this.Json(serializer.Serialize(up), JsonRequestBehavior.AllowGet);
        }

    }
}
