﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class Ouverture_Cloture_CaisseController : Controller
    {
        Services service = new Services();
        public class JqwAjax
        {
            public int id { get; set; }
            public DateTime Date_Ouverture { get; set; }
            public DateTime? Date_Cloture { get; set; }
            public string Caisse { get; set; }
            public string Caissier { get; set; }
            public string Statut { get; set; }
            public string Utilisateur { get; set; }          
        }

        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.TP_Ouverture_Cloture_Caisse
                           orderby c.Etat , c.Date_Ouverture descending
                           where (c.Date_Ouverture >= filtre.DateDebut && c.Date_Ouverture < filtre.CDateFin) || c.Etat == 0
                           select new JqwAjax
                           {
                               id = c.ID_Ouverture_Cloture_Caisse,
                               Date_Ouverture = c.Date_Ouverture,
                               Date_Cloture = c.Date_Cloture,
                               Caisse = c.TP_Caisse.Libelle,
                               Caissier = db.Vue_Personnel.Where(p => p.ID_Personnel == c.ID_Personnel).Select(p => p.Libelle).FirstOrDefault(),
                               Statut = (c.Etat == 0 ? "Ouverte" : (c.Etat == 1 ? "Clôurer" : "Annuler")),                            
                               Utilisateur = db.Vue_Personnel.Where(p=>p.ID_Personnel == c.Create_Code_User).Select(p=>p.Libelle).FirstOrDefault()
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult Index()
        {           
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_Ouverture_Cloture_Caisse", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Session["DateDebut"] = DateTime.Today.AddDays(-7).ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Ouverture_Cloture_Caisse/Index.cshtml", filtre);
            tab[1] = "GESTION DES OUVERTURES/CLÔTURES CAISSES";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des clients</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        
        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {         
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Ajouter_Ouverture_Cloture_Caisse", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            TempData["ID_Personnel"] = new SelectList(from c in db.TG_Personnels
                                                      where c.ID_Personnel != 1
                                                      select new
                                                      {
                                                          ID_Element = c.ID_Personnel,
                                                          Libelle = c.Nom
                                                      }, "ID_Element", "Libelle");
            TempData["ID_Caisse"] = new SelectList(from c in db.TP_Caisse
                                                      select new
                                                      {
                                                          ID_Element = c.ID_Caisse,
                                                          Libelle = c.Libelle
                                                      }, "ID_Element", "Libelle");
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Ouverture_Cloture_Caisse/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
      
        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(Ouverture_Cloture_CaisseVM Param_Var)
        {
            try
            {
                //Param_Var.ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
                if (ModelState.IsValid)
                {
                    StoreEntities db = new StoreEntities();
                    if(db.TP_Ouverture_Cloture_Caisse.Where(p=>p.ID_Caisse == Param_Var.ID_Caisse && p.Etat == 0).FirstOrDefault() == null)
                    {
                        string messageRetour = string.Empty;
                        Ouverture_Cloture_CaisseBM Local_Var = new Ouverture_Cloture_CaisseBM();
                        Local_Var.Save(Param_Var, Session["Code_User"].ToString());
                        TempData["messagesucess"] = "Ajout effectué avec succès";
                        string[] tab = new string[2];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        
                        TempData["messagesucess"] = "Cette caisse est déjà ouverte";
                        string[] tab = new string[1];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);                       
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    
                }
                else
                {
                    string[] tab = new string[1];
                    TempData["messageerror"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Ouverture_Cloture_Caisse/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName("Consulter")]
        public JsonResult Consulter(int id)
        {
            TempData["id"] = id;
            Ouverture_Cloture_CaisseBM Local_Var = new Ouverture_Cloture_CaisseBM();
            Ouverture_Cloture_CaisseVM Return_Var = Local_Var.GetEtatClorureById(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Ouverture_Cloture_Caisse/Consulter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Valider(int id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Valider_Ouverture_Cloture_Caisse", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                Ouverture_Cloture_CaisseBM Local_Var = new Ouverture_Cloture_CaisseBM();
                Local_Var.Valider(id, Session["Code_User"].ToString());
                TempData["messagesucess"] = "Valider effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                //====== envoyer mail rapport ======/
                Ouverture_Cloture_CaisseVM Return_Var = Local_Var.GetEtatClorureById(id);
                string[] Email = { "teguia.herve@oufarez.com" };
                string[] Libelle = { "Rapport Clôture caisse" };
                string Titre = "Rapport Clôture caisse";
                string Body = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Ouverture_Cloture_Caisse/pdfConsulter.cshtml", Return_Var);
                //service.SendEmail(Titre, Body, Email, Libelle);
                //======= ======/
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Ouverture_Cloture_Caisse/Valider/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        //[ActionName("Modifier")]
        //public JsonResult Modifier(int id)
        //{          
        //    string[] tab_Habillitation = new string[1];
        //    if (!service.VerifierHabiliationCRUD("Modifier_Ouverture_Cloture_Caisse", this, ref tab_Habillitation))
        //    {
        //        return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
        //    }
        //    Locked_RecordsBM veroux = new Locked_RecordsBM();
        //    string message = "";
        //    if (veroux.IsVerouller(id, "TP_Ouverture_Cloture_Caisse", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
        //    {
        //        string[] table = new string[1];
        //        table[0] = message;
        //        return this.Json(table, JsonRequestBehavior.AllowGet);
        //    }
        //    Ouverture_Cloture_CaisseBM Local_Var = new Ouverture_Cloture_CaisseBM();
        //    Ouverture_Cloture_CaisseVM Return_Var = Local_Var.GetByID(id);
        //    if (Return_Var == null)//aucun element trouve
        //    {
        //        TempData["messagenotification"] = "Aucun élément correspondant";
        //        string[] table = new string[1];
        //        table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
        //        return this.Json(table, JsonRequestBehavior.AllowGet);
        //    }
        //    string[] tab = new string[1];
        //    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Ouverture_Cloture_Caisse/Modifier.cshtml", Return_Var);
        //    return this.Json(tab, JsonRequestBehavior.AllowGet);
        //}


        //[HttpPost, ActionName("Modifier")]
        //[ValidateAntiForgeryToken]
        //public JsonResult Modifier(int id, Ouverture_Cloture_CaisseVM Param_Var)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            string messageRetour = string.Empty;
        //            // TODO: Add update logic here
        //            Ouverture_Cloture_CaisseBM Return_Var = new Ouverture_Cloture_CaisseBM();                  
        //            Return_Var.Edit(id, Param_Var, Session["Code_User"].ToString());
        //            // libère le verrou précédemment pauser
        //            Locked_RecordsBM veroux = new Locked_RecordsBM();
        //            veroux.Liberer(id, "TP_Ouverture_Cloture_Caisse");
        //            TempData["messagesucess"] = "Modification effectuée avec succès";
        //            string[] tab = new string[2];
        //            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
        //            tab[1] = "";
        //            return this.Json(tab, JsonRequestBehavior.AllowGet);                   
        //        }
        //        else
        //        {
        //            TempData["messagewarning"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
        //            string[] tab = new string[1];
        //            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
        //            return this.Json(tab, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //persist les logs
        //        LogsBM Log = new LogsBM();
        //        Log.Save(this, ex, "POST: /Ouverture_Cloture_Caisse/Modifier/", Session["Code_User"].ToString());
        //        TempData["messageerror"] = "Une exception généré ";
        //        string[] tab = new string[1];
        //        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
        //        return this.Json(tab, JsonRequestBehavior.AllowGet);
        //    }
        //}


        //[ActionName("Activer_Suspendre")]
        //public JsonResult Activer_Suspendre(string id)
        //{           
        //    string[] tab_Habillitation = new string[1];
        //    if (!service.VerifierHabiliationCRUD("Activer_suspendre_Ouverture_Cloture_Caisse", this, ref tab_Habillitation))
        //    {
        //        return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
        //    }
        //    try
        //    {
        //        string[] tab = id.Split(';');
        //        for (int i = 0; i < tab.Length; i++)
        //        {
        //            Ouverture_Cloture_CaisseBM Local_Var = new Ouverture_Cloture_CaisseBM();
        //            Local_Var.Activer_Suspendre(int.Parse(tab[i]), Session["Code_User"].ToString());
        //        }
        //        TempData["messagesucess"] = "Action effectuée avec succès";
        //        string[] tab1 = new string[1];
        //        tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
        //        return this.Json(tab1, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        //persist les logs
        //        LogsBM Log = new LogsBM();
        //        Log.Save(this, ex, "POST: /Ouverture_Cloture_Caisse/Modifier/", Session["Code_User"].ToString());
        //        TempData["messageerror"] = "Une exception généré ";
        //        string[] tab = new string[1];
        //        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
        //        return this.Json(tab, JsonRequestBehavior.AllowGet);
        //    }           
        //}

    }
}
