﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;
using System.Data;
using System.Data.Entity;
using System.Text;

using System;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class GradesController : Controller
    {
        [ActionName("deverouiller")]
        public JsonResult deverouiller()
        {
            Locked_RecordsBM logrecord = new Locked_RecordsBM();
            logrecord.LibererTous(int.Parse(Session["Code_User"].ToString()));
            return this.Json("", JsonRequestBehavior.AllowGet);
        }

        /*
        // GET: /Grades/
        Services service = new Services();
        public class AjaxGrid
        {
            public int id { get; set; }
            public string libelle { get; set; }
            public string abrege { get; set; }
            public string Statut { get; set; }
        }

        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();
            var resultat = (from c in db.TG_Grades
                            orderby c.Libelle
                            select new AjaxGrid
                            {
                                id = c.ID_Grade,
                                libelle = c.Libelle,
                                abrege = c.Abrege,
                                Statut = c.Actif ? "Actif" : "Suspendu"
                            });
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Index")]
        public JsonResult Index()
        {
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Grades/Index.cshtml", null);
            tab[1] = "Liste des Grades";
            tab[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Administration</a></li> <li class='active'><a href='#'>Grades</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Grades/Ajouter
        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {
            StoreEntities db = new StoreEntities();
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Grades/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Grades/Ajouter
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(GradesVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    GradesBM Appr = new GradesBM();
                    if (Appr.IsNotExist(Param_Var, ref messageRetour))
                    {
                        Appr.Save(Param_Var, Session["Code_User"].ToString());
                        TempData["messagesucess"] = "Ajout effectué avec succès";
                        string[] tab = new string[2];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Grades/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

       

        //
        // GET: /Grades/Modifier/5
        [ActionName("Modifier")]
        public JsonResult Modifier(int id)
        {
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(id, "TG_Grades", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            var app = db.TG_Grades.Find(id);
            GradesBM ObjetBM = new GradesBM();
            GradesVM LocalVM = ObjetBM.GetByID(id);
            if (LocalVM == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Grades/Modifier.cshtml", LocalVM);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Grades/Modifier/5
        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(int id, GradesVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    GradesBM ObjetBM = new GradesBM();
                    if (ObjetBM.IsNotExist(Param_Var, id, ref messageRetour)) //vérifie si le arrondissement existe déjà
                    {

                        ObjetBM.Edit(id, Param_Var, Session["Code_User"].ToString());

                        // libère le verrou précédemment pauser
                        Locked_RecordsBM veroux = new Locked_RecordsBM();
                        veroux.Liberer(id, "TG_Grades");

                        TempData["messagesucess"] = "Modification effectuée avec succès";
                        string[] tab = new string[2];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    TempData["messagewarning"] = "Le Modele n'est pas valide ";
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Grades/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Grades/Supprimer/5
        [ActionName("Supprimer")]
        public JsonResult Supprimer(string id)
        {
            string[] tab = id.Split(';');
            for (int i = 0; i < tab.Length; i++)
            {
                GradesBM ObjetBM = new GradesBM();
                ObjetBM.Delete(int.Parse(tab[i]), Session["Code_User"].ToString());
            }
            TempData["messagesucess"] = "Suppression effectuée avec succès";
            string[] tab1 = new string[1];
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab1, JsonRequestBehavior.AllowGet);

        }
        //
        // GET: /Grades/Activer_Suspender/5
        [ActionName("Activer_Suspendre")]
        public JsonResult Activer_Suspendre(string id)
        {
            string[] tab = id.Split(';');
            for (int i = 0; i < tab.Length; i++)
            {
                GradesBM Local_Var = new GradesBM();
                Local_Var.Activer_Suspendre(int.Parse(tab[i]), Session["Code_User"].ToString());
            }
            TempData["messagesucess"] = "Action effectuée avec succès";
            string[] tab1 = new string[1];
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab1, JsonRequestBehavior.AllowGet);
        }
         * */
    }
}
