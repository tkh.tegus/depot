﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;
using System.IO;
using PROJETMODELE.Models.BML;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class ExportImportController : Controller
    {
        Services service = new Services();

     
        public ActionResult Confirm(bool ok, int? nombre)
        {
            string result = "";
            if (ok)
            {
                ViewBag.Message = " Import Effectué avec succés ";
                ViewBag.nombre = ((int)nombre).ToString();
                string[] tab = Session["result"].ToString().Split(';');
                if (tab.Length > 0)
                {
                    for (int i = 0; i < tab.Length; i++)
                    {
                        result = i == 0 ? tab[i] : result + ";" + tab[i];
                    }
                    ViewBag.code = result;
                }
            }
            else
            {
                ViewBag.Message = " Le format d'import est le .csv ";
            }
            return View();
        }
        public ActionResult modele(int id)
        {
            if (id == 0 || id == 1)//
                service.GetModele("client.csv", this);
           return View("Confirm");
        }
    }
}
