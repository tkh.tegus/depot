﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;
using System.IO;
using System.Web.Script.Serialization;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class PersonnelsController : Controller
    {
        Services service = new Services();

        public class AjaxGrid
        {
            public int id { get; set; }
            public string matricule { get; set; }
            public string nom_prenom { get; set; }
            public DateTime? date_naissance { get; set; }
            public DateTime? date_recrutement { get; set; }
            public string sex { get; set; }
            public string telephone { get; set; }
            public string mail { get; set; }
            public string adresse { get; set; }
            public string grade { get; set; }
            public string localite { get; set; }
            public bool? display { get; set; }
            public string Statut { get; set; }
        }
        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();
            var resultat = (from c in db.TG_Personnels
                            join d in db.Vue_Personnel
                            on c.ID_Personnel 
                            equals d.ID_Personnel  
                            orderby c.Nom, c.Prenom
                            select new AjaxGrid
                            {
                                id = c.ID_Personnel,
                                nom_prenom = d.Libelle,                     
                               // sex = c.Sexe ? "Homme" : "Femme",                       
                                //mail = c.Mail,                     
                                display = c.Display,                                                         
                                Statut = c.Actif ? "Actif" : "Suspendu"
                            });
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Index()
        {
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Personnels/Index.cshtml", null);
            tab[1] = "Liste du Personnel";
            tab[2] = "<li><a href='#'>Accueil</a></li> <li><a href='#'>Administration</a></li> <li class='active'><a href='#'>Personnel</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        
        //
        // GET: /Personnels/Details/5
        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {
            // envoie de la liste des Localites
            StoreEntities db = new StoreEntities();
            
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Personnels/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Grades/Ajouter
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(PersonnelsVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    PersonnelsBM Appr = new PersonnelsBM();
                    Appr.Save(Param_Var, Session["Code_User"].ToString());
                    TempData["messagesucess"] = "Ajout effectué avec succès";
                    string[] tab = new string[2];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Personnels/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Localite/Modifier/5
        [ActionName("Modifier")]
        public JsonResult Modifier(int id)
        {
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(id,"TG_Personnels", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            var app = db.TG_Personnels.Find(id);
            PersonnelsBM ObjetBM = new PersonnelsBM();
            PersonnelsVM LocalVM = ObjetBM.GetByID(id);
            if (LocalVM == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
           
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Personnels/Modifier.cshtml", LocalVM);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Personnels/Modifier/5
        [HttpPost, ActionName ( "Modifier" )]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier ( int id, PersonnelsVM Param_Var )
        {

            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    // TODO: Add update logic here
                    PersonnelsBM ObjetBMReturn = new PersonnelsBM ( );
                   
                        ObjetBMReturn.Edit(id, Param_Var, Session["Code_User"].ToString());
                        Locked_RecordsBM veroux = new Locked_RecordsBM ( );
                        veroux.Liberer ( id,"TG_Personnels" );
                        TempData["messagesucess"] = "Modification effectuée avec succès";
                        string[] tab = new string[2];
                        tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["messagewarning"] = "Le Modele n'est pas valide ";
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                    return this.Json ( tab, JsonRequestBehavior.AllowGet );
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM ( );
                Log.Save(this, ex, "POST: /Personnels/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                return this.Json ( tab, JsonRequestBehavior.AllowGet );
            }
        }

        //
        // GET: /Grades/Supprimer/5
        [ActionName("Supprimer")]
        public JsonResult Supprimer(string id)
        {
            string[] tab = id.Split(';');
            for (int i = 0; i < tab.Length; i++)
            {
                PersonnelsBM ObjetBM = new PersonnelsBM();
                ObjetBM.Delete(int.Parse(tab[i]), Session["Code_User"].ToString());
            }
            TempData["messagesucess"] = "Suppression effectuée avec succès";
            string[] tab1 = new string[1];
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab1, JsonRequestBehavior.AllowGet);

        }

        [ActionName ( "Activer" )]
        public ActionResult Activer ( string id )
        {
            try
            {
                string[] allid = id.Split ( ';' );
                for (int i = 0; i < allid.Length; i++)
                {
                    PersonnelsBM ObjetBMReturn = new PersonnelsBM ( );
                    ObjetBMReturn.Activer(int.Parse(allid[i]), Session["Code_User"].ToString());
                }
                return View ( );
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM ( );
                Log.Save(this, ex, "POST: /Personnels/Activer/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une erreur est survenue durant l'activation ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                return this.Json ( tab, JsonRequestBehavior.AllowGet );
            }
        }

        public void ExportEmploye()
        {
            service.ExportEmploye(this);
        }


    }
}