﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class Ristourne_ClientController : Controller
    {
        Services service = new Services();
        public class JqwAjax
        {
            public int id { get; set; }
            public string Produit { get; set; }
            public DateTime Date { get; set; }
            public double Ristourne { get; set; }
            public string statut { get; set; }
            public string Cumuler { get; set; }
            public string Utilisateur { get; set; }
        }

        public JsonResult ajax(int id)
        {
            StoreEntities db = new StoreEntities();

            var resultat = from c in db.TP_Ristourne_Client
                           where c.ID_Client == id
                           orderby c.Actif descending, c.TP_Produit.Libelle
                           select new JqwAjax
                           {
                               id = c.ID_Ristourne_Client,
                               Produit = c.TP_Produit.Libelle,
                               Ristourne = c.Ristourne,
                               Date = c.Date,
                               Cumuler = (c.Cumuler_Ristourne == true ? "OUI" : "NON"),
                               statut = c.Actif == true ? "Actif" : "Suspendu",
                               Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == c.Create_Code_User).Select(p => p.Libelle).FirstOrDefault()
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

       
        public JsonResult Index(int id)
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_ristourne_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            var lib = db.TP_Client.Where(p => p.ID_Client == id).Select(p => p.Nom).FirstOrDefault();
            TempData["id"] = id;
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Ristourne_Client/Index.cshtml", null);
            tab[1] = "GESTION DES RISTOURNES : "+lib;
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Client</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        
        [ActionName("Ajouter")]
        public JsonResult Ajouter(int id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Ajouter_ristourne_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            var liste_id = db.TP_Ristourne_Client.Where(p => p.ID_Client == id).Select(p => p.ID_Produit).ToList();
            TempData["ID_Produit"] = new SelectList(from c in db.TP_Produit
                                                    orderby c.Libelle
                                                    where c.Actif
                                                    select new
                                                    {
                                                        ID_Produit = c.ID_Produit,
                                                        Libelle = c.Libelle
                                                    }, "ID_Produit", "Libelle");
            Ristourne_ClientVM Return_var = new Ristourne_ClientVM();
            Return_var.ID_Client = id;
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Ristourne_Client/Ajouter.cshtml", Return_var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        
        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(Ristourne_ClientVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    Ristourne_ClientBM Local_Var = new Ristourne_ClientBM();
                    if (Local_Var.IsNotExist(Param_Var, ref messageRetour))
                    {
                        
                        Local_Var.Save(Param_Var, Session["Code_User"].ToString());
                        TempData["messagesucess"] = "Ajout effectué avec succès";
                        string[] tab = new string[2];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string[] tab = new string[1];
                    TempData["messageerror"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                   
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Ristourne_Client/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

       
        [ActionName("Modifier")]
        public JsonResult Modifier(int id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Modifier_ristourne_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(id, "TP_Ristourne_Client", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }

            Ristourne_ClientBM Local_Var = new Ristourne_ClientBM();
            Ristourne_ClientVM Return_Var = Local_Var.GetByID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            TempData["ID_Produit"] = new SelectList(from c in db.TP_Produit
                                                    orderby c.Libelle
                                                    where c.Actif
                                                    select new
                                                    {
                                                        ID_Produit = c.ID_Produit,
                                                        Libelle = c.Libelle
                                                    }, "ID_Produit", "Libelle");
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Ristourne_Client/Modifier.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(int id, Ristourne_ClientVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    // TODO: Add update logic here
                    Ristourne_ClientBM Return_Var = new Ristourne_ClientBM();
                    
                    
                    if (Return_Var.IsNotExist(Param_Var, id, ref messageRetour)) //vérifie si le Type_Depenses existe déjà
                    {
                        Return_Var.Edit(id, Param_Var, Session["Code_User"].ToString());

                        // libère le verrou précédemment pauser
                        Locked_RecordsBM veroux = new Locked_RecordsBM();
                        veroux.Liberer(id, "TP_Ristourne_Client");

                        TempData["messagesucess"] = "Modification effectuée avec succès";
                        string[] tab = new string[2];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    TempData["messagewarning"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Ristourne_Client/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

      
        [ActionName("Activer_Suspendre")]
        public JsonResult Activer_Suspendre(string id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Activer_suspendre_ristourne_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            string[] tab = id.Split(';');
            for (int i = 0; i < tab.Length; i++)
            {
                Ristourne_ClientBM Local_Var = new Ristourne_ClientBM();
                Local_Var.Activer_Suspendre(int.Parse(tab[i]), Session["Code_User"].ToString());
            }
            TempData["messagesucess"] = "Action effectuée avec succès";
            string[] tab1 = new string[1];
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab1, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Supprimer")]
        public JsonResult Supprimer(string id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Supprimer_ristourne_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            string[] tab = id.Split(';');
            for (int i = 0; i < tab.Length; i++)
            {
                Ristourne_ClientBM Local_Var = new Ristourne_ClientBM();
                Local_Var.Delete(int.Parse(tab[i]), Session["Code_User"].ToString());
            }
            TempData["messagesucess"] = "Action effectuée avec succès";
            string[] tab1 = new string[1];
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab1, JsonRequestBehavior.AllowGet);
        }

    }
}
