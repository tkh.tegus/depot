﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class Evenement_ConfigController : Controller
    {
        /*
        StoreEntities db = new StoreEntities();

        //
        // GET: /Evenement_Config/
        Services service = new Services();
        public class JqwEvenement_Config
        {
            public string id { get; set; }
            public string Utilisateur { 
                get {
                    return Nom + ((Prenom != null) ? " " +Prenom : "");
                }
            }
            public string Nom { get; set; }
            public string Prenom { get; set; }
            public string Poste { get; set; }
            public string evenement { get; set; }
            public bool actif { get; set; }
            public bool available { get; set; }
        }
        public class JqwEvenement
        {
            public string id { get; set; }
            public string Date { get; set; }
            public string description { get; set; }
            public string Etat { get; set; }
            public bool available { get; set; }
        }

        //public JsonResult ajaxevent()
        //{
        //    Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
        //    int Code_User = int.Parse(Session["Code_User"].ToString());
        //    StoreEntities db = new StoreEntities();
        //    var Table = new List<JqwEvenement>
        //    {
        //    };


        //    var resultat = from c in db.VP_Evenement
        //                   orderby c.Date
        //                   where c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin && c.Etat == false && c.ID_Personnel == Code_User
        //                   select c;
        //    foreach (var item in resultat)
        //    {
        //        Table.Add(new JqwEvenement() { id = item.ID_Evenement.ToString(), Date = item.Date.ToString("dd/MM/yyyy HH:mm"), description = item.Description, Etat = item.statut });
        //    }
        //    resultat = from c in db.VP_Evenement
        //               orderby c.Date
        //               where c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin && c.Etat == true && c.ID_Personnel == Code_User
        //               select c;
        //    foreach (var item in resultat)
        //    {
        //        Table.Add(new JqwEvenement() { id = item.ID_Evenement.ToString(), Date = item.Date.ToString("dd/MM/yyyy HH:mm"), description = item.Description, Etat = item.statut });
        //    }
        //    return this.Json(Table, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();
            var Table = new List<JqwEvenement_Config>
            {
            };
            var resultat = from c in db.TG_Evenement_Config orderby c.TG_Evenement_Type.Titre select c;
            foreach (TG_Evenement_Config c in resultat)
            {
                Table.Add(new JqwEvenement_Config() { id = c.ID_Config_Event.ToString(), Nom = c.TG_Personnels.Nom, Prenom = c.TG_Personnels.Prenom, Poste = c.TG_Personnels.TG_Grades.Libelle, evenement = c.TG_Evenement_Type.Titre });
            }
            return this.Json(Table, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Index()
        {
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Evenement_Config/index.cshtml", null);
            tab[1] = "Configuration des évènements";
            tab[2] = "  <a href='#'>Accueil</a><div class='breadcrumb_divider'></div> <a href='#'>Administration</a><div class='breadcrumb_divider'></div> <a href='#'>Paramétrage</a><div class='breadcrumb_divider'></div><a href='#'>Configuration</a><div class='breadcrumb_divider'></div><a class='current'>Configurerévénement</a>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        public String Compte()
        {
            StoreEntities db = new StoreEntities();
            int Code_User = int.Parse(Session["Code_User"].ToString());
            return "";
           // return (from c in db.VP_Evenement orderby c.Date where c.Etat == false && c.ID_Personnel == Code_User select c).Count().ToString();
        }

        //
        // GET: /Evenement_Config/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Evenement_Config/Ajouter
        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {

            // envoie de la liste des ID_Personnel
            TempData["ID_Personnel"] = new SelectList(from p in db.Vue_Personnel orderby (p.Libelle) where (p.Actif == true) select new { ID_Personnel = p.ID_Personnel, Libelle = p.Libelle }, "ID_Personnel", "Libelle");

            // envoie de la liste des Types Evenements
            TempData["EvenementItem"] = new SelectList(from p in db.TG_Evenement_Type orderby (p.Titre) where (p.Inform_Back_End == true) select (p), "ID_Type_Evenement", "Titre");
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Evenement_Config/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Evenement_Config/Ajouter
        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(Evenement_Config Evenement_Config_Var)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    Evenement_ConfigBM ObjetBMRetunr = new Evenement_ConfigBM();
                    if (ObjetBMRetunr.IsNotExist(Evenement_Config_Var.ID_Personnel, Evenement_Config_Var.ID_Type_Evenement))
                    {
                        ObjetBMRetunr.Save(Evenement_Config_Var, Session["Code_User"].ToString());
                        TempData["messagesucess"] = "Ajout effectué avec succès";
                        string[] tab = new string[2];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = "Cette association existe déjà ";
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Evenement_Config/Ajouter", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Evenement_Config/Supprimer/5
        [ActionName("Supprimer")]
        public JsonResult Supprimer(string id)
        {
            try
            {
                string[] allid = id.Split(';');
                for (int i = 0; i < allid.Length; i++)
                {
                    Evenement_ConfigBM ObjetBMReturn = new Evenement_ConfigBM();
                    ObjetBMReturn.Delete(int.Parse(allid[i]), Session["Code_User"].ToString());
                }
                TempData["messagesucess"] = "Suppression effectuée avec succès";
                string[] tab1 = new string[1];
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Evenement_Config/Supprimer/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une erreur est survenue durant la suppression ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Liste()
        {
            Session["DateDebut"] = DateTime.Today.AddDays(-7).ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Evenement_Config/Liste.cshtml", filtre);
            tab[1] = "Liste des évènements";
            tab[2] = "<a href='#'>Acceuil</a> <div class='breadcrumb_divider'></div><a class='current'>Liste des évènements</a>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ActionName("valide")]
        public JsonResult valide(int id)
        {
            StoreEntities db = new StoreEntities();
            var evenement = db.TG_Evenements.Find(id);
            evenement.Etat = true;
            evenement.Edit_Code_User = int.Parse(Session["Code_User"].ToString());
            evenement.Edit_Date = DateTime.Now;
            db.SaveChanges();
            TempData["messagesucess"] = "Etat changé avec succés";
            string[] tab = new string[2];//2 pour savoir que c'est ok
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = "";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        */
    }
}