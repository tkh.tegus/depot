﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class DecaissementController : Controller
    {
        Services service = new Services();        
        public class JqwAjax
        {
            public Int64 id { get; set; }
            public DateTime Date { get; set; }
            public double Montant { get; set; }
            public string Caisse { get; set; }
            public string Fournisseur { get; set; }
            public string Motif { get; set; }
            public string Utilisateur { get; set; }
            public string Statut { get; set; }
            public string NUM_DOC { get; set; }
        }

        public JsonResult ajax(int id)
        {
            //id = 0 => Décaissement
            //id = 1 => Payement ristourne client
            //id = 2 => Dépense
            StoreEntities db = new StoreEntities();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.TP_Decaissement
                           orderby c.Etat, c.Date descending
                           where (c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin) && c.Type_Element == id
                           select new JqwAjax
                           {
                               id = c.ID_Decaissement,
                               Date = c.Date,
                               Caisse = c.TP_Caisse.Libelle,
                               NUM_DOC = c.NUM_DOC,
                               Montant = c.Montant,                             
                               Motif = c.Motif,
                               Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == c.Create_Code_User).Select(p => p.Libelle).FirstOrDefault(),
                               Statut = (c.Etat == 0) ? "En Attente de validation" : (c.Etat == 1) ? "Valider" : "Annuler",
                               Fournisseur = (c.Type_Element == 0 ? db.TP_Fournisseur.Where(p=>p.ID_Fournisseur == c.ID_Element).FirstOrDefault().Nom : (c.Type_Element == 1 ? db.TP_Client.Where(p=>p.ID_Client == c.ID_Element).FirstOrDefault().Nom : db.TP_Type_Depense.Where(p=>p.ID_Type_Depense == c.ID_Element).FirstOrDefault().Libelle))
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Index")]
        public JsonResult Index(int id)
        {
            //id = 0 => Décaissement
            //id = 1 => Payement ristourne client
            //id = 2 => Dépense
            string[] tab_Habillitation = new string[3];
            if ((id == 0 && !service.VerifierHabiliationIndex("Gestion_des_reglement_fournisseur", this, ref tab_Habillitation)) || (id == 1 && !service.VerifierHabiliationIndex("Gestion_des_reglement_ristourne", this, ref tab_Habillitation)) || (id == 2 && !service.VerifierHabiliationIndex("Gestion_des_depenses", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            var ouverture = db.TP_Ouverture_Cloture_Caisse.Where(p => p.ID_Caisse == ID_Caisse && p.Etat == 0).FirstOrDefault();
            if (ouverture != null)
            {
                Session["DateDebut"] = ouverture.Date_Ouverture.ToString();
            }
            else
            {
                Session["DateDebut"] = DateTime.Today.ToString();
            }
            Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            
            TempData["type"] = id;
            if (id == 0)
            {               
                string[] tab = new string[3];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Decaissement/Index.cshtml", filtre);
                tab[1] = "GESTION DES RÈGLEMENTS FOURNISSEURS";
                tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des décaissements</li>";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (id == 1)
            {              
                string[] tab = new string[3];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Decaissement/Index.cshtml", filtre);
                tab[1] = "GESTION DES RISTOURNES";
                tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des ristournes clients</li>";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else
            {
                
                string[] tab = new string[3];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Decaissement/Index.cshtml", filtre);
                tab[1] = "GESTION DES DÉPENSES";
                tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des dépenses</li>";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            
        }
      
        [ActionName("Ajouter")]
        public JsonResult Ajouter(int id)
        {
            //id = 0 => Décaissement
            //id = 1 => Payement ristourne client
            //id = 2 => Dépense
            string[] tab_Habillitation = new string[1];
            if ((id == 0 && !service.VerifierHabiliationCRUD("Ajouter_reglement_fournisseur", this, ref tab_Habillitation)) || (id == 1 && !service.VerifierHabiliationCRUD("Ajouter_reglement_ristourne", this, ref tab_Habillitation)) || (id == 2 && !service.VerifierHabiliationCRUD("Ajouter_depense", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities(); 
            if (id == 0)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Fournisseur
                                                            where c.Actif
                                                            orderby c.Nom
                                                            select new
                                                            {
                                                                ID_Element = c.ID_Fournisseur,
                                                                Libelle = c.Nom
                                                            }, "ID_Element", "Libelle");
            }
            else if (id == 1)
            {
                var ristourne = db.TP_Ristourne_Client.Where(p => p.Actif == true).Select(p => p.ID_Client).Distinct().ToList();
                TempData["ID_Element"] = new SelectList(from c in db.TP_Client
                                                        where c.Actif && ristourne.Contains(c.ID_Client)
                                                        orderby c.Nom
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Client,
                                                            Libelle = c.Nom
                                                        }, "ID_Element", "Libelle");
            }
            else
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Type_Depense
                                                       where c.Actif
                                                       orderby c.Libelle
                                                       select new
                                                       {
                                                           ID_Element = c.ID_Type_Depense,
                                                           Libelle = c.Libelle
                                                       }, "ID_Element", "Libelle");
            }
            DecaissementVM Return_Var = new DecaissementVM();
            Return_Var.Type_Element = (byte)id;
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Decaissement/Ajouter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Ajouter")]
        [ValidateAntiForgeryToken]
        public JsonResult Ajouter(DecaissementVM Param_Var)
        {
            Ouverture_Cloture_CaisseBM caisse = new Ouverture_Cloture_CaisseBM();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            if (!caisse.IsCaisseOpen(ID_Caisse))
            {
                TempData["messagewarning"] = "Veuillez ouvrir une caisse";
                string[] tab = new string[2];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

            try
            {
                Param_Var.ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    DecaissementBM ObjetModel = new DecaissementBM();
                    Int64 id = ObjetModel.Save(Param_Var, Session["Code_User"].ToString());
                    TempData["messagesucess"] = "Ajout effectué avec succès";
                    string[] tab = new string[2];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = id.ToString();
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Decaissement/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }
       
        [ActionName("Modifier")]
        public JsonResult Modifier(string id)
        {
            Int64 ID_Decaissement = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            //LocalVM.Type_Element = 0 => Décaissement
            //LocalVM.Type_Element = 1 => Payement ristourne client
            //LocalVM.Type_Element = 2 => Dépense
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Modifier_reglement_fournisseur", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Modifier_reglement_ristourne", this, ref tab_Habillitation)) || (type == 2 && !service.VerifierHabiliationCRUD("Modifier_depense", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(ID_Decaissement, "TP_Decaissement", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            DecaissementBM ObjetBM = new DecaissementBM();
            DecaissementVM LocalVM = ObjetBM.GetByID(ID_Decaissement);
            if (LocalVM == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            
            StoreEntities db = new StoreEntities();
            if (LocalVM.Type_Element == 0)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Fournisseur
                                                            where c.Actif
                                                            orderby c.Nom
                                                            select new
                                                            {
                                                                ID_Element = c.ID_Fournisseur,
                                                                Libelle = c.Nom
                                                            }, "ID_Element", "Libelle");
            }
            else if (LocalVM.Type_Element == 1)
            {
                var ristourne = db.TP_Ristourne_Client.Where(p => p.Actif == true).Select(p => p.ID_Client).Distinct().ToList();
                TempData["ID_Element"] = new SelectList(from c in db.TP_Client
                                                       where c.Actif && ristourne.Contains(c.ID_Client)
                                                       orderby c.Nom
                                                       select new
                                                       {
                                                           ID_Element = c.ID_Client,
                                                           Libelle = c.Nom
                                                       }, "ID_Element", "Libelle");
            }
            else
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Type_Depense
                                                             where c.Actif
                                                             orderby c.Libelle
                                                             select new
                                                             {
                                                                 ID_Element = c.ID_Type_Depense,
                                                                 Libelle = c.Libelle
                                                             }, "ID_Element", "Libelle");
            }

            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Decaissement/Modifier.cshtml", LocalVM);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(string id, DecaissementVM Param_Var)
        {
            Int64 ID_ = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    DecaissementBM ObjetBM = new DecaissementBM();
                    ObjetBM.Edit(ID_, Param_Var, Session["Code_User"].ToString());
                    // libère le verrou précédemment pauser
                    Locked_RecordsBM veroux = new Locked_RecordsBM();
                    veroux.Liberer(ID_, "TP_Decaissement");
                    TempData["messagesucess"] = "Modification effectuée avec succès";
                    string[] tab = new string[2];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["messagewarning"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Decaissement/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName("Valider")]
        public JsonResult Valider(string id)
        {
            Int64 ID_Decaissement = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Valider_reglement_fournisseur", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Valider_reglement_ristourne", this, ref tab_Habillitation)) || (type == 2 && !service.VerifierHabiliationCRUD("Valider_depense", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                string messageRetour = string.Empty;
                DecaissementBM Local_Var = new DecaissementBM();
                Local_Var.Valider(ID_Decaissement, int.Parse(Session["Code_User"].ToString()));
                TempData["messagesucess"] = "Valider effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Decaissement/Valider/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }  
            
        }

        [ActionName("Annuler")]
        public JsonResult Annuler(string id)
        {
            Int64 ID_Decaissement = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Annuler_reglement_fournisseur", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Annuler_reglement_ristourne", this, ref tab_Habillitation)) || (type == 2 && !service.VerifierHabiliationCRUD("Annuler_depense", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                DecaissementBM Local_Var = new DecaissementBM();
                Local_Var.Annuler(ID_Decaissement, int.Parse(Session["Code_User"].ToString()));
                TempData["messagesucess"] = "Action effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Decaissement/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            
        }

        [ActionName("Consulter")]
        public JsonResult Consulter(Int64 id)
        {
            TempData["id"] = id;
            DecaissementBM Local_Var = new DecaissementBM();
            DecaissementVM Return_Var = Local_Var.GetByID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Decaissement/Consulter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
    }
}
