﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Models.BML.TP;

namespace PROJETMODELE.Controllers
{
     [Authorize]
    public class UtilisateursController : Controller
    {
        Services service = new Services();
        StoreEntities db = new StoreEntities();
        public class UplaodeFile
        {
            public bool success { get; set; }
            public string file { get; set; }
            public string message { get; set; }
        }
        public class JqxwGrid
        {
            public int id { get; set; }
            public string Personnel { get; set; }
            public string Poste { get; set; }
            public int Code_Profil { get; set; }
            public string Profil
            {
                get
                {
                    string ListeProfil = "";
                    StoreEntities db = new StoreEntities();
                    var user = db.TG_Profiles_Utilisateurs.Where(p => p.Code_User == id && p.TG_Utilisateurs.Display == true && p.Actif == true).Select(p => p);
                    foreach (var i in user)
                    {
                        if(i.TG_Profiles.Actif == true)
                            ListeProfil += i.TG_Profiles.Libelle + " - ";
                    }
                    return ListeProfil;
                }
            }
            public string Login { get; set; }
            public string Client { get; set; }
            public string Caisse { get; set; }
            public bool Actif { get; set; }
            public string Statut { get; set; }
            public string Telephone { get; set; }
            public string Sexe { get; set; }
        }

        public JsonResult ajax()
        {
            int ID_Element = int.Parse(Session["ID_Element"].ToString());
            StoreEntities db = new StoreEntities();
            var Table = new List<JqxwGrid>
            {
            };
            var resultat = from c in db.TG_Utilisateurs where c.Display==true && c.ID_Personnel != 1 orderby c.Actif descending, c.TG_Personnels.Nom select c;
            foreach (TG_Utilisateurs user in resultat)
            {
                Table.Add(new JqxwGrid()
                {
                    id = user.Code_User,
                    Personnel = user.TG_Personnels.Nom,
                    Login = user.Login,                  
                    //Caisse = String.Join(" - ", db.TP_Utilisateur_Caisse.Where(p=>p.ID_Utilisateur == user.ID_Personnel).Select(p=>p.TP_Caisse.Libelle).ToArray()),
                    Actif = user.Actif,
                   // Telephone = user.TG_Personnels.Mail,
                    Statut = (user.Actif) ? "Actif" : "Suspendu",
                    //Sexe = (user.TG_Personnels.Sexe) ? "Masculin" : "Feminin"
                });
            } 
            if (ID_Element == 0)
            {
                return this.Json(Table, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var Utilisateurs_lier_au_profil = from p in db.TG_Profiles_Utilisateurs where p.Code_Profile == ID_Element select p.Code_User;
                return this.Json(Table.Where(p => Utilisateurs_lier_au_profil.Contains(p.id)), JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult Index()
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_utilisateurs", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            var profil = (from c in db.TG_Profiles orderby c.Libelle where c.Actif == true select c).ToList();
            int Code_Profile = profil.FirstOrDefault().Code_Profile;

            profil.Add(new TG_Profiles { Code_Profile = 0, Libelle = "All" });
            TempData["Drop_Liste_Code_Profile"] = new SelectList(profil, "Code_Profile", "Libelle", 0);

            Session["ID_Element"] = 0;

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Utilisateurs/Index.cshtml", null);
            tab[1] = "GESTION DES UTILISATEURS";
            tab[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Administration</a></li> <li class='active'><a href='#'>Liste des utilisateurs</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Utilisateurs/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Utilisateurs/Ajouter
        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Ajouter_Utilisateur", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
           
            // envoie de la liste des Personnels
            TempData["PersonnelsItem"] = new SelectList(from p in db.Vue_Personnel
                                                    let Perso_alredy_use = from user in db.TG_Utilisateurs
                                                                           select user.ID_Personnel
                                                    where Perso_alredy_use.Contains(p.ID_Personnel) == false //&& p.Display == true
                                                    orderby p.Libelle
                                                    select (p), "ID_Personnel", "Libelle");

            // envoie de la liste des Profiles
            TempData["Profiles_Out_Item"] = new SelectList(from p in db.TG_Profiles where (p.Actif == true && p.Code_Profile != 1) orderby p.Libelle select (p), "Code_Profile", "Libelle");
            // envoie de la liste des Services l'ors de l'ajout cette liste ne doit contenir aucun profil c'est pour ca quon a (p.Code_Profile==-1)
            TempData["Profiles_In_Item"] = new SelectList(from p in db.TG_Profiles where (p.Code_Profile == -1) select (p), "Code_Profile", "Libelle");
            string chemin = Path.Combine(Server.MapPath("~/Content/pictures"), "current" + Session["Code_User"].ToString() + ".png");
            System.IO.File.Delete(chemin);
          
            TempData["ID_Caisse"] = new SelectList(from c in db.TP_Caisse
                                                   orderby c.Libelle
                                                   where c.Actif
                                                   select new
                                                   {
                                                       ID_Caisse = c.ID_Caisse,
                                                       Libelle = c.Libelle,
                                                   }, "ID_Caisse", "Libelle");

            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Utilisateurs/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Utilisateurs/Ajouter
        [HttpPost, ActionName ( "Ajouter" )]
        [ValidateAntiForgeryToken]
        public JsonResult Ajouter ( Utilisateurs Utilisateur_Var )
        {
            try
            {

                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                   string messageRetour = string.Empty;
                  
                        string ancient = Request.Form.Get("ancient");
                        string profile = Request.Form.Get("profile");
                        UtilisateursBM ObjetBMRetunr = new UtilisateursBM();
                    if (ObjetBMRetunr.IsNotExist(Utilisateur_Var.Login, ref messageRetour))
                     {
                        int idpersonnel = ObjetBMRetunr.Save(Utilisateur_Var, profile, Session["Code_User"].ToString());
                       
                       
                            //string chemin = Path.Combine(Server.MapPath("~/Content/pictures"), "current" + Session["Code_User"].ToString() + ".png");
                            //string defauflt = Path.Combine(Server.MapPath("~/Content/pictures"), "user.png");
                            ////int idpersonnel = ObjetBMRetunr.Save(Personnel_Var, Session["Code_User"].ToString());
                            //String fileuser = Path.Combine(Server.MapPath("~/Content/pictures"), idpersonnel.ToString() + ".png");

                            //if (System.IO.File.Exists(chemin))//le fichier current existe 
                            //{
                            //    System.IO.File.Copy(chemin, fileuser);
                            //    System.IO.File.Delete(chemin);
                            //}
                            //else
                            //{
                            //    System.IO.File.Copy(defauflt, fileuser);
                            //}   
                            TempData["messagesucess"] = "Ajout effectué avec succès";
                            string[] tab = new string[2];//2 pour savoir que c'est ok
                            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                            tab[1] = "";
                            return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Utilisateurs/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult UploadFile(int? id)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            UplaodeFile up = new UplaodeFile();
            up.file = "current" + Session["Code_User"].ToString() + ".png";
            if (Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = Request.Files["UploadedImage"];

                if (httpPostedFile != null)
                {
                    bool valid = false;
                    string ext = Path.GetExtension(httpPostedFile.FileName), chemin = "";
                    ext = ext.ToLower();
                    switch (ext)
                    {
                        case ".jpg":
                            valid = true;
                            break;
                        case ".png":
                            valid = true;
                            break;
                        case ".gif":
                            valid = true;
                            break;
                        case ".jpeg":
                            valid = true;
                            break;
                        case ".ico":
                            valid = true;
                            break;
                    }
                    if (!valid)
                    {
                        up.message = "Les formats de photo sont .jpg,.png,.gif,.jpeg,.ico";
                        up.success = false;
                        return this.Json(serializer.Serialize(up), JsonRequestBehavior.AllowGet);

                    }
                    chemin = Path.Combine(Server.MapPath("~/Content/pictures"), "current" + Session["Code_User"].ToString() + ".png");
                    System.IO.File.Delete(chemin);
                    httpPostedFile.SaveAs(chemin);
                    FileStream file = new FileStream(chemin, FileMode.Open, FileAccess.Read);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(file, false, false);

                    if ((id == null) && (img.Height < 80 || img.Width < 80 || img.Height > 300 || img.Width > 300))
                    {
                        img.Dispose();
                        file.Close();
                        System.IO.File.Delete(chemin);
                        up.message = "La hauteur et la largeur des images doivent être compris entre 80 et 300 px";
                        up.success = false;
                        return this.Json(serializer.Serialize(up), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        img.Dispose();
                        file.Close();
                    }
                    // photo = Session["Code_User"].ToString() + ".png";
                    up.message = "Image uploadé avec succés";
                    up.success = true;
                    return this.Json(serializer.Serialize(up), JsonRequestBehavior.AllowGet);

                }
            }
            up.message = "Aucune image sélectionnée";
            up.success = false;
            return this.Json(serializer.Serialize(up), JsonRequestBehavior.AllowGet);
        }
        //
        // GET: /Utilisateurs/Modifier/5
        [ActionName ( "Modifier" )]
        public JsonResult Modifier ( int id )
        {

            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Modifier_Utilisateur", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            
            Locked_RecordsBM veroux = new Locked_RecordsBM ( );
            string message = "";
            if (veroux.IsVerouller(id, "TG_Utilisateurs", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            UtilisateursBM ObjetLocal = new UtilisateursBM();
            Utilisateurs returnObjet = ObjetLocal.GetByID(id);
            if (returnObjet == null)
            {
                TempData["messagenotification"] = "Aucun utilisateur correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            // envoie de la liste des Localites
            TempData["PersonnelsItem"] = new SelectList(from p in db.Vue_Personnel orderby p.Libelle select (p), "ID_Personnel", "Libelle");

            // envoie de la liste des Grades
            TempData["Profiles_Out_Item"] = new SelectList(from prof in db.TG_Profiles // pour tous les profiles
                                                           let prof_use = from pr in db.TG_Profiles_Utilisateurs  //prof_use contient les prif du user 
                                                                          where pr.Code_User == id
                                                                          select pr.Code_Profile
                                                           where (prof.Actif == true && prof.Code_Profile != 1 && prof_use.Contains(prof.Code_Profile) == false)
                                                           orderby prof.Libelle
                                                           select (prof), "Code_Profile", "Libelle");

            // envoie de la liste des Profiles
            TempData["Profiles_In_Item"] = new SelectList(from p in db.TG_Profiles
                                                          join C_User in db.TG_Profiles_Utilisateurs on p.Code_Profile equals C_User.Code_Profile
                                                          where (p.Actif == true && C_User.Code_User == id)
                                                          select (p), "Code_Profile", "Libelle");
                       
            string chemin = Path.Combine(Server.MapPath("~/Content/pictures"), "current" + Session["Code_User"].ToString() + ".png");
            System.IO.File.Delete(chemin);
            TempData["chemin"] = id.ToString() + ".png";
            TempData["ID_Caisse"] = new SelectList(from c in db.TP_Caisse
                                                   orderby c.Libelle
                                                   where c.Actif
                                                   select new
                                                   {
                                                       ID_Caisse = c.ID_Caisse,
                                                       Libelle = c.Libelle,
                                                   }, "ID_Caisse", "Libelle");

            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Utilisateurs/Modifier.cshtml", returnObjet);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        
        //
        // POST: /Utilisateurs/Modifier/5
        [HttpPost, ActionName ( "Modifier" )]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier ( int id, Utilisateurs Utilisateurs_Var )
        {
            try
            {
                if (ModelState.IsValid)//ModelState.IsValid
                {
                    string messageRetour = string.Empty;
                    // TODO: Add update logic here
                    UtilisateursBM ObjetBMReturn = new UtilisateursBM ( );
                    if (ObjetBMReturn.IsNotExist ( Utilisateurs_Var.Login, id, ref messageRetour )) //vérifie si l'élément existe déjà
                    {
                        string ancient = Request.Form.Get ( "ancient" );
                        string profile = Request.Form.Get ( "profile" );
                        
                        int ID_Personnel = ObjetBMReturn.Edit(id, Utilisateurs_Var, ancient, profile, Session["Code_User"].ToString());
                        
                        // libère le verrou précédemment pauser
                        Locked_RecordsBM veroux = new Locked_RecordsBM ( );
                        veroux.Liberer ( id, "TG_Utilisateurs" );
                        //string chemin = Path.Combine(Server.MapPath("~/Content/pictures"), "current" + Session["Code_User"].ToString() + ".png");
                        //string defauflt = Path.Combine(Server.MapPath("~/Content/pictures"), "user.png");
                        //int idpersonnel = id;
                        //String fileuser = Path.Combine(Server.MapPath("~/Content/pictures"), idpersonnel.ToString() + ".png");

                        //if (System.IO.File.Exists(chemin))//le fichier current existe 
                        //{
                        //    System.IO.File.Copy(chemin, fileuser,true);
                        //    System.IO.File.Delete(chemin);
                        //}
                        TempData["messagesucess"] = "Modification effectuée avec succès";
                        string[] tab = new string[2];
                        tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                        tab[1] = "";
                        return this.Json ( tab, JsonRequestBehavior.AllowGet );
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                        return this.Json ( tab, JsonRequestBehavior.AllowGet );
                    }
                }
                else
                {
                    TempData["messagewarning"] = "Le Modele n'est pas valide ";
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                    return this.Json ( tab, JsonRequestBehavior.AllowGet );
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM ( );
                Log.Save(this, ex, "POST: /Utilisateurs/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                return this.Json ( tab, JsonRequestBehavior.AllowGet );
            }
        }
        
        //
        // GET: /Utilisateurs/Supprimer/5
        [ActionName ( "Supprimer" )]
        public ActionResult Supprimer ( string id )
        {
            try
            {
                string[] allid = id.Split ( ';' );
                for (int i = 0; i < allid.Length; i++)
                {
                    UtilisateursBM ObjetBMReturn = new UtilisateursBM ( );
                    ObjetBMReturn.Delete(int.Parse(allid[i]), Session["Code_User"].ToString());
                }

                TempData["messagesucess"] = "Opération effectuée avec succès";
                string[] tab1 = new string[1];
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM ( );
                Log.Save(this, ex, "POST: /Utilisateurs/Supprimer/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une erreur est survenue durant l'opération ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                return this.Json ( tab, JsonRequestBehavior.AllowGet );
            }
        }

        [ActionName ( "Activer" )]
        public ActionResult Activer ( string id )
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Activer_suspendre_Utilisateur", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                string[] allid = id.Split ( ';' );
                for (int i = 0; i < allid.Length; i++)
                {
                    UtilisateursBM ObjetBMReturn = new UtilisateursBM ( );
                    ObjetBMReturn.Activer(int.Parse(allid[i]), Session["Code_User"].ToString());
                }
                return View ( );
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM ( );
                Log.Save(this, ex, "POST: /Utilisateur/Activer/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une erreur est survenue durant l'activation ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                return this.Json ( tab, JsonRequestBehavior.AllowGet );
            }
        }

        [ActionName("Reinitialiser")]
        public ActionResult Reinitialiser(string id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Reinitaliser_Utilisateur", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                string[] allid = id.Split(';');
                for (int i = 0; i < allid.Length; i++)
                {
                    UtilisateursBM ObjetBMReturn = new UtilisateursBM();
                    ObjetBMReturn.Reinitialiser(int.Parse(allid[i]), Session["Code_User"].ToString());
                }
                TempData["messagesucess"] = "Réinitialisation effectuée avec succès";
                string[] tab1 = new string[1];
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Utilisateur/Activer/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une erreur est survenue durant l'activation ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }


    }
}