﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class CommandesController : Controller
    {
        //
        // GET: /Commandes/
        Services service = new Services();
        StoreEntities db = new StoreEntities();
        public class JqwGrades
        {
            public int id { get; set; }
            public string Commande { get; set; }
            public string Profil { get; set; }
            public bool Executer { get; set; }
            public string ImgExecuter { get { return (Executer) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool Disponible { get; set; }
            public string ImgDisponible { get { return (Executer) ? "icn_alert_success.png" : "pwd.png"; } }
        }

        public class JqwRecapCmd
        {
            public int Code_Commande { get; set; }
            public int Code_Commande_Parent { get; set; }
            public string Libelle { get; set; }
        }

        public class JqwRecapProfil
        {
            public int Code_Profil { get; set; }
            public string Libelle { get; set; }
        }

        public class JqwRecapDroit
        {
            public int Code_Profil { get; set; }
            public int Code_Commande { get; set; }
            public bool Executer { get; set; }
            public bool Disponible { get; set; }
        }

        public class JqwRecap
        {
            public int id { get; set; }
            public string Commande { get; set; }
            public string Parent { get; set; }
            public bool AD { get; set; }
            public string _AD { get { return (AD) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool RC { get; set; }
            public string _RC { get { return (RC) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool RS { get; set; }
            public string _RS { get { return (RS) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool CC { get; set; }
            public string _CC { get { return (CC) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool CL { get; set; }
            public string _CL { get { return (CL) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool CA { get; set; }
            public string _CA { get { return (CA) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool ComB { get; set; }
            public string _ComB { get { return (ComB) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool DG { get; set; }
            public string _DG { get { return (DG) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool ComK { get; set; }
            public string _ComK { get { return (ComK) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool ResCai { get; set; }
            public string _ResCai { get { return (ResCai) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool CT { get; set; }
            public string _CT { get { return (CT) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
            public bool RA { get; set; }
            public string _RA { get { return (RA) ? "icn_alert_success.png" : "icn_alert_error.png"; } }
        }

        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();

            var Table = new List<JqwGrades>
            {
            };
            int Code_Commande = int.Parse(Session["ID_Element"].ToString());

            var resultat = from c in db.FN_Recapitulatif_Commande(Code_Commande)
                           select c;
            foreach (var c in resultat)
            {
                Table.Add(new JqwGrades()
                {
                    id = c.Code_Commande,
                    Profil = c.Profil,
                    Commande = c.Commande,
                    Executer = c.Executer,
                    Disponible = c.Disponible
                });
            }
            return this.Json(Table, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxrecap()
        {
            StoreEntities db = new StoreEntities();
            var Table = new List<JqwRecap>{};
            var resultat = from c in db.FN_Recapitulatif_Commande_Ensemble(0) select c;
            foreach (var c in resultat)
            {
                Table.Add(new JqwRecap()
                {
                    id = c.Code_Commande,
                    AD = c.AD,
                    CA = c.CA,
                    CC = c.CC,
                    CL = c.CL,
                    ComB = c.ComB,
                    ComK = c.ComK,
                    Commande = c.Commande,
                    Parent = c.Commande_Parent,
                    CT = c.CT,
                    DG = c.DG,
                    RA = c.RA,
                    RC = c.RC,
                    ResCai = c.ResCai,
                    RS = c.RS
                });
            }
            return this.Json(Table, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Commandes/Index
        public JsonResult Index(int code_profile = 1)
        {
            Session["ID_Element"] = 1;
            // envoie de la liste des Profiles
            TempData["ProfilesItem"] = new SelectList(from Commande in db.TG_Profiles where Commande.Code_Profile != 1 && Commande.Display == true && Commande.Actif == true select (Commande), "Code_Profile", "Libelle");

            CommandesBM ObjetBMRetunr = new CommandesBM();

            string liste = string.Empty;

            ObjetBMRetunr.creer_noeud(1, "Application", "Autoriser les utilisateurs du profile à se connecter au logiciel", ref liste, code_profile);

            TempData["liste"] = "<div id='jqxTree'><ul>" + liste + "</ul></div>";

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Commandes/index.cshtml", null);
            tab[1] = "GESTION DES HABILITATIONS";
            tab[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Administration</a></li> <li class='active'><a href='#'>Habilitation</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Droit_Profile(int id)
        {
            // envoie de la liste des Profiles
            ViewBag.ProfilesItem = new SelectList(from Commande in db.TG_Profiles select (Commande), "Code_Profile", "Libelle");

            CommandesBM ObjetBMRetunr = new CommandesBM();

            string liste = string.Empty;

            ObjetBMRetunr.creer_noeud(1, "Application", "Autoriser les utilisateurs du profile à se connecter au logiciel", ref liste, id);

            ViewBag.liste = "<div id='jqxTree'><ul>" + liste + "</ul></div>";

            string[] tab1 = new string[1];
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json("<div id='jqxTree'><ul>" + liste + "</ul></div>", JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Commandes/Index
        [HttpPost, ActionName("Index")]
        public JsonResult Index(FormCollection collection)
        {

            CommandesBM ObjetBMRetunr = new CommandesBM();

            string ID_Commande_Cheked = collection["ID_Commande_Cheked"];

            string Droit = collection["droit"];

            string profiles = collection["profiles"];

            if (string.IsNullOrWhiteSpace(ID_Commande_Cheked))
            {
                TempData["messagewarning"] = "Vous devez selectionner au moins un droit ";
            }
            else if (string.IsNullOrWhiteSpace(Droit))
            {
                TempData["messagewarning"] = "Vous devez selectionner l'autorisation a affecter au profile ";
            }
            else
            {
                string[] ID_Commande = ID_Commande_Cheked.Split(';');
                foreach (string c in ID_Commande)
                {
                    ObjetBMRetunr.EditDroit(int.Parse(c), int.Parse(profiles), int.Parse(Droit));
                }
                TempData["messagesucess"] = "Attribution effectée avec succès ";
            }
            ViewBag.ProfilesItem = new SelectList(from Commande in db.TG_Profiles where Commande.Actif == true select (Commande), "Code_Profile", "Libelle", profiles);

            string liste = string.Empty;

            ObjetBMRetunr.creer_noeud(1, "Application", "Autoriser les utilisateurs du profile à se connecter au logiciel", ref liste, int.Parse(profiles));

            string[] tab = new string[2];
            tab[1] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[0] = "<div id='jqxTree'><ul>" + liste + "</ul></div>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Commandes/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Commandes/Ajouter
        [ActionName("Ajouter")]
        public ActionResult Ajouter()
        {
            // envoie de la liste des Profiles
            ViewBag.ProfilesItem = new SelectList(from Commande in db.TG_Profiles select (Commande), "Code_Profile", "Libelle");
            TempData["VoirElemenAttente"] = 0;
            CommandesBM ObjetBMRetunr = new CommandesBM();

            string liste = string.Empty;

            ObjetBMRetunr.creer_noeud(1, "Application", "Autoriser les utilisateurs du profile à se connecter au logiciel", ref liste, 1);

            ViewBag.liste = liste;
            int Code_User = int.Parse(Session["Code_User"].ToString());

            // envoie de la liste des commandes
            ViewBag.CommandesItem = new SelectList(from Commande in db.TG_Commandes select (Commande), "Code_Commande", "Libelle");

            return View();
        }

        //
        // POST: /Commandes/Ajouter
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(Commandes Commande_Var)
        {
            string[] tab = new string[2];
            try
            {

                CommandesBM ObjetBMRetunr = new CommandesBM();
                string liste = string.Empty;
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    ObjetBMRetunr.Save(Commande_Var);

                    TempData["messagesucess"] = "Commande inserée avec succès ";

                }
                else
                {
                    TempData["messagewarning"] = "Le modèle est incorrect assurez-vous d'avoir rempli tous les champs et choisis la commande parent ";
                }

                ObjetBMRetunr.creer_noeud(1, "Application", "Autoriser les utilisateurs du profile à se connecter au logiciel", ref liste, 1);

                ViewBag.liste = "<div id='jqxTree'><ul>" + liste + "</ul></div>";
                tab[1] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[0] = "<div id='jqxTree'><ul>" + liste + "</ul></div>";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Commandes/Ajouter", Session["Code_User"].ToString());
                return this.Json("<div id='jqxTree'>ras</div>", JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Commandes/Modifier/5
        [ActionName("Modifier")]
        public ActionResult Modifier(int id)
        {

            ViewBag.CommandesItem = new SelectList(from Commande in db.TG_Commandes select (Commande), "Code_Commande", "Libelle");

            CommandesBM ObjetLocal = new CommandesBM();
            Commandes returnObjet = ObjetLocal.GetByID(id);
            if (returnObjet == null)
            {
                TempData["messagewarning"] = "Aucune Commande correspondant";
                RedirectToAction("Index");
                return this.Json("Aucuns Commande correspondant", JsonRequestBehavior.AllowGet);
            }

            // gestion du verou

            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = string.Empty;
            if (veroux.IsVerouller(id, "TG_Commandes", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                return this.Json(message, JsonRequestBehavior.AllowGet);
            }

            return View(returnObjet);
        }

        //
        // POST: /Commandes/Modifier/5
        [HttpPost, ActionName("Modifier")]
        public JsonResult Modifier(int id, Commandes Commandes_Var)
        {
            try
            {
                if (ModelState.IsValid)//ModelState.IsValid
                {
                    // TODO: Add update logic here
                    CommandesBM ObjetBMReturn = new CommandesBM();
                    if (ObjetBMReturn.IsNotExist(Commandes_Var.Libelle)) //vérifie si l'élément existe déjà
                    {
                        ObjetBMReturn.Edit(id, Commandes_Var);

                        // libère le verrou précédemment pauser
                        Locked_RecordsBM veroux = new Locked_RecordsBM();
                        veroux.Liberer(id, "TG_Commandes");

                        TempData["messagesucess"] = "Modification effectuée avec succès";
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/Message.cshtml", null);
                        RedirectToAction("Index");
                        return this.Json("", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = "La Commande existe déjà ";
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    TempData["messagewarning"] = "Le Modele n'est pas valide ";
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Commandes/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

    }
}