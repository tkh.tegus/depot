﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;


namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class MessagesController : Controller
    {
        /*
        //
        // GET: /Messages/
        Services service = new Services();
        StoreEntities db = new StoreEntities();
        public class JqwGrades
        {
            // identifiant contient l'identifiant de chaque element concatener avec le type de l'élément E pour un examen et C pour une consultation
            // c'est ce champs qui est utilisé pour l'identifiant dans la grille
            public string Nom { get; set; }
            public int ID_Message { get; set; }
            public string Contenu { get; set; }
            public bool Sens { get; set; }
            public string Objet { get; set; }
            public DateTime Date_Envoie { get; set; }
            public int Code_User_Emetteur { get; set; }
            public int Code_User_Destinataire { get; set; }
        }
        public JsonResult Index()
        {
            TempData["nonlu"] = NONLU();
            TempData["lu"] = LU();
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Messages/Index.cshtml", null);
            tab[1] = "Gestion des Messages";
            tab[2] = "Vous êtes ici : <a href='#'>Accueil</a> > <span>Messages</span> > <span>Liste de message</span>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);

        }
        public JsonResult repondre(int message, int dest)
        {
            Newmessage messages = new Newmessage();
            int code_personnel = int.Parse(Session["Code_User"].ToString());
            //  var adminuser = 
            var user = db.TG_Utilisateurs.Where(p => p.ID_Personnel == code_personnel).FirstOrDefault();
            int id_profile = user.TG_Profiles_Utilisateurs.FirstOrDefault().Code_Profile;
            var useradmin = db.TG_Profiles_Utilisateurs.Where(p => p.Code_Profile == 1).Select(p => p.Code_User).ToList();
            TempData["destinataire"] = id_profile == 1 ? new SelectList(from p in db.TG_Utilisateurs where p.Code_User != user.Code_User && p.Actif == true select (new { p.Code_User, p.TG_Personnels.Nom }), "Code_User", "Nom", dest) : new SelectList(from p in db.TG_Utilisateurs where p.Code_User != user.Code_User && p.Actif == true && useradmin.Contains(p.Code_User) select (new { p.Code_User, p.TG_Personnels.Nom }), "Code_User", "Nom", dest);
            messages.Code_User = dest;
            Services services = new Services();
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Messages/repondre.cshtml", messages);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult repondre(int message, int dest, Newmessage messages)
        {
            try
            {

                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    messages.Code_User = dest;
                    MessagesBM mesBM = new MessagesBM();
                    mesBM.SaveReponse(messages, message, Session["Code_User"].ToString());
                    TempData["messagesucess"] = "Message envoyé avec succès";
                    string[] tab = new string[2];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Messages/reponse/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ajouter()
        {
            int code_personnel = int.Parse(Session["Code_User"].ToString());
            var user = db.TG_Utilisateurs.Where(p => p.ID_Personnel == code_personnel).FirstOrDefault();
            int id_profile = user.TG_Profiles_Utilisateurs.FirstOrDefault().Code_Profile;
            var useradmin = db.TG_Profiles_Utilisateurs.Where(p => p.Code_Profile == 1).Select(p => p.Code_User).ToList();
            TempData["destinataire"] = id_profile == 1 ? new SelectList(from p in db.TG_Utilisateurs where p.Code_User != user.Code_User && p.Actif == true select (new { p.Code_User, p.TG_Personnels.Nom }), "Code_User", "Nom") : new SelectList(from p in db.TG_Utilisateurs where p.Code_User != user.Code_User && p.Actif == true && useradmin.Contains(p.Code_User) select (new { p.Code_User, p.TG_Personnels.Nom }), "Code_User", "Nom");

            Services services = new Services();
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Messages/ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        // POST: /Grades/Create
        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult ajouter(Newmessage messages)
        {
            try
            {

                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    MessagesBM mesBM = new MessagesBM();
                    mesBM.Save(messages, Session["Code_User"].ToString());
                    TempData["messagesucess"] = "Message envoyé avec succès";
                    string[] tab = new string[2];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Messages/ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult updatelu(int id)
        {
            MessagesBM messages = new MessagesBM();
            messages.PutRead(id);
            return this.Json("ok", JsonRequestBehavior.AllowGet);
        }


        private string NONLU()
        {
            int code_personnel = int.Parse(Session["Code_User"].ToString());
            var user = db.TG_Utilisateurs.Where(p => p.ID_Personnel == code_personnel).FirstOrDefault();
            var ALL = (from M in db.TG_Messages
                       join ME in db.TG_Message_Echange on M.ID_Message equals ME.ID_Message
                       where ME.Lu == false && ME.Code_User_Destinataire == user.Code_User
                       select (new { ME.ID_Message, ME.Contenu, ME.Sens, ME.Objet, ME.Date_Envoie, ME.Code_User_Emetteur, ME.Code_User_Destinataire }
                             ));
            var allnonlu = (from t in ALL
                            join U in db.TG_Utilisateurs
                                on t.Code_User_Emetteur equals U.Code_User
                            select (
                             new JqwGrades()
                             {
                                 Nom = U.TG_Personnels.Nom + (U.TG_Personnels.Prenom == null ? "" : " " + U.TG_Personnels.Prenom),
                                 ID_Message = t.ID_Message,
                                 Contenu = t.Contenu,
                                 Sens = t.Sens,
                                 Objet = t.Objet,
                                 Date_Envoie = t.Date_Envoie,
                                 Code_User_Emetteur = t.Code_User_Emetteur,
                                 Code_User_Destinataire = t.Code_User_Destinataire
                             }
                                   ));
            string tab = "";
            string val = "";
            string[] parcourir;
            bool enter = false, enter1 = false;
            foreach (JqwGrades item in allnonlu)
            {
                enter = false;
                enter1 = true;
                parcourir = tab.Split('¤');
                for (int i = 0; i < parcourir.Length - 1; i++)
                {
                    if (parcourir[i] == item.ID_Message.ToString())
                        enter = true;
                }
                if (!enter)
                {
                    tab = tab + item.ID_Message.ToString() + "¤";

                    val = "<tr class='nonlu'> <td class='name'>" + item.Nom + "</td><td class='objet'>" + item.Objet + "</td><td class='datem'>" + item.Date_Envoie.ToLongDateString() + "  " + item.Date_Envoie.ToLongTimeString() + "</td></tr> <tr class='body'><td colspan='3'><a style='display:inline' class='btn' href='/Messages/repondre/?message=" + item.ID_Message.ToString() + "&dest=" + item.Code_User_Emetteur.ToString() + "' ><input type='hidden' value='" + item.ID_Message.ToString() + "' >Répondre<i class='icon-share-alt icon-white'></i></a><br/><br/>" + allmessagesread(item.ID_Message.ToString()) + " </td> </tr>" + val;
                }
            }
            if (enter1)
                val = val + "</table>";
            else
                val = val + "<tr> <td> Vous avez lu tous les messages figurant dans votre boîte de réception.</td> </tr></table>";

            return "<table class='messag'>" + val;
        }
        private string LU()
        {
            int code_personnel = int.Parse(Session["Code_User"].ToString());
            var user = db.TG_Utilisateurs.Where(p => p.ID_Personnel == code_personnel).FirstOrDefault();
            //on rejete tous ceux qui n'ont pas été lu
            var rejette = (from ME in db.TG_Message_Echange
                           where ME.Lu == false && ME.Code_User_Destinataire == user.Code_User
                           select ME);

            var ALL = (from M in db.TG_Messages
                       join ME in db.TG_Message_Echange on M.ID_Message equals ME.ID_Message
                       where ME.Lu == true && ME.Code_User_Destinataire == user.Code_User
                       select (
                        new
                        {
                            ME.ID_Message,
                            ME.Contenu,
                            ME.Sens,
                            ME.Objet,
                            ME.Date_Envoie,
                            ME.Code_User_Emetteur,
                            ME.Code_User_Destinataire
                        }));

            var alllu = (from t in ALL
                         join U in db.TG_Utilisateurs
                             on t.Code_User_Emetteur equals U.Code_User
                         select (
                          new JqwGrades()
                          {
                              Nom = U.TG_Personnels.Nom + (U.TG_Personnels.Prenom == null ? "" : " " + U.TG_Personnels.Prenom),
                              ID_Message = t.ID_Message,
                              Contenu = t.Contenu,
                              Sens = t.Sens,
                              Objet = t.Objet,
                              Date_Envoie = t.Date_Envoie,
                              Code_User_Emetteur = t.Code_User_Emetteur,
                              Code_User_Destinataire = t.Code_User_Destinataire
                          }
                                ));
            string tab = "";
            string val = "";
            string[] parcourir;
            bool enter = false, enter1 = false, save = true;
            foreach (JqwGrades item in alllu)
            {
                enter = false;
                enter1 = true;
                save = true;
                parcourir = tab.Split('¤');
                for (int i = 0; i < parcourir.Length - 1; i++)//permet de recupérer q'un seul element
                {
                    if (parcourir[i] == item.ID_Message.ToString())
                        enter = true;
                }
                if (!enter)
                {
                    if (rejette.Count() == 0)
                    {
                        tab = tab + item.ID_Message.ToString() + "¤";
                        val = "<tr class='lu'> <td class='name'>" + item.Nom + "</td><td class='objet'>" + lastobjet(item.ID_Message.ToString()) + "</td><td class='datem'>" + item.Date_Envoie.ToLongDateString() + " " + item.Date_Envoie.ToLongTimeString() + "</td></tr><tr class='body'> <td colspan='3'> <a style='display:inline' class='btn' href='/Messages/repondre/?message=" + item.ID_Message.ToString() + "&dest=" + item.Code_User_Emetteur.ToString() + "' ><input type='hidden' value='" + item.ID_Message.ToString() + "' >Répondre<i class='icon-share-alt icon-white'></i></a><br/><br/>" + allmessagesread(item.ID_Message.ToString()) + " </td> </tr>" + val;
                    }
                    else
                    {
                        foreach (var rej in rejette)
                        {
                            if (rej.ID_Message == item.ID_Message)
                            {
                                save = false;
                                break;
                            }
                        }
                        if (save)
                        {
                            tab = tab + item.ID_Message.ToString() + "¤";
                            val = "<tr class='lu'> <td class='name'>" + item.Nom + "</td><td class='objet'>" + lastobjet(item.ID_Message.ToString()) + "</td><td class='datem'>" + item.Date_Envoie.ToLongDateString() + " " + item.Date_Envoie.ToLongTimeString() + "</td></tr><tr class='body'> <td colspan='3'> <a style='display:inline' class='btn' href='/Messages/repondre/?message=" + item.ID_Message.ToString() + "&dest=" + item.Code_User_Emetteur.ToString() + "' ><input type='hidden' value='" + item.ID_Message.ToString() + "' >Répondre<i class='icon-share-alt icon-white'></i></a><br/><br/>" + allmessagesread(item.ID_Message.ToString()) + " </td> </tr>" + val;
                        }
                    }

                }
            }
            if (enter1)
                val = val + "</table>";
            else
                val = val + "<tr> <td> Vous avez lu tous les messages figurant dans votre boîte de réception.</td> </tr></table>";

            return "<table class='messag'>" + val;
        }
        private string allmessagesread(String idmessage)
        {
            string val = "";//, val1 = "";
            int code_personnel = int.Parse(Session["Code_User"].ToString());
            //bool enter = false;
            int idmess = int.Parse(idmessage);
            var ALL = (from M in db.TG_Messages
                       join ME in db.TG_Message_Echange on M.ID_Message equals ME.ID_Message
                       where ME.ID_Message == idmess
                       select (new { ME.ID_Message, ME.Contenu, ME.Sens, ME.Objet, ME.Date_Envoie, ME.Code_User_Emetteur, ME.Code_User_Destinataire }
                             ));

            var allsend = (from t in ALL
                           join U in db.TG_Utilisateurs
                               on t.Code_User_Emetteur equals U.Code_User
                           select (
                            new
                            {
                                U.TG_Personnels.Nom,
                                t.ID_Message,
                                t.Contenu,
                                t.Sens,
                                t.Objet,
                                t.Date_Envoie,
                                t.Code_User_Emetteur,
                                t.Code_User_Destinataire
                            }
                                  ));
            foreach (var item in allsend)
            {
                val = "<div class='rows'> <div class='daterows'> " + item.Date_Envoie.ToLongDateString() + " " + item.Date_Envoie.ToLongTimeString() + "</div>" + item.Contenu + "</div>" + val;
            }
            return val;
        }
        private string lastobjet(String idmessage)
        {
            int idmess = int.Parse(idmessage);
            var objet = (from M in db.TG_Messages
                         join ME in db.TG_Message_Echange on M.ID_Message equals ME.ID_Message
                         where ME.ID_Message == idmess
                         orderby ME.Date_Envoie descending
                         select (new { ME.Objet })).FirstOrDefault();
            if (objet != null)
                return objet.Objet;
            else
                return "";



        }

        public int NumberOfMessage()
        {
            MessagesBM messages = new MessagesBM();
            int code_personnel = int.Parse(Session["Code_User"].ToString());
            var user = db.TG_Utilisateurs.Where(p => p.ID_Personnel == code_personnel).FirstOrDefault();
            return messages.NumberOfMessage(user.Code_User);
        }

      */
    }
}
