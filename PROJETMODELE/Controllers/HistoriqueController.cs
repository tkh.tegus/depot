﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    public class HistoriqueController : Controller
    {
        Services service = new Services();

        public class Element
        {
            public byte ID_Element { get; set; }
            public string Libelle { get; set; }
        }

        public class JqwAjax
        {
            public string id { get; set; }
            public DateTime date { get; set; }
            public string mouvement { get; set; }
            public string numero { get; set; }
            public double rang { get; set; }
            public double montant { get; set; }
            public double solde_progressif { get; set; }

            public double casier { get; set; }
            public double casier_progressif { get; set; }
            public double bouteille { get; set; }
            public double bouteille_progressif { get; set; }
            public double plastique { get; set; }
            public double plastique_progressif { get; set; }
        }

        #region Historique solde Client
        public JsonResult AjaxHistoriqueSoldeClient()
        {
            StoreEntities db = new StoreEntities();
            string Element = Session["ID_Element"].ToString();
            int ID_Client = int.Parse(Element.Split(';')[1]);
            byte Type = byte.Parse(Element.Split(';')[0]);
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var Resultat = from c in db.FN_Historique_Solde_Client(ID_Client, Type)
                           where (c.Date > filtre.DateDebut && c.Date <= filtre.CDateFin)
                           orderby c.Rang
                           select new JqwAjax
                           {
                               id = c.Type,
                               date = c.Date,
                               mouvement = c.Mouvement,
                               numero = c.Numero,
                               rang = c.Rang,
                               montant = c.Quantite,
                               solde_progressif = c.Stock_Porgressif
                           };

            return this.Json(Resultat, JsonRequestBehavior.AllowGet);
        }


        public JsonResult IndexHistoriqueSoldeClient(int id)
        {
           DroitsBM droitUtilisateur = new DroitsBM();
           if (!droitUtilisateur.ExecuteCommande(Session["Code_User"].ToString(), "Historique_Etat_Solde_Client"))
            {
                string[] tab_Habillitation = new string[3];
                tab_Habillitation[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Login/Habillitation.cshtml", null);
                tab_Habillitation[1] = "Accès réfusé";
                tab_Habillitation[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Habillitation</a></li> <li class='active'><a href='#'>Accès réfusé</a></li>";
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }

            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            StoreEntities db = new StoreEntities();

            int Code_User = int.Parse(Session["Code_User"].ToString());
            TempData["ID_Client"] = new SelectList(from c in db.TP_Client
                                                   where c.Actif == true
                                                   orderby c.Nom
                                                   select new
                                                   {
                                                       ID_Client = c.ID_Client,
                                                       Client = c.Nom
                                                   }, "ID_Client", "Client");
            var liste = new List<Element>
            {
                new Element { ID_Element = 0, Libelle = "Liquide" },
                new Element { ID_Element = 1, Libelle = "Emballage" },
                new Element { ID_Element = 2, Libelle = "Liquide & Emballage" },
            };
            TempData["Type"] = new SelectList(liste, "ID_Element", "Libelle");
            Session["ID_Element"] = Session["ID_Element"].ToString() + ";" + id;

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Historique/IndexHistoriqueSoldeClient.cshtml", filtre);
            tab[1] = "HISTORIQUE SOLDE CLIENT";
            tab[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Comptabilité</a></li> <li class='active'><a href='#'>Historique des soldes clients</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Historique_Solde/Consulter_Client
        [ActionName("Consulter_Etat_Client")]
        public JsonResult Consulter_Etat_Client(string id)
        {
            int ID = int.Parse(id.Split(';')[0]), Type = int.Parse(id.Split(';')[1]);
            TempData["id"] = ID;
            int Code_User = int.Parse(Session["Code_User"].ToString());
            if (Type.Equals(0)) // Encaissement
            {
                EncaissementBM Local_Var = new EncaissementBM();
                EncaissementVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Encaissement/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(1)) // Regulation solde client
            {
                Regulation_SoldeBM Local_Var = new Regulation_SoldeBM();
                Regulation_SoldeVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }

                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Solde/Consulter.cshtml", Return_Var);

                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(2)) // Vente
            {
                VenteBM Local_Var = new VenteBM();
                VenteVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Vente/Consulter.cshtml", Return_Var);

                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
           
            else if (Type.Equals(3)) // Retour emballage
            {
                Retour_EmballageBM Local_Var = new Retour_EmballageBM();
                Retour_EmballageVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Retour_Emballage/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json("", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Historique solde Fournisseur
        public JsonResult AjaxHistoriqueSoldeFournisseur()
        {
            StoreEntities db = new StoreEntities();
            string Element = Session["ID_Element"].ToString();
            int ID_Fournisseur = int.Parse(Element.Split(';')[1]);
            byte Type = byte.Parse(Element.Split(';')[0]);
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var Resultat = from c in db.FN_Historique_Solde_Fournisseur(ID_Fournisseur, Type)
                           where (c.Date > filtre.DateDebut && c.Date <= filtre.CDateFin)
                           orderby c.Rang
                           select new JqwAjax
                           {
                               id = c.Type,
                               date = c.Date,
                               mouvement = c.Mouvement,
                               numero = c.Numero,
                               rang = c.Rang,
                               montant = c.Quantite,
                               solde_progressif = c.Stock_Porgressif
                           };

            return this.Json(Resultat, JsonRequestBehavior.AllowGet);
        }


        public JsonResult IndexHistoriqueSoldeFournisseur(string id)
        {
            DroitsBM droitUtilisateur = new DroitsBM();
            if (!droitUtilisateur.ExecuteCommande(Session["Code_User"].ToString(), "Historique_Etat_Solde_Fournisseur"))
            {
                string[] tab_Habillitation = new string[3];
                tab_Habillitation[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Login/Habillitation.cshtml", null);
                tab_Habillitation[1] = "Accès réfusé";
                tab_Habillitation[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Habillitation</a></li> <li class='active'><a href='#'>Accès réfusé</a></li>";
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }

            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            StoreEntities db = new StoreEntities();

            int Code_User = int.Parse(Session["Code_User"].ToString());
            TempData["ID_Fournisseur"] = new SelectList(from c in db.TP_Fournisseur
                                                   where c.Actif == true
                                                   orderby c.Nom
                                                   select new
                                                   {
                                                       ID_Fournisseur = c.ID_Fournisseur,
                                                       Libelle = c.Nom
                                                   }, "ID_Fournisseur", "Libelle");
            var liste = new List<Element>
            {
                new Element { ID_Element = 0, Libelle = "Liquide" },
                new Element { ID_Element = 1, Libelle = "Emballage" },
                new Element { ID_Element = 2, Libelle = "Liquide & Emballage" },
            };
            TempData["Type"] = new SelectList(liste, "ID_Element", "Libelle");
            Session["ID_Element"] = Session["ID_Element"].ToString() + ";" + id;

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Historique/IndexHistoriqueSoldeFournisseur.cshtml", filtre);
            tab[1] = "HISTORIQUE SOLDE FOURNISSEUR";
            tab[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Comptabilité</a></li> <li class='active'><a href='#'>Historique des soldes fournisseur</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Historique_Solde/Consulter_Client
        [ActionName("Consulter_Etat_Fournisseur")]
        public JsonResult Consulter_Etat_Fournisseur(string id)
        {
            int ID = int.Parse(id.Split(';')[0]), Type = int.Parse(id.Split(';')[1]);
            TempData["id"] = ID;
            int Code_User = int.Parse(Session["Code_User"].ToString());
            if (Type.Equals(0)) // Commande
            {
                CommandeBM Local_Var = new CommandeBM();
                CommandeVM Return_Var = Local_Var.GetByID(ID, int.Parse(Session["Code_User"].ToString()));
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun service correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                StoreEntities db = new StoreEntities();
                TempData["Commande_Liquide"] = (from c in db.TP_Commande_Liquide
                                                where c.ID_Commande == ID
                                                select new CommandeController.JqwxLiquide
                                                {
                                                    id = c.ID_Commande_Liquide,
                                                    ID_Produit = c.ID_Produit,
                                                    Produit = c.TP_Produit.Libelle,
                                                    Quantite_Casier_Liquide = c.Quantite_Casier_Liquide,
                                                    Quantite_Bouteille_Liquide = c.Quantite_Bouteille_Liquide,
                                                    Quantite_Casier_Emballage = c.Quantite_Casier_Emballage,
                                                    Quantite_Plastique_Emballage = c.Quantite_Plastique_Emballage,
                                                    Quantite_Bouteille_Emballage = c.Quantite_Bouteille_Emballage,


                                                }).ToList();

                TempData["Commande_Emballage"] = (from c in db.TP_Commande_Emballage
                                                  where c.ID_Commande == ID
                                                  select new CommandeController.JqwxEmballage
                                                  {
                                                      id = c.ID_Commande_Emballage,
                                                      ID_Emballage = c.ID_Emballage,
                                                      Emballage = c.TP_Emballage.Code,
                                                      Quantite_Casier = c.Quantite_Casier,
                                                      Quantite_Plastique = c.Quantite_Plastique,
                                                      Quantite_Bouteille = c.Quantite_Bouteille,

                                                  }).ToList();
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Commande/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(1)) // Decaissement
            {
                DecaissementBM Local_Var = new DecaissementBM();
                DecaissementVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Decaissement/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(2)) // Regularisation
            {
                Regulation_SoldeBM Local_Var = new Regulation_SoldeBM();
                Regulation_SoldeVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }

                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Solde/Consulter.cshtml", Return_Var);

                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

            else if (Type.Equals(3)) // Retour emballage
            {
                Retour_EmballageBM Local_Var = new Retour_EmballageBM();
                Retour_EmballageVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Retour_Emballage/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json("", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Historique solde Caisse
        public JsonResult AjaxHistoriqueSoldeCaisse()
        {
            StoreEntities db = new StoreEntities();
            int ID_Caisse = int.Parse(Session["ID_Element"].ToString());
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var Resultat = from c in db.FN_Historique_Solde_Caisse(ID_Caisse)
                           where (c.Date > filtre.DateDebut && c.Date <= filtre.CDateFin)
                           orderby c.Rang
                           select new JqwAjax
                           {
                               id = c.Type,
                               date = c.Date,
                               mouvement = c.Mouvement,
                               numero = c.Numero,
                               rang = c.Rang,
                               montant = c.Quantite,
                               solde_progressif = c.Stock_Porgressif
                           };

            return this.Json(Resultat, JsonRequestBehavior.AllowGet);
        }


        public JsonResult IndexHistoriqueSoldeCaisse(int id)
        {
            DroitsBM droitUtilisateur = new DroitsBM();
            if (!droitUtilisateur.ExecuteCommande(Session["Code_User"].ToString(), "Historique_Etat_Solde_Caisse"))
            {
                string[] tab_Habillitation = new string[3];
                tab_Habillitation[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Login/Habillitation.cshtml", null);
                tab_Habillitation[1] = "Accès réfusé";
                tab_Habillitation[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Habillitation</a></li> <li class='active'><a href='#'>Accès réfusé</a></li>";
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }

            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            StoreEntities db = new StoreEntities();

            int Code_User = int.Parse(Session["Code_User"].ToString());
            TempData["ID_Caisse"] = new SelectList(from c in db.TP_Caisse
                                                   where c.Actif == true
                                                   orderby c.Libelle
                                                   select new
                                                   {
                                                       ID_Caisse = c.ID_Caisse,
                                                       Libelle = c.Libelle
                                                   }, "ID_Caisse", "Libelle");
            Session["ID_Element"] = id;
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Historique/IndexHistoriqueSoldeCaisse.cshtml", filtre);
            tab[1] = "HISTORIQUE SOLDE CAISSE";
            tab[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Comptabilité</a></li> <li class='active'><a href='#'>Historique des soldes caisse</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Historique_Solde/Consulter_Caisse
        [ActionName("Consulter_Etat_Caisse")]
        public JsonResult Consulter_Etat_Caisse(string id)
        {
            int ID = int.Parse(id.Split(';')[0]), Type = int.Parse(id.Split(';')[1]);
            TempData["id"] = ID;
            int Code_User = int.Parse(Session["Code_User"].ToString());
            if (Type.Equals(0)) // Encaissement
            {
                EncaissementBM Local_Var = new EncaissementBM();
                EncaissementVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Encaissement/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(1)) // Decaissement
            {
                DecaissementBM Local_Var = new DecaissementBM();
                DecaissementVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Decaissement/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(2)) // Regulation solde client
            {
                Regulation_SoldeBM Local_Var = new Regulation_SoldeBM();
                Regulation_SoldeVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }

                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Solde/Consulter.cshtml", Return_Var);

                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(3)) // Regulation solde client
            {
                VenteBM Local_Var = new VenteBM();
                VenteVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Vente/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json("", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Historique solde Ristourne
        public JsonResult AjaxHistoriqueSoldeRistourne()
        {
            StoreEntities db = new StoreEntities();
            int ID_Client = int.Parse(Session["ID_Element"].ToString());
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var Resultat = from c in db.FN_Historique_Solde_Ristourne_Client(ID_Client)
                           where (c.Date > filtre.DateDebut && c.Date <= filtre.CDateFin)
                           orderby c.Rang
                           select new JqwAjax
                           {
                               id = c.Type,
                               date = c.Date,
                               mouvement = c.Mouvement,
                               numero = c.Numero,
                               rang = c.Rang,
                               montant = c.Quantite,
                               solde_progressif = c.Stock_Porgressif
                           };

            return this.Json(Resultat, JsonRequestBehavior.AllowGet);
        }


        public JsonResult IndexHistoriqueSoldeRistourne(int id)
        {
            DroitsBM droitUtilisateur = new DroitsBM();
            if (!droitUtilisateur.ExecuteCommande(Session["Code_User"].ToString(), "Historique_Etat_Solde_Ristourne"))
            {
                string[] tab_Habillitation = new string[3];
                tab_Habillitation[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Login/Habillitation.cshtml", null);
                tab_Habillitation[1] = "Accès réfusé";
                tab_Habillitation[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Habillitation</a></li> <li class='active'><a href='#'>Accès réfusé</a></li>";
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }

            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            StoreEntities db = new StoreEntities();

            int Code_User = int.Parse(Session["Code_User"].ToString());
            var ristourne = db.TP_Ristourne_Client.Select(p => p.ID_Client).Distinct().ToList();
            TempData["ID_Client"] = new SelectList(from c in db.TP_Client
                                                   where c.Actif == true && ristourne.Contains(c.ID_Client)
                                                   orderby c.Nom
                                                   select new
                                                   {
                                                       ID_Client = c.ID_Client,
                                                       Libelle = c.Nom
                                                   }, "ID_Client", "Libelle");
            Session["ID_Element"] = id;
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Historique/IndexHistoriqueSoldeRistourne.cshtml", filtre);
            tab[1] = "HISTORIQUE SOLDE RISTOURNE CLIENT";
            tab[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Comptabilité</a></li> <li class='active'><a href='#'>Historique des Ristourne</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Historique_Solde/Consulter_Caisse
        [ActionName("Consulter_Etat_Ristourne")]
        public JsonResult Consulter_Etat_Ristourne(string id)
        {
            int ID = int.Parse(id.Split(';')[0]), Type = int.Parse(id.Split(';')[1]);
            TempData["id"] = ID;
            int Code_User = int.Parse(Session["Code_User"].ToString());
            if (Type.Equals(0)) // Encaissement
            {
                VenteBM Local_Var = new VenteBM();
                VenteVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Vente/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(1)) // Decaissement
            {
                DecaissementBM Local_Var = new DecaissementBM();
                DecaissementVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Decaissement/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json("", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Historique stock
        public JsonResult AjaxHistoriqueStock()
        {
            StoreEntities db = new StoreEntities();
            string Element = Session["ID_Element"].ToString();
            int ID_Element = int.Parse(Element.Split(';')[1]);
            byte Type = byte.Parse(Element.Split(';')[0]);
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var Resultat = from c in db.FN_Historique_Stock(ID_Element, Type)
                           where (c.Date > filtre.DateDebut && c.Date <= filtre.CDateFin)
                           orderby c.Rang
                           select new JqwAjax
                           {
                               id = c.Type,
                               date = c.Date,
                               mouvement = c.Mouvement,
                               numero = c.Numero,
                               rang = c.Rang,
                               casier = c.Casier,
                               casier_progressif = c.Casier_Porgressif,
                               bouteille = c.Bouteille,
                               bouteille_progressif = c.Bouteille_Porgressif,
                               plastique = c.Plastique,
                               plastique_progressif = c.Plastique_Porgressif
                           };

            return this.Json(Resultat, JsonRequestBehavior.AllowGet);
        }


        public JsonResult IndexHistoriqueStock(int id)
        {
            DroitsBM droitUtilisateur = new DroitsBM();
            if (!droitUtilisateur.ExecuteCommande(Session["Code_User"].ToString(), "Historique_Etat_Stock"))
            {
                string[] tab_Habillitation = new string[3];
                tab_Habillitation[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Login/Habillitation.cshtml", null);
                tab_Habillitation[1] = "Accès réfusé";
                tab_Habillitation[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Habillitation</a></li> <li class='active'><a href='#'>Accès réfusé</a></li>";
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }

            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            StoreEntities db = new StoreEntities();

            int Code_User = int.Parse(Session["Code_User"].ToString());
            int Type = int.Parse(Session["ID_Element"].ToString());
            if(Type == 0)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Produit
                                                            where c.Actif == true
                                                            orderby c.Libelle
                                                            select new
                                                            {
                                                                ID_Element = c.ID_Produit,
                                                                Libelle = c.Code +" : "+ c.Libelle
                                                            }, "ID_Element", "Libelle");
            }
            else
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Emballage
                                                        where c.Actif == true && c.ID_Emballage != 5 && c.ID_Emballage != 4
                                                        orderby c.Description
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Emballage,
                                                            Libelle = c.Code+" : "+c.Description
                                                        }, "ID_Element", "Libelle");
            }           
            var liste = new List<Element>
            {
                new Element { ID_Element = 0, Libelle = "Liquide" },
                new Element { ID_Element = 1, Libelle = "Emballage" },
            };

            TempData["Type"] = new SelectList(liste, "ID_Element", "Libelle");
            Session["ID_Element"] = Type.ToString() + ";" + id.ToString();
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Historique/IndexHistoriqueStock.cshtml", filtre);
            tab[1] = "HISTORIQUE STOCK";
            tab[2] = "<li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Comptabilité</a></li> <li class='active'><a href='#'>Historique des stock</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Historique_Solde/Consulter_Client
        [ActionName("Consulter_Etat_Stock")]
        public JsonResult Consulter_Etat_Stock(string id)
        {
            int ID = int.Parse(id.Split(';')[0]), Type = int.Parse(id.Split(';')[1]);
            TempData["id"] = ID;
            int Code_User = int.Parse(Session["Code_User"].ToString());
            if (Type.Equals(0)) // Commande
            {
                CommandeBM Local_Var = new CommandeBM();
                CommandeVM Return_Var = Local_Var.GetByID(ID, int.Parse(Session["Code_User"].ToString()));
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun service correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                StoreEntities db = new StoreEntities();
                TempData["Commande_Liquide"] = (from c in db.TP_Commande_Liquide
                                                where c.ID_Commande == ID
                                                select new CommandeController.JqwxLiquide
                                                {
                                                    id = c.ID_Commande_Liquide,
                                                    ID_Produit = c.ID_Produit,
                                                    Produit = c.TP_Produit.Libelle,
                                                    Quantite_Casier_Liquide = c.Quantite_Casier_Liquide,
                                                    Quantite_Bouteille_Liquide = c.Quantite_Bouteille_Liquide,
                                                    Quantite_Casier_Emballage = c.Quantite_Casier_Emballage,
                                                    Quantite_Plastique_Emballage = c.Quantite_Plastique_Emballage,
                                                    Quantite_Bouteille_Emballage = c.Quantite_Bouteille_Emballage,
                                                }).ToList();

                TempData["Commande_Emballage"] = (from c in db.TP_Commande_Emballage
                                                  where c.ID_Commande == ID
                                                  select new CommandeController.JqwxEmballage
                                                  {
                                                      id = c.ID_Commande_Emballage,
                                                      ID_Emballage = c.ID_Emballage,
                                                      Emballage = c.TP_Emballage.Code,
                                                      Quantite_Casier = c.Quantite_Casier,
                                                      Quantite_Plastique = c.Quantite_Plastique,
                                                      Quantite_Bouteille = c.Quantite_Bouteille,

                                                  }).ToList();
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Commande/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(1)) // Vente
            {
                VenteBM Local_Var = new VenteBM();
                VenteVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Vente/Consulter.cshtml", Return_Var);

                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(2)) // Regulation stock
            {
                Regulation_StockBM Local_Var = new Regulation_StockBM();
                Regulation_StockVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }

                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Solde/Consulter.cshtml", Return_Var);

                return this.Json("", JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(3)) // Regulation solde client
            {
                Retour_EmballageBM Local_Var = new Retour_EmballageBM();
                Retour_EmballageVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Retour_Emballage/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (Type.Equals(4)) // Inventaire
            {
                InventaireBM Local_Var = new InventaireBM();
                InventaireVM Return_Var = Local_Var.GetByID(ID);
                if (Return_Var == null)//aucun element trouve
                {
                    TempData["messagenotification"] = "Aucun élément correspondant";
                    string[] table = new string[1];
                    table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(table, JsonRequestBehavior.AllowGet);
                }
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Inventaire/Consulter.cshtml", Return_Var);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetElement(int id)
        {
            StoreEntities db = new StoreEntities();
            if (id == 0)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Produit
                                                        where c.Actif == true
                                                        orderby c.Libelle
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Produit,
                                                            Libelle = c.Code + " : " + c.Libelle
                                                        }, "ID_Element", "Libelle");
            }
            else
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Emballage
                                                        where c.Actif == true && c.ID_Emballage != 5 && c.ID_Emballage != 4
                                                        orderby c.Description
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Emballage,
                                                            Libelle = c.Code + " : " + c.Description
                                                        }, "ID_Element", "Libelle");
            }
            return this.Json(TempData["ID_Element"], JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
