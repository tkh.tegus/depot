﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class Regulation_StockController : Controller
    {
        Services service = new Services();
        public class JqwAjax
        {
            public Int64 id { get; set; }
            public DateTime Date { get; set; }
            public string Element { get; set; }
            public int Quantite_Casier { get; set; }
            public int Quantite_Plastique { get; set; }
            public int Quantite_Bouteille { get; set; }
            public string Motif { get; set; }
            public string Utilisateur { get; set; }
            public string Statut { get; set; }
            public string NUM_DOC { get; set; }
        }

        public JsonResult ajax(int id)
        {
            StoreEntities db = new StoreEntities();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.TP_Regulation_Stock
                           orderby c.Etat, c.Date descending
                           where (c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin) && c.Type_Element == id
                           select new JqwAjax
                           {
                               id = c.ID_Regulation_Stock,
                               Date = c.Date,
                               Element = (c.Type_Element == 0 ? db.TP_Produit.Where(p => p.ID_Produit == c.ID_Element).FirstOrDefault().Libelle : db.TP_Emballage.Where(p => p.ID_Emballage == c.ID_Element).FirstOrDefault().Code),
                               NUM_DOC = c.NUM_DOC,
                               Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == c.Create_Code_User).Select(p => p.Libelle).FirstOrDefault(),
                               Quantite_Casier = c.Quantite_Casier,
                               Quantite_Plastique = c.Quantite_Plastique,
                               Quantite_Bouteille = c.Quantite_Bouteille,
                               Motif = c.Motif,
                               Statut = (c.Etat == 0) ? "En Attente de validation" : (c.Etat == 1) ? "Valider" : "Annuler"
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Index")]
        public JsonResult Index(int id)
        {
            //id = 0 => Régulation Liquide
            //id = 1 => Régulation Emballage
            string[] tab_Habillitation = new string[3];
            if ((id == 0 && !service.VerifierHabiliationIndex("Gestion_des_regulations_stock_liquide", this, ref tab_Habillitation)) || (id == 1 && !service.VerifierHabiliationIndex("Gestion_des_regulations_stock_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            var ouverture = db.TP_Ouverture_Cloture_Caisse.Where(p => p.ID_Caisse == ID_Caisse && p.Etat == 0).FirstOrDefault();
            if (ouverture != null)
            {
                Session["DateDebut"] = ouverture.Date_Ouverture.ToString();
            }
            else
            {
                Session["DateDebut"] = DateTime.Today.ToString();
            }
            Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            TempData["type"] = id;
            if (id == 0)
            {
                string[] tab = new string[3];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Stock/Index.cshtml", filtre);
                tab[1] = "GESTION DES RÉGULATIONS LIQUIDES";
                tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des décaissements</li>";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else 
            {
                string[] tab = new string[3];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Stock/Index.cshtml", filtre);
                tab[1] = "GESTION DES RÉGULATIONS EMBALLAGES";
                tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des ristournes clients</li>";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            
        }

        
        [ActionName("Ajouter")]
        public JsonResult Ajouter(int id)
        {
            //id = 0 => Régulation Liquide
            //id = 1 => Régulation Emballage      
            string[] tab_Habillitation = new string[1];
            if ((id == 0 && !service.VerifierHabiliationCRUD("Ajouter_regulations_stock_liquide", this, ref tab_Habillitation)) || (id == 1 && !service.VerifierHabiliationCRUD("Ajouter_regulations_stock_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            if (id == 0)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Produit
                                                        where c.Actif && c.Produit_Decomposer == false
                                                        orderby c.Libelle
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Produit,
                                                            Libelle = c.Code+" : "+c.Libelle
                                                        }, "ID_Element", "Libelle");
            }
            else
            {
                var prod = db.TP_Produit.Where(p => p.Valeur_Emballage == false || p.Produit_Decomposer == true).Select(p => p.ID_Emballage).ToList();
                TempData["ID_Element"] = new SelectList(from c in db.TP_Emballage
                                                        where c.Actif && !prod.Contains(c.ID_Emballage)
                                                        orderby c.Code
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Emballage,
                                                            Libelle = c.Code
                                                        }, "ID_Element", "Libelle");
            }
            
            Regulation_StockVM Return_Var = new Regulation_StockVM();
            Return_Var.Type_Element = (byte)id;
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Stock/Ajouter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

      
        [HttpPost, ActionName("Ajouter")]
        [ValidateAntiForgeryToken]
        public JsonResult Ajouter(Regulation_StockVM Param_Var)
        {
            Ouverture_Cloture_CaisseBM caisse = new Ouverture_Cloture_CaisseBM();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            if (!caisse.IsCaisseOpen(ID_Caisse))
            {
                TempData["messagewarning"] = "Veuillez ouvrir une caisse";
                string[] tab = new string[2];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    Regulation_StockBM ObjetModel = new Regulation_StockBM();

                    Int64 id = ObjetModel.Save(Param_Var, Session["Code_User"].ToString());
                    TempData["messagesucess"] = "Ajout effectué avec succès";
                    string[] tab = new string[2];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = id.ToString();
                    return this.Json(tab, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Regulation_Stock/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        
        [ActionName("Modifier")]
        public JsonResult Modifier(string id)
        {
            int ID_ = int.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            //id = 0 => Régulation Liquide
            //id = 1 => Régulation Emballage           
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Modifier_regulations_stock_liquide", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Modifier_regulations_stock_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(ID_, "TP_Regulation_Stock", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            Regulation_StockBM ObjetBM = new Regulation_StockBM();
            Regulation_StockVM LocalVM = ObjetBM.GetByID(ID_);
            if (LocalVM == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            
            StoreEntities db = new StoreEntities();
            if (LocalVM.Type_Element == 0)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Produit
                                                        where c.Actif && c.Produit_Decomposer == false
                                                        orderby c.Libelle
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Produit,
                                                            Libelle = c.Code + " : " + c.Libelle
                                                        }, "ID_Element", "Libelle");
            }
            else
            {
                var prod = db.TP_Produit.Where(p => p.Valeur_Emballage == false || p.Produit_Decomposer == true).Select(p => p.ID_Emballage).ToList();
                TempData["ID_Element"] = new SelectList(from c in db.TP_Emballage
                                                        where c.Actif && !prod.Contains(c.ID_Emballage)
                                                        orderby c.Code
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Emballage,
                                                            Libelle = c.Code
                                                        }, "ID_Element", "Libelle");
            }


            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Stock/Modifier.cshtml", LocalVM);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(string id, Regulation_StockVM Param_Var)
        {
            int ID_ = int.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    Regulation_StockBM ObjetBM = new Regulation_StockBM();


                    ObjetBM.Edit(ID_, Param_Var, Session["Code_User"].ToString());

                    // libère le verrou précédemment pauser
                    Locked_RecordsBM veroux = new Locked_RecordsBM();
                    veroux.Liberer(ID_, "TP_Regulation_Stock");

                    TempData["messagesucess"] = "Modification effectuée avec succès";
                    string[] tab = new string[2];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    TempData["messagewarning"] = "Le Modele n'est pas valide ";
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Regulation_Stock/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

      
        [ActionName("Valider")]
        public JsonResult Valider(string id)
        {
            Int64 ID_Regulation = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Valider_regulations_stock_liquide", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Valider_regulations_stock_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                Regulation_StockBM Local_Var = new Regulation_StockBM();
                Local_Var.Valider(ID_Regulation, Session["Code_User"].ToString());

                TempData["messagesucess"] = "Valider effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Regulation_Stock/Valider/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }


        }


        //
        // GET: /Ventes/Annuler/5
        [ActionName("Annuler")]
        public JsonResult Annuler(string id)
        {
            Int64 ID_Regulation = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Annuler_regulations_stock_liquide", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Annuler_regulations_stock_emballage", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                Regulation_StockBM Local_Var = new Regulation_StockBM();
                Local_Var.Annuler(ID_Regulation, Session["Code_User"].ToString());

                TempData["messagesucess"] = "Action effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Regulation_Stock/Annuler/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }


        }

        
        [ActionName("Consulter")]
        public JsonResult Consulter(Int64 id)
        {


            TempData["id"] = id;
            Regulation_StockBM Local_Var = new Regulation_StockBM();
            Regulation_StockVM Return_Var = Local_Var.GetByID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }

            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Stock/Consulter.cshtml", Return_Var);

            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

    }
}
