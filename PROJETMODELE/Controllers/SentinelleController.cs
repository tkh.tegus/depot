﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Models.BML;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;
using System.Text;

namespace PROJETMODELE.Controllers
{
     [Authorize]
    public class SentinelleController : Controller
    {

         StoreEntities db = new StoreEntities();
        public class JqwGrades
        {
            public Int64 id { get; set; }
            public DateTime date { get; set; }
            public string description { get; set; }
            public string action { get; set; }
            public bool available { get; set; }
        }

        public class Modules
        {
            public int id { get; set; }
            public string Libelle { get; set; }
        }
        public class LModules
        {
            public List<Modules> liste { get; set; }
        }
        Services services = new Services();
        public JsonResult ajax()
        {

            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            int ID_Personnel = int.Parse(Session["VarItems"].ToString().Split(';')[1]);
            int ID_Action = int.Parse(Session["VarItems"].ToString().Split(';')[0]);
            StoreEntities db = new StoreEntities();
            var Table = new List<JqwGrades>
            {
            };
            var resultat = from c in db.TG_Sentinelles
                           where 
                           (ID_Action == 6 ? true : c.Action == ID_Action ) &&
                           c.Code_User == ID_Personnel &&
                           (c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin)
                           orderby c.ID_Sentinelle descending
                           select new JqwGrades
                           {
                               id = c.ID_Sentinelle,
                               description = c.Description,
                               date = c.Date
                           };
            //foreach (TG_Sentinelles sentinelles in resultat)
            //{
            //    Table.Add(new JqwGrades() { 
            //        id = sentinelles.ID_Sentinelle.ToString(),
            //        description = sentinelles.Description,
            //        date = sentinelles.Date.ToString("dd/MM/yyyy HH:mm"), 
            //        action = sentinelles.Action == 0 ? "Ajout":(sentinelles.Action == 1? "Modification":"Suppression"), 
            //        available = false });
            //}
            return new JsonResult()
            {
                Data = resultat,
                MaxJsonLength = int.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult Index()
        {
            Session["DateDebut"] = DateTime.Today.AddDays(-7).ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
           
            LModules liste = new LModules();
            liste.liste = new List<Modules> { };
            liste.liste.Add(new Modules{ id = 0 ,Libelle = "Titre de propriété" });
            liste.liste.Add( new Modules { id = 1, Libelle = "Renouvellement" });
            liste.liste.Add(new Modules { id = 2, Libelle = "Inscription" });
            liste.liste.Add(new Modules { id = 3, Libelle = "Extension" });
            liste.liste.Add(new Modules { id = 4, Libelle = "Priorité" });
            liste.liste.Add(new Modules { id = 5, Libelle = "Compagnie" });
            liste.liste = (from p in liste.liste orderby p.Libelle select p).ToList();
            liste.liste.Insert(0,new Modules { id = 6, Libelle = "Tous les actions" });

            TempData["ID_Actions"] = new SelectList(liste.liste, "id", "Libelle");

            TempData["ID_Personnel"] = new SelectList(from c in db.Vue_Personnel
                                                     orderby c.Libelle
                                                     select new 
                                                     {
                                                         ID = c.ID_Personnel,
                                                         Libelle = c.Libelle
                                                     }, "ID", "Libelle");
            int ID_Personnel = ((SelectList)TempData["ID_Personnel"]).First() == null ? 0 : int.Parse(((SelectList)TempData["ID_Personnel"]).First().Value);
            Session["VarItems"] = string.Format("6;{0}", ID_Personnel);
            string[] tab = new string[3];
            tab[0] = services.GetHtmlByView(ControllerContext, TempData, "~/Views/Sentinelle/index.cshtml", filtre);
            tab[1] = "Gestion du journal";
            tab[2] = "<li><a href='#'>Accueil</a></li> <li><a href='#'>Administration</a></li> <li class='active'><a href='#'>Journal</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        //
        // GET: /Sentinelle/
        public ActionResult configure()
        {
          var sentinelle =   db.TG_Sentinelles_Options.Find(1);
          Sentinelle option = new Sentinelle();
          option.Sentinelles = sentinelle.Sentinelle;
          option.SentinelleConnexion = sentinelle.SentinelleConnexion;
          option.SentinelleDelete = sentinelle.SentinelleDelete;
          option.SentinelleEdit = sentinelle.SentinelleEdit;
          option.SentinelleEndConnexion = sentinelle.SentinelleEndConnexion;
          option.SentinelleInsert = sentinelle.SentinelleInsert;
          option.SentinellePrint = sentinelle.SentinellePrint;
          option.SentinelleLife = sentinelle.SentinelleLife.ToString();
          string[] tab = new string[1];
          tab[0] = services.GetHtmlByView(ControllerContext, TempData, "~/Views/Sentinelle/configure.cshtml", option);
          return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult configure(Sentinelle sentinelle)
        {
            int resul;
            TG_Sentinelles_Options option = db.TG_Sentinelles_Options.Find(1);
            if (int.TryParse(sentinelle.SentinelleLife,out resul))
            {
                option.SentinelleLife = resul;
            }
            option.Sentinelle = sentinelle.Sentinelles;
            option.SentinelleConnexion = sentinelle.SentinelleConnexion;
            option.SentinelleDelete = sentinelle.SentinelleDelete;
            option.SentinelleEdit = sentinelle.SentinelleEdit;
            option.SentinelleEndConnexion = sentinelle.SentinelleEndConnexion;
            option.SentinelleInsert = sentinelle.SentinelleInsert;
            option.SentinellePrint = sentinelle.SentinellePrint;
            db.SaveChanges();
            TempData["messagesucess"] = "La sentinelle a été configurée correctement";
            string[] tab = new string[2];//2 pour savoir que c'est ok
            tab[0] = services.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = "";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }


    }
}
