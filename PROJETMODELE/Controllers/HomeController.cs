﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Tools;
using System.Text;
using PROJETMODELE.Models.DAL;
using System.Data.Objects;
using PROJETMODELE.Models.VML.TP;

namespace PROJETMODELE.Controllers
{
     [Authorize]
    public class HomeController : Controller
    {
        StoreEntities db = new StoreEntities();
        Services service = new Services();
        public class Element
        {
            public int ID_Produit { get; set; }
            public int Casier { get; set; }
            public int Bouteille { get; set; }
            public int Nombre_Bouteille_Par_Casier { get; set; }
            public bool Valeur_Emballage { get; set; }
        }
        public ActionResult Index()
        {
            /*
            AccueilVM Return_var = new AccueilVM();
            int casier_12 = 0;
            int casier_24 = 0;
            int casier_15 = 0;
            int bouteille_12 = 0;
            int bouteille_24 = 0;
            int bouteille_15 = 0;
            int Nombre_Palette = 0;
            
            var produit_vente = (from c in db.TP_Vente_Liquide
                         let vente = db.TP_Vente.Where(p=>p.ID_Vente == c.ID_Vente &&  p.Date.Day == DateTime.Now.Day && p.Date.Month == DateTime.Now.Month && p.Date.Year == DateTime.Now.Year && p.Etat == 1).FirstOrDefault()
                         where vente != null   
                         select new Element{
                                ID_Produit = c.ID_Produit,
                                Casier = c.Quantite_Casier_Liquide,
                                Bouteille = c.Quantite_Bouteille_Liquide,
                                Nombre_Bouteille_Par_Casier = c.Nombre_Bouteille_Par_Casier,
                                Valeur_Emballage = c.TP_Produit.Valeur_Emballage,
                            }).ToList();
              
            foreach (var produit in produit_vente)
            {
                if (produit.Valeur_Emballage)
                {
                    if (produit.Nombre_Bouteille_Par_Casier == 12)
                    {
                        casier_12 += produit.Casier;
                        bouteille_12 += produit.Bouteille;
                    }

                    if (produit.Nombre_Bouteille_Par_Casier == 15)
                    {
                        casier_15 += produit.Casier;
                        bouteille_15 += produit.Bouteille;
                    }

                    if (produit.Nombre_Bouteille_Par_Casier == 24)
                    {
                        casier_24 += produit.Casier;
                        bouteille_24 += produit.Bouteille;
                    }
                }
                else
                {
                    Nombre_Palette += produit.Casier;
                }
               
            }

            casier_12 += bouteille_12 / 12;
            casier_15 += bouteille_15 / 15;
            casier_24 += bouteille_24 / 24;

            bouteille_12 = bouteille_12 % 12;
            bouteille_15 = bouteille_15 % 15;
            bouteille_24 = bouteille_24 % 24;

            double Encaissement = db.TP_Encaissement.Where(p => p.Date.Day == DateTime.Now.Day && p.Date.Month == DateTime.Now.Month && p.Date.Year == DateTime.Now.Year && p.Etat == 1).FirstOrDefault() == null ? 0 : db.TP_Encaissement.Where(p => p.Date.Day == DateTime.Now.Day && p.Date.Month == DateTime.Now.Month && p.Date.Year == DateTime.Now.Year && p.Etat == 1).Select(p => p.Montant).Sum();
            
            double Decaissement = db.TP_Decaissement.Where(p => p.Date.Day == DateTime.Now.Day && p.Date.Month == DateTime.Now.Month && p.Date.Year == DateTime.Now.Year && p.Etat == 1).FirstOrDefault() == null ? 0 : db.TP_Decaissement.Where(p => p.Date.Day == DateTime.Now.Day && p.Date.Month == DateTime.Now.Month && p.Date.Year == DateTime.Now.Year && p.Etat == 1).Select(p => p.Montant).Sum();
            var facture = (from c in db.FN_Vente()
                           orderby c.Etat, c.Date descending
                           where (c.Date.Day == DateTime.Now.Day && c.Date.Month == DateTime.Now.Month && c.Date.Year == DateTime.Now.Year && c.Etat == 1)
                           select new PROJETMODELE.Controllers.VenteController.JqwAjax
                           {
                               id = c.ID_Vente,                                                          
                               Montant_Verser = c.Versement,
                               Montant_Liquide = c.Montant_Liquide + c.Montant_Emballage - c.Montant_Retour_Emballage,                            
                           }).ToList();
            double Versement = facture.Select(p => p.Montant_Verser).Sum();
            double vente_ = facture.Select(p => p.Montant_Liquide).Sum();
            var solde = (from c in db.FN_Etat_Solde_Caisse(DateTime.Now, DateTime.Now)
                           select new 
                           {
                               Solde = c.Solde,
                           }).ToList();

            Return_var.Solde_Caisse = solde.Select(p => p.Solde).Sum();
            Return_var.Nombre_Casier_12 = casier_12;
            Return_var.Nombre_Casier_15 = casier_15;
            Return_var.Nombre_Casier_24 = casier_24;
            Return_var.Nombre_Bouteille_12 = bouteille_12;
            Return_var.Nombre_Bouteille_15 = bouteille_15;
            Return_var.Nombre_Bouteille_24 = bouteille_24;
            Return_var.Encaissement = Encaissement;
            Return_var.Decaissement = Decaissement;
            Return_var.Versement = Versement;
            Return_var.Vente = vente_;

            Return_var.Nombre_Palette = Nombre_Palette;
            return View(Return_var);
            */
            return View();
        }     
    }
}
