﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class FournisseurController : Controller
    {
        Services service = new Services();
        public class JqwAjax
        {
            public int id { get; set; }
            public string Nom { get; set; }
            public string Adresse { get; set; }
            public string Telephone { get; set; }
            public string Utilisateur { get; set; }  
            public string statut { get; set; }
        }

        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();
            var resultat = from c in db.TP_Fournisseur
                           orderby c.Actif descending, c.Nom
                           select new JqwAjax
                           {
                               id = c.ID_Fournisseur,
                               Nom = c.Nom,
                               Adresse = c.Adresse,
                               Telephone = c.Telephone,
                               statut = c.Actif == true ? "Actif" : "Suspendu",
                               Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == c.Create_Code_User).Select(p => p.Libelle).FirstOrDefault()
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Index()
        {          
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_fournisseur", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Fournisseur/Index.cshtml", null);
            tab[1] = "GESTION DES FOURNISSEURS";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des fournisseurs</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
       
        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {          
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Ajouter_Fournisseur", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Fournisseur/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

       
        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(FournisseurVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    FournisseurBM Local_Var = new FournisseurBM();
                    Local_Var.Save(Param_Var, Session["Code_User"].ToString());
                    TempData["messagesucess"] = "Ajout effectué avec succès";
                    string[] tab = new string[2];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string[] tab = new string[1];
                    TempData["messageerror"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Fournisseur/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

       
        [ActionName("Modifier")]
        public JsonResult Modifier(int id)
        {
           
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Modifier_Fournisseur", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(id, "TP_Fournisseur", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            FournisseurBM Local_Var = new FournisseurBM();
            FournisseurVM Return_Var = Local_Var.GetByID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Fournisseur/Modifier.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(int id, FournisseurVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    // TODO: Add update logic here
                    FournisseurBM Return_Var = new FournisseurBM();
                    Return_Var.Edit(id, Param_Var, Session["Code_User"].ToString());
                    // libère le verrou précédemment pauser
                    Locked_RecordsBM veroux = new Locked_RecordsBM();
                    veroux.Liberer(id, "TP_Fournisseur");
                    TempData["messagesucess"] = "Modification effectuée avec succès";
                    string[] tab = new string[2];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["messagewarning"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Fournisseur/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }
     
        [ActionName("Activer_Suspendre")]
        public JsonResult Activer_Suspendre(string id)
        {            
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Activer_suspendre_Fournisseur", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                string[] tab = id.Split(';');
                for (int i = 0; i < tab.Length; i++)
                {
                    FournisseurBM Local_Var = new FournisseurBM();
                    Local_Var.Activer_Suspendre(int.Parse(tab[i]), Session["Code_User"].ToString());
                }
                TempData["messagesucess"] = "Action effectuée avec succès";
                string[] tab1 = new string[1];
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Famille/Activer_Suspendre/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }                
        }
    }
}
