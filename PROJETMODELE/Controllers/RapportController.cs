﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PROJETMODELE.Models.VML.TG;

namespace PROJETMODELE.Controllers
{
    public class RapportController : Controller
    {
        Services service = new Services();
        public class Element
        {
            public int ID_Element { get; set; }
            public string Libelle { get; set; }
        }

        public class JqwAjaxEtatSolde
        {
            public int id { get; set; }
            public string nom { get; set; }
            public string adresse { get; set; }
            public string telephone { get; set; }
            public double? solde { get; set; }
            public double part_marche_ { get; set; }
            public double part_marche
            {
                get
                {
                    return Math.Round(part_marche_, 2);
                }
                set { }
            }
            public double livraison { get; set; }
            public double reglement { get; set; }

            // Evolution 
            public DateTime date { get; set; }
            public double? entree { get; set; }
            public double? sortie { get; set; }
            public double valeur_boutique { get; set; }
        }

        #region Part de marché

        public JsonResult ajaxStat_Client_Fournisseur()
        {
            StoreEntities db = new StoreEntities();
            int ID_Element = int.Parse(Session["ID_Element"].ToString());         
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = (from c in db.FN_Stat_Client_Fournisseur(filtre.DateDebut, filtre.CDateFin, ID_Element)
                           orderby c.Part_Marche descending
                           select new JqwAjaxEtatSolde
                           {
                               id = c.ID_Element,
                               nom = c.Nom,
                               adresse = c.Adresse,
                               telephone = c.Telephone,
                               livraison = c.Livraison,
                               part_marche_ = c.Part_Marche,
                               reglement = c.Reglement,
                               solde = c.Solde
                           });
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IndexStat_Client_Fournisseur()
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_Stat_Client_Fournisseur", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            DateTime date = new DateTime(DateTime.Now.Year, 01, 01);
            Session["DateDebut"] = date.ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            var liste = new List<Element>
            {
                new Element { ID_Element = 0, Libelle = "CLIENT" },
                new Element { ID_Element = 1, Libelle = "FOURNISSEUR" },
            };
            TempData["Type"] = new SelectList(liste, "ID_Element", "Libelle");
            Session["ID_Element"] = int.Parse(((SelectList)TempData["Type"]).FirstOrDefault().Value.ToString());

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Rapport/IndexStat_Client_Fournisseur.cshtml", filtre);
            tab[1] = "PART MARCHÉ CLIENT/FOURNISSEUR";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Etat</a></li> <li class='active'>Rapport Client et Fournisseur</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        #endregion     

        #region Chiffre Affaire
        public class JqwAjaxChiffreAffaire
        {
            public int id { get; set; }
            public string Element { get; set; }
            public int Quantite_Casier { get; set; }
            public int Quantite_Bouteille { get; set; }
            public double Montant_Achat { get; set; }
            public double Montant_Vente { get; set; }
            public int Conditionnement { get; set; }
            public double QteProduit
            {
                get
                {
                    return Quantite_Casier + (Quantite_Bouteille / Conditionnement);
                }
                set { }
            }

            public double Ecart
            {
                get
                {
                    return Montant_Vente - Montant_Achat;
                }
                set { }
            }
        }

        public JsonResult AjaxChiffreAffaire()
        {
            StoreEntities db = new StoreEntities();
            int ID_Element = int.Parse(Session["ID_Element"].ToString());
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            if(ID_Element == 0)//par client
            {
                var resultat = (from c in db.FN_Chiffre_Affaire_Client(filtre.DateDebut, filtre.DateFin)
                                group c by new { c.ID_Client, c.Client } into p
                                select new JqwAjaxChiffreAffaire
                                {
                                    id = p.Key.ID_Client,
                                    Element = p.Key.Client,
                                    Conditionnement = p.Sum(x => x.Conditionnement),
                                    Quantite_Casier = p.Sum(x => x.Quantite_Casier),
                                    Quantite_Bouteille = p.Sum(x => x.Quantite_Bouteille),
                                    Montant_Achat = (double)p.Sum(x => x.Montant_Achat),
                                    Montant_Vente = (double)p.Sum(x => x.Montant_Vente),
                                }).OrderBy(p=>p.Element);
                return this.Json(resultat, JsonRequestBehavior.AllowGet);
            }
            else if(ID_Element == 1)//par produit
            {
                var resultat = (from c in db.FN_Chiffre_Affaire_Produit(filtre.DateDebut, filtre.DateFin, 0)
                                group c by new { c.ID_Produit, c.Produit } into p
                                select new JqwAjaxChiffreAffaire
                                {
                                    id = p.Key.ID_Produit,
                                    Element = p.Key.Produit,
                                    Conditionnement = p.Sum(x => x.Conditionnement),
                                    Quantite_Casier = p.Sum(x => x.Quantite_Casier),
                                    Quantite_Bouteille = p.Sum(x => x.Quantite_Bouteille),
                                    Montant_Achat = (double)p.Sum(x => x.Montant_Achat),
                                    Montant_Vente = (double)p.Sum(x => x.Montant_Vente),
                                }).OrderBy(p => p.Element);
                return this.Json(resultat, JsonRequestBehavior.AllowGet);
            }
            else //par famille
            {
                var resultat = (from c in db.FN_Chiffre_Affaire_Famille(filtre.DateDebut, filtre.DateFin, 0)
                                group c by new { c.ID_Famille, c.Famille } into p
                                select new JqwAjaxChiffreAffaire
                                {
                                    id = p.Key.ID_Famille,
                                    Element = p.Key.Famille,
                                    Conditionnement = p.Sum(x => x.Conditionnement),
                                    Quantite_Casier = p.Sum(x => x.Quantite_Casier),
                                    Quantite_Bouteille = p.Sum(x => x.Quantite_Bouteille),
                                    Montant_Achat = (double)p.Sum(x => x.Montant_Achat),
                                    Montant_Vente = (double)p.Sum(x => x.Montant_Vente),
                                }).OrderBy(p => p.Element);
                return this.Json(resultat, JsonRequestBehavior.AllowGet);
            }         
        }

        public JsonResult IndexChiffreAffaire()
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Rapport_Chiffre_Affaire", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }

            DateTime date = new DateTime(DateTime.Now.Year, 01, 01);
            Session["DateDebut"] = date.ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            var liste = new List<Element>
            {
                new Element {ID_Element = 0, Libelle = "CLIENT" },
                new Element {ID_Element = 2, Libelle = "FAMILLE" },
                new Element {ID_Element = 1, Libelle = "PRODUIT" },
            };
            TempData["Type"] = new SelectList(liste, "ID_Element", "Libelle");
            Session["ID_Element"] = "2";
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Rapport/IndexChiffreAffaire.cshtml", filtre);
            tab[1] = "CHIFFRE D'AFFAIRE";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Etat</a></li> <li class='active'>Evolution Boutique</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Commandes

        public JsonResult AjaxChiffreCommandes()
        {
            StoreEntities db = new StoreEntities();
            int ID_Element = int.Parse(Session["ID_Element"].ToString());
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            if (ID_Element == 0)//par Fournisseur
            {
                var resultat = (from c in db.FN_Chiffre_Affaire_Fournisseur(filtre.DateDebut, filtre.DateFin)
                                group c by new { c.ID_Fournisseur, c.Fournisseur } into p
                                select new JqwAjaxChiffreAffaire
                                {
                                    id = p.Key.ID_Fournisseur,
                                    Element = p.Key.Fournisseur,
                                    Conditionnement = p.Sum(x => x.Conditionnement),
                                    Quantite_Casier = p.Sum(x => x.Quantite_Casier),
                                    Quantite_Bouteille = p.Sum(x => x.Quantite_Bouteille),
                                    Montant_Achat = (double)p.Sum(x => x.Montant)
                                }).OrderBy(p=>p.Element);
                return this.Json(resultat, JsonRequestBehavior.AllowGet);
            }
            else //par Famille
            {
                var resultat = (from c in db.FN_Chiffre_Affaire_Famille(filtre.DateDebut, filtre.DateFin, 1)
                                group c by new { c.ID_Famille, c.Famille } into p
                                select new JqwAjaxChiffreAffaire
                                {
                                    id = p.Key.ID_Famille,
                                    Element = p.Key.Famille,
                                    Conditionnement = p.Sum(x => x.Conditionnement),
                                    Quantite_Casier = p.Sum(x => x.Quantite_Casier),
                                    Quantite_Bouteille = p.Sum(x => x.Quantite_Bouteille),
                                    Montant_Achat = (double)p.Sum(x => x.Montant_Achat)
                                }).OrderBy(p=>p.Element);
                return this.Json(resultat, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult IndexChiffreCommandes()
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Rapport_Chiffre_Commande", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }

            DateTime date = new DateTime(DateTime.Now.Year, 01, 01);
            Session["DateDebut"] = date.ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            var liste = new List<Element>
            {
                new Element {ID_Element = 0, Libelle = "FOURNISSEUR" },
                new Element {ID_Element = 1, Libelle = "FAMILLE" },
            };
            TempData["Type"] = new SelectList(liste, "ID_Element", "Libelle");
            Session["ID_Element"] = "0";
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Rapport/IndexChiffreCommandes.cshtml", filtre);
            tab[1] = "CHIFFRE DES COMMANDES";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Etat</a></li> <li class='active'>Commandes Produit</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Evolution Capital
        public JsonResult ajaxEvolution_Boutique()
        {
            StoreEntities db = new StoreEntities();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.FN_Stat_EvolutionCapital()
                           orderby c.Date
                           where (c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin)
                           select new JqwAjaxEtatSolde
                           {
                               date = c.Date,
                               entree = c.Entree,
                               sortie = c.Sortie,
                               valeur_boutique = c.Montant_Porgressif
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IndexEvolution()
        {
           
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Visualiser_Evolution_Btq", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            DateTime date = new DateTime(DateTime.Now.Year, 01, 01);
            Session["DateDebut"] = date.ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Rapport/IndexEvolution.cshtml", filtre);
            tab[1] = "ÉVOLUTION BOUTIQUE";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Etat</a></li> <li class='active'>Evolution Boutique</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
