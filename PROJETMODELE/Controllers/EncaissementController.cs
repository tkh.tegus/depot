﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class EncaissementController : Controller
    {
        Services service = new Services();
        StoreEntities db = new StoreEntities();
        public class JqwAjax 
        {
            public Int64 id { get; set; }
            public DateTime Date { get; set; }
            public string Client { get; set; }
            public string Caisse { get; set; } 
            public double Montant { get; set; }
            public string NUM_DOC { get; set; } 
            public string Utilisateur { get; set; }
            public string Statut { get; set; }
            public string Motif { get; set; }
        }
     
        public JsonResult ajax()
        {
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.TP_Encaissement
                           orderby c.Etat, c.Date descending
                           where (c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin)
                           select new JqwAjax
                           {
                               id = c.ID_Encaissement,
                               Date = c.Date,
                               NUM_DOC = c.NUM_DOC,
                               Client = c.TP_Client.Nom,
                               Caisse = c.TP_Caisse.Libelle,
                               Montant = c.Montant,
                               Statut = (c.Etat == 0) ? "En Attente de validation" : (c.Etat == 1) ? "Valider" : "Annuler",
                               Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == c.Create_Code_User).Select(p => p.Libelle).FirstOrDefault(),
                               Motif = c.Motif,
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }
        
        [ActionName("Index")]
        public JsonResult Index()
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_reglements_clients", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            var ouverture = db.TP_Ouverture_Cloture_Caisse.Where(p => p.ID_Caisse == ID_Caisse && p.Etat == 0).FirstOrDefault();
            if (ouverture != null)
            {
                Session["DateDebut"] = ouverture.Date_Ouverture.ToString();
            }
            else
            {
                Session["DateDebut"] = DateTime.Today.ToString();
            }
            Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Encaissement/Index.cshtml", filtre);
            tab[1] = "GESTION DES RÈGLEMENTS CLIENTS";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des règlements clients</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
       
        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Ajouter_reglement_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();                      
            TempData["ID_Client"] = new SelectList(from c in db.TP_Client
                                                   orderby c.Nom
                                                   where  c.Actif
                                                    select new
                                                    {
                                                        ID_Client = c.ID_Client,
                                                        Nom = c.Nom,
                                                    }, "ID_Client", "Nom");                   
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Encaissement/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Ajouter")]
        [ValidateAntiForgeryToken]
        public JsonResult Ajouter(EncaissementVM Param_Var)
        {
            Ouverture_Cloture_CaisseBM caisse = new Ouverture_Cloture_CaisseBM();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            if (!caisse.IsCaisseOpen(ID_Caisse))
            {
                TempData["messagewarning"] = "Veuillez ouvrir une caisse";
                string[] tab = new string[2];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

            try
            {
                Param_Var.ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
                string messageRetour = string.Empty;
                EncaissementBM ObjetModel = new EncaissementBM();
                if (ModelState.IsValid)
                {
                    Int64 id = ObjetModel.Save(Param_Var, Session["Code_User"].ToString());
                    TempData["messagesucess"] = "Ajout effectué avec succès";
                    string[] tab = new string[2];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = id.ToString();
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["messagewarning"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    string[] tab = new string[1];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                } 
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Encaissement/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName("Modifier")]
        public JsonResult Modifier(int id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Modifier_reglement_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            TempData["id"] = id;
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(id, "TP_Encaissement", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            EncaissementBM ObjetBM = new EncaissementBM();
            EncaissementVM LocalVM = ObjetBM.GetByID(id);
            if (LocalVM == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            TempData["ID_Client"] = new SelectList(from c in db.TP_Client
                                                   orderby c.Nom
                                                   where c.Actif
                                                   select new
                                                   {
                                                       ID_Client = c.ID_Client,
                                                       Nom = c.Nom,
                                                   }, "ID_Client", "Nom");
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Encaissement/Modifier.cshtml", LocalVM);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
       
        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(Int64 id, EncaissementVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    EncaissementBM ObjetBM = new EncaissementBM();
                    ObjetBM.Edit(id, Param_Var, Session["Code_User"].ToString());
                    // libère le verrou précédemment pauser
                    Locked_RecordsBM veroux = new Locked_RecordsBM();
                    veroux.Liberer(id, "TP_Encaissement");
                    TempData["messagesucess"] = "Modification effectuée avec succès";
                    string[] tab = new string[2];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["messagewarning"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Encaissement/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }
       
        [ActionName("Valider")]
        public JsonResult Valider(Int64 id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Valider_reglement_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                EncaissementBM Local_Var = new EncaissementBM();
                Local_Var.Valider(id, int.Parse(Session["Code_User"].ToString()));
                TempData["messagesucess"] = "Valider effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Encaissement/Valider/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }        
        }


        [ActionName("Annuler")]
        public JsonResult Annuler(Int64 id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Annuler_reglement_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                EncaissementBM Local_Var = new EncaissementBM();
                Local_Var.Annuler(id, int.Parse(Session["Code_User"].ToString()));
                TempData["messagesucess"] = "Action effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Encaissement/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName("Consulter")]
        public JsonResult Consulter(Int64 id)
        {
            TempData["id"] = id;
            EncaissementBM Local_Var = new EncaissementBM();
            EncaissementVM Return_Var = Local_Var.GetByID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Encaissement/Consulter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
    }
}
