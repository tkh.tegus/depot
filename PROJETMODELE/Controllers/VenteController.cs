﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class VenteController : Controller
    {
        Services service = new Services();
        public class Element
        {
            public int ID_Element { get; set; }
            public string Libelle { get; set; }
        }
        public class JqwAjax
        {
            public Int64 id { get; set; }
            public int ID_Client { get; set; }
            public string Client { get; set; }
            public string Nom_Client { get; set; }
            public DateTime Date { get; set; }
            public double Montant_Liquide { get; set; }
            public double Montant_Emballage { get; set; }
            public double Montant_Retour_Emballage { get; set; }
            public double Montant_Verser { get; set; }
            public double Montant { get; set; }
            public string NUM_DOC { get; set; }
            public string Statut { get; set; }
            public string Utilisateur { get; set; }
        }
        public class JqwxLiquide
        {
            public Int64 id { get; set; }
            public int ID_Client { get; set; }
            public int ID_Produit { get; set; }
            public string Produit { get; set; }            
            public bool Valeur_Emballage { get; set; }
            public int Quantite_Casier_Liquide { get; set; }
            public int Quantite_Bouteille_Liquide { get; set; }
            public int Quantite_Casier_Emballage { get; set; }
            public int Quantite_Plastique_Emballage { get; set; }
            public int Quantite_Bouteille_Emballage { get; set; }
            public double Prix_Plastique { get; set; }
            public double Prix_Bouteille { get; set; }
            public int Conditionnement { get; set; }
            public int Prix_Liquide_Vente_ { get; set; }
            public double Prix_Liquide_Vente {
                get
                {
                    double pu = Prix_Liquide_Vente_;
                    if (Cumuler_Ristourne == false)
                    {
                        pu = pu - Ristourne;
                    }
                    return pu;
                }
                set { }
            }
            public double Prix_Emballage { get; set; }
            public double Ristourne { get; set; }
            public bool Cumuler_Ristourne { get; set; }
            public double Montant_Liquide
            {
                get
                {
                    StoreEntities db = new StoreEntities();
                    double mt = 0;
                    double pu = Prix_Liquide_Vente_;
                    if (Cumuler_Ristourne == false)
                    {
                        pu = pu - Ristourne;
                    }
                    mt = Quantite_Casier_Liquide * pu + Math.Round((Quantite_Bouteille_Liquide * (pu / Conditionnement)),0);
                    return mt;
                
                }
                set { }
            }
            public double Montant_Emballage
            {
                get
                {
                    
                    return Quantite_Casier_Emballage * (Prix_Plastique + Prix_Bouteille * Conditionnement) + (Quantite_Plastique_Emballage * Prix_Plastique) + (Quantite_Bouteille_Emballage * Prix_Bouteille);
                   
                    
                }
                set { }
            }
            public double Total
            {
                get
                {
                    return Montant_Liquide + Montant_Emballage;
                }
                set { }
            }
            public string Str_Quantite
            {
                get
                {
                    return Quantite_Casier_Liquide + "." + Quantite_Bouteille_Liquide;
                }
                set { }
            }
        }

        public class JqwxEmballage
        {
            public Int64 id { get; set; }
            public int ID_Emballage { get; set; }
            public string Emballage { get; set; }
            public int Quantite_Casier { get; set; }
            public int Quantite_Plastique { get; set; }
            public int Quantite_Bouteille { get; set; }
            public double Prix_Plastique { get; set; }
            public double Prix_Bouteille { get; set; }
            public int Nombre_Bouteille_Par_Casier { get; set; }
            public double Montant
            {
                get
                {
                    return Quantite_Casier * (Prix_Plastique + (Prix_Bouteille * Nombre_Bouteille_Par_Casier)) + (Prix_Plastique * Quantite_Plastique) + (Prix_Bouteille * Quantite_Bouteille);
                }
                set { }
            }
        }

        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.FN_Vente()
                           orderby c.Etat, c.Date descending
                           where (c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin)
                           select new JqwAjax
                           {
                               id = c.ID_Vente,
                               Date = c.Date,
                               NUM_DOC = c.NUM_DOC,
                               Client = c.Client,
                               Montant_Liquide = c.Montant_Liquide,
                               Montant_Emballage = c.Montant_Emballage,
                               Montant_Retour_Emballage = c.Montant_Retour_Emballage,
                               Montant_Verser = c.Montant_Verser
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxLiquide()
        {
            StoreEntities db = new StoreEntities();
            int Code_User = int.Parse(Session["Code_User"].ToString());        
            var resultat = from c in db.TP_Vente_Liquide_Temp
                           orderby c.TP_Produit.Libelle
                           where c.Create_Code_User == Code_User
                           select new JqwxLiquide
                           {
                               id = c.ID_Vente_Liquide_Temp,
                               ID_Produit = c.ID_Produit,
                               Produit = c.TP_Produit.Libelle,
                               Quantite_Casier_Liquide = c.Quantite_Casier_Liquide,
                               Quantite_Bouteille_Liquide = c.Quantite_Bouteille_Liquide,
                               Quantite_Casier_Emballage = c.Quantite_Casier_Emballage,
                               Quantite_Plastique_Emballage = c.Quantite_Plastique_Emballage,
                               Quantite_Bouteille_Emballage = c.Quantite_Bouteille_Emballage,
                               Prix_Bouteille = (double)c.PU_Bouteille,
                               Prix_Plastique = (double)c.PU_Plastique,
                               Prix_Liquide_Vente_ = (int)c.PU_Vente,
                               Cumuler_Ristourne = (bool)c.Cumuler_Ristourne,
                               Ristourne = (double)c.Ristourne_Client,
                               Conditionnement = (int)c.Conditionnement,
                              
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxCmdLiquideDetail(Int64 id)
        {
            StoreEntities db = new StoreEntities();
            int Code_User = int.Parse(Session["Code_User"].ToString());
            var resultat = from c in db.TP_Vente_Liquide
                           orderby c.TP_Produit.Libelle
                           where c.ID_Vente == id
                           select new JqwxLiquide
                           {
                               id = c.ID_Vente_Liquide,
                               ID_Produit = c.ID_Produit,
                               Produit = c.TP_Produit.Libelle,
                               Quantite_Casier_Liquide = c.Quantite_Casier_Liquide,
                               Quantite_Bouteille_Liquide = c.Quantite_Bouteille_Liquide,
                               Quantite_Casier_Emballage = c.Quantite_Casier_Emballage,
                               Quantite_Plastique_Emballage = c.Quantite_Plastique_Emballage,
                               Quantite_Bouteille_Emballage = c.Quantite_Bouteille_Emballage,
                               Prix_Plastique = (int)c.PU_Plastique,
                               Prix_Bouteille = (int)c.PU_Bouteille,
                               Prix_Liquide_Vente_ = (int)c.PU_Vente,
                               Cumuler_Ristourne = (bool)c.Cumuler_Ristourne,
                               Ristourne = (double)c.Ristourne_Client,
                               Conditionnement = (int)c.Conditionnement,
                               Valeur_Emballage = c.TP_Produit.Valeur_Emballage,
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxEmballage()
        {
            StoreEntities db = new StoreEntities();
            int Code_User = int.Parse(Session["Code_User"].ToString());
            var resultat = from c in db.TP_Vente_Emballage_Temp
                           where c.Create_Code_User == Code_User
                           select new JqwxEmballage
                           {
                               id = c.ID_Vente_Emballage_Temp,
                               ID_Emballage = c.ID_Emballage,
                               Emballage = c.TP_Emballage.Code,
                               Quantite_Casier = c.Quantite_Casier,
                               Quantite_Plastique = c.Quantite_Plastique,
                               Quantite_Bouteille = c.Quantite_Bouteille,
                               Prix_Plastique = c.PU_Plastique,
                               Prix_Bouteille = c.PU_Bouteille,
                               Nombre_Bouteille_Par_Casier = c.Conditionnement
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxCmdEmballageDetail(Int64 id)
        {
            StoreEntities db = new StoreEntities();
            int Code_User = int.Parse(Session["Code_User"].ToString());
            var resultat = from c in db.TP_Vente_Emballage
                           where c.ID_Vente == id
                           select new JqwxEmballage
                           {
                               id = c.ID_Vente_Emballage,
                               ID_Emballage = c.ID_Emballage,
                               Emballage = c.TP_Emballage.Code,
                               Quantite_Casier = c.Quantite_Casier,
                               Quantite_Plastique = c.Quantite_Plastique,
                               Quantite_Bouteille = c.Quantite_Bouteille,
                               Prix_Plastique = c.PU_Plastique,
                               Prix_Bouteille = c.PU_Bouteille,
                               Nombre_Bouteille_Par_Casier = c.Conditionnement
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Index()
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_ventes", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            var ouverture = db.TP_Ouverture_Cloture_Caisse.Where(p => p.ID_Caisse == ID_Caisse && p.Etat == 0).FirstOrDefault();
            if(ouverture != null)
            {
                Session["DateDebut"] = ouverture.Date_Ouverture.ToString();
            }
            else
            {
                Session["DateDebut"] = DateTime.Today.ToString();
            }
            
            Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Vente/Index.cshtml", filtre);
            tab[1] = "GESTION DES FACTURATIONS";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Opération</a></li> <li class='active'>Liste des factures</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }


        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Ajouter_Vente", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();

            TempData["ID_Client"] = new SelectList(from c in db.TP_Client
                                                   where c.Actif
                                                   orderby c.Nom
                                                   select new
                                                   {
                                                       ID_Client = c.ID_Client,
                                                       Libelle = c.Nom
                                                   }, "ID_Client", "Libelle");

            VenteVM Return_Var = new VenteVM();
            Return_Var.ID_Client = 1;
            int ID_Client = 1; // (((TempData["ID_Client"] as SelectList).Count() == 0) ? 0 : (int.Parse((TempData["ID_Client"] as SelectList).FirstOrDefault().Value)));
            Session["ID_Element"] = ID_Client.ToString();
            TempData["ID_Produit"] = new SelectList(from c in db.TP_Produit
                                                    where c.Actif
                                                    orderby c.Libelle
                                                    select new
                                                    {
                                                        ID_Produit = c.ID_Produit,
                                                        Libelle = c.Code +" - "+ c.Libelle
                                                    }, "ID_Produit", "Libelle");
            TempData["Solde_Liquide"] =  db.FN_Etat_Solde_Client(0, DateTime.Now, DateTime.Now).FirstOrDefault(p => p.ID_Client == ID_Client).Solde;
            TempData["Solde_Emballage"] =  db.FN_Etat_Solde_Client(1, DateTime.Now, DateTime.Now).FirstOrDefault(p => p.ID_Client == ID_Client).Solde;
            db.Vider_Vente_Liquide_Temp(int.Parse(Session["Code_User"].ToString()));
            db.Vider_Vente_Emballage_Temp(int.Parse(Session["Code_User"].ToString()));
            
            
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Vente/Ajouter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(VenteVM Param_Var)
        {
            Ouverture_Cloture_CaisseBM caisse = new Ouverture_Cloture_CaisseBM();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            if (!caisse.IsCaisseOpen(ID_Caisse))
            {
                TempData["messagewarning"] = "Veuillez ouvrir une caisse";
                string[] tab = new string[2];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

            StoreEntities db = new StoreEntities();
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    int code_user = int.Parse(Session["Code_User"].ToString());
                    // verifier si la commande contient des produits
                    if (db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == code_user).FirstOrDefault() != null)
                    {
                        VenteBM Local_Var = new VenteBM();
                        Param_Var.ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
                        Int64 ID_Vente = Local_Var.Save(Param_Var, code_user);
                        TempData["messagesucess"] = "Facturation effectué avec succès";
                        string[] tab = new string[5];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = ID_Vente.ToString();
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messageerror"] = "La facture ne contient aucun produit";
                        string[] tab = new string[1];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    TempData["messageerror"] = "Le modèle n'est pas valide";
                    string[] tab = new string[1];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Vente/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("Add_Liquide")]
        [ValidateAntiForgeryToken]
        public JsonResult Add_Liquide(VenteVM Param_Var)
        {
            StoreEntities db = new StoreEntities();
            try
            {
                ModelState.Remove("Versement_Liquide");
                ModelState.Remove("Consigne_Emballage");
                string message = string.Empty;
                if (ModelState.IsValid)
                {
                    ProduitBM produit = new ProduitBM();
                    var variable = db.TG_Variable.FirstOrDefault().Verifier_Etat_Stock;
                    if (variable == true && !produit.VerifieEtatStock(Param_Var.ID_Produit, (Param_Var.Quantite_Casier_Liquide == null ? 0 : Param_Var.Quantite_Casier_Liquide.Value), (Param_Var.Quantite_Bouteille_Liquide == null ? 0 : Param_Var.Quantite_Bouteille_Liquide.Value), ref message))
                    {
                        TempData["messagewarning"] = message;
                        string[] tab = new string[1];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }

                    if (Param_Var.Quantite_Casier_Liquide > 0 || Param_Var.Quantite_Bouteille_Liquide > 0)
                    {
                        VenteBM Local_Var = new VenteBM();
                        Local_Var.Save_Detail_Liquide(Param_Var, int.Parse(Session["Code_User"].ToString()));
                        var Facture = db.FN_Montant_Facture_Temp(int.Parse(Session["Code_User"].ToString())).FirstOrDefault();
                        string[] tab = new string[5];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Liquide);
                        tab[2] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Emballage);
                        tab[3] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Retour_Emballage);
                        tab[4] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagesucess"] = "Veuillez renseigner les quantités liquide";
                        string[] tab = new string[1];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    TempData["messageerror"] = "Le modèle n'est pas valide";
                    string[] tab = new string[1];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
               
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Vente/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("Add_Emballage")]
        [ValidateAntiForgeryToken]
        public JsonResult Add_Emballage(VenteVM Param_Var)
        {
            try
            {
                StoreEntities db = new StoreEntities();
                int Code_User = int.Parse(Session["Code_User"].ToString());
                string messageRetour = string.Empty;
                VenteBM Local_Var = new VenteBM();
                if (Param_Var.Casier_12 > 0 || Param_Var.Plastique_12 > 0 || Param_Var.Bouteille_12 > 0)
                {
                    Param_Var.ID_Emballage = 1;
                    Param_Var.Quantite_Casier_Emballage_Retour = (Param_Var.Casier_12 == null ? 0 : Param_Var.Casier_12.Value);
                    Param_Var.Quantite_Plastique_Emballage_Retour = (Param_Var.Plastique_12 == null ? 0 : Param_Var.Plastique_12.Value);
                    Param_Var.Quantite_Bouteille_Emballage_Retour = (Param_Var.Bouteille_12 == null ? 0 : Param_Var.Bouteille_12.Value);
                    Local_Var.Save_Detail_Emballage(Param_Var, int.Parse(Session["Code_User"].ToString()));
                }
                else
                {
                    Param_Var.Quantite_Casier_Emballage_Retour = 0;
                    Param_Var.Quantite_Plastique_Emballage_Retour = 0;
                    Param_Var.Quantite_Bouteille_Emballage_Retour = 0;
                }

                if (Param_Var.Casier_15 > 0 || Param_Var.Plastique_15 > 0 || Param_Var.Bouteille_15 > 0)
                {
                    Param_Var.ID_Emballage = 2;
                    Param_Var.Quantite_Casier_Emballage_Retour = (Param_Var.Casier_15 == null ? 0 : Param_Var.Casier_15.Value);
                    Param_Var.Quantite_Plastique_Emballage_Retour = (Param_Var.Plastique_15 == null ? 0 : Param_Var.Plastique_15.Value);
                    Param_Var.Quantite_Bouteille_Emballage_Retour = (Param_Var.Bouteille_15 == null ? 0 : Param_Var.Bouteille_15.Value);
                    Local_Var.Save_Detail_Emballage(Param_Var, int.Parse(Session["Code_User"].ToString()));
                }
                else
                {
                    Param_Var.Quantite_Casier_Emballage_Retour = 0;
                    Param_Var.Quantite_Plastique_Emballage_Retour = 0;
                    Param_Var.Quantite_Bouteille_Emballage_Retour = 0;
                }

                if (Param_Var.Casier_24 > 0 || Param_Var.Plastique_24 > 0 || Param_Var.Bouteille_24 > 0)
                {
                    Param_Var.ID_Emballage = 3;
                    Param_Var.Quantite_Casier_Emballage_Retour = (Param_Var.Casier_24 == null ? 0 : Param_Var.Casier_24.Value);
                    Param_Var.Quantite_Plastique_Emballage_Retour = (Param_Var.Plastique_24 == null ? 0 : Param_Var.Plastique_24.Value);
                    Param_Var.Quantite_Bouteille_Emballage_Retour = (Param_Var.Bouteille_24 == null ? 0 : Param_Var.Bouteille_24.Value);
                    Local_Var.Save_Detail_Emballage(Param_Var, int.Parse(Session["Code_User"].ToString()));
                }
                else
                {
                    Param_Var.Quantite_Casier_Emballage_Retour = 0;
                    Param_Var.Quantite_Plastique_Emballage_Retour = 0;
                    Param_Var.Quantite_Bouteille_Emballage_Retour = 0;
                }

                string[] tab = new string[5];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                var Facture = db.FN_Montant_Facture_Temp(int.Parse(Session["Code_User"].ToString())).FirstOrDefault();
                tab[1] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Liquide);
                tab[2] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Emballage);
                tab[3] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Retour_Emballage);
                tab[4] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Vente/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Supprimer_Detail_Liquide")]
        public JsonResult Supprimer_Detail_Liquide(string id)
        {
            StoreEntities db = new StoreEntities();
            string[] ID = id.Split(';');
            for (int i = 0; i < ID.Length; i++)
            {
                VenteBM Local_Var = new VenteBM();
                Local_Var.Supprimer_Detail_Liquide(int.Parse(ID[i]));
            }
            int Code_User = int.Parse(Session["Code_User"].ToString());
            string[] tab = new string[5];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            var Facture = db.FN_Montant_Facture_Temp(int.Parse(Session["Code_User"].ToString())).FirstOrDefault();
            tab[1] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Supprimer_Detail_Emballage")]
        public JsonResult Supprimer_Detail_Emballage(string id)
        {
            StoreEntities db = new StoreEntities();
            string[] ID = id.Split(';');
            for (int i = 0; i < ID.Length; i++)
            {
                VenteBM Local_Var = new VenteBM();
                Local_Var.Supprimer_Detail_Emballage(int.Parse(ID[i]));
            }
            int Code_User = int.Parse(Session["Code_User"].ToString());
            string[] tab = new string[5];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            var Facture = db.FN_Montant_Facture_Temp(int.Parse(Session["Code_User"].ToString())).FirstOrDefault();
            tab[1] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }


        [ActionName("Annuler_Vente")]
        public JsonResult Annuler_Vente(VenteVM Param_Var)
        {
            StoreEntities db = new StoreEntities();
            db.Vider_Vente_Liquide_Temp(int.Parse(Session["Code_User"].ToString()));
            db.Vider_Vente_Emballage_Temp(int.Parse(Session["Code_User"].ToString()));
            string[] tab = new string[5];
            TempData["messagesucess"] = "Facturation annuler avec succès";
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Modifier")]
        public JsonResult Modifier(Int64 id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Modifier_Vente", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            TempData["id"] = id;
            VenteBM Local_Var = new VenteBM();
            VenteVM Return_Var = Local_Var.GetByCmdID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            String MessageRetour = String.Empty;
            StoreEntities db = new StoreEntities();
            TempData["ID_Client"] = new SelectList(from c in db.TP_Client
                                                   where c.ID_Client == Return_Var.ID_Client
                                                   select new
                                                   {
                                                       ID_Client = c.ID_Client,
                                                       Libelle = c.Nom
                                                   }, "ID_Client", "Libelle");

            TempData["ID_Produit"] = new SelectList(from c in db.TP_Produit
                                                    orderby c.Libelle
                                                    where c.Actif
                                                    select new
                                                    {
                                                        ID_Produit = c.ID_Produit,
                                                        Libelle = c.Code + " - "+ c.Libelle
                                                    }, "ID_Produit", "Libelle");
            var Facture = db.FN_Montant_Facture(id).FirstOrDefault();
            TempData["Montant_Liquide"] = service.SeparateurMillier((Facture == null ? 0 : Facture.Montant_Liquide));
            TempData["Montant_Emballage"] = service.SeparateurMillier((Facture == null ? 0 : Facture.Montant_Emballage));
            TempData["Montant_Retour_Emballage"] = service.SeparateurMillier((Facture == null ? 0 : Facture.Montant_Retour_Emballage));
            TempData["Montant"] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Vente/Modifier.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(Int64 id, VenteVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    VenteBM ObjetBM = new VenteBM();
                    ObjetBM.Edit(id, Param_Var, int.Parse(Session["Code_User"].ToString()));
                    // libère le verrou précédemment pauser
                    Locked_RecordsBM veroux = new Locked_RecordsBM();
                    veroux.Liberer(id, "TP_Vente");
                    TempData["messagesucess"] = "Modification effectuée avec succès";
                    string[] tab = new string[2];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["messagewarning"] = "Le Modele n'est pas valide ";
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Vente/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("Edit_Detail_Liquide")]
        [ValidateAntiForgeryToken]
        public JsonResult Edit_Detail_Liquide(Int64 id, VenteVM Param_Var)
        {
            try
            {
                ModelState.Remove("Versement_Liquide");
                ModelState.Remove("Consigne_Emballage");
                if (ModelState.IsValid)
                {
                    StoreEntities db = new StoreEntities();
                    VenteBM Local_Var = new VenteBM();
                    if (Param_Var.Quantite_Casier_Liquide > 0 || Param_Var.Quantite_Bouteille_Liquide > 0)
                    {
                        string messageRetour = string.Empty;
                        Local_Var.Edit_Detail_Liquide(id, Param_Var, Session["Code_User"].ToString());
                        var Facture = db.FN_Montant_Facture(id).FirstOrDefault();
                        string[] tab = new string[5];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Liquide);
                        tab[2] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Emballage);
                        tab[3] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Retour_Emballage);
                        tab[4] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = "Veuillez renseigner les quantités liquide";
                        string[] tab = new string[1];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messagewarning"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Vente/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Supprimer_Detail_Edit_Liquide")]
        public JsonResult Supprimer_Detail_Edit_Liquide(string id)
        {
       
            string[] ID = id.Split(';');
            Int64 ID_Vente = 0;
            StoreEntities db = new StoreEntities();
            VenteBM Local_Var = new VenteBM();
            int ID_Mat_Com = int.Parse(ID[0]);
            for (int i = 0; i < ID.Length; i++)
            {
                ID_Vente = Local_Var.Supprimer_Detail_Edit_Liquide(Int64.Parse(ID[i]));
            }
           
            string[] tab = new string[5];
            var Facture = db.FN_Montant_Facture(ID_Vente).FirstOrDefault();
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Edit_Detail_Emballage")]
        [ValidateAntiForgeryToken]
        public JsonResult Edit_Detail_Emballage(Int64 id, VenteVM Param_Var)
        {
            try
            {
                ModelState.Remove("Versement_Liquide");
                ModelState.Remove("Consigne_Emballage");
                if (ModelState.IsValid)
                {
                    VenteBM Local_Var = new VenteBM();
                    StoreEntities db = new StoreEntities();
                    if (Param_Var.Casier_12 > 0 || Param_Var.Plastique_12 > 0 || Param_Var.Bouteille_12 > 0)
                    {
                        Param_Var.ID_Emballage = 1;
                        Param_Var.Quantite_Casier_Emballage_Retour = (Param_Var.Casier_12 == null ? 0 : Param_Var.Casier_12.Value);
                        Param_Var.Quantite_Plastique_Emballage_Retour = (Param_Var.Casier_12 == null ? 0 : Param_Var.Casier_12.Value);
                        Param_Var.Quantite_Bouteille_Emballage_Retour = (Param_Var.Casier_12 == null ? 0 : Param_Var.Casier_12.Value);
                        Local_Var.Edit_Detail_Emballage(id, Param_Var, Session["Code_User"].ToString());
                    }
                    else
                    {
                        Param_Var.Quantite_Casier_Emballage_Retour = 0;
                        Param_Var.Quantite_Plastique_Emballage_Retour = 0;
                        Param_Var.Quantite_Bouteille_Emballage_Retour = 0;
                    }

                    if (Param_Var.Casier_15 > 0 || Param_Var.Plastique_15 > 0 || Param_Var.Bouteille_15 > 0)
                    {
                        Param_Var.ID_Emballage = 5;
                        Param_Var.Quantite_Casier_Emballage_Retour = (Param_Var.Casier_15 == null ? 0 : Param_Var.Casier_15.Value);
                        Param_Var.Quantite_Plastique_Emballage_Retour = (Param_Var.Plastique_15 == null ? 0 : Param_Var.Plastique_15.Value);
                        Param_Var.Quantite_Bouteille_Emballage_Retour = (Param_Var.Bouteille_15 == null ? 0 : Param_Var.Bouteille_15.Value);
                        Local_Var.Edit_Detail_Emballage(id, Param_Var, Session["Code_User"].ToString());
                    }
                    else
                    {
                        Param_Var.Quantite_Casier_Emballage_Retour = 0;
                        Param_Var.Quantite_Plastique_Emballage_Retour = 0;
                        Param_Var.Quantite_Bouteille_Emballage_Retour = 0;
                    }

                    if (Param_Var.Casier_24 > 0 || Param_Var.Plastique_24 > 0 || Param_Var.Bouteille_24 > 0)
                    {
                        Param_Var.ID_Emballage = 2;
                        Param_Var.Quantite_Casier_Emballage_Retour = (Param_Var.Casier_24 == null ? 0 : Param_Var.Casier_24.Value);
                        Param_Var.Quantite_Plastique_Emballage_Retour = (Param_Var.Plastique_24 == null ? 0 : Param_Var.Plastique_24.Value);
                        Param_Var.Quantite_Bouteille_Emballage_Retour = (Param_Var.Bouteille_24 == null ? 0 : Param_Var.Bouteille_24.Value);
                        Local_Var.Edit_Detail_Emballage(id, Param_Var, Session["Code_User"].ToString());
                    }
                    else
                    {
                        Param_Var.Quantite_Casier_Emballage_Retour = 0;
                        Param_Var.Quantite_Plastique_Emballage_Retour = 0;
                        Param_Var.Quantite_Bouteille_Emballage_Retour = 0;
                    }

                    TempData["messagesucess"] = "Emballage modifier avec succès";
                    string[] tab = new string[5];//2 pour savoir que c'est ok
                    var Facture = db.FN_Montant_Facture(id).FirstOrDefault();
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Liquide);
                    tab[2] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Emballage);
                    tab[3] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Retour_Emballage);
                    tab[4] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modèle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Vente/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Supprimer_Detail_Edit_Emballage")]
        public JsonResult Supprimer_Detail_Edit_Emballage(string id)
        {
            StoreEntities db = new StoreEntities();
            Int64 ID_Vente = 0;
            string[] ID = id.Split(';');
            for (int i = 0; i < ID.Length; i++)
            {
                VenteBM Local_Var = new VenteBM();
                ID_Vente = Local_Var.Supprimer_Detail_Edit_Emballage(Int64.Parse(ID[i]));
            }
            string[] tab = new string[5];
            var Facture = db.FN_Montant_Facture(ID_Vente).FirstOrDefault();
            TempData["messagesucess"] = "Emballage supprimer avec succès";
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Ventes/Consulter
        [ActionName("Consulter")]
        public JsonResult Consulter(Int64 id)
        {
            TempData["id"] = id;
            VenteBM Local_Var = new VenteBM();
            VenteVM Return_Var = Local_Var.GetByID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Vente/Consulter.cshtml", Return_Var);

            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeClient(int id)
        {
            StoreEntities db = new StoreEntities();
            Session["ID_Element"] = id;
            double Solde_Liquide =  db.FN_Etat_Solde_Client(0, DateTime.Now, DateTime.Now).FirstOrDefault(p => p.ID_Client == id).Solde;
            double Solde_Emballage =  db.FN_Etat_Solde_Client(1, DateTime.Now, DateTime.Now).FirstOrDefault(p => p.ID_Client == id).Solde;
            db.Vider_Vente_Liquide_Temp(int.Parse(Session["Code_User"].ToString()));
            db.Vider_Vente_Emballage_Temp(int.Parse(Session["Code_User"].ToString()));
            string[] tab = new string[4];
            tab[0] = service.SeparateurMillier(Solde_Liquide);
            tab[1] = service.SeparateurMillier(Solde_Emballage);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

       
        [ActionName("Valider")]
        public JsonResult Valider(Int64 id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Valider_Vente", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            VenteBM Local_Var = new VenteBM();
            Local_Var.Valider(id, int.Parse(Session["Code_User"].ToString()));
            TempData["messagesucess"] = "Action effectuée avec succès";
            string[] tab1 = new string[2];//2 pour savoir que c'est ok
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab1[1] = "";
            return this.Json(tab1, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Annuler")]
        public JsonResult Annuler(Int64 id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Annuler_Vente", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            VenteBM Local_Var = new VenteBM();
            Local_Var.Annuler(id, int.Parse(Session["Code_User"].ToString()));
            TempData["messagesucess"] = "Action effectuée avec succès";
            string[] tab1 = new string[2];//2 pour savoir que c'est ok
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab1[1] = "";
            return this.Json(tab1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadEmballage()
        {
            StoreEntities db = new StoreEntities();
            int Code_User = int.Parse(Session["Code_USer"].ToString());
            int casier_C12 = 0;
            int platique_C12 = 0;
            int bouteille_C12 = 0;
            if (db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 12 && p.Produit_Decomposer == false).FirstOrDefault() != null)
            {
                casier_C12 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 12 && p.Produit_Decomposer == false).Select(p => p.Quantite_Casier_Emballage).Sum();
                platique_C12 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 12 && p.Produit_Decomposer == false).Select(p => p.Quantite_Plastique_Emballage).Sum();
                bouteille_C12 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 12 && p.Produit_Decomposer == false).Select(p => p.Quantite_Bouteille_Emballage).Sum();

            }

            if(db.TP_Vente_Liquide_Temp.Where(p=>p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Plastique_Base == 1).FirstOrDefault() != null)
            {
                int casier = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Plastique_Base == 1).Select(p => p.Quantite_Casier_Emballage).Sum();
                int plastique = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Plastique_Base == 1).Select(p => p.Quantite_Plastique_Emballage).Sum();               
                platique_C12 = platique_C12 + casier + plastique;               
            }
            if (db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Bouteille_Base == 1).FirstOrDefault() != null)
            {
                int bouteille1 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Bouteille_Base == 1).Select(p => p.Quantite_Casier_Emballage * (int)p.Conditionnement).Sum();
                int bouteille = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Bouteille_Base == 1).Select(p => p.Quantite_Bouteille_Emballage).Sum();              
                bouteille_C12 = bouteille_C12 + bouteille + bouteille1;
            }


            int casier_C15 = 0;
            int platique_C15 = 0;
            int bouteille_C15 = 0;
            if (db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 15 && p.Produit_Decomposer == false).FirstOrDefault() != null)
            {
                casier_C15 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 15 && p.Produit_Decomposer == false).Select(p => p.Quantite_Casier_Emballage).Sum();
                platique_C15 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 15 && p.Produit_Decomposer == false).Select(p => p.Quantite_Plastique_Emballage).Sum();
                bouteille_C15 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 15 && p.Produit_Decomposer == false).Select(p => p.Quantite_Bouteille_Emballage).Sum();
            }

            if (db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Plastique_Base == 2).FirstOrDefault() != null)
            {
                int casier = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Plastique_Base == 2).Select(p => p.Quantite_Casier_Emballage).Sum();
                int plastique = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Plastique_Base == 2).Select(p => p.Quantite_Plastique_Emballage).Sum();
                platique_C15 = platique_C15 + casier + plastique;
            }
            if (db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Bouteille_Base == 2).FirstOrDefault() != null)
            {
                int bouteille1 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Bouteille_Base == 2).Select(p => p.Quantite_Casier_Emballage * (int)p.Conditionnement).Sum();
                int bouteille = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Bouteille_Base == 2).Select(p => p.Quantite_Bouteille_Emballage).Sum();
                bouteille_C15 = bouteille_C15 + bouteille + bouteille1;
            }

            int casier_C24 = 0;
            int platique_C24 = 0;
            int bouteille_C24 = 0;
            if (db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 24 && p.Produit_Decomposer == false).FirstOrDefault() != null)
            {
                casier_C24 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 24 && p.Produit_Decomposer == false).Select(p => p.Quantite_Casier_Emballage).Sum();
                platique_C24 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 24 && p.Produit_Decomposer == false).Select(p => p.Quantite_Plastique_Emballage).Sum();
                bouteille_C24 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 24 && p.Produit_Decomposer == false).Select(p => p.Quantite_Bouteille_Emballage).Sum();
            }

            if (db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Plastique_Base == 3).FirstOrDefault() != null)
            {
                int casier = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Plastique_Base == 3).Select(p => p.Quantite_Casier_Emballage).Sum();
                int plastique = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Plastique_Base == 3).Select(p => p.Quantite_Plastique_Emballage).Sum();
                platique_C24 = platique_C24 + casier + plastique;
            }
            if (db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Bouteille_Base == 3).FirstOrDefault() != null)
            {
                int bouteille1 = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Bouteille_Base == 3).Select(p => p.Quantite_Casier_Emballage * (int)p.Conditionnement).Sum();
                int bouteille = db.TP_Vente_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Produit_Decomposer == true && p.TP_Produit.ID_Bouteille_Base == 3).Select(p => p.Quantite_Bouteille_Emballage).Sum();
                bouteille_C24 = bouteille_C24 + bouteille + bouteille1;
            }

            string[] tab = new string[9];
            tab[0] = casier_C12.ToString();
            tab[1] = platique_C12.ToString();
            tab[2] = bouteille_C12.ToString();

            tab[3] = casier_C15.ToString();
            tab[4] = platique_C15.ToString();
            tab[5] = bouteille_C15.ToString();

            tab[6] = casier_C24.ToString();
            tab[7] = platique_C24.ToString();
            tab[8] = bouteille_C24.ToString();

            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpGet, ActionName("Edit_Grille")]
        public JsonResult Edit_Grille(Int64 id, int key, int value)
        {
            VenteBM ObjetBM = new VenteBM();
            StoreEntities db = new StoreEntities();
            ObjetBM.Edit_Grille(id, key, value);
            int Code_User = int.Parse(Session["Code_User"].ToString());
            var Facture = db.FN_Montant_Facture_Temp(Code_User).FirstOrDefault();
            string[] tab = new string[5];//2 pour savoir que c'est ok
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpGet, ActionName("Edit_Grille_1")]
        public JsonResult Edit_Grille_1(Int64 id, int key, int value)
        {
            VenteBM ObjetBM = new VenteBM();
            StoreEntities db = new StoreEntities();
            Int64 ID_Vente = ObjetBM.Edit_Grille_1(id, key, value);
            int Code_User = int.Parse(Session["Code_User"].ToString());
            var Facture = db.FN_Montant_Facture(ID_Vente).FirstOrDefault();
            string[] tab = new string[5];//2 pour savoir que c'est ok
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Facture == null ? 0 : Facture.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Facture == null ? 0 : (Facture.Montant_Liquide + Facture.Montant_Emballage - Facture.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }


    }
}
