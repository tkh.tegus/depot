﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class CommandeController : Controller
    {
        Services service = new Services();
        public class Element
        {
            public int ID_Element { get; set; }
            public string Libelle { get; set; }
        }

        public class JqwAjax
        {
            public Int64 id { get; set; }
            public DateTime Date { get; set; }
            public string Fournisseur { get; set; }
            public double Montant_Liquide { get; set; }
            public double Montant_Emballage { get; set; }
            public double Montant_Retour_Emballage { get; set; }
            public double Montant { get; set; }
            public string NUM_DOC { get; set; }
            public string Statut { get; set; }
            public string Utilisateur { get; set; }            
        }

        public class JqwxLiquide
        {
            public Int64 id { get; set; }
            public int ID_Produit { get; set; }
            public string Produit { get; set; }
            public int Quantite_Casier_Liquide { get; set; }
            public int Quantite_Bouteille_Liquide { get; set; }
            public int Quantite_Casier_Emballage { get; set; }
            public int Quantite_Plastique_Emballage { get; set; }
            public int Quantite_Bouteille_Emballage { get; set; }
            public int PU_Plastique { get; set; }
            public double PU_Bouteille { get; set; }
            public int Conditionnement { get; set; }
            public double PU_Achat { get; set; }
            
            public double Montant_Liquide
            {
                get
                {
                    Services service = new Services();
                    return PU_Achat*Quantite_Casier_Liquide + Math.Round(((PU_Achat/Conditionnement)*Quantite_Bouteille_Liquide),0);                       
                }
                set { }
            }
            public double Montant_Emballage
            {
                get
                {                    
                    return Quantite_Casier_Emballage * (PU_Plastique + (PU_Bouteille * Conditionnement)) + (Quantite_Plastique_Emballage * PU_Plastique) + (Quantite_Bouteille_Emballage * PU_Bouteille);
                }
                set { }
            }
            public double Total
            {
                get
                {
                    return Montant_Liquide + Montant_Emballage;
                }
                set { }
            }
           
        }

        public class JqwxEmballage
        {
            public Int64 id { get; set; }
            public int ID_Emballage { get; set; }
            public string Emballage { get; set; }
            public int Quantite_Casier { get; set; }
            public int Quantite_Plastique { get; set; }
            public int Quantite_Bouteille { get; set; }
            public int PU_Plastique { get; set; }
            public double PU_Bouteille { get; set; }
            public int Conditionnement { get; set; }
            public double Montant
            {
                get
                {
                    return Quantite_Casier * (PU_Plastique + (PU_Bouteille * Conditionnement)) + (PU_Plastique * Quantite_Plastique) + (PU_Bouteille * Quantite_Bouteille);
                }
                set { }
            }
        }

        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.FN_Commande()
                           orderby c.Etat, c.Date descending
                           where (c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin)
                           select new JqwAjax
                           {
                               id = c.ID_Commande,
                               Date = c.Date,
                               NUM_DOC = c.NUM_DOC,
                               Fournisseur = c.Fournisseur,
                               Montant = c.Montant,
                               Montant_Liquide = c.Montant_Liquide,
                               Montant_Emballage = c.Montant_Emballage,
                               Montant_Retour_Emballage = c.Montant_Retour_Emballage,
                               Statut = (c.Etat == 0) ? "En attente de livraison" : (c.Etat == 1) ? "Livrer" : "Annuler"
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxLiquide()
        {
            StoreEntities db = new StoreEntities();
            int Code_User = int.Parse(Session["Code_User"].ToString());
            var resultat = from c in db.TP_Commande_Liquide_Temp
                           orderby c.TP_Produit.Libelle
                           where c.Create_Code_User == Code_User
                           select new JqwxLiquide
                           {
                               id = c.ID_Commande_Liquide_Temp,
                               ID_Produit = c.ID_Produit,
                               Produit = c.TP_Produit.Libelle,
                               Quantite_Casier_Liquide = c.Quantite_Casier_Liquide,
                               Quantite_Bouteille_Liquide = c.Quantite_Bouteille_Liquide,
                               Quantite_Casier_Emballage = c.Quantite_Casier_Emballage,
                               Quantite_Plastique_Emballage = c.Quantite_Plastique_Emballage,
                               Quantite_Bouteille_Emballage = c.Quantite_Bouteille_Emballage,
                               PU_Achat = (double)c.PU_Achat,
                               PU_Plastique = (int)c.PU_Plastique,
                               PU_Bouteille = (int)c.PU_Bouteille,
                               Conditionnement = (int)c.Conditionnement
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxCmdLiquideDetail(Int64 id)
        {
            StoreEntities db = new StoreEntities();
           
            var resultat = from c in db.TP_Commande_Liquide
                           orderby c.TP_Produit.Libelle
                           where c.ID_Commande == id
                           select new JqwxLiquide
                           {
                               id = c.ID_Commande_Liquide,
                               ID_Produit = c.ID_Produit,
                               Produit = c.TP_Produit.Libelle,
                               Quantite_Casier_Liquide = c.Quantite_Casier_Liquide,
                               Quantite_Bouteille_Liquide = c.Quantite_Bouteille_Liquide,
                               Quantite_Casier_Emballage = c.Quantite_Casier_Emballage,
                               Quantite_Plastique_Emballage = c.Quantite_Plastique_Emballage,
                               Quantite_Bouteille_Emballage = c.Quantite_Bouteille_Emballage,
                               PU_Achat = (double)c.PU_Achat,
                               PU_Plastique = (int)c.PU_Plastique,
                               PU_Bouteille = (int)c.PU_Bouteille,
                               Conditionnement = (int)c.Conditionnement
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxEmballage()
        {
            StoreEntities db = new StoreEntities();
            int Code_User = int.Parse(Session["Code_User"].ToString());
            var resultat = from c in db.TP_Commande_Emballage_Temp
                           where c.Create_Code_User == Code_User
                           select new JqwxEmballage
                           {
                               id = c.ID_Commande_Emballage_Temp,
                               ID_Emballage = c.ID_Emballage,
                               Emballage = c.TP_Emballage.Code,
                               Quantite_Casier = c.Quantite_Casier,
                               Quantite_Plastique = c.Quantite_Plastique,
                               Quantite_Bouteille = c.Quantite_Bouteille,
                               PU_Plastique = (int)c.PU_Plastique,
                               PU_Bouteille = (double)c.PU_Bouteille
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxCmdEmballageDetail(Int64 id)
        {
            StoreEntities db = new StoreEntities();
            
            var resultat = from c in db.TP_Commande_Emballage
                           where c.ID_Commande == id
                           select new JqwxEmballage
                           {
                               id = c.ID_Commande_Emballage,
                               ID_Emballage = c.ID_Emballage,
                               Emballage = c.TP_Emballage.Code,
                               Quantite_Casier = c.Quantite_Casier,
                               Quantite_Plastique = c.Quantite_Plastique,
                               Quantite_Bouteille = c.Quantite_Bouteille,
                               PU_Plastique = (int)c.PU_Plastique,
                               PU_Bouteille = (double)c.PU_Bouteille
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Index()
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_commandes", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            var ouverture = db.TP_Ouverture_Cloture_Caisse.Where(p => p.ID_Caisse == ID_Caisse && p.Etat == 0).FirstOrDefault();
            if (ouverture != null)
            {
                Session["DateDebut"] = ouverture.Date_Ouverture.ToString();
            }
            else
            {
                Session["DateDebut"] = DateTime.Today.ToString();
            }
            Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Commande/Index.cshtml", filtre);
            tab[1] = "GESTION DES COMMANDES";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Opération</a></li> <li class='active'>Liste des commandes</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }


        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Ajouter_Commande", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            TempData["ID_Fournisseur"] = new SelectList(from c in db.TP_Fournisseur
                                                   where c.Actif
                                                   select new
                                                   {
                                                       ID_Fournisseur = c.ID_Fournisseur,
                                                       Libelle = c.Nom
                                                   }, "ID_Fournisseur", "Libelle");
            TempData["ID_Produit"] = new SelectList(from c in db.TP_Produit
                                                    orderby c.Libelle
                                                    where c.Actif
                                                    select new
                                                    {
                                                        ID_Produit = c.ID_Produit,
                                                        Libelle = c.Code+" : "+ c.Libelle
                                                    }, "ID_Produit", "Libelle");
            db.Vider_Commande_Liquide_Temp(int.Parse(Session["Code_User"].ToString()));
            db.Vider_Commande_Emballage_Temp(int.Parse(Session["Code_User"].ToString()));
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Commande/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(CommandeVM Param_Var)
        {
            Ouverture_Cloture_CaisseBM caisse = new Ouverture_Cloture_CaisseBM();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            if (!caisse.IsCaisseOpen(ID_Caisse))
            {
                TempData["messagewarning"] = "Veuillez ouvrir une caisse";
                string[] tab = new string[2];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

            StoreEntities db = new StoreEntities();
            try
            {
                string messageRetour = string.Empty;
                int code_user = int.Parse(Session["Code_User"].ToString());
                // verifier si la commande contient des produits
                if (db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == code_user).FirstOrDefault() != null)
                {
                    CommandeBM Local_Var = new CommandeBM();
                    Int64 ID_Commande = Local_Var.Save(Param_Var, int.Parse(Session["Code_User"].ToString()));
                    TempData["messagesucess"] = "Ajout effectué avec succès";
                    string[] tab = new string[5];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = ID_Commande.ToString();
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData["messageerror"] = "La commande ne contient aucun produit.";
                    string[] tab = new string[1];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Commande/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("Add_Liquide")]
        [ValidateAntiForgeryToken]
        public JsonResult Add_Liquide(CommandeVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (Param_Var.Quantite_Casier_Liquide > 0 || Param_Var.Quantite_Bouteille_Liquide > 0)
                    {
                        StoreEntities db = new StoreEntities();                       
                        string messageRetour = string.Empty;
                        CommandeBM Local_Var = new CommandeBM();
                        int Code_User = int.Parse(Session["Code_User"].ToString());
                        Local_Var.Save_Detail_Liquide(Param_Var, Code_User);
            
                        var Commande = db.FN_Montant_Commande_Temp(Code_User).FirstOrDefault();
                        string[] tab = new string[5];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = (service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Liquide));
                        tab[2] = (service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Emballage));
                        tab[3] = (service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Retour_Emballage));
                        tab[4] = (service.SeparateurMillier(Commande == null ? 0 : (Commande.Montant_Liquide+ Commande.Montant_Emballage - Commande.Montant_Retour_Emballage)));
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagesucess"] = "Veuillez renseigner les quantités liquide";
                        string[] tab = new string[1];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    TempData["messageerror"] = "Le modèle n'est pas valide";
                    string[] tab = new string[1];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }               
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Commande/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("Add_Emballage")]
        [ValidateAntiForgeryToken]
        public JsonResult Add_Emballage(CommandeVM Param_Var)
        {
            try
            {
                StoreEntities db = new StoreEntities();
                int Code_User = int.Parse(Session["Code_User"].ToString());
                string messageRetour = string.Empty;
                CommandeBM Local_Var = new CommandeBM();
                if (Param_Var.Casier_12 > 0 || Param_Var.Plastique_12 > 0 || Param_Var.Bouteille_12 > 0)
                {
                    Param_Var.ID_Emballage = 1;
                    Param_Var.Quantite_Casier_Emballage_Retour = (int)Param_Var.Casier_12;
                    Param_Var.Quantite_Plastique_Emballage_Retour = (int)Param_Var.Plastique_12;
                    Param_Var.Quantite_Bouteille_Emballage_Retour = (int)Param_Var.Bouteille_12;
                    Local_Var.Save_Detail_Emballage(Param_Var, int.Parse(Session["Code_User"].ToString()));
                }

                if (Param_Var.Casier_15 > 0 || Param_Var.Plastique_15 > 0 || Param_Var.Bouteille_15 > 0)
                {
                    Param_Var.ID_Emballage = 2;
                    Param_Var.Quantite_Casier_Emballage_Retour = (int)Param_Var.Casier_15;
                    Param_Var.Quantite_Plastique_Emballage_Retour = (int)Param_Var.Plastique_15;
                    Param_Var.Quantite_Bouteille_Emballage_Retour = (int)Param_Var.Bouteille_15;
                    Local_Var.Save_Detail_Emballage(Param_Var, int.Parse(Session["Code_User"].ToString()));
                }

                if (Param_Var.Casier_24 > 0 || Param_Var.Plastique_24 > 0 || Param_Var.Bouteille_24 > 0)
                {
                    Param_Var.ID_Emballage = 3;
                    Param_Var.Quantite_Casier_Emballage_Retour = (int)Param_Var.Casier_24;
                    Param_Var.Quantite_Plastique_Emballage_Retour = (int)Param_Var.Plastique_24;
                    Param_Var.Quantite_Bouteille_Emballage_Retour = (int)Param_Var.Bouteille_24;
                    Local_Var.Save_Detail_Emballage(Param_Var, int.Parse(Session["Code_User"].ToString()));
                }
                var Commande = db.FN_Montant_Commande_Temp(int.Parse(Session["Code_User"].ToString())).FirstOrDefault();
                string[] tab = new string[5];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Liquide);
                tab[2] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Emballage);
                tab[3] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Retour_Emballage);
                tab[4] = service.SeparateurMillier(Commande == null ? 0 : (Commande.Montant_Liquide + Commande.Montant_Emballage - Commande.Montant_Retour_Emballage));
                return this.Json(tab, JsonRequestBehavior.AllowGet);
                
                
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Commande/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Supprimer_Detail_Liquide")]
        public JsonResult Supprimer_Detail_Liquide(string id)
        {            
            StoreEntities db = new StoreEntities();
            CommandeBM Local_Var = new CommandeBM();
            string[] tab_ID = id.Split(';');
            for (int i = 0; i < tab_ID.Length; i++)
            {

                Local_Var.Supprimer_Detail_Liquide(int.Parse(tab_ID[i]));
            }

            int Code_User = int.Parse(Session["Code_User"].ToString());
            var Commande = db.FN_Montant_Commande_Temp(int.Parse(Session["Code_User"].ToString())).FirstOrDefault();
            string[] tab = new string[5];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Commande == null ? 0 : (Commande.Montant_Liquide + Commande.Montant_Emballage - Commande.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Supprimer_Detail_Emballage")]
        public JsonResult Supprimer_Detail_Emballage(string id)
        {
            
            StoreEntities db = new StoreEntities();

            string[] tab_ID = id.Split(';');
            CommandeBM Local_Var = new CommandeBM();
            for (int i = 0; i < tab_ID.Length; i++)
            {

                Local_Var.Supprimer_Detail_Emballage(int.Parse(tab_ID[i]));
            }

            int Code_User = int.Parse(Session["Code_User"].ToString());
            var Commande = db.FN_Montant_Commande_Temp(int.Parse(Session["Code_User"].ToString())).FirstOrDefault();
            string[] tab = new string[5];         
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Commande == null ? 0 : (Commande.Montant_Liquide + Commande.Montant_Emballage - Commande.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }


        // GET: /Commandes/Annuler_Commande/5
        [ActionName("Annuler_Commande")]
        public JsonResult Annuler_Commande(CommandeVM Param_Var)
        {
            StoreEntities db = new StoreEntities();
            db.Vider_Commande_Liquide_Temp(int.Parse(Session["Code_User"].ToString()));
            db.Vider_Commande_Emballage_Temp(int.Parse(Session["Code_User"].ToString()));
            string[] tab = new string[5];
            TempData["messagesucess"] = "Commande annuler avec succès";
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);           
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Modifier")]
        public JsonResult Modifier(Int64 id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Modifier_Commande", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(id, "TP_Commande", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }

            TempData["id"] = id;
            CommandeBM Local_Var = new CommandeBM();
            CommandeVM Return_Var = Local_Var.GetByCmdID(id, int.Parse(Session["Code_User"].ToString()));
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            TempData["ID_Fournisseur"] = new SelectList(from c in db.TP_Fournisseur
                                                        where c.Actif
                                                        select new
                                                        {
                                                            ID_Fournisseur = c.ID_Fournisseur,
                                                            Libelle = c.Nom
                                                        }, "ID_Fournisseur", "Libelle");

            TempData["ID_Produit"] = new SelectList(from c in db.TP_Produit
                                                    orderby c.Libelle
                                                    where c.Actif
                                                    select new
                                                    {
                                                        ID_Produit = c.ID_Produit,
                                                        Libelle = c.Libelle
                                                    }, "ID_Produit", "Libelle");
            var Commande = db.FN_Montant_Commande(id).FirstOrDefault();
            TempData["Montant_Liquide"] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Liquide);
            TempData["Montant_Emballage"] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Emballage);
            TempData["Montant_Retour_Emballage"] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Retour_Emballage);
            TempData["Montant"] = service.SeparateurMillier(Commande == null ? 0 : (Commande.Montant_Liquide+ Commande.Montant_Emballage - Commande.Montant_Retour_Emballage));
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Commande/Modifier.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(Int64 id, CommandeVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    CommandeBM ObjetBM = new CommandeBM();
               
                    ObjetBM.Edit(id, Param_Var, int.Parse(Session["Code_User"].ToString()));
                    // libère le verrou précédemment pauser
                    Locked_RecordsBM veroux = new Locked_RecordsBM();
                    veroux.Liberer(id, "TP_Commande");

                    TempData["messagesucess"] = "Modification effectuée avec succès";
                    string[] tab = new string[2];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    TempData["messagewarning"] = "Le Modele n'est pas valide ";
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Commande/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, ActionName("Edit_Detail_Liquide")]
        [ValidateAntiForgeryToken]
        public JsonResult Edit_Detail_Liquide(Int64 id, CommandeVM Param_Var)
        {
            try
            {              
                StoreEntities db = new StoreEntities();
            
                if (ModelState.IsValid)
                {
                    CommandeBM Local_Var = new CommandeBM();                   
                    if (Param_Var.Quantite_Casier_Liquide > 0 || Param_Var.Quantite_Bouteille_Liquide > 0)
                    {
                        string messageRetour = string.Empty;
                        Local_Var.Edit_Detail_Liquide(id, Param_Var, Session["Code_User"].ToString());
                        var Commande = db.FN_Montant_Commande(id).FirstOrDefault();
                        string[] tab = new string[5];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Liquide);
                        tab[2] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Emballage);
                        tab[3] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Retour_Emballage);
                        tab[4] = service.SeparateurMillier(Commande == null ? 0 : (Commande.Montant_Liquide + Commande.Montant_Emballage - Commande.Montant_Retour_Emballage));
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagesucess"] = "Veuillez renseigner les quantités liquide";
                        string[] tab = new string[1];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Commande/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Supprimer_Detail_Edit_Liquide")]
        public JsonResult Supprimer_Detail_Edit_Liquide(string id)
        {
           
            StoreEntities db = new StoreEntities();
            Int64 ID_Commande = 0;
            string[] tab_ID = id.Split(';');
            CommandeBM Local_Var = new CommandeBM();           
            for (int i = 0; i < tab_ID.Length; i++)
            {
                ID_Commande = Local_Var.Supprimer_Detail_Edit_Liquide(Int64.Parse(tab_ID[i]));
            }
            var Commande = db.FN_Montant_Commande(ID_Commande).FirstOrDefault();
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Commande == null ? 0 : (Commande.Montant_Liquide + Commande.Montant_Emballage - Commande.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Edit_Detail_Emballage")]
        [ValidateAntiForgeryToken]
        public JsonResult Edit_Detail_Emballage(Int64 id, CommandeVM Param_Var)
        {
            try
            {
                
                StoreEntities db = new StoreEntities();
                CommandeBM Local_Var = new CommandeBM();
                Param_Var.ID_Commande = id;
                if (ModelState.IsValid)
                {
                    if (Param_Var.Casier_12 > 0 || Param_Var.Plastique_12 > 0 || Param_Var.Bouteille_12 > 0)
                    {
                        Param_Var.ID_Emballage = 1;
                        Param_Var.Quantite_Casier_Emballage_Retour = (int)Param_Var.Casier_12;
                        Param_Var.Quantite_Plastique_Emballage_Retour = (int)Param_Var.Plastique_12;
                        Param_Var.Quantite_Bouteille_Emballage_Retour = (int)Param_Var.Bouteille_12;
                        Local_Var.Edit_Detail_Emballage(id, Param_Var, Session["Code_User"].ToString());
                    }

                    if (Param_Var.Casier_15 > 0 || Param_Var.Plastique_15 > 0 || Param_Var.Bouteille_15 > 0)
                    {
                        Param_Var.ID_Emballage = 5;
                        Param_Var.Quantite_Casier_Emballage_Retour = (int)Param_Var.Casier_15;
                        Param_Var.Quantite_Plastique_Emballage_Retour = (int)Param_Var.Plastique_15;
                        Param_Var.Quantite_Bouteille_Emballage_Retour = (int)Param_Var.Bouteille_15;
                        Local_Var.Edit_Detail_Emballage(id, Param_Var, Session["Code_User"].ToString());
                    }

                    if (Param_Var.Casier_24 > 0 || Param_Var.Plastique_24 > 0 || Param_Var.Bouteille_24 > 0)
                    {
                        Param_Var.ID_Emballage = 2;
                        Param_Var.Quantite_Casier_Emballage_Retour = (int)Param_Var.Casier_24;
                        Param_Var.Quantite_Plastique_Emballage_Retour = (int)Param_Var.Plastique_24;
                        Param_Var.Quantite_Bouteille_Emballage_Retour = (int)Param_Var.Bouteille_24;
                        Local_Var.Edit_Detail_Emballage(id, Param_Var, Session["Code_User"].ToString());
                    }                   
                    string[] tab = new string[5];//2 pour savoir que c'est ok
                    var Commande = db.FN_Montant_Commande(id).FirstOrDefault();
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Liquide);
                    tab[2] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Emballage);
                    tab[3] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Retour_Emballage);
                    tab[4] = service.SeparateurMillier(Commande == null ? 0 : (Commande.Montant_Liquide + Commande.Montant_Emballage - Commande.Montant_Retour_Emballage));
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Commande/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Supprimer_Detail_Edit_Emballage")]
        public JsonResult Supprimer_Detail_Edit_Emballage(string id)
        {        
            StoreEntities db = new StoreEntities();
            Int64 ID_Commande = 0;
            string[] tab_ID = id.Split(';');
            int ID_Mat_Com = int.Parse(tab_ID[0]);
            for (int i = 0; i < tab_ID.Length; i++)
            {
                CommandeBM Local_Var = new CommandeBM();
                ID_Commande = Local_Var.Supprimer_Detail_Edit_Emballage(Int64.Parse(tab_ID[i]));
            }
            
            string[] tab = new string[3];
            var Commande = db.FN_Montant_Commande(ID_Commande).FirstOrDefault();
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Commande == null ? 0 : (Commande.Montant_Liquide + Commande.Montant_Emballage - Commande.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Edit_Grille(Int64 id, int key, int value)
        {
            CommandeBM ObjetBM = new CommandeBM();
            StoreEntities db = new StoreEntities();
            ObjetBM.Edit_Grille(id, key, value);
            int Code_User = int.Parse(Session["Code_User"].ToString());
            var Commande = db.FN_Montant_Commande_Temp(Code_User).FirstOrDefault();
            string[] tab = new string[5];//2 pour savoir que c'est ok
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab[1] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Liquide);
            tab[2] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Emballage);
            tab[3] = service.SeparateurMillier(Commande == null ? 0 : Commande.Montant_Retour_Emballage);
            tab[4] = service.SeparateurMillier(Commande == null ? 0 : (Commande.Montant_Liquide + Commande.Montant_Emballage - Commande.Montant_Retour_Emballage));
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Commandes/Consulter
        [ActionName("Consulter")]
        public JsonResult Consulter(Int64 id)
        {


            TempData["id"] = id;
            CommandeBM Local_Var = new CommandeBM();
            CommandeVM Return_Var = Local_Var.GetByID(id, int.Parse(Session["Code_User"].ToString()));
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun service correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            TempData["Commande_Liquide"] = (from c in db.TP_Commande_Liquide
                                            where c.ID_Commande == id
                                            select new JqwxLiquide
                                            {
                                                id = c.ID_Commande_Liquide,
                                                ID_Produit = c.ID_Produit,
                                                Produit = c.TP_Produit.Libelle,
                                                Quantite_Casier_Liquide = c.Quantite_Casier_Liquide,
                                                Quantite_Bouteille_Liquide = c.Quantite_Bouteille_Liquide,
                                                Quantite_Casier_Emballage = c.Quantite_Casier_Emballage,
                                                Quantite_Plastique_Emballage = c.Quantite_Plastique_Emballage,
                                                Quantite_Bouteille_Emballage = c.Quantite_Bouteille_Emballage,
                                                
                                               
                                            }).ToList();

            TempData["Commande_Emballage"] = (from c in db.TP_Commande_Emballage
                                              where c.ID_Commande == id
                                              select new JqwxEmballage
                                              {
                                                  id = c.ID_Commande_Emballage,
                                                  ID_Emballage = c.ID_Emballage,
                                                  Emballage = c.TP_Emballage.Code,
                                                  Quantite_Casier = c.Quantite_Casier,
                                                  Quantite_Plastique = c.Quantite_Plastique,
                                                  Quantite_Bouteille = c.Quantite_Bouteille,
                                                 
                                              }).ToList();
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Commande/Consulter.cshtml", Return_Var);

            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ChangeClient(int id)
        {
            StoreEntities db = new StoreEntities();
            //var client = db.TP_Compte.Where(p => p.ID_Type_Compte == 1 && p.ID_Element == id).FirstOrDefault();
            string[] tab = new string[4];
            /*if (client != null)
            {
                tab[0] = client.Solde_Liquide.ToString();
                tab[1] = client.Solde_Emballage.ToString();

            }
            else
            {
                tab[0] = "0";
                tab[0] = "1";
            }*/
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

       

        //
        // GET: /Commandes/Annuler/5
        [ActionName("Valider")]
        public JsonResult Valider(Int64 id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Valider_Commande", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            CommandeBM Local_Var = new CommandeBM();
            Local_Var.Valider(id, int.Parse(Session["Code_User"].ToString()));

            TempData["messagesucess"] = "Valider effectuée avec succès";
            string[] tab1 = new string[2];//2 pour savoir que c'est ok
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab1[1] = "";
            return this.Json(tab1, JsonRequestBehavior.AllowGet);

        }

        public JsonResult LoadEmballage()
        {
            StoreEntities db = new StoreEntities();
            int Code_User = int.Parse(Session["Code_USer"].ToString());
            int casier_C12 = 0;
            int platique_C12 = 0;
            int bouteille_C12 = 0;
            if (db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 12).FirstOrDefault() != null)
            {
                casier_C12 = db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 12).Select(p => p.Quantite_Casier_Emballage).Sum();
                platique_C12 = db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 12).Select(p => p.Quantite_Plastique_Emballage).Sum();
                bouteille_C12 = db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 12).Select(p => p.Quantite_Bouteille_Emballage).Sum();

            }


            int casier_C15 = 0;
            int platique_C15 = 0;
            int bouteille_C15 = 0;
            if (db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 15).FirstOrDefault() != null)
            {
                casier_C15 = db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 15).Select(p => p.Quantite_Casier_Emballage).Sum();
                platique_C15 = db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 15).Select(p => p.Quantite_Plastique_Emballage).Sum();
                bouteille_C15 = db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 15).Select(p => p.Quantite_Bouteille_Emballage).Sum();
            }


            int casier_C24 = 0;
            int platique_C24 = 0;
            int bouteille_C24 = 0;
            if (db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 24).FirstOrDefault() != null)
            {
                casier_C24 = db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 24).Select(p => p.Quantite_Casier_Emballage).Sum();
                platique_C24 = db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 24).Select(p => p.Quantite_Plastique_Emballage).Sum();
                bouteille_C24 = db.TP_Commande_Liquide_Temp.Where(p => p.Create_Code_User == Code_User && p.Conditionnement == 24).Select(p => p.Quantite_Bouteille_Emballage).Sum();
            }

            string[] tab = new string[9];
            tab[0] = casier_C12.ToString();
            tab[1] = platique_C12.ToString();
            tab[2] = bouteille_C12.ToString();

            tab[3] = casier_C15.ToString();
            tab[4] = platique_C15.ToString();
            tab[5] = bouteille_C15.ToString();

            tab[6] = casier_C24.ToString();
            tab[7] = platique_C24.ToString();
            tab[8] = bouteille_C24.ToString();

            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Ventes/Annuler/5
        [ActionName("Annuler")]
        public JsonResult Annuler(Int64 id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Annuler_Commande", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            CommandeBM Local_Var = new CommandeBM();
            Local_Var.Annuler(id, int.Parse(Session["Code_User"].ToString()));

            TempData["messagesucess"] = "Annuler effectuée avec succès";
            string[] tab1 = new string[2];//2 pour savoir que c'est ok
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            tab1[1] = "";
            return this.Json(tab1, JsonRequestBehavior.AllowGet);
        }  

    }
}
