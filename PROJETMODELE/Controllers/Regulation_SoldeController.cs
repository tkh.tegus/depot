﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class Regulation_SoldeController : Controller
    {
        Services service = new Services();
        public class JqwAjax
        {
            public Int64 id { get; set; }
            public DateTime Date { get; set; }
            public string Client { get; set; }
            public double Montant { get; set; }
            public double Montant_Emballage { get; set; }
            public string Motif { get; set; }
            public string Utilisateur { get; set; }
            public string Statut { get; set; }
            public string NUM_DOC { get; set; }
        }

        public JsonResult ajax(int id)
        {
            StoreEntities db = new StoreEntities();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.TP_Regulation_Solde
                           orderby c.Etat, c.Date descending
                           where (c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin) && c.Type_Element == id
                           select new JqwAjax
                           {
                               id = c.ID_Regulation_Solde,
                               Date = c.Date,
                               Client = (c.Type_Element == 0 ? db.TP_Fournisseur.Where(p=>p.ID_Fournisseur == c.ID_Element).FirstOrDefault().Nom : (c.Type_Element == 1 ? db.TP_Client.Where(p=>p.ID_Client == c.ID_Element).FirstOrDefault().Nom : db.TP_Caisse.Where(p=>p.ID_Caisse == c.ID_Element).FirstOrDefault().Libelle)),
                               NUM_DOC = c.NUM_DOC,
                               Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == c.Create_Code_User).Select(p => p.Libelle).FirstOrDefault(),
                               Montant = c.Montant,
                               Motif = c.Motif,
                               Statut = (c.Etat == 0) ? "En Attente de validation" : (c.Etat == 1) ? "Valider" : "Annuler"
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Index")]
        public JsonResult Index(int id)
        {
            //id = 0 => Régulation Solde Fournisseur
            //id = 1 => Régulation Solde Client
            //id = 2 => Régulation Solde Caisse
            string[] tab_Habillitation = new string[3];
            if ((id == 0 && !service.VerifierHabiliationIndex("Gestion_des_regulations_solde_fournisseur", this, ref tab_Habillitation)) || (id == 1 && !service.VerifierHabiliationIndex("Gestion_des_regulations_solde_client", this, ref tab_Habillitation)) || (id == 2 && !service.VerifierHabiliationIndex("Gestion_des_regulations_solde_caisse", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            var ouverture = db.TP_Ouverture_Cloture_Caisse.Where(p => p.ID_Caisse == ID_Caisse && p.Etat == 0).FirstOrDefault();
            if (ouverture != null)
            {
                Session["DateDebut"] = ouverture.Date_Ouverture.ToString();
            }
            else
            {
                Session["DateDebut"] = DateTime.Today.ToString();
            }
            Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            TempData["type"] = id;
            if (id == 0)
            {
                string[] tab = new string[3];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Solde/Index.cshtml", filtre);
                tab[1] = "GESTION DES RÉGULATIONS FOURNISSEURS";
                tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des décaissements</li>";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else if (id == 1)
            {
                string[] tab = new string[3];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Solde/Index.cshtml", filtre);
                tab[1] = "GESTION DES RÉGULATIONS CLIENTS";
                tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des ristournes clients</li>";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            else
            {

                string[] tab = new string[3];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Solde/Index.cshtml", filtre);
                tab[1] = "GESTION DES RÉGULATIONS CAISSES";
                tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des dépenses</li>";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Regulation_Solde/Ajouter
        [ActionName("Ajouter")]
        public JsonResult Ajouter(int id)
        {
            //id = 0 => Régulation Solde Fournisseur
            //id = 1 => Régulation Solde Client
            //id = 2 => Régulation Solde Caisse
            string[] tab_Habillitation = new string[1];
            if ((id == 0 && !service.VerifierHabiliationCRUD("Ajouter_regulations_solde_fournisseur", this, ref tab_Habillitation)) || (id == 1 && !service.VerifierHabiliationCRUD("Ajouter_regulations_solde_client", this, ref tab_Habillitation)) || (id == 2 && !service.VerifierHabiliationCRUD("Ajouter_regulations_solde_caisse", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();

            if (id == 0)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Fournisseur
                                                        where c.Actif
                                                        orderby c.Nom
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Fournisseur,
                                                            Libelle = c.Nom
                                                        }, "ID_Element", "Libelle");
            }
            else if (id == 1)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Client
                                                        where c.Actif
                                                        orderby c.Nom
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Client,
                                                            Libelle = c.Nom
                                                        }, "ID_Element", "Libelle");
            }
            else
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Caisse
                                                        where c.Actif
                                                        orderby c.Libelle
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Caisse,
                                                            Libelle = c.Libelle
                                                        }, "ID_Element", "Libelle");
            }
            Regulation_SoldeVM Return_Var = new Regulation_SoldeVM();
            Return_Var.Type_Element = (byte)id;
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Solde/Ajouter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }


        //
        // POST: /Regulation_Solde/Ajouter
        [HttpPost, ActionName("Ajouter")]
        [ValidateAntiForgeryToken]
        public JsonResult Ajouter(Regulation_SoldeVM Param_Var)
        {
            Ouverture_Cloture_CaisseBM caisse = new Ouverture_Cloture_CaisseBM();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            if (!caisse.IsCaisseOpen(ID_Caisse))
            {
                TempData["messagewarning"] = "Veuillez ouvrir une caisse";
                string[] tab = new string[2];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    Regulation_SoldeBM ObjetModel = new Regulation_SoldeBM();

                    Int64 id = ObjetModel.Save(Param_Var, Session["Code_User"].ToString());
                    TempData["messagesucess"] = "Ajout effectué avec succès";
                    string[] tab = new string[2];//2 pour savoir que c'est ok
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = id.ToString();
                    return this.Json(tab, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Regulation_Solde/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        
        [ActionName("Modifier")]
        public JsonResult Modifier(string id)
        {
            int ID_ = int.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            //id = 0 => Régulation Solde Fournisseur
            //id = 1 => Régulation Solde Client
            //id = 2 => Régulation Solde Caisse
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Modifier_regulations_solde_fournisseur", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Modifier_regulations_solde_client", this, ref tab_Habillitation)) || (type == 2 && !service.VerifierHabiliationCRUD("Modifier_regulations_solde_caisse", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(ID_, "TP_Regulation_Solde", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            Regulation_SoldeBM ObjetBM = new Regulation_SoldeBM();
            Regulation_SoldeVM LocalVM = ObjetBM.GetByID(ID_);
            if (LocalVM == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            
            StoreEntities db = new StoreEntities();
            if (LocalVM.Type_Element == 0)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Fournisseur
                                                        where c.Actif
                                                        orderby c.Nom
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Fournisseur,
                                                            Libelle = c.Nom
                                                        }, "ID_Element", "Libelle");
            }
            else if (LocalVM.Type_Element == 1)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Client
                                                        where c.Actif
                                                        orderby c.Nom
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Client,
                                                            Libelle = c.Nom
                                                        }, "ID_Element", "Libelle");
            }
            else
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Type_Depense
                                                        where c.Actif
                                                        orderby c.Libelle
                                                        select new
                                                        {
                                                            ID_Element = c.ID_Type_Depense,
                                                            Libelle = c.Libelle
                                                        }, "ID_Element", "Libelle");
            }

            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Solde/Modifier.cshtml", LocalVM);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Regulation_Solde/Modifier/5
        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(string id, Regulation_SoldeVM Param_Var)
        {
            int ID_ = int.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    Regulation_SoldeBM ObjetBM = new Regulation_SoldeBM();


                    ObjetBM.Edit(ID_, Param_Var, Session["Code_User"].ToString());

                    // libère le verrou précédemment pauser
                    Locked_RecordsBM veroux = new Locked_RecordsBM();
                    veroux.Liberer(ID_, "TP_Regulation_Solde");

                    TempData["messagesucess"] = "Modification effectuée avec succès";
                    string[] tab = new string[2];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    TempData["messagewarning"] = "Le Modele n'est pas valide ";
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Regulation_Solde/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Ventes/Annuler/5
        [ActionName("Valider")]
        public JsonResult Valider(string id)
        {
            Int64 ID_Regulation = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Valider_regulations_solde_fournisseur", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Valider_regulations_solde_client", this, ref tab_Habillitation)) || (type == 2 && !service.VerifierHabiliationCRUD("Valider_regulations_solde_caisse", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                Regulation_SoldeBM Local_Var = new Regulation_SoldeBM();
                Local_Var.Valider(ID_Regulation, Session["Code_User"].ToString());

                TempData["messagesucess"] = "Valider effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Regulation_Solde/Valider/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }  
            

        }


        //
        // GET: /Ventes/Annuler/5
        [ActionName("Annuler")]
        public JsonResult Annuler(string id)
        {
            Int64 ID_Regulation = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Annuler_regulations_solde_fournisseur", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Annuler_regulations_solde_client", this, ref tab_Habillitation)) || (type == 2 && !service.VerifierHabiliationCRUD("Annuler_regulations_solde_caisse", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                Regulation_SoldeBM Local_Var = new Regulation_SoldeBM();
                Local_Var.Annuler(ID_Regulation, Session["Code_User"].ToString());

                TempData["messagesucess"] = "Action effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Regulation_Solde/Annuler/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }  
            

        }

        //
        // GET: /Ventes/Consulter
        [ActionName("Consulter")]
        public JsonResult Consulter(Int64 id)
        {


            TempData["id"] = id;
            Regulation_SoldeBM Local_Var = new Regulation_SoldeBM();
            Regulation_SoldeVM Return_Var = Local_Var.GetByID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }

            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Regulation_Solde/Consulter.cshtml", Return_Var);

            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

    }
}
