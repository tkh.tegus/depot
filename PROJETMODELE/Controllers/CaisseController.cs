﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class CaisseController : Controller
    {
        Services service = new Services();
        public class JqwAjax
        {
            public int id { get; set; }
            public string libelle { get; set; }
            public string Utilisateur { get; set; }
            public string statut { get; set; }
        }

        public JsonResult ajax()
        {
            StoreEntities db = new StoreEntities();
            var resultat = from c in db.TP_Caisse
                           orderby c.Actif descending, c.Libelle
                           select new JqwAjax
                           {
                               id = c.ID_Caisse,
                               libelle = c.Libelle,
                               Utilisateur = db.Vue_Personnel.Where(p=>p.ID_Personnel == c.Create_Code_User).Select(p=>p.Libelle).FirstOrDefault(),
                               statut = (c.Actif) ? "Actif" : "Suspendu"
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Index")]
        public JsonResult Index()
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_caisse", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Caisse/Index.cshtml", null);
            tab[1] = "GESTION DES CAISSES";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Liste des caisses</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        
        [ActionName("Ajouter")]
        public JsonResult Ajouter()
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Ajouter_Caisse", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Caisse/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost, ActionName("Ajouter")]
        [ValidateAntiForgeryToken]
        public JsonResult Ajouter(CaisseVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    CaisseBM ObjetModel = new CaisseBM();

                    if (ObjetModel.IsNotExist(Param_Var, ref messageRetour))
                    {
                        ObjetModel.Save(Param_Var, Session["Code_User"].ToString());
                        TempData["messagesucess"] = "Ajout effectué avec succès";
                        string[] tab = new string[2];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string[] tab = new string[1];
                    TempData["messageerror"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);               
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Caisse/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Caisse/Modifier/5
        [ActionName("Modifier")]
        public JsonResult Modifier(int id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Modifier_Caisse", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(id, "TP_Caisse", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            CaisseBM ObjetBM = new CaisseBM();
            CaisseVM LocalVM = ObjetBM.GetByID(id);
            if (LocalVM == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }


            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Caisse/Modifier.cshtml", LocalVM);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Caisse/Modifier/5
        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(int id, CaisseVM Param_Var)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    CaisseBM ObjetBM = new CaisseBM();

                    if (ObjetBM.IsNotExist(Param_Var, id, ref messageRetour)) //vérifie si le arrondissement existe déjà
                    {
                        ObjetBM.Edit(id, Param_Var, Session["Code_User"].ToString());
                        // libère le verrou précédemment pauser
                        Locked_RecordsBM veroux = new Locked_RecordsBM();
                        veroux.Liberer(id, "TP_Caisse");
                        TempData["messagesucess"] = "Modification effectuée avec succès";
                        string[] tab = new string[2];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    TempData["messagewarning"] = ModelState.Values.SelectMany(p => p.Errors).Select(p => p.ErrorMessage).FirstOrDefault();
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Caisse/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName("Activer_Suspendre")]
        public JsonResult Activer_Suspendre(string id)
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Activer_suspendre_Caisse", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                string[] tab = id.Split(';');
                for (int i = 0; i < tab.Length; i++)
                {
                    CaisseBM Local_Var = new CaisseBM();
                    Local_Var.Activer_Suspendre(int.Parse(tab[i]), Session["Code_User"].ToString());
                }
                TempData["messagesucess"] = "Action effectuée avec succès";
                string[] tab1 = new string[1];
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Caisse/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }            
        }
    }
}
