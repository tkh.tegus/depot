﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;
using System.Data;
using System.Data.Entity;
using System.Text;

namespace PROJETMODELE.Controllers
{
   [Authorize]
    public class CorbeilleController : Controller
    {
       /*
        Services service = new Services();

        public class JqwGrille
        {
            public string id { get; set; }
            public string Element { get; set; }
            public string Date { get; set; }
            public string Utilisateur { get; set; }
            public string Description { get; set; }
        }

        public JsonResult ajax()
        {

            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            StoreEntities db = new StoreEntities();
            var Table = new List<JqwGrille>
            {
            };

            // cet element est incomplet car il manque le nom de l'utilisateur
            var Elementcorbeilleincomplet = from ElementCorbeille in db.TG_Corbeille
                           join table in db.TG_Tables on ElementCorbeille.ID_Table equals table.ID_Table
                                            where ElementCorbeille.Actif == true && (ElementCorbeille.Date > filtre.DateDebut && ElementCorbeille.Date <= filtre.CDateFin)
                           orderby ElementCorbeille.ID_Corbeille descending
                           select (new { ElementCorbeille.ID_Corbeille, ElementCorbeille.Code_User, ElementCorbeille.Date, ElementCorbeille.Description, table.LibelleAfficher });

            // joint l'élément de la corbeille au personnel pour pouvoir faire apparaitre le nom de celui qui a supprimer l'élément
            var Elementcorbeillecomplet = from corbeille in Elementcorbeilleincomplet
                                          join personnel in db.TG_Personnels on corbeille.Code_User equals personnel.ID_Personnel
                                          orderby corbeille.ID_Corbeille descending
                                          select (new { 
                                              corbeille.ID_Corbeille, 
                                              corbeille.Code_User, 
                                              corbeille.Date, 
                                              corbeille.Description, 
                                              corbeille.LibelleAfficher, 
                                              personnel.Nom, 
                                              personnel.Prenom });
            
            foreach (var Corbeille in Elementcorbeillecomplet)
            {
                var person = db.TG_Personnels.Find(Corbeille.Code_User);

                Table.Add(new JqwGrille() { 
                    id = Corbeille.ID_Corbeille.ToString(),
                    Element = Corbeille.LibelleAfficher, 
                    Date = Corbeille.Date.ToString("dd/MM/yyyy HH:mm"),
                    Description = Corbeille.Description,
                    Utilisateur = person.Nom +" "+ (person.Prenom == null ? "" : person.Prenom)
                });
            }
            return new JsonResult()
            {
                Data = Table,
                MaxJsonLength = int.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        //
        // GET: /Corbeille/
        public JsonResult Index()
        {
            Session["DateDebut"] = DateTime.Today.AddDays(-14).ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Corbeille/index.cshtml", filtre);
            tab[1] = "Gestion de la corbeille";
            tab[2] = "<a href='#'>Administration</a> <div class='breadcrumb_divider'></div><a class='current'>Gestion de la corbeille</a>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// permet de restorer un element de la corbeille
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ActionName("Restorer")]
        public JsonResult Restorer(string id)
        {
            string[] tab = id.Split(';');
            for (int i = 0; i < tab.Length; i++)
            {
                CorbeilleBM ObjetLocal = new CorbeilleBM();
                ObjetLocal.RestorerElement(int.Parse(tab[i]));
            }
            TempData["messagesucess"] = "Restoration effectuée avec succès";
            string[] tab1 = new string[1];
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab1, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// permet de vider la corbeille
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ActionName("Vider")]
        public JsonResult Vider()
        {
            CorbeilleBM ObjetLocal = new CorbeilleBM();
            ObjetLocal.Vider();
            TempData["messagesucess"] = "La corbeille a été vidée avec succès";
            string[] tab1 = new string[1];
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab1, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// permet de supprimer un élément definitivement de la corbeille
        /// pour quil ne s'affiche plus
        /// </summary>
        /// <returns></returns>
        [ActionName("Supprimer")]
        public JsonResult Supprimer(string id)
        {
            CorbeilleBM ObjetLocal = new CorbeilleBM();
            string[] tab = id.Split(';');
            for (int i = 0; i < tab.Length; i++)
            {
                ObjetLocal.Delete(int.Parse(tab[i]));
            }
            TempData["messagesucess"] = "Suppression éffectuée avec succè";
            string[] tab1 = new string[1];
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab1, JsonRequestBehavior.AllowGet);

        }

       */
    }
}
