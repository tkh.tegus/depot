﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using PROJETMODELE.Models.BML;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;
using System.Web.Script.Serialization;
using System.IO;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.VML.TP;
using System.Data.SqlClient;
using PROJETMODELE.Models.BML.TG;
using System.Configuration;

namespace PROJETMODELE.Controllers
{

    [Authorize]
    public class BaseController : Controller
    {
        Services service = new Services();
        private class UplaodeFile
        {
            public string file { get; set; }
            public string message { get; set; }
            public bool success { get; set; }
        }

        private class Items
        {
            public int ID { get; set; }
            public int message { get; set; }
        }

        private class ItemsList
        {
            public SelectList bopi { get; set; }
            public SelectList page { get; set; }
        }
        public ActionResult Aide()
        {
            return View("~/Views/Recherche/Aide.cshtml");

        }
        
       
        [ActionName("BuildFiltre")]
        public JsonResult BuildFiltre(FormCollection FiltreClient)
        {
            string message = string.Empty;
            DateTime DateDebut = DateTime.Parse(FiltreClient["DateDebut"].ToString(), System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR"));
            DateTime DateFin = DateTime.Parse(FiltreClient["DateFin"].ToString(), System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR"));
            if (DateFin >= DateDebut)
            {
                Session["DateDebut"] = DateDebut;
                Session["DateFin"] = DateFin;
                Session["Filtre"] = "1";
                TempData["messagesucess"] = "Filtre effectué avec succès";
            }
            else
            {
                TempData["messageerror"] = "La date de début ne doit pas être inférieure à la date de fin";
            }

            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);

        }

        public JsonResult SetTypeCompteID(string id)
        {
            Session["ID_Type_Compte"] = id;
            return this.Json("", JsonRequestBehavior.AllowGet);
        }

        [ActionName("GetDataGrid")]
        public void GetDataGrid(string id)
        {
            if (id == "pdf")
            {
                service.WriteFile(Request.Params["json"], Request.Params["titre"], this);
            }
            else if (id == "xls" || id == "csv")
            {
                service.GetCsvFormat(Request.Params["json"], Request.Params["titre"], this);
            }
        }

        public void GetFile(string id)
        {

            if (id == "pdf")
            {
                TempData["id"] = service.ReadFile(this);
                service.ExportPdf(this);
            }
            else if (id == "xls" || id == "csv")
            {
                service.GetFile(Session["Code_User"].ToString() + ".csv", this);
            }
        }       

        public JsonResult ChangerElment(int id)
        {
            Session["ID_Element"] = id;
            
            return this.Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangerE(int id)
        {
            Session["Titre"] = "id";
            return this.Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetTitre()
        {
            Session["ID_Titre"] = Request.Form["q"].ToString();
            return this.Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetIdExport()
        {
            Session["ID_Element"] = Request.Form["q"].ToString();
            return this.Json("", JsonRequestBehavior.AllowGet);
        }
 
        public JsonResult SetFiltreID(string id)
        {
            Session["ID_Element"] = id;
            return this.Json("", JsonRequestBehavior.AllowGet);
        }
       
        [HttpPost]
        public JsonResult SaveFiles(int? id)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var files = Directory.GetFiles(Server.MapPath("~/Uploads"), "current" + Session["Code_User"].ToString() + "_*.*");
                foreach (var item in files)
                {
                    System.IO.File.Delete(item);
                }
                UplaodeFile up = new UplaodeFile();
                String nomdufichier = "", Extension = "", ContentType = "", Size = "";
                if (Request.Files.AllKeys.Any())
                {
                    int NOMBRE = Request.Files.AllKeys.Count();

                    Session["Nombre"] = NOMBRE.ToString();
                    for (int i = 0; i < NOMBRE; i++)
                    {
                        // Get the uploaded image from the Files collection
                        var httpPostedFile = Request.Files["UploadedImage" + i.ToString()];

                        if (httpPostedFile != null)
                        {
                            string ext = Path.GetExtension(httpPostedFile.FileName), chemin = "";
                            nomdufichier += Path.GetFileName(httpPostedFile.FileName) + "&";
                            Extension += ext + "&";
                            ContentType += httpPostedFile.ContentType + "&";
                            Size += httpPostedFile.ContentLength + "&";
                            up.file = "current" + Session["Code_User"].ToString() + "_" + i.ToString() + ext;
                            chemin = Path.Combine(Server.MapPath("~/Uploads"), up.file);

                            httpPostedFile.SaveAs(chemin);
                            // photo = Session["Code_User"].ToString() + ".png";
                            up.message = "Fichier uploadé avec succés";

                        }
                    }
                    Session["nomdufichier"] = nomdufichier;
                    Session["Extension"] = Extension;
                    Session["ContentType"] = ContentType;
                    Session["Size"] = Size;
                    up.success = true;
                    return this.Json(serializer.Serialize(up), JsonRequestBehavior.AllowGet);

                }
                up.message = "Aucun fichier sélectionné";
                up.success = false;
                return this.Json(serializer.Serialize(up), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return this.Json("", JsonRequestBehavior.AllowGet);
            }

        }

        

        //Save DB
        public JsonResult Backup_BD()
        {
            StoreEntities db = new StoreEntities();
            var variable = db.TG_Variable.FirstOrDefault();
            string User = "admin";
            string Password = variable.Password_Sauvegarde.Decript_MIU();
            string strConnexion =string.Format("Data Source=DESKTOP-P7RPO7P\\MSSQLSERVER2008;Initial Catalog=BD_Facturation;User ID={0};Password={1}",User, Password);
            string filePath = @"" + variable.Dossier_Sauvegarde;
            try
            {
                SqlConnection con = new SqlConnection(strConnexion);
                string database = con.Database.ToString();
                //string cmd = "BACKUP DATABASE [" + database + "] TO DISK='" + filePath + "" + database + "_" + DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".bak' WITH MEDIAPASSWORD='C0mplexP@ssW0rd'";
                string cmd = "BACKUP DATABASE [" + database + "] TO DISK='" + filePath + "" + database + "_" + DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".bak'";
                using (SqlCommand command = new SqlCommand(cmd, con))
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }
                    command.ExecuteNonQuery();
                    con.Close();
                }
                TempData["messagesucess"] = "Sauvegarde effectué avec succès";
                string[] tab = new string[2];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Sauvegarde/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
           
        }

        #region export Produit et stock
        public void ExporterListeProduit()
        {
            
            StoreEntities db = new StoreEntities();
            ProduitVM Impression = new ProduitVM();
            Impression.Libelle = "LISTE DES PRODUITS";
            TempData["Data"] = (from c in db.TP_Produit
                                orderby c.TP_Famille.Libelle, c.Libelle
                                select new ProduitController.JqwAjax
                                {
                                    Code = c.Code,
                                    Libelle = c.Libelle,
                                    Producteur = c.TP_Famille.Libelle,
                                    PU_Achat = c.PU_Achat,
                                    PU_Vente = c.PU_Vente,
                                    Produit_Decomposer = c.Produit_Decomposer,
                                    Valeur_Emballage = c.Valeur_Emballage,
                                    Conditionnement = c.TP_Emballage.Conditionnement
                                }).ToList();
            service.TEG_IMPRESSION(Impression.Libelle.Replace(' ', '_'), Impression, "~/Views/Produit/PDFListeProduit.cshtml", this, true);
        
        }

        public void ExporterEtatStock(int id)
        {

            StoreEntities db = new StoreEntities();
            ProduitVM Impression = new ProduitVM();
            TempData["type"] = id.ToString();
            int ID_Element = id;
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            if(id == 0)
            {
                Impression.Libelle = "ETAT STOCK LIQUIDE DU "+filtre.DateFin.ToString("dd/MM/yyyy");
            }
            else
            {
                Impression.Libelle = "ETAT STOCK EMBALLAGE DU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            TempData["Data"] = (from c in db.FN_Etat_Stock(ID_Element, filtre.DateDebut, filtre.CDateFin)
                           where ID_Element == 1 ? (c.ID_Element != 5 && c.ID_Element != 4) : true
                           orderby c.Code
                           select new EtatController.JqwAjaxEtatSolde
                           {
                               id = c.ID_Element,
                               libelle = c.Libelle,
                               code = c.Code,
                               valeur = (ID_Element == 0 ? c.Valeur_Final_Vente : c.Valeur_Final_Emballage),
                               valeur_Achat = (ID_Element == 0 ? c.Valeur_Final_Achat : c.Valeur_Final_Emballage),
                               casier = c.Stock_Casier,
                               plastique = c.Stock_Plastique,
                               bouteil = c.Stock_Bouteille
                           }).ToList();
            service.TEG_IMPRESSION(Impression.Libelle.Replace(' ', '_'), Impression, "~/Views/Etat/PDFStock.cshtml", this, true);

        }

        public void ExporterHistoriqueStock(string id)
        {
            int type = int.Parse(id.Split(';')[0]);
            int ID_Element = int.Parse(id.Split(';')[1]);
            StoreEntities db = new StoreEntities();
            ProduitVM Impression = new ProduitVM();
            TempData["type"] = type.ToString();
            
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            if (type == 0)
            {
                Impression.Libelle = "HISTORIQUE STOCK LIQUIDE";
                TempData["Produit"] = db.TP_Produit.Find(ID_Element).Libelle;
            }
            else
            {
                Impression.Libelle = "HISTORIQUE STOCK EMBALLAGE";
                TempData["Produit"] = db.TP_Emballage.Find(ID_Element).Code;
            }
            TempData["Date_Debut"] = filtre.DateDebut.ToString("dd/MM/yyyy");
            TempData["Date_Fin"] = filtre.DateFin.ToString("dd/MM/yyyy");
            TempData["Data"] = (from c in db.FN_Historique_Stock(ID_Element, type)
                                where (c.Date > filtre.DateDebut && c.Date <= filtre.CDateFin)
                                orderby c.Rang
                                select new HistoriqueController.JqwAjax
                                {
                                    id = c.Type,
                                    date = c.Date,
                                    mouvement = c.Mouvement,
                                    numero = c.Numero,
                                    rang = c.Rang,
                                    casier = c.Casier,
                                    casier_progressif = c.Casier_Porgressif,
                                    bouteille = c.Bouteille,
                                    bouteille_progressif = c.Bouteille_Porgressif,
                                    plastique = c.Plastique,
                                    plastique_progressif = c.Plastique_Porgressif
                                }).ToList();
            service.TEG_IMPRESSION(Impression.Libelle.Replace(' ', '_'), Impression, "~/Views/Historique/PDFHistoriqueStock.cshtml", this, true);

        }
    #endregion

        #region Export solde client
        public void ExporterEtatSoldeClient(int id)
        {

            StoreEntities db = new StoreEntities();
            ClientVM Impression = new ClientVM();
            TempData["type"] = id.ToString();
            int ID_Element = id;
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            if (id == 0)
            {
                Impression.Nom = "SOLDE LIQUIDE DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            else if(id == 1)
            {
                Impression.Nom = "SOLDE EMBALLAGE DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            else
            {
                Impression.Nom = "SOLDE LIQUIDE ET EMBALLAGE DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            TempData["Data"] = (from c in db.FN_Etat_Solde_Client(ID_Element, filtre.DateDebut, filtre.CDateFin)
                                orderby c.Client
                                select new EtatController.JqwAjaxEtatSolde
                                {
                                    id = c.ID_Client,
                                    libelle = c.Client,
                                    initial = c.Initial,
                                    vente = c.Vente,
                                    versement = c.Versement,
                                    encaissement = c.Encaissement,
                                    retour_emballage = c.Retour_emballage,
                                    regularisation = c.Regulation,
                                    solde = c.Solde
                                }).ToList();
            service.TEG_IMPRESSION(Impression.Nom.Replace(' ', '_'), Impression, "~/Views/Etat/PDFEtatSoldeClient.cshtml", this, true);

        }

        public void ExporterHistoriqueSoldeClient(string id)
        {

            StoreEntities db = new StoreEntities();
            ClientVM Impression = new ClientVM();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            int ID_Client = int.Parse(id.Split(';')[1].ToString());
            int Type = int.Parse(id.Split(';')[0].ToString());
            string Libelle = db.TP_Client.Find(ID_Client).Nom;
            if (Type == 0)
            {
                Impression.Nom = "HISTORIQUE LIQUIDE DE " + Libelle + " DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            else if (Type == 1)
            {
                Impression.Nom = "HISTORIQUE EMBALLAGE DE " + Libelle + " DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            else
            {
                Impression.Nom = "HISTORIQUE LIQUIDE ET EMBALLAGE DE " + Libelle + " DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }

            TempData["Data"] = (from c in db.FN_Historique_Solde_Client(ID_Client, Type)
                               where (c.Date > filtre.DateDebut && c.Date <= filtre.CDateFin)
                               orderby c.Rang
                               select new HistoriqueController.JqwAjax
                               {
                                   id = c.Type,
                                   date = c.Date,
                                   mouvement = c.Mouvement,
                                   numero = c.Numero,
                                   rang = c.Rang,
                                   montant = c.Quantite,
                                   solde_progressif = c.Stock_Porgressif
                               }).ToList();
            service.TEG_IMPRESSION(Impression.Nom.Replace(' ', '_'), Impression, "~/Views/Historique/PDFHistoriqueSoldeClient.cshtml", this, true);

        }
        #endregion

        #region Export solde fournissuer
        public void ExporterEtatSoldeFournisseur(int id)
        {

            StoreEntities db = new StoreEntities();
            FournisseurVM Impression = new FournisseurVM();
            TempData["type"] = id.ToString();
            int ID_Element = id;
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            if (id == 0)
            {
                Impression.Nom = "SOLDE LIQUIDE DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            else if(id == 1)
            {
                Impression.Nom = "SOLDE EMBALLAGE DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            else
            {
                Impression.Nom = "SOLDE LIQUIDE ET EMBALLAGE DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            TempData["Data"] = (from c in db.FN_Etat_Solde_Fournisseur(ID_Element, filtre.DateDebut, filtre.CDateFin)
                                orderby c.Fournisseur
                                select new EtatController.JqwAjaxEtatSolde
                                {
                                    id = c.ID_Fournisseur,
                                    libelle = c.Fournisseur,
                                    initial = c.Initial,
                                    commande = c.Commande,
                                    reglement = c.Reglement,
                                    regularisation = c.Regulation_Liquide,
                                    retour_emballage = c.Retour_Emballage,
                                    solde = c.Solde
                                }).ToList();
            service.TEG_IMPRESSION(Impression.Nom.Replace(' ', '_'), Impression, "~/Views/Etat/PDFEtatSoldeFournisseur.cshtml", this, true);

        }

        public void ExporterHistoriqueSoldeFournisseur(string id)
        {

            StoreEntities db = new StoreEntities();
            FournisseurVM Impression = new FournisseurVM();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            string Libelle = db.TP_Fournisseur.Find(int.Parse(id.Split(';')[1].ToString())).Nom;
            int ID_Fournisseur = int.Parse(id.Split(';')[1].ToString());
            int Type = int.Parse(id.Split(';')[0].ToString());
            if (Type == 0)
            {
                Impression.Nom = "HISTORIQUE LIQUIDE DE " + Libelle+  " DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            else if (Type == 1)
            {
                Impression.Nom = "HISTORIQUE EMBALLAGE DE " + Libelle+  " DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }
            else
            {
                Impression.Nom = "HISTORIQUE LIQUIDE ET EMBALLAGE DE " + Libelle+  " DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            }

            TempData["Data"] = (from c in db.FN_Historique_Solde_Fournisseur(ID_Fournisseur,Type)
                                where (c.Date > filtre.DateDebut && c.Date <= filtre.CDateFin)
                                orderby c.Rang
                                select new HistoriqueController.JqwAjax
                                {
                                    id = c.Type,
                                    date = c.Date,
                                    mouvement = c.Mouvement,
                                    numero = c.Numero,
                                    rang = c.Rang,
                                    montant = c.Quantite,
                                    solde_progressif = c.Stock_Porgressif
                                }).ToList();
            service.TEG_IMPRESSION(Impression.Nom.Replace(' ', '_'), Impression, "~/Views/Historique/PDFHistoriqueSoldeFournisseur.cshtml", this, true);

        }
        #endregion

        #region Export solde caisse 
        public void ExporterEtatSoldeCaisse()
        {

            StoreEntities db = new StoreEntities();
            CaisseVM Impression = new CaisseVM();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            Impression.Libelle = "SOLDE DE LA CAISSE DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
          
            TempData["Data"] = (from c in db.FN_Etat_Solde_Caisse(filtre.DateDebut, filtre.CDateFin)
                                orderby c.Caisse
                                select new EtatController.JqwAjaxEtatSolde
                                {
                                    id = c.ID_Caisse,
                                    libelle = c.Caisse,
                                    initial = c.Initial,
                                    vente = c.Vente,
                                    encaissement = c.Encaissement,
                                    decaissement = c.Decaissement,
                                    depense = c.Depense,
                                    regularisation = c.Regulation_Caisse,
                                    ristourne = c.Ristourne,
                                    solde = c.Solde
                                }).ToList();
            service.TEG_IMPRESSION(Impression.Libelle.Replace(' ', '_'), Impression, "~/Views/Etat/PDFEtatSoldeCaisse.cshtml", this, true);

        }

        public void ExporterHistoriqueSoldeCaisse(int id)
        {

            StoreEntities db = new StoreEntities();
            CaisseVM Impression = new CaisseVM();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            string Libelle = string.Empty;
            TempData["Libelle"] = Libelle = db.TP_Caisse.Find(id).Libelle;
            Impression.Libelle = "HISTORIQUE DE LA CAISSE " + Libelle + " DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");

            TempData["Data"] = (from c in db.FN_Historique_Solde_Caisse(id)
                           where (c.Date > filtre.DateDebut && c.Date <= filtre.CDateFin)
                           select new HistoriqueController.JqwAjax
                                {
                                    id = c.Type,
                                    date = c.Date,
                                    mouvement = c.Mouvement,
                                    numero = c.Numero,
                                    rang = c.Rang,
                                    montant = c.Quantite,
                                    solde_progressif = c.Stock_Porgressif
                                }).ToList();
            service.TEG_IMPRESSION(Impression.Libelle.Replace(' ', '_'), Impression, "~/Views/Historique/PDFHistoriqueSoldeCaisse.cshtml", this, true);

        }
        #endregion
      
        #region Export solde ristourne 
        public void ExporterEtatSoldeRistourne()
        {
            StoreEntities db = new StoreEntities();
            Ristourne_ClientVM Impression = new Ristourne_ClientVM();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            Impression.Client = "SOLDE DES RISTOURNES DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");
            var ristourne = db.TP_Ristourne_Client.Where(p=>p.Actif == true).Select(p => p.ID_Client).Distinct().ToList();
            TempData["Data"] = (from c in db.FN_Etat_Solde_Ristourne_Client(filtre.DateDebut, filtre.CDateFin)
                                orderby c.Client
                                where ristourne.Contains(c.ID_Client)
                                select new EtatController.JqwAjaxEtatSolde
                                {
                                    id = c.ID_Client,
                                    libelle = c.Client,
                                    initial = c.Initial,
                                    vente = c.Vente,
                                    reglement = c.Reglement,
                                    solde = c.Solde
                                }).ToList();
            service.TEG_IMPRESSION(Impression.Client.Replace(' ', '_'), Impression, "~/Views/Etat/PDFEtatSoldeRistourne.cshtml", this, true);

        }

        public void ExporterHistoriqueSoldeRistourne(int id)
        {

            StoreEntities db = new StoreEntities();
            Ristourne_ClientVM  Impression = new Ristourne_ClientVM ();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            string Libelle = Libelle = db.TP_Client.Find(id).Nom;
            Impression.Client = "HISTORIQUE DES RISTOURNES DU CLIENT " + Libelle + " DU " + filtre.DateDebut.ToString("dd/MM/yyyy") + " AU " + filtre.DateFin.ToString("dd/MM/yyyy");

            TempData["Data"] = (from c in db.FN_Historique_Solde_Caisse(id)
                                where (c.Date > filtre.DateDebut && c.Date <= filtre.CDateFin)
                                select new HistoriqueController.JqwAjax
                                {
                                    id = c.Type,
                                    date = c.Date,
                                    mouvement = c.Mouvement,
                                    numero = c.Numero,
                                    rang = c.Rang,
                                    montant = c.Quantite,
                                    solde_progressif = c.Stock_Porgressif
                                }).ToList();
            service.TEG_IMPRESSION(Impression.Client.Replace(' ', '_'), Impression, "~/Views/Historique/PDFHistoriqueSoldeRistourne.cshtml", this, true);

        }
        #endregion

        public void Rapport_Cloture_Caisse(int id)
        {
            Ouverture_Cloture_CaisseBM ObjetBM = new Ouverture_Cloture_CaisseBM();
            Ouverture_Cloture_CaisseVM Return_Var = ObjetBM.GetEtatClorureById(id);
            service.TEG_IMPRESSION("Cloture_caisse", Return_Var, "~/Views/Ouverture_Cloture_Caisse/pdfConsulter.cshtml", this, true);
            //service.HTMLToPdf("Cloture_caisse", this, "~/Views/Ouverture_Cloture_Caisse/pdfConsulter.cshtml", Return_Var, true);
        }


        public void FicheInventaire(int id)
        {
            InventaireBM ObjetBM = new InventaireBM();
            InventaireVM Return_Var = ObjetBM.GetByID(id);
            service.TEG_IMPRESSION("Inventaire_DU_"+ Return_Var.Date_Creation.ToLongDateString() +"", Return_Var, "~/Views/Inventaire/pdfConsulter.cshtml", this, true);
        }

        [ActionName("Caisse_Ouverte")]
        public JsonResult Caisse_Ouverte()
        {
            StoreEntities db = new StoreEntities();
            int tab = db.TP_Ouverture_Cloture_Caisse.Where(p=>p.Etat == 0).Count();
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        public void Send(int id)
        {
            Ouverture_Cloture_CaisseBM ObjetBM = new Ouverture_Cloture_CaisseBM();
            Ouverture_Cloture_CaisseVM Return_Var = ObjetBM.GetEtatClorureById(id);
            string[] Email = { "teguia.herve@gmail.com" };
            string[] Libelle = { "teste"};
            string Titre = "Achat Numéro : ";
            string Body = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Ouverture_Cloture_Caisse/pdfConsulter.cshtml", Return_Var);
            service.SendEmail(Titre, Body, Email, Libelle);
        }

        public JsonResult ResetDatabase()
        {
            try
            {
                StoreEntities db = new StoreEntities();
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Vente_Liquide_Temp");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Vente_Liquide");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Vente_Emballage_Temp");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Vente_Emballage");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Vente");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Ristourne_Client");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Retour_Emballage_Temp");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Retour_Emballage_Detail");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Retour_Emballage");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Regulation_Stock");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Regulation_Solde");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Ouverture_Cloture_Caisse");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Inventaire_Detail");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Inventaire");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Encaissement");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Decaissement");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Commande_Liquide_Temp");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Commande_Liquide");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Commande_Emballage_Temp");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Commande_Emballage");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Commande");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Client WHERE ID_Client > 1");
                db.Database.ExecuteSqlCommand("UPDATE TP_Client SET Create_Code_User = 1, Edit_Code_User = 1 WHERE ID_Client = 1");
                db.Database.ExecuteSqlCommand("DELETE FROM TG_Profiles_Utilisateurs WHERE ID_Profiles_Utilisateur > 2");
                db.Database.ExecuteSqlCommand("UPDATE TG_Profiles_Utilisateurs SET Create_Code_User = 1, Edit_Code_User = 1 WHERE ID_Profiles_Utilisateur = 1");
                db.Database.ExecuteSqlCommand("DELETE FROM TG_Utilisateurs WHERE Code_User > 2");
                db.Database.ExecuteSqlCommand("UPDATE TG_Utilisateurs SET Create_Code_User = 1, Edit_Code_User = 1");
                db.Database.ExecuteSqlCommand("DELETE FROM TG_Personnels WHERE ID_Personnel > 2");
                db.Database.ExecuteSqlCommand("UPDATE TG_Personnels SET ID_Caisse = 1");
                db.Database.ExecuteSqlCommand("DELETE FROM TP_Caisse WHERE ID_Caisse > 1");
                db.Database.ExecuteSqlCommand("UPDATE TP_Caisse SET Create_Code_User = 1, Edit_Code_User = 1 WHERE ID_Caisse = 1");
                db.Database.ExecuteSqlCommand("DELETE FROM TG_Logs");

                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Vente_Liquide_Temp, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Vente_Liquide, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Vente_Emballage_Temp, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Vente_Emballage, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Vente, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Ristourne_Client, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Retour_Emballage_Temp, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Retour_Emballage_Detail, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Retour_Emballage, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Regulation_Stock, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Regulation_Solde, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Ouverture_Cloture_Caisse, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Inventaire_Detail, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Inventaire, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Encaissement, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Decaissement, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Commande_Liquide_Temp, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Commande_Liquide, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Commande_Emballage_Temp, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Commande_Emballage, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Commande, RESEED, 0)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Client, RESEED, 1)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TG_Profiles_Utilisateurs, RESEED, 2)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TG_Utilisateurs, RESEED, 2)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TG_Personnels, RESEED, 2)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TP_Caisse, RESEED, 1)");
                db.Database.ExecuteSqlCommand("DBCC CHECKIDENT(TG_Logs, RESEED, 0)");

                TempData["messagesucess"] = "Renitialisation effectuée avec succès";
                string[] tab = new string[2];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Sauvegarde/Ajouter/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}
