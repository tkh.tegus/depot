﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Tools;

namespace PROJETMODELE.Controllers
{
     [Authorize]
    public class ProfilesController : Controller
    {
        Services service = new Services ( );
        StoreEntities db = new StoreEntities();
        public class JqwProfiles
        {
            public string id { get; set; }
            public string Libelle { get; set; }
            public String actif { get; set; }
            public bool available { get; set; }
        }

        public JsonResult ajax ()
        {
            StoreEntities db = new StoreEntities ( );
            var Table = new List<JqwProfiles>
            {
            };
            var resultat = from c in db.TG_Profiles where c.Code_Profile != 1 && c.Display == true orderby c.Actif descending, c.Libelle select c;
            foreach (TG_Profiles Profile in resultat)
            {
                Table.Add ( new JqwProfiles ( ) 
                { 
                    id = Profile.Code_Profile.ToString ( ), Libelle = Profile.Libelle, actif = (Profile.Actif ? "Actif" : "Suspendu"), available = false } );
            }
            return this.Json ( Table, JsonRequestBehavior.AllowGet );
        }



        //
        // GET: /Profiles/

        public JsonResult Index ()
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_profiles", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Profiles/Index.cshtml", null);
            tab[1] = "GESTION DES PROFILS";
            tab[2] = "<li><a class='more' href='/Home/Index''>Accueil</a></li> <li><a href='#'>Administration</a></li> <li class='active'><a href='#'>Listes des profils</a></li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Profiles/Details/5
        [ActionName ( "Consulter" )]
        public ActionResult Consulter ( int id )
        {
            return View ( );
        }

        //
        // GET: /Profiles/Ajouter
        [ActionName ( "Ajouter" )]
        public JsonResult Ajouter ()
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Ajouter_Profile", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            // // envoie de la liste des utilisateurs
            //TempData["User_Out_Item"] = new SelectList(from p in db.TG_Utilisateurs join C_Perso in db.TG_Personnels on p.ID_Personnel equals C_Perso.ID_Personnel where (p.Actif == true) select (new { p.Code_User, C_Perso.Nom}), "Code_User", "Nom");

            //// envoie de la liste des Services l'ors de l'ajout cette liste ne doit contenir aucun profil c'est pour ca quon a (p.Code_Profile==-1)
            //TempData["User_In_Item"] = new SelectList(from p in db.TG_Utilisateurs join C_Perso in db.TG_Personnels on p.ID_Personnel equals C_Perso.ID_Personnel where (p.Code_User == -1) select (new { p.Code_User, C_Perso.Nom }), "Code_User", "Nom");
            
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Profiles/Ajouter.cshtml", null);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Profiles/Ajouter
        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(Profiles Profiles)
        {

            try
            {

                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    string messageRetour = string.Empty;
                    ProfilesBM ObjetBMRetunr = new ProfilesBM();
                    if (ObjetBMRetunr.IsNotExist(Profiles.Libelle, ref messageRetour)) //vérifie si le Libelle existe déjà
                    {
                        string Liste_ancient = ""; // Request.Form.Get("ancient");
                        string Liste_nouveau = "";// Request.Form.Get("profile");

                        ObjetBMRetunr.Save(Profiles, Liste_nouveau, Session["Code_User"].ToString());
                        TempData["messagesucess"] = "Ajout effectué avec succès";
                        string[] tab = new string[2];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string[] tab = new string[2];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    tab[1] = "";
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Profiles/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Profiles/Edit/5
        [ActionName("Modifier")]
        public JsonResult Modifier(int id)
        {

            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Modifier_Profile", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            //// envoie de la liste des utilisateurs non lié au profil
            //IEnumerable<TG_Utilisateurs> User_Non_Lier = from All_Use in db.TG_Utilisateurs
            //                                             let User_In = from _use in db.TG_Profiles_Utilisateurs
            //                                                           where _use.Code_Profile == id
            //                                                           select _use.Code_User
            //                                             where User_In.Contains(All_Use.Code_User) == false
            //                                             select All_Use;

            //// envoie de la liste des utilisateurs  lié au profil
            //IEnumerable<TG_Utilisateurs> User_Lier = from All_Use in db.TG_Utilisateurs
            //                                         let User_In = from _use in db.TG_Profiles_Utilisateurs
            //                                                       where _use.Code_Profile == id
            //                                                       select _use.Code_User
            //                                         where User_In.Contains(All_Use.Code_User) == true
            //                                         select All_Use;


            //// personnel_user non lier
            //TempData["User_Out_Item"] = new SelectList(from p in User_Non_Lier
            //                                       join C_Perso in db.TG_Personnels on p.ID_Personnel equals C_Perso.ID_Personnel
            //                                       where (p.Actif == true)
            //                                       select (new { p.Code_User, C_Perso.Nom }), "Code_User", "Nom");

            //// personnel_user lier
            //TempData["User_In_Item"] = new SelectList(from p in User_Lier
            //                                      join C_Perso in db.TG_Personnels on p.ID_Personnel equals C_Perso.ID_Personnel
            //                                      where (p.Actif == true)
            //                                      select (new { p.Code_User, C_Perso.Nom }), "Code_User", "Nom");




            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(id, "TG_Profiles", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }

            ProfilesBM Profile = new ProfilesBM();
            Profiles returnProfile = Profile.GetByID(id);
            if (returnProfile == null)
            {
                TempData["messagewarning"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Profiles/Modifier.cshtml", returnProfile);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        // POST: /Grades/Modifier/5

        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(int id, Profiles Profiles)
        {

            try
            {
                if (ModelState.IsValid)//ModelState.IsValid
                {
                    string messageRetour = string.Empty;
                    // TODO: Add update logic here
                    ProfilesBM Profile = new ProfilesBM();
                    if (Profile.IsNotExist(Profiles.Libelle, id, ref messageRetour)) //vérifie si le Profile existe déjà
                    {
                        string ancient = ""; // Request.Form.Get("ancient");
                        string profile = ""; // Request.Form.Get("profile");
                        Profile.Edit(id, Profiles, ancient, profile, Session["Code_User"].ToString());

                        // libère le verrou précédemment pauser
                        Locked_RecordsBM ver = new Locked_RecordsBM();
                        ver.Liberer(id, "TG_Profiles");
                        TempData["messagesucess"] = "Modification effectuée avec succès";
                        string[] tab = new string[2];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["messagewarning"] = messageRetour;
                        string[] tab = new string[1];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    TempData["messagewarning"] = "Le Modele n'est pas valide ";
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Profiles/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Grades/Delete/5
        [ActionName("Supprimer")]
        public JsonResult Supprimer(string id)
        {
            string[] tab = id.Split(';');
            for (int i = 0; i < tab.Length; i++)
            {
                ProfilesBM Grade = new ProfilesBM();
                Grade.Delete(int.Parse(tab[i]), Session["Code_User"].ToString());
            }
            TempData["messagesucess"] = "Action effectuée avec succès";
            string[] tab1 = new string[1];
            tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
            return this.Json(tab1, JsonRequestBehavior.AllowGet);

        }

        [ActionName ( "Activer" )]
        public ActionResult Activer ( string id )
        {
            string[] tab_Habillitation = new string[1];
            if (!service.VerifierHabiliationCRUD("Activer_suspendre_Profile", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                string[] allid = id.Split ( ';' );
                for (int i = 0; i < allid.Length; i++)
                {
                    ProfilesBM Profile = new ProfilesBM ( );
                    Profile.Activer(int.Parse(allid[i]), Session["Code_User"].ToString());
                }
                return View ( );
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM ( );
                Log.Save(this, ex, "POST: /Profiles/Activer/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une erreur est survenue durant l'activation";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView ( ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null );
                return this.Json ( tab, JsonRequestBehavior.AllowGet );
            }
        }


    }
}

