﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.BML.TP;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class Retour_EmballageController : Controller
    {
        Services service = new Services();
        public class JqwAjax
        {
            public Int64 id { get; set; }

            public int ID_Element { get; set; }
            public int Type_Element { get; set; }
            public string Libelle
            {
                get
                {
                    StoreEntities db = new StoreEntities();
                    if (Type_Element == 0)
                    {
                        return db.TP_Fournisseur.Where(p => p.ID_Fournisseur == ID_Element).Select(p => p.Nom).FirstOrDefault();
                    }
                    else
                    {
                        return db.TP_Client.Where(p => p.ID_Client == ID_Element).Select(p => p.Nom).FirstOrDefault();
                    }
                    
                }
                set { }
            }
            public double Montant { get; set; }
            public string NUM_DOC { get; set; }
            public DateTime Date { get; set; }
            public string Statut { get; set; }
            public string Utilisateur { get; set; }
        }
        public JsonResult ajax(byte id)
        {
            StoreEntities db = new StoreEntities();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.FN_Retour_Emballage()
                           where (c.Date >= filtre.DateDebut && c.Date < filtre.CDateFin) && c.Type_Element == id
                           orderby c.Etat, c.Date descending
                           select new JqwAjax
                           {
                               id = c.ID_Retour_Emballage,
                               ID_Element = c.ID_Element,
                               Type_Element = c.Type_Element,
                               NUM_DOC = c.NUM_DOC,
                               Date = c.Date,
                               Montant = c.Montant,
                               Statut = (c.Etat == 0) ? "En attende validation" : (c.Etat == 1) ? "Valider" : "Annuler",
                               //Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == c.Code_User).Select(p => p.Libelle).FirstOrDefault()
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Index(int id)
        {
            //id = 0 Retour Fournisseur
            //id = 1 Retour Client
            string[] tab_Habillitation = new string[3];
            if ((id == 0 && !service.VerifierHabiliationIndex("Gestion_des_retour_emballage_fournisseur", this, ref tab_Habillitation)) || (id == 1 && !service.VerifierHabiliationIndex("Gestion_des_retour_emballage_client", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            var ouverture = db.TP_Ouverture_Cloture_Caisse.Where(p => p.ID_Caisse == ID_Caisse && p.Etat == 0).FirstOrDefault();
            if (ouverture != null)
            {
                Session["DateDebut"] = ouverture.Date_Ouverture.ToString();
            }
            else
            {
                Session["DateDebut"] = DateTime.Today.ToString();
            }
            Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            TempData["id"] = id;
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Retour_Emballage/Index.cshtml", filtre);
            tab[1] = "GESTION DES RETOUR EMBALLAGE";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Donnée de Base</a></li> <li class='active'>Client</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ActionName("Ajouter")]
        public JsonResult Ajouter(int id)
        {
            //id = 0 Retour Fournisseur
            //id = 1 Retour Client
            string[] tab_Habillitation = new string[1];
            if ((id == 0 && !service.VerifierHabiliationCRUD("Ajouter_des_retour_emballage_fournisseur", this, ref tab_Habillitation)) || (id == 1 && !service.VerifierHabiliationCRUD("Ajouter_des_retour_emballage_client", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            StoreEntities db = new StoreEntities();
            if (id == 0)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Fournisseur
                                                        where c.Actif
                                                        select new
                                                        {
                                                            ID_Fournisseur = c.ID_Fournisseur,
                                                            Libelle = c.Nom
                                                        }, "ID_Fournisseur", "Libelle");
            }
            else
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Client
                                                        where c.Actif
                                                        select new
                                                        {
                                                            ID_Fournisseur = c.ID_Client,
                                                            Libelle = c.Nom
                                                        }, "ID_Fournisseur", "Libelle");
            }
            
            Retour_EmballageVM Return_Var = new Retour_EmballageVM();
            Return_Var.Type_Element = (byte)id;
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Retour_Emballage/Ajouter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Ajouter")]
        public JsonResult Ajouter(Retour_EmballageVM Param_Var)
        {
            Ouverture_Cloture_CaisseBM caisse = new Ouverture_Cloture_CaisseBM();
            int ID_Caisse = int.Parse(Session["ID_Caisse"].ToString());
            if (!caisse.IsCaisseOpen(ID_Caisse))
            {
                TempData["messagewarning"] = "Veuillez ouvrir une caisse";
                string[] tab = new string[2];//2 pour savoir que c'est ok
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }

            try
            {
                
                Retour_EmballageBM Local_Var = new Retour_EmballageBM();
                if (ModelState.IsValid)
                {
                    if (Param_Var.Qte_Casier_12 == 0 && Param_Var.Qte_Plastique_12 == 0 && Param_Var.Qte_Bouteille_12 == 0 && Param_Var.Qte_Casier_15 == 0 && Param_Var.Qte_Plastique_15 == 0 && Param_Var.Qte_Bouteille_15 == 0 && Param_Var.Qte_Casier_24 == 0 && Param_Var.Qte_Plastique_24 == 0 && Param_Var.Qte_Bouteille_24 == 0)
                    {
                        string[] tab = new string[1];
                        TempData["messageerror"] = "Veuillez renseigner ou moins une des quantité ";
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);

                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string messageRetour = string.Empty;
                        Int64 id = Local_Var.Save(Param_Var, Session["Code_User"].ToString());
                        TempData["messagesucess"] = "Ajout effectué avec succès";
                        string[] tab = new string[2];//2 pour savoir que c'est ok
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = id.ToString();
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    
                }
                else
                {
                    string[] tab = new string[1];
                    TempData["messageerror"] = "Le modéle n'est pas valide ";
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Retour_Emballage/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[2];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab[1] = "";
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName("Modifier")]
        public JsonResult Modifier(string id)
        {
            int ID_ = int.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            //id = 0 Retour Fournisseur
            //id = 1 Retour Client
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Modifier_des_retour_emballage_fournisseur", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Modifier_des_retour_emballage_client", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Locked_RecordsBM veroux = new Locked_RecordsBM();
            string message = "";
            if (veroux.IsVerouller(ID_, "TP_Retour_Emballage", ref message, ControllerContext, TempData, Session["Code_User"].ToString()))
            {
                string[] table = new string[1];
                table[0] = message;
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }

            Retour_EmballageBM Local_Var = new Retour_EmballageBM();
            Retour_EmballageVM Return_Var = Local_Var.GetByID(ID_);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            
            StoreEntities db = new StoreEntities();
            if (Return_Var.Type_Element == 0)
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Fournisseur
                                                        where c.Actif
                                                        select new
                                                        {
                                                            ID_Fournisseur = c.ID_Fournisseur,
                                                            Libelle = c.Nom
                                                        }, "ID_Fournisseur", "Libelle");
            }
            else
            {
                TempData["ID_Element"] = new SelectList(from c in db.TP_Client
                                                        where c.Actif
                                                        select new
                                                        {
                                                            ID_Fournisseur = c.ID_Client,
                                                            Libelle = c.Nom
                                                        }, "ID_Fournisseur", "Libelle");
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Retour_Emballage/Modifier.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ActionName("Modifier")]
        [ValidateAntiForgeryToken]
        public JsonResult Modifier(string id, Retour_EmballageVM Param_Var)
        {
            Int64 ID_ = Int64.Parse(id.Split(';')[1]);
            int type = int.Parse(id.Split(';')[0]);
            try
            {
                if (ModelState.IsValid)
                {
                    if (Param_Var.Qte_Casier_12 == 0 && Param_Var.Qte_Plastique_12 == 0 && Param_Var.Qte_Bouteille_12 == 0 && Param_Var.Qte_Casier_15 == 0 && Param_Var.Qte_Plastique_15 == 0 && Param_Var.Qte_Bouteille_15 == 0 && Param_Var.Qte_Casier_24 == 0 && Param_Var.Qte_Plastique_24 == 0 && Param_Var.Qte_Bouteille_24 == 0)
                    {
                        string[] tab = new string[1];
                        TempData["messageerror"] = "Veuillez renseigner ou moins une des quantité ";
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);

                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string messageRetour = string.Empty;
                        // TODO: Add update logic here
                        Retour_EmballageBM Return_Var = new Retour_EmballageBM();
                        Return_Var.Edit(ID_, Param_Var, Session["Code_User"].ToString());
                        // libère le verrou précédemment pauser
                        Locked_RecordsBM veroux = new Locked_RecordsBM();
                        veroux.Liberer(ID_, "TP_Retour_Emballage");
                        TempData["messagesucess"] = "Modification effectuée avec succès";
                        string[] tab = new string[2];
                        tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                        tab[1] = "";
                        return this.Json(tab, JsonRequestBehavior.AllowGet);
                    }
                   

                }
                else
                {
                    TempData["messagewarning"] = "Le Modele n'est pas valide ";
                    string[] tab = new string[1];
                    tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                    return this.Json(tab, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Retour_Emballage/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName("Valider")]
        public JsonResult Valider(string id)
        {
            int type = int.Parse(id.Split(';')[0]);
            Int64 ID_Retour = int.Parse(id.Split(';')[1]);
            //id = 0 Retour Fournisseur
            //id = 1 Retour Client
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Valider_des_retour_emballage_fournisseur", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Valider_des_retour_emballage_client", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                Retour_EmballageBM Local_Var = new Retour_EmballageBM();
                Local_Var.Valider(ID_Retour, Session["Code_User"].ToString());

                TempData["messagesucess"] = "Valider effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Retour_Emballage/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
            

        }

        [ActionName("Annuler")]
        public JsonResult Annuler(string id)
        {
            int type = int.Parse(id.Split(';')[0]);
            Int64 ID_Retour = int.Parse(id.Split(';')[1]);
            //id = 0 Retour Fournisseur
            //id = 1 Retour Client
            string[] tab_Habillitation = new string[1];
            if ((type == 0 && !service.VerifierHabiliationCRUD("Annuler_des_retour_emballage_fournisseur", this, ref tab_Habillitation)) || (type == 1 && !service.VerifierHabiliationCRUD("Annuler_des_retour_emballage_client", this, ref tab_Habillitation)))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            try
            {
                Retour_EmballageBM Local_Var = new Retour_EmballageBM();
                Local_Var.Annuler(ID_Retour, Session["Code_User"].ToString());
                TempData["messagesucess"] = "Action effectuée avec succès";
                string[] tab1 = new string[2];//2 pour savoir que c'est ok
                tab1[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                tab1[1] = "";
                return this.Json(tab1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //persist les logs
                LogsBM Log = new LogsBM();
                Log.Save(this, ex, "POST: /Retour_Emballage/Modifier/", Session["Code_User"].ToString());
                TempData["messageerror"] = "Une exception généré ";
                string[] tab = new string[1];
                tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(tab, JsonRequestBehavior.AllowGet);
            }
           
        }

        [ActionName("Consulter")]
        public JsonResult Consulter(Int64 id)
        {
            TempData["id"] = id;
            Retour_EmballageBM Local_Var = new Retour_EmballageBM();
            Retour_EmballageVM Return_Var = Local_Var.GetByID(id);
            if (Return_Var == null)//aucun element trouve
            {
                TempData["messagenotification"] = "Aucun élément correspondant";
                string[] table = new string[1];
                table[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Shared/_Message.cshtml", null);
                return this.Json(table, JsonRequestBehavior.AllowGet);
            }
            string[] tab = new string[1];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Retour_Emballage/Consulter.cshtml", Return_Var);
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
    }
}
