﻿using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PROJETMODELE.Models.VML.TG;

namespace PROJETMODELE.Controllers
{
    [Authorize]
    public class EtatController : Controller
    {
        Services service = new Services();
        public class Element 
        {          
            public byte ID_Element { get; set; }
            public string Libelle { get; set; }
        }

        public class JqwAjaxEtatSolde
        {
            public int id { get; set; }
            public string libelle { get; set; }
            public double initial { get; set; }
            public double vente { get; set; }
            public double commande { get; set; }
            public double versement { get; set; }
            public double encaissement { get; set; }
            public double regularisation { get; set; }
            public double solde { get; set; }
            public double reglement { get; set; }
            public double retour_emballage { get; set; }
            public double decaissement { get; set; }
            public double depense { get; set; }
            public double ristourne { get; set; }
            // stock
            public double casier { get; set; }
            public double plastique { get; set; }
            public double bouteil { get; set; }
            public int? valeur { get; set; }
            public string code { get; set; }
            public int?  valeur_Achat { get; set; }
            public int? valeur_Plastique { get; set; }
        }

        #region Etat Solde Client

        public JsonResult ajaxEtatSoldeClient()
        {
            StoreEntities db = new StoreEntities();
            byte ID_Element = byte.Parse(Session["ID_Element"].ToString());
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.FN_Etat_Solde_Client(ID_Element, filtre.DateDebut, filtre.CDateFin)
                           orderby c.Client
                           select new JqwAjaxEtatSolde
                           {
                               id = c.ID_Client,
                               libelle = c.Client,
                               initial = c.Initial,
                               vente = c.Vente,
                               versement = c.Versement,
                               encaissement = c.Encaissement,
                               retour_emballage = c.Retour_emballage,
                               regularisation = c.Regulation,
                               solde = c.Solde
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IndexSoldeClient()
        {
           
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_etat_solde_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }

            Session["DateDebut"] = DateTime.Today.AddDays(-7).ToString();
            Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            var liste = new List<Element>
            {
                new Element { ID_Element = 0, Libelle = "Liquide" },
                new Element { ID_Element = 1, Libelle = "Emballage" },
                new Element { ID_Element = 2, Libelle = "Liquide & Emballage" },
            };
            TempData["Type"] = new SelectList(liste, "ID_Element", "Libelle");
            Session["ID_Element"] = ((SelectList)TempData["Type"]).FirstOrDefault() == null ? (byte)0 : byte.Parse(((SelectList)TempData["Type"]).FirstOrDefault().Value.ToString());

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Etat/IndexSoldeClient.cshtml", filtre);
            tab[1] = "ETAT SOLDE CLIENT";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Etat</a></li> <li class='active'>Etat Solde Client</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
   
        #endregion

        #region Etat Solde Fournisseur

        public JsonResult ajaxEtatSoldeFornisseur()
        {
            StoreEntities db = new StoreEntities();
            byte ID_Element = byte.Parse(Session["ID_Element"].ToString());
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.FN_Etat_Solde_Fournisseur(ID_Element, filtre.DateDebut, filtre.CDateFin)
                           orderby c.Fournisseur
                           select new JqwAjaxEtatSolde
                           {
                               id = c.ID_Fournisseur,
                               libelle = c.Fournisseur,
                               initial = c.Initial,
                               commande = c.Commande,
                               reglement = c.Reglement,
                               regularisation = c.Regulation_Liquide,
                               retour_emballage = c.Retour_Emballage,
                               solde = c.Solde
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IndexSoldeFournisseur()
        {
           
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_etat_solde_Fournisseur", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Session["DateDebut"] = DateTime.Today.AddDays(-7).ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            var liste = new List<Element>
            {
                new Element { ID_Element = 0, Libelle = "Liquide" },
                new Element { ID_Element = 1, Libelle = "Emballage" },
                new Element { ID_Element = 2, Libelle = "Liquide & Emballage" },
            };
            TempData["Type"] = new SelectList(liste, "ID_Element", "Libelle");
            Session["ID_Element"] =  byte.Parse(((SelectList)TempData["Type"]).FirstOrDefault().Value.ToString());

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Etat/IndexSoldeFournisseur.cshtml", filtre);
            tab[1] = "ETAT SOLDE FOURNISSEUR";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Etat</a></li> <li class='active'>Etat Solde Fournisseur</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Etat Solde Caisse

        public JsonResult ajaxEtatSoldeCaisse()
        {
            StoreEntities db = new StoreEntities();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.FN_Etat_Solde_Caisse(filtre.DateDebut, filtre.CDateFin)
                           orderby c.Caisse
                           select new JqwAjaxEtatSolde
                           {
                               id = c.ID_Caisse,
                               libelle = c.Caisse,
                               initial = c.Initial,
                               vente = c.Vente,
                               encaissement = c.Encaissement,
                               decaissement = c.Decaissement,
                               depense = c.Depense,
                               regularisation = c.Regulation_Caisse,
                               ristourne = c.Ristourne,
                               solde = c.Solde
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IndexSoldeCaisse()
        {
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_etat_caisse", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Session["DateDebut"] = DateTime.Today.AddDays(-7).ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Etat/IndexSoldeCaisse.cshtml", filtre);
            tab[1] = "ETAT SOLDE CAISSE";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Etat</a></li> <li class='active'>Etat Solde Caisse</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Etat Solde Ristourne

        public JsonResult ajaxEtatSoldeRistourne()
        {
            StoreEntities db = new StoreEntities();
            var ristourne = db.TP_Ristourne_Client.Where(p=>p.Actif == true).Select(p => p.ID_Client).Distinct().ToList();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.FN_Etat_Solde_Ristourne_Client(filtre.DateDebut, filtre.CDateFin)
                           orderby c.Client
                           where ristourne.Contains(c.ID_Client)
                           select new JqwAjaxEtatSolde
                           {
                               id = c.ID_Client,
                               libelle = c.Client,
                               initial = c.Initial,
                               vente = c.Vente,
                               reglement = c.Reglement,
                               solde = c.Solde
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IndexSoldeRistourne()
        {
           
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_etat_ristourne_client", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Session["DateDebut"] = DateTime.Today.AddDays(-7).ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Etat/IndexSoldeRistourne.cshtml", filtre);
            tab[1] = "ETAT SOLDE RISTOURNE CLIENT";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Etat</a></li> <li class='active'>Etat Solde Ristourne</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Etat Stock
        public JsonResult ajaxEtatStock()
        {
            StoreEntities db = new StoreEntities();
            int ID_Element = int.Parse(Session["ID_Element"].ToString());
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());
            var resultat = from c in db.FN_Etat_Stock(ID_Element, filtre.DateDebut, filtre.CDateFin)
                           where ID_Element == 1 ? (c.ID_Element != 5 && c.ID_Element != 4) : true
                           orderby c.Code
                           select new JqwAjaxEtatSolde
                           {
                               id = c.ID_Element,
                               libelle = c.Libelle,
                               code = c.Code,
                               valeur = (ID_Element == 0 ? c.Valeur_Final_Vente : c.Valeur_Final_Emballage) ,
                               valeur_Achat = (ID_Element == 0 ? c.Valeur_Final_Achat : c.Valeur_Final_Emballage) ,
                             
                               casier = c.Stock_Casier,
                               plastique = c.Stock_Plastique,
                               bouteil = c.Stock_Bouteille
                           };
            return this.Json(resultat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IndexEtatStock()
        {
            
            string[] tab_Habillitation = new string[3];
            if (!service.VerifierHabiliationIndex("Gestion_des_etat_stock", this, ref tab_Habillitation))
            {
                return this.Json(tab_Habillitation, JsonRequestBehavior.AllowGet);
            }
            Session["DateDebut"] = DateTime.Today.AddDays(-7).ToString(); Session["DateFin"] = DateTime.Today.ToString();
            Filtre filtre = new Filtre(Session["DateDebut"].ToString(), Session["DateFin"].ToString());

            var liste = new List<Element>
            {
                new Element { ID_Element = 0, Libelle = "Liquide" },
                new Element { ID_Element = 1, Libelle = "Emballage" },
            };
            TempData["Type"] = new SelectList(liste, "ID_Element", "Libelle");           
            Session["ID_Element"] = 0;
            string[] tab = new string[3];
            tab[0] = service.GetHtmlByView(ControllerContext, TempData, "~/Views/Etat/IndexEtatStock.cshtml", filtre);
            tab[1] = "ETAT STOCK";
            tab[2] = " <li><a  class='more' href='/Home/Index'>Accueil</a></li> <li><a href='#'>Etat</a></li> <li class='active'>Etat Stock</li>";
            return this.Json(tab, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
