﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;
using iTextSharp.text;
using PROJETMODELE.Models.DAL;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using System.Net.Mail;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using iTextSharp.text.pdf;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Models.BML.TG;
using PROJETMODELE.Models.VML.TG;

namespace PROJETMODELE.Tools
{
    public class Services
    { 
        public string FormatBytes(long bytes)
        {
            if (bytes >= 0x1000000000000000) { return ((double)(bytes >> 50) / 1024).ToString("0.### EB"); }
            if (bytes >= 0x4000000000000) { return ((double)(bytes >> 40) / 1024).ToString("0.### PB"); }
            if (bytes >= 0x10000000000) { return ((double)(bytes >> 30) / 1024).ToString("0.### TB"); }
            if (bytes >= 0x40000000) { return ((double)(bytes >> 20) / 1024).ToString("0.### GB"); }
            if (bytes >= 0x100000) { return ((double)(bytes >> 10) / 1024).ToString("0.### MB"); }
            if (bytes >= 0x400) { return ((double)(bytes) / 1024).ToString("0.###") + " KB"; }
            return bytes.ToString("0 Bytes");
        }
      
        public string RemoveDiacritics(string stIn)
        {
            string stFormD = stIn.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int ich = 0; ich < stFormD.Length; ich++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(stFormD[ich]);
                }
            }
            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }
       
    
        public void SetPathImage(byte[] IMAGE,string filename)
        {
            MemoryStream ms = new MemoryStream(IMAGE);
            System.Drawing.Image Image = System.Drawing.Bitmap.FromStream(ms);
            Image.Save(filename);
        }
        public byte[] GetImage(string filePath)
        {
            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            byte[] img = br.ReadBytes((int)fs.Length);
            br.Close();
            fs.Close();
            return img;
        }
        public string GetHtmlByView(ControllerContext Controlcontex, TempDataDictionary tempData, string cheminView, Object hydrater)
        {
            var content = string.Empty;
            var view = ViewEngines.Engines.FindView(Controlcontex, cheminView, null);
            using (var writer = new StringWriter())
            {
                var context = new ViewContext(Controlcontex, view.View, new ViewDataDictionary(hydrater), tempData, writer);
                view.View.Render(context, writer);
                writer.Flush();
                return writer.ToString();
            }
        }
        public StringBuilder GetStringAjax(object data)
        {
            StringBuilder strg = new StringBuilder("");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.RecursionLimit = serializer.MaxJsonLength = 2147483647;
            serializer.Serialize(data, strg);
            return strg;
        }

        public void WriteLog(Controller controller, string action, Exception ex, string Code_User)
        {
            StoreEntities db = new StoreEntities();
            var personnel = Code_User == "0" ? db.TG_Personnels.ToList().FirstOrDefault() : db.TG_Personnels.Find(int.Parse((string)Code_User));
            // Instanciation du StreamWriter avec passage du nom du fichier 
            StreamWriter monStreamWriter = File.AppendText(controller.Server.MapPath("~") + "log.log");
            monStreamWriter.WriteLine("****************************************************************" + DateTime.Now.ToString() + "************************************************************************");
            monStreamWriter.WriteLine("Action :" + action);
            monStreamWriter.WriteLine("Personnel : " + personnel.Nom + (personnel.Prenom == null ? "" : "  " + personnel.Prenom));
            monStreamWriter.WriteLine("Message :" + (ex.InnerException == null ? "" : ex.InnerException.Message) + " ------" + ex.StackTrace);
            // monStreamWriter.WriteLine("****************************************************************************************************************************************************************");
            monStreamWriter.Close();
        }

        public string GetDateFormatddMMyyyy(DateTime date)
        {
            return date.ToString("dd/MM/yyyy");
        }
       
        public bool ConnexionOk(string bd, string user, string pass, string server)
        {
            IDbConnection con = new SqlConnection("Data Source=" + server + ";Initial Catalog=" + bd + ";user id=" + user + ";password=" + pass + ";");
            IDbCommand command = con.CreateCommand();
            command.CommandText = "SELECT * FROM TG_Variables";
            command.CommandType = CommandType.Text;
            bool ok = false;
            try
            {
                con.Open();
                IDataReader li = command.ExecuteReader();
                li.Read();
                ok = true;
            }
            catch (Exception ex)
            {

            }
            finally
            {

                con.Close();
                con.Dispose();
            }
            return ok;
        }

        public void ExportEmploye(Controller controller)
        {
            HTMLToPdfKRT.HTMLToPdf("Personnels", controller, "~/Views/Personnels/pdf.cshtml", null, null, "", null, null, true);

        }
    
        public void GetModele(string name, Controller controller)
        {

            string FilePath = System.IO.Path.Combine(controller.Server.MapPath("/"), "modele/" + name);
            controller.Response.ClearHeaders();
            controller.Response.Clear();
            controller.Response.ClearContent();
            FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
            fs.Close();
            string chaineattach = "attachment;filename= " + name;//fichier.";//"attachment;filename= fichier.pdf";
            string ext = Path.GetExtension(name);
            string contenttype = "application/msword";
            switch (ext)
            {
                case ".csv":
                    contenttype = "application/vnd.ms-excel";
                    break;

            }
            controller.Response.ContentType = contenttype;
            controller.Response.Buffer = true;
            controller.Response.Charset = "";
            controller.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            controller.Response.AddHeader("content-disposition", chaineattach);
            controller.Response.BinaryWrite(bytes);
            controller.Response.Flush();
            controller.Response.End();
        }

        public void GetFile(string fichier, Controller controller)
        {
            string FilePath = System.IO.Path.Combine(controller.Server.MapPath("/"), "Uploads/" + fichier);
            controller.Response.ClearHeaders();
            controller.Response.Clear();
            controller.Response.ClearContent();
            FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
            fs.Close();
            string chaineattach = "attachment;filename= " + fichier;//fichier.";//"attachment;filename= fichier.pdf";
            string ext = Path.GetExtension(fichier);
            string contenttype = "application/msword";
            String name = "";
            switch (ext)
            {
                case ".xls":
                case ".csv":
                    contenttype = "application/vnd.ms-excel";
                    name = "Excell";
                    break;
                case ".pdf":
                    contenttype = "application/pdf";
                    name = "Pdf";
                    break;

            }
            chaineattach = "attachment;filename= " + name + ext;
            controller.Response.ContentType = contenttype;
            controller.Response.Buffer = true;
            controller.Response.Charset = "";
            controller.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            controller.Response.AddHeader("content-disposition", chaineattach);
            controller.Response.BinaryWrite(bytes);
            controller.Response.Flush();
            controller.Response.End();
        }
       

        public void SendEmail(string titre, string body, string[] email, string[] libelle)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("tkh.tegus@gmail.com", "TEG-Facturation");
            // Destinataires (il en faut au moins un)
            for (int i = 0; i < email.Length; i++)
            {
                msg.To.Add(new MailAddress(email[i], libelle[i]));
            }
            // Envoi du message SMTP
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential("tkh.tegus@gmail.com", "password");

            // Envoi du mail
            // Texte du mail (facultatif)
            msg.BodyEncoding = System.Text.Encoding.UTF8;
            msg.IsBodyHtml = true;
            msg.Subject = titre;
            msg.Body = body;
            msg.IsBodyHtml = true;
            try
            {
                client.Send(msg);
            }
            catch (Exception ex)
            {

            }
        }

        public void GetCsvFormat(String chaine, string titre, Controller controller)
        {
            var Items = JsonConvert.DeserializeObject<JArray>(chaine).SelectMany(o => ((JObject)o).Properties()).ToList();
            String ligne = "";
            string File = System.IO.Path.Combine(controller.Server.MapPath("/Uploads"), controller.Session["Code_User"].ToString() + ".csv");
            FileStream Stream = System.IO.File.Open(File, FileMode.Create);
            bool entete = true;
            string enteteS = titre;
            using (TextWriter tw = new StreamWriter(Stream, Encoding.UTF8))
            {
                bool contientDonne = Items.Count > 0;
                double convertisseur = 0;
                foreach (var prop in Items)
                {
                    string strLine = string.Empty;
                    //Parcourt toutes les colonnes
                    if (prop.Name != "uid" && prop.Name != "id" && prop.Name != "dataindex" && prop.Name != "available")//libelle a rétiré
                    {
                        if (ligne == "")
                        {
                            ligne = double.TryParse(prop.Value.ToString(), out convertisseur) ? SeparateurMillier(convertisseur) : prop.Value.ToString();
                        }
                        else
                        {
                            ligne += ";" + (double.TryParse(prop.Value.ToString(), out convertisseur) ? SeparateurMillier(convertisseur) : prop.Value.ToString());
                        }

                    }
                    if (prop.Name == "uid")//c'est une ligne qui va commencé met fin de la ligne precedante
                    {
                        if (entete)//nous sommes au niveau de l'entête
                        {
                            tw.WriteLine(titre);
                            entete = false;
                        }
                        tw.WriteLine(ligne);
                        ligne = "";

                    }
                }
                if (!contientDonne)//si ça ne contient pas de donnée on écrit le titre
                {
                    tw.WriteLine(titre);
                }
            }
        }
        public void WriteFile(String chaine, string titre, Controller controller)
        {
            System.IO.File.WriteAllText(System.IO.Path.Combine(controller.Server.MapPath("/Uploads"), controller.Session["Code_User"].ToString() + ".txt"), chaine);
            System.IO.File.WriteAllText(System.IO.Path.Combine(controller.Server.MapPath("/Uploads"), controller.Session["Code_User"].ToString() + "_.txt"), titre);
        }

        public String ReadFile(Controller controller)
        {
            string titre = System.IO.File.ReadAllText(System.IO.Path.Combine(controller.Server.MapPath("/Uploads"), controller.Session["Code_User"].ToString() + "_.txt"));
            string[] tableau = titre.Split(';');
            titre = "";
            for (int i = 0; i < tableau.Length; i++)
            {
                titre = titre + "<th>" + tableau[i] + "</th>";
            }
            var teste = System.IO.File.ReadAllText(System.IO.Path.Combine(controller.Server.MapPath("/Uploads"), controller.Session["Code_User"].ToString() + ".txt"));
            var Items = JsonConvert.DeserializeObject<JArray>(teste).SelectMany(o => ((JObject)o).Properties()).ToList();
            bool contientDonne = Items.Count > 0;
            double convertisseur = 0;
            String ligne = "";
            bool entete = true;
            string enteteS = "";
            string table = "";
            foreach (var prop in Items)
            {
                string strLine = string.Empty;
                //Parcourt toutes les colonnes
                if (prop.Name != "uid" && prop.Name != "id" && prop.Name != "dataindex" && prop.Name != "available")//libelle a rétiré
                {
                    ligne += "<td>" + (double.TryParse(prop.Value.ToString(), out convertisseur) ? SeparateurMillier(convertisseur) : prop.Value.ToString()) + "</td>";
                    enteteS += "<th>" + prop.Name + "</th>";
                }
                if (prop.Name == "uid")//c'est une ligne qui va commencé met fin de la ligne precedante
                {
                    if (entete)//nous sommes au niveau de l'entête
                    {
                        table = table + "<tr style='text-align:center;color:#333333;font-weight:bold;font-size: 14pt;'>" + titre + "</tr>";
                        entete = false;
                    }
                    table = table + "<tr style='text-align:center;font-size: 10pt;'>" + ligne + "</tr>";
                    ligne = "";

                }
            }
            if (!contientDonne)//si ça ne contient pas de donnée on écrit le titre
            {
                table = "<tr style='text-align:center;color:#333333;font-weight:bold;font-size: 14pt;'>" + titre + "</tr>";
            }
            return "<table border='0.5'>" + table + "</table>";
        }
     
        public void ExportPdf(Controller controller)
        {
            HTMLToPdfKRT.HTMLToPdf("Pdf", controller, "~/Views/Shared/pdf.cshtml", null, null, "", null, null, true);

        }
      
        public string SeparateurMillier(double? Param)
         {
            double montant = (Param == null ? 0 : Param.Value);
            System.Globalization.NumberFormatInfo separateur = new System.Globalization.CultureInfo("en-NZ", false).NumberFormat;
            separateur.NumberGroupSeparator = " ";
            separateur.NumberDecimalDigits = 0;
            string number = montant.ToString("N", separateur);
            //return number.Split('.')[1] == "00" ? number.Split('.')[0] : number;
            return number.Split('.')[0];
        }
      
        public string GetUserNameByCodeUse(int Code_User)
        {
            StoreEntities db = new StoreEntities();
            return (from use in db.Vue_Personnel where use.ID_Personnel == Code_User select (use.Libelle)).FirstOrDefault();
        }

        public string GetDays(DayOfWeek days)
        {
            string jours = "";
            switch (days)
            {
                case DayOfWeek.Monday: jours = "Lundi";
                    break;
                case DayOfWeek.Thursday: jours = "Mardi";
                    break;
                case DayOfWeek.Wednesday: jours = "Mercredi";
                    break;
                case DayOfWeek.Tuesday: jours = "Jeudi";
                    break;
                case DayOfWeek.Friday: jours = "Vendredi";
                    break;
                case DayOfWeek.Saturday: jours = "Samedi";
                    break;
                case DayOfWeek.Sunday: jours = "Dimance";
                    break;
                default: jours = "Lundi";
                    break;
            }
            return jours;
        }

        public string GetFormatIlya(DateTime aujourdhui, DateTime evenement)
        {
            TimeSpan time = new TimeSpan(aujourdhui.Ticks - evenement.Ticks);
            string temps = "";
            if (time.Days >= 7)//si celà depasse 1 semaine  1 Janvier 12:45
            {
                temps = evenement.Day + "  " + GetMonth(evenement.Month) + " " + evenement.ToString("HH:mm");
            }
            else//si c'est moins d'une semainde
            {
                if (time.Days >= 1) //Mardi 15:45
                {
                    temps = GetDays(evenement.DayOfWeek) + " " + evenement.ToString("HH:mm"); ;
                }
                else
                {
                    if (time.Hours >= 1)
                    {
                        temps = "il y a " + time.Hours.ToString() + " Heures";
                    }
                    else if (time.Minutes >= 1)
                    {
                        temps = "il y a " + time.Minutes.ToString() + " minutes";
                    }
                    else
                    {
                        temps = "il y a " + time.Seconds.ToString() + " secondes";
                    }

                }
            }
            return temps;
        }

        public string GetMonth(int mois)
        {
            string jours = "";
            switch (mois)
            {
                case 1: jours = "Janvier";
                    break;
                case 2: jours = "Février";
                    break;
                case 3: jours = "Mars";
                    break;
                case 4: jours = "Avril";
                    break;
                case 5: jours = "Mai";
                    break;
                case 6: jours = "Juin";
                    break;
                case 7: jours = "Juillet";
                    break;
                case 8: jours = "Août";
                    break;
                case 9: jours = "Septembre";
                    break;
                case 10: jours = "Octobre";
                    break;
                case 11: jours = "Novembre";
                    break;
                case 12: jours = "Décembre";
                    break;
                default: jours = "Lundi";
                    break;


            }
            return jours;
        }

        public void HTMLToPdf(string name, Controller controller, string cheminView, Object hydrater, bool? A4)
        {
            StoreEntities db = new StoreEntities();
            string FilePath = System.IO.Path.Combine(controller.Server.MapPath("/"), name);
            string HTML = GetHtmlByView(controller.ControllerContext, controller.TempData, cheminView, hydrater);
            Document document;
            if (A4 == null)
                document = new Document(PageSize.A4.Rotate());
            else
                document = new Document();
            PdfWriter pdfwritter = PdfWriter.GetInstance(document, new FileStream(FilePath, FileMode.Create));
            PDFEvent pdfEvent = new PDFEvent();
            pdfEvent.Filigramme = pdfEvent.piedPage = false;
            /*
            if (societe.Filigrame == true)
            {
                pdfEvent.Filigramme = true;
                string chemin = controller.Server.MapPath("/Images/Logo/");
                string urllogo = System.IO.Path.Combine(chemin, "f_" + societe.Logo_Nom + societe.Logo_Extention);
                pdfEvent.chemin = urllogo;
            }
            if (controller.Session["ID_Societe"].ToString() == "20")
            {
                pdfEvent.piedPage = true;
                string cheminpied = controller.Server.MapPath("/Images/Logo/");
                string urllogopied = System.IO.Path.Combine(cheminpied, "footer-edf.jpg");
                pdfEvent.cheminpied = urllogopied;
            }
            */
           
            pdfwritter.PageEvent = pdfEvent;
            document.Open();
            Font font = new Font(Font.FontFamily.TIMES_ROMAN, 5, Font.NORMAL);
            font.SetColor(12, 42, 45);
            iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
            iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document, null, styles);
            try
            {
                hw.Parse(new StringReader(HTML));
            }
            catch (Exception ex)
            {
            }
            document.Close();
            controller.Response.ClearHeaders();
            controller.Response.Clear();
            controller.Response.ClearContent();
            FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            Byte[] bytes = br.ReadBytes((Int32)fs.Length);
            fs.Close();
            name = name + ".pdf";
            string chaineattach = "attachment;filename= " + name;//fichier.";//"attachment;filename= fichier.pdf";
            string ext = Path.GetExtension(name);
            string contenttype = "application/msword";
            switch (ext)
            {
                case ".pdf":
                    contenttype = "application/pdf";
                    break;

            }

            controller.Response.ContentType = contenttype;
            controller.Response.Buffer = true;
            controller.Response.Charset = "";
            controller.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            controller.Response.AddHeader("content-disposition", chaineattach);
            controller.Response.BinaryWrite(bytes);
            if (System.IO.File.Exists(FilePath))
                System.IO.File.Delete(FilePath);
            controller.Response.Flush();
            controller.Response.End();
        }

        public ActionResult LoadPdfNavigateur(string name, Controller controller, string cheminView, Object hydrater, bool A4)
        {
            //string html = GetHtmlByView(controller.ControllerContext, controller.TempData, "~/Views/Vente/PrintDemo.cshtml", customerList);
            string filePath = System.IO.Path.Combine(controller.Server.MapPath("/"), name);
            string html = GetHtmlByView(controller.ControllerContext, controller.TempData, cheminView, hydrater);
            String htmlText = html.ToString();
            MemoryStream workStream = new MemoryStream();
            Document document = new Document();

            PdfWriter.GetInstance(document, workStream).CloseStream = false; ;

            document.Open();
            iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document);
            //hw.Parse(new StringReader(htmlText));
            try
            {
                hw.SetPendingTD(true);
                hw.SetPendingTR(true);
                hw.Parse(new StringReader(html.ToString()));
            }
            catch (Exception ex)
            {
            }
            document.Close();


            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;


            controller.Response.ContentType = "application/pdf";
            controller.Response.Buffer = true;
            controller.Response.Charset = "";
            controller.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //controller.Response.AddHeader("content-disposition", chaineattach);
            controller.Response.BinaryWrite(byteInfo);
            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
            controller.Response.Flush();
            controller.Response.End();

            return new BinaryContentResult(byteInfo, "application/pdf"); // new FileStreamResult(workStream, "application/pdf");

        }

        public void GetFileBytes(Byte[] bytes, string fichier, Controller controller)
        {
            string chaineattach = "attachment;filename= " + fichier;//fichier.";//"attachment;filename= fichier.pdf";
            chaineattach = "attachment;filename= " + fichier;
            controller.Response.ContentType = "application/pdf";
            controller.Response.Buffer = true;
            controller.Response.Charset = "";
            controller.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            controller.Response.AddHeader("content-disposition", chaineattach);
            controller.Response.BinaryWrite(bytes);
            controller.Response.Flush();
            controller.Response.End();

        }

        public void TEG_IMPRESSION(string nom_fichier, object Hydrater, string CheminView, Controller controller, bool Portrait)
        {
            string HTML = GetHtmlByView(controller.ControllerContext, controller.TempData, CheminView, Hydrater);
            var HTML_TO_PDF = new NReco.PdfGenerator.HtmlToPdfConverter();
            //pour le debogage
            HTML_TO_PDF.Quiet = false;
            HTML_TO_PDF.LogReceived += (sender, e) =>
            {
                StreamWriter monStreamWriter = File.AppendText(controller.Server.MapPath("~") + "log.log");
                monStreamWriter.WriteLine("****************************************************************" + DateTime.Now.ToString() + "************************************************************************");
                monStreamWriter.WriteLine("Action : GENERATE HTML TO PDF");
                monStreamWriter.WriteLine(String.Format("WkHtmlToPdf Log: {0}", e.Data));
                monStreamWriter.Close();
            };

            //HTML_TO_PDF.PageHeaderHtml = GetHtmlByView(controller.ControllerContext, controller.TempData, "~/Views/Shared/_EntetePDF.cshtml", null);
            HTML_TO_PDF.PageFooterHtml = GetHtmlByView(controller.ControllerContext, controller.TempData, "~/Views/Shared/_Footer.cshtml", null);
            HTML_TO_PDF.Orientation = Portrait ? NReco.PdfGenerator.PageOrientation.Portrait : NReco.PdfGenerator.PageOrientation.Landscape;
            GetFileBytes(HTML_TO_PDF.GeneratePdf(HTML, null), nom_fichier.Replace(" ", "_") + ".pdf", controller);

        }

        public bool VerifierHabiliationIndex(string droit, Controller controller, ref string[] tab_Habillitation)
        {
            DroitsBM droitUtilisateur = new DroitsBM();
            tab_Habillitation[0] = GetHtmlByView(controller.ControllerContext, controller.TempData, "~/Views/Login/Habillitation.cshtml", null);
            tab_Habillitation[1] = "Accès refusé";
            tab_Habillitation[2] = "<a href = '/Home/Index' ><i class='ouf-home'></i>Accueil</a>  <div class='breadcrumb_divider'></div> <a href='#'>Habilitation</a><div class='breadcrumb_divider'></div> <a href='#' class='current'>Accès refusé</a>";
            return droitUtilisateur.ExecuteCommande(controller.Session["Code_User"].ToString(), droit);
        }

        public bool VerifierHabiliationCRUD(string droit, Controller controller, ref string[] tab_Habillitation)
        {
            DroitsBM droitUtilisateur = new DroitsBM();
            controller.TempData["messagewarning"] = "Accès refusé";
            tab_Habillitation[0] = GetHtmlByView(controller.ControllerContext, controller.TempData, "~/Views/Shared/_Message.cshtml", null);
            return droitUtilisateur.ExecuteCommande(controller.Session["Code_User"].ToString(), droit);
        }

        public string convertir(double chiffre)
        {
            int centaine, dizaine, unite, reste, y;
            bool dix = false;
            string lettre = "";
            //strcpy(lettre, "");

            reste = (int)chiffre / 1;

            for (int i = 1000000000; i >= 1; i /= 1000)
            {
                y = reste / i;
                if (y != 0)
                {
                    centaine = y / 100;
                    dizaine = (y - centaine * 100) / 10;
                    unite = y - (centaine * 100) - (dizaine * 10);
                    switch (centaine)
                    {
                        case 0:
                            break;
                        case 1:
                            lettre += "cent ";
                            break;
                        case 2:
                            if ((dizaine == 0) && (unite == 0)) lettre += "deux cents ";
                            else lettre += "deux cent ";
                            break;
                        case 3:
                            if ((dizaine == 0) && (unite == 0)) lettre += "trois cents ";
                            else lettre += "trois cent ";
                            break;
                        case 4:
                            if ((dizaine == 0) && (unite == 0)) lettre += "quatre cents ";
                            else lettre += "quatre cent ";
                            break;
                        case 5:
                            if ((dizaine == 0) && (unite == 0)) lettre += "cinq cents ";
                            else lettre += "cinq cent ";
                            break;
                        case 6:
                            if ((dizaine == 0) && (unite == 0)) lettre += "six cents ";
                            else lettre += "six cent ";
                            break;
                        case 7:
                            if ((dizaine == 0) && (unite == 0)) lettre += "sept cents ";
                            else lettre += "sept cent ";
                            break;
                        case 8:
                            if ((dizaine == 0) && (unite == 0)) lettre += "huit cents ";
                            else lettre += "huit cent ";
                            break;
                        case 9:
                            if ((dizaine == 0) && (unite == 0)) lettre += "neuf cents ";
                            else lettre += "neuf cent ";
                            break;
                    }// endSwitch(centaine)

                    switch (dizaine)
                    {
                        case 0:
                            break;
                        case 1:
                            dix = true;
                            break;
                        case 2:
                            lettre += "vingt ";
                            break;
                        case 3:
                            lettre += "trente ";
                            break;
                        case 4:
                            lettre += "quarante ";
                            break;
                        case 5:
                            lettre += "cinquante ";
                            break;
                        case 6:
                            lettre += "soixante ";
                            break;
                        case 7:
                            dix = true;
                            lettre += "soixante ";
                            break;
                        case 8:
                            lettre += "quatre-vingt ";
                            break;
                        case 9:
                            dix = true;
                            lettre += "quatre-vingt ";
                            break;
                    } // endSwitch(dizaine)

                    switch (unite)
                    {
                        case 0:
                            if (dix) lettre += "dix ";
                            break;
                        case 1:
                            if (dix) lettre += "onze ";
                            else lettre += "un ";
                            break;
                        case 2:
                            if (dix) lettre += "douze ";
                            else lettre += "deux ";
                            break;
                        case 3:
                            if (dix) lettre += "treize ";
                            else lettre += "trois ";
                            break;
                        case 4:
                            if (dix) lettre += "quatorze ";
                            else lettre += "quatre ";
                            break;
                        case 5:
                            if (dix) lettre += "quinze ";
                            else lettre += "cinq ";
                            break;
                        case 6:
                            if (dix) lettre += "seize ";
                            else lettre += "six ";
                            break;
                        case 7:
                            if (dix) lettre += "dix-sept ";
                            else lettre += "sept ";
                            break;
                        case 8:
                            if (dix) lettre += "dix-huit ";
                            else lettre += "huit ";
                            break;
                        case 9:
                            if (dix) lettre += "dix-neuf ";
                            else lettre += "neuf ";
                            break;
                    } // endSwitch(unite)

                    switch (i)
                    {
                        case 1000000000:
                            if (y > 1) lettre += "milliards ";
                            else lettre += "milliard ";
                            break;
                        case 1000000:
                            if (y > 1) lettre += "millions ";
                            else lettre += "million ";
                            break;
                        case 1000:
                            lettre += "mille ";
                            break;
                    }
                } // end if(y!=0)
                reste -= y * i;
                dix = false;
            } // end for
            if (lettre.Length == 0) lettre += "zero";

            return lettre;
        }

        public VariableVM GetSocieteInfo()
        {
            VariableBM ObjetBM = new VariableBM();
            return ObjetBM.GetByID();
        }

    }
}