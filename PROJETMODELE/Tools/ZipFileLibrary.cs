﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ionic.Zip;

namespace PROJETMODELE.Tools
{
    public static class ZipFileLibrary
    {
        public static void ZipAllFille(string[] Files, string nameZip)
        {
            using (ZipFile zip = new ZipFile())
            {
                foreach (var item in Files)
                {
                    //zip.Password = "0123";
                    zip.AddFile(item,".");
                }
                zip.Save(@"F:\"+nameZip+".zip");
            }
        }
    }
}