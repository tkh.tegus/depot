﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace PROJETMODELE.Tools
{
    public class PDFEvent : PdfPageEventHelper 
    {
        Image image;
        public string chemin;

        Image imagepied;
        public string cheminpied;

        PdfContentByte cb;
        PdfTemplate headerTemplate, footerTemplate;
        BaseFont bf = null;
        public bool Filigramme;
        public bool piedPage;
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            if (Filigramme)
            {
                image = Image.GetInstance(chemin);
                image.ScaleToFit(1240, 700);
                image.Alignment = iTextSharp.text.Image.UNDERLYING;
                image.SetAbsolutePosition(20, 0);
            }

            if (piedPage)
            {
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                headerTemplate = cb.CreateTemplate(100, 100);
                footerTemplate = cb.CreateTemplate(50, 50);
                imagepied = Image.GetInstance(cheminpied);
                imagepied.ScaleToFit(1000, 100);
                imagepied.Alignment = iTextSharp.text.Image.UNDERLYING;
                imagepied.SetAbsolutePosition(30, 20);
            }
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);
            if (Filigramme)
            {
                document.Add(image);
            }
            if (piedPage)
            {
                document.Add(imagepied);
            }
        }
    }
}