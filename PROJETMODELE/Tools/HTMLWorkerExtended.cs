﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;

namespace PROJETMODELE.Tools
{

    public class HTMLWorkerExtended : HTMLWorker
    {
        public HTMLWorkerExtended(IDocListener document)
            : base(document)
        {
           
        }
        public override void StartElement( string tag, IDictionary<string, string> str)
        {
            if (tag.Equals("tr"))
            {

                var Tabk = str;
                //table.AddCols(cells);
                //table.EndRow();
                //stack.Push(table);
                //skipText = true;
               // return;
            }
            //balise qui permet d'introduire une nouvelle pâge
            if (tag.Equals("krtnewpage"))
                document.Add(Chunk.NEXTPAGE);
            else if (tag.Equals("krtancre"))//permet de faire l'ancrage
            {
                Anchor click = new Anchor("Click to go to Target");
                click.Reference = "#target";
                Paragraph p1 = new Paragraph();
                p1.Add(click);
                document.Add(p1);
                Paragraph p2 = new Paragraph();
                p2.Add(new Chunk("\n\n\n\n\n\n\n\n"));
                document.Add(p2);
                Anchor target = new Anchor("This is the Target");
                target.Name = "target";
                Paragraph p3 = new Paragraph();
                p3.Add(target);
                document.Add(p3);
            }
            else
                base.StartElement(tag, str);
               
           
        }
    }
}