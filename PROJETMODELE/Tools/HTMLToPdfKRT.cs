﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace PROJETMODELE.Tools
{
    public static class HTMLToPdfKRT
    {
        /// <summary>
        /// Permet de convertir du HTML en pdf
        /// </summary>
        /// <param name="name">nom du fichier a renvoyer</param>
        /// <param name="controller">objet controller</param>
        /// <param name="cheminView">chemin de la vue a convertir</param>
        /// <param name="hydrater">objet a hydrater avec la vue</param>
        /// <param name="A4">Fichier en format A4</param>
        /// <param name="cheminpic">chemin d'une photo</param>
        /// <param name="largeur">largeur de la photo</param>
        /// <param name="hauteur">hauteur de la photo</param>
        /// <param name="delete">permet d'indiquer s'il faut supprimer le fichier une fois envoyé</param>
        public static void HTMLToPdf(string name, Controller controller, string cheminView, Object hydrater, bool? A4, string cheminpic, int? largeur, int? hauteur, bool delete)
        {
            string FilePath = System.IO.Path.Combine(controller.Server.MapPath("/template"), name);
            Services service = new Services();
            string HTML = service.GetHtmlByView(controller.ControllerContext, controller.TempData, cheminView, hydrater);
            Document document;
            if (A4 == null)
                document = new Document(PageSize.A4.Rotate());
            else
                document = new Document();
            PdfWriter.GetInstance(document, new FileStream(FilePath, FileMode.Create));
            document.Open();
            if (cheminpic != "")
            {
                Image pdfImage = Image.GetInstance(cheminpic.ToString());
                pdfImage.ScaleToFit(80, 80);
                pdfImage.Alignment = iTextSharp.text.Image.UNDERLYING;
                pdfImage.SetAbsolutePosition((int)largeur, (int)hauteur);
                document.Add(pdfImage);

            }
            ////si le fichier n'existe pas 
            //if (!System.IO.File.Exists(cheminpic))
            //{
            //    string yellow = controller.Server.MapPath("/Content/pictures");
            //    cheminpic = System.IO.Path.Combine(yellow, "user.png");
            //}

            Font font = new Font(Font.FontFamily.TIMES_ROMAN, 5, Font.NORMAL);
            font.SetColor(12, 42, 45);
            StyleSheet styles = new StyleSheet();
            styles.LoadTagStyle("body", "face", "KAIU");
            styles.LoadTagStyle("body", "encoding", "Identity-H");
            styles.LoadTagStyle("body", "leading", "12,0");

            styles.LoadTagStyle("td", "face", "KAIU");
            styles.LoadTagStyle("td", "encoding", "Identity-H");
            styles.LoadTagStyle("td", "line-height", "50,0");
            // iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document, null, styles);

            HTMLWorker hw = new HTMLWorker(document, null, styles); //new HTMLWorkerExtended(document);
            try
            {

                hw.SetPendingTD(true);
                hw.SetPendingTR(true);
                
                hw.Parse(new StringReader(HTML));
            }
            catch (Exception ex)
            {
            }

            document.Close();
            if (delete)
            {
                controller.Response.ClearHeaders();
                controller.Response.Clear();
                controller.Response.ClearContent();
                FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                fs.Close();
                name = name + ".pdf";
                string chaineattach = "attachment;filename= " + name;//fichier.";//"attachment;filename= fichier.pdf";
                string ext = Path.GetExtension(name);
                string contenttype = "application/msword";
                switch (ext)
                {
                    case ".pdf":
                        contenttype = "application/pdf";
                        break;

                }

                controller.Response.ContentType = contenttype;
                controller.Response.Buffer = true;
                controller.Response.Charset = "";
                controller.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                controller.Response.AddHeader("content-disposition", chaineattach);
                controller.Response.BinaryWrite(bytes);
                if (System.IO.File.Exists(FilePath))
                    System.IO.File.Delete(FilePath);
                controller.Response.Flush();
                controller.Response.End();
            }

        }
        /// <summary>
        /// Permet de convertir du HTML en introduisant des bookmark
        /// </summary>
        /// <param name="name"></param>
        /// <param name="controller"></param>
        /// <param name="cheminView"></param>
        /// <param name="hydrater"></param>
        /// <param name="A4"></param>
        /// <param name="cheminpic"></param>
        /// <param name="largeur"></param>
        /// <param name="hauteur"></param>
        /// <param name="delete"></param>
        public static void HTMLToPdfWithBookMark(string name, Controller controller, string cheminView, Object hydrater, bool? A4, string cheminpic, int? largeur, int? hauteur, bool delete)
        {
            string FilePath = System.IO.Path.Combine(controller.Server.MapPath("/template"), name);
            Services service = new Services();
            string HTML = service.GetHtmlByView(controller.ControllerContext, controller.TempData, cheminView, hydrater);
            Document document;
            Chapter chapter = CreateChapterContent(HTML);
            if (A4 == null)
                document = new Document(PageSize.A4.Rotate());
            else
                document = new Document();
            PdfWriter pdfwritter =  PdfWriter.GetInstance(document, new FileStream(FilePath, FileMode.Create));
            PDFEvent pdfEvent = new PDFEvent();
            string chemin = controller.Server.MapPath("/Content/alphinoor/images/");
            string urllogo = System.IO.Path.Combine(chemin, "icone.png");
            pdfEvent.chemin = urllogo;
            pdfwritter.PageEvent = pdfEvent;
          //permet de mettre le mot de passe 
          //   pdfwritter.SetEncryption(PdfWriter.STRENGTH128BITS, "0", "01", PdfWriter.AllowPrinting);
            document.Open();
            document.Add(chapter);
            if (cheminpic != "")
            {
                Image pdfImage = Image.GetInstance(cheminpic.ToString());
                pdfImage.ScaleToFit(80, 80);
                pdfImage.Alignment = iTextSharp.text.Image.UNDERLYING;
                pdfImage.SetAbsolutePosition((int)largeur, (int)hauteur);

                document.Add(pdfImage);

            }
            ////si le fichier n'existe pas 
            //if (!System.IO.File.Exists(cheminpic))
            //{
            //    string yellow = controller.Server.MapPath("/Content/pictures");
            //    cheminpic = System.IO.Path.Combine(yellow, "user.png");
            //}

            document.Close();
            if (delete)
            {
                controller.Response.ClearHeaders();
                controller.Response.Clear();
                controller.Response.ClearContent();
                FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                fs.Close();
                name = name + ".pdf";
                string chaineattach = "attachment;filename= " + name;//fichier.";//"attachment;filename= fichier.pdf";
                string ext = Path.GetExtension(name);
                string contenttype = "application/msword";
                switch (ext)
                {
                    case ".pdf":
                        contenttype = "application/pdf";
                        break;

                }

                controller.Response.ContentType = contenttype;
                controller.Response.Buffer = true;
                controller.Response.Charset = "";
                controller.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                controller.Response.AddHeader("content-disposition", chaineattach);
                controller.Response.BinaryWrite(bytes);
                if (System.IO.File.Exists(FilePath))
                    System.IO.File.Delete(FilePath);
                controller.Response.Flush();
                controller.Response.End();
            }

        }
        public static int SplitPDFAndTransformToImage(string inputPath, Controller controller, int NumberBeginpage)
        {
            string outputPath = controller.Server.MapPath("/Uploads/");
            FileInfo file = new FileInfo(inputPath);
            string name = file.Name.Substring(0, file.Name.LastIndexOf("."));
            PdfReader reader = new PdfReader(inputPath);
            int PageCount = NumberBeginpage;
            int pageLentgth = reader.NumberOfPages;
            for (int pagenumber = PageCount + 1; pagenumber <= pageLentgth; pagenumber++)
            {
                string filenamePDF = System.IO.Path.Combine(outputPath, (pagenumber - 1).ToString() + "_" + controller.Session["Code_User"].ToString() + ".pdf");
                string filenameImage = System.IO.Path.Combine(outputPath, (pagenumber - 1).ToString() + "_" + controller.Session["Code_User"].ToString() + ".png");
                // ExtractPage(inputPath, filenamePDF, pagenumber);
                Document document = new Document();
                PdfCopy copy = new PdfCopy(document, new FileStream(filenamePDF, FileMode.Create));
                document.Open();
                copy.AddPage(copy.GetImportedPage(reader, pagenumber));
                document.Close();
                //convertir le fichier en image
                ConvertImage(filenamePDF, filenameImage);
                System.IO.File.Delete(filenamePDF);
            }
            reader.Close();
            return reader.NumberOfPages;

        }

        public static void ExtractPage(string sourcePdfPath, string outputPdfPath,  int pageNumber )
        {
            PdfReader reader = null;
            Document document = null;
            PdfCopy pdfCopyProvider = null;
            PdfImportedPage importedPage = null;

            try
            {
                // Intialize a new PdfReader instance with the contents of the source Pdf file:
                reader = new PdfReader(sourcePdfPath);
 
                // Capture the correct size and orientation for the page:
                document = new Document(reader.GetPageSizeWithRotation(pageNumber));
 
                // Initialize an instance of the PdfCopyClass with the source 
                // document and an output file stream:
                pdfCopyProvider = new PdfCopy(document, 
                    new System.IO.FileStream(outputPdfPath, System.IO.FileMode.Create));

                document.Open();
 
                // Extract the desired page number:
                importedPage = pdfCopyProvider.GetImportedPage(reader, pageNumber);
                pdfCopyProvider.AddPage(importedPage);
                document.Close();
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void ConvertImage(String pathPDF, String pathImage)
        {
            Spire.Pdf.PdfDocument doc = new Spire.Pdf.PdfDocument();
            doc.LoadFromFile(pathPDF);
            for (int i = 0; i < doc.Pages.Count; i++)
            {
                //  String fileName = String.Format("Sample4-img-{0}.png", i);
                using (System.Drawing.Image image = doc.SaveAsImage(i))
                {
                    image.Save(pathImage, System.Drawing.Imaging.ImageFormat.Png);
                }
            }

            doc.Close();
        }

        /// <summary>
        /// Create chapter content from html
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static Chapter CreateChapterContent(string html)
        {

            html = HttpUtility.HtmlDecode(html);
            iTextSharp.text.Font bookmarkFont = iTextSharp.text.FontFactory.GetFont
            (iTextSharp.text.FontFactory.HELVETICA, 20, iTextSharp.text.Font.NORMAL, new BaseColor(0, 0, 0));
            Services service = new Services();
            Chapter chapter = new Chapter(new Paragraph(""), 0);
            chapter.NumberDepth = 0;
            StyleSheet styles = new StyleSheet();
            // Split H2 Html Tag
            string pattern = @"<\s*krth2[^>]*>(.*?)<\s*/krth2\s*>";
            string[] result = Regex.Split(html, pattern);
            int sectionIndex = 0;
            foreach (var item in result)
            {
                if (string.IsNullOrEmpty(item)) continue;

                //if (sectionIndex % 2 == 0)
                if (sectionIndex % 2 != 0)
                {
                    chapter.Add(Chunk.NEXTPAGE);
                    Paragraph paragraph = new Paragraph(service.RemoveDiacritics(item), bookmarkFont);
                    paragraph.Alignment = Element.ALIGN_CENTER;
                    paragraph.SpacingAfter = 50;
                    chapter.AddSection(20f, paragraph, 0);

                }
                else
                {
                    foreach (IElement element in HTMLWorker.ParseToList(new StringReader(item), styles))
                    {
                        chapter.Add(element);
                    }
                }

                sectionIndex++;
            }

            chapter.BookmarkTitle = "Alphinoor Results";
            return chapter;
        }
       
        public static void MergeFiles(string destinationFile, string[] sourceFiles, string[] pageName)
        {
            try
            {
                int f = 0;
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(sourceFiles[f]);
                // we retrieve the total number of pages
                int n = reader.NumberOfPages;
                //Console.WriteLine("There are " + n + " pages in the original file.");
                // step 1: creation of a document-object
                Document document = new Document(reader.GetPageSizeWithRotation(1));
                // step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(destinationFile, FileMode.Create));
                // step 3: we open the document
                document.Open();
                PdfContentByte cb = writer.DirectContent;
                PdfImportedPage page;

                int rotation;
                // step 4: we add content
                int pdfPageName = 0;
                bool pdfNewPageFlag = true;
                while (f < sourceFiles.Length)
                {
                    int i = 0;
                    while (i < n)
                    {
                        i++;
                        document.SetPageSize(reader.GetPageSizeWithRotation(i));
                        document.NewPage();
                        if (pdfNewPageFlag == true)
                        {
                            Chapter chapter = new Chapter(pageName[pdfPageName], pdfPageName + 1);
                            document.Add(chapter);
                            pdfNewPageFlag = false;
                            pdfPageName++;
                        }

                        page = writer.GetImportedPage(reader, i);
                        rotation = reader.GetPageRotation(i);

                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(i).Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }
                        //Console.WriteLine("Processed page " + i);

                    }
                    f++;
                    if (f < sourceFiles.Length)
                    {
                        reader = new PdfReader(sourceFiles[f]);
                        // we retrieve the total number of pages
                        n = reader.NumberOfPages;


                        //Console.WriteLine("There are " + n + " pages in the original file.");
                        //
                        pdfNewPageFlag = true;
                    }
                }
                // step 5: we close the document
                document.Close();
            }
            catch (Exception e)
            {
                //Console.Error.WriteLine(e.Message);
                //Console.Error.WriteLine(e.StackTrace);
            }
        }

        public static void BookmarkDemo()
        {
            string path = @"F:\";

            Document doc = new Document();
            PdfWriter.GetInstance(doc, new FileStream(path + "/Anchors.pdf", FileMode.Create));

            doc.Open();

            //Font link = FontFactory.GetFont("Arial", 12, Font.UNDERLINE, new BaseColor(0, 0, 255));

            //Anchor anchor = new Anchor("www.mikesdotnetting.com", link);

            //anchor.Reference = "http://www.mikesdotnetting.com";

            //doc.Add(anchor);
            Chapter chapter1 = new Chapter(new Paragraph("This is Chapter 1"), 1);

            Section section1 = chapter1.AddSection(20f, "Section 1.1", 2);

            Section section2 = chapter1.AddSection(20f, "Section 1.2", 2);

            Section subsection1 = section2.AddSection(20f, "Subsection 1.2.1", 3);

            Section subsection2 = section2.AddSection(20f, "Subsection 1.2.2", 3);

            Section subsubsection = subsection2.AddSection(20f, "Sub Subsection 1.2.2.1", 4);

            Chapter chapter2 = new Chapter(new Paragraph("This is Chapter 2"), 1);

            Section section3 = chapter2.AddSection("Section 2.1", 2);

            Section subsection3 = section3.AddSection("Subsection 2.1.1", 3);

            Section section4 = chapter2.AddSection("Section 2.2", 2);

            chapter1.BookmarkTitle = "Changed Title";

            chapter1.BookmarkOpen = true;

            chapter2.BookmarkOpen = false;

            doc.Add(chapter1);

            doc.Add(chapter2);
        }

        public static void ExtractAllImages(string path, ref int nombre, string OutputLogo)
        {

            using (BitMiracle.Docotic.Pdf.PdfDocument pdf = new BitMiracle.Docotic.Pdf.PdfDocument(path))
            {
                //for (int i = 0; i < pdf.Images.Count; i++)
                //{
                //   pdf.Images[i].Save(OutputLogo+@"\" + string.Format("{0}", nombre));
                //   nombre++;
                 
                //}
            }
        }

       public static int SplitPDF(string inputPath, Controller controller, int NumberBeginpage , int debutDepot,string OutputLogo)
       {
           string outputPath = controller.Server.MapPath("/Uploads/");
           FileInfo file = new FileInfo(inputPath);
           string name = file.Name.Substring(0, file.Name.LastIndexOf("."));
           PdfReader reader = new PdfReader(inputPath);
           int PageCount = NumberBeginpage;
           int pageLentgth = reader.NumberOfPages;
           int debutNumDepot = debutDepot;
           for (int pagenumber = PageCount + 1; pagenumber <= pageLentgth; pagenumber++)
           {
               string filenamePDF = System.IO.Path.Combine(outputPath, (pagenumber - 1).ToString() + "_"+ controller.Session["Code_User"].ToString() + ".pdf");
              Document document = new Document();
               PdfCopy copy = new PdfCopy(document, new FileStream(filenamePDF, FileMode.Create));
               document.Open();
               copy.AddPage(copy.GetImportedPage(reader, pagenumber));
               document.Close();
           }
           reader.Close();

           for (int pagenumber = PageCount + 1; pagenumber <= pageLentgth; pagenumber++)
           {
               string filenamePDF = System.IO.Path.Combine(outputPath, (pagenumber - 1).ToString() + "_" + controller.Session["Code_User"].ToString() + ".pdf");
               ExtractAllImages(filenamePDF, ref debutNumDepot,OutputLogo);
               System.IO.File.Delete(filenamePDF);
           }
           return 1 + debutNumDepot - debutDepot;

       }
    }
}