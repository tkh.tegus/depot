﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using DocumentFormat.OpenXml.Wordprocessing;
using NotesFor.HtmlToOpenXml;

namespace PROJETMODELE.Tools
{
    public static class XmlToWord
    {
        public static void XMLTOWORDFILE(string HTML, Controller controller, string name)
        {
            string html = HTML;
            string filename = System.IO.Path.Combine(controller.Server.MapPath("/template/"), controller.Session["Code_User"].ToString()+".docx");
            if (File.Exists(filename)) File.Delete(filename);

            using (MemoryStream generatedDocument = new MemoryStream())
            {
                using (WordprocessingDocument package = WordprocessingDocument.Create(generatedDocument, WordprocessingDocumentType.Document))
                {
                    MainDocumentPart mainPart = package.MainDocumentPart;
                    if (mainPart == null)
                    {
                        mainPart = package.AddMainDocumentPart();
                        new Document(new Body()).Save(mainPart);
                    }
                    // todo elever les commetaire et corriger
                    //HtmlConverter converter = new HtmlConverter(mainPart);

                    ////converter.ParseHtml(html);

                    ////mainPart.Document.Save();
                    //converter.ImageProcessing = ImageProcessing.ManualProvisioning;
                    ////converter.ProvisionImage += OnProvisionImage;

                    //Body body = mainPart.Document.Body;

                    //converter.ParseHtml(html);
                    //mainPart.Document.Save();

                   // AssertThatOpenXmlDocumentIsValid(package);
                }

                File.WriteAllBytes(filename, generatedDocument.ToArray());

                //renvoye le fichier comme flux de donner

                controller.Response.ClearHeaders();
                controller.Response.Clear();
                controller.Response.ClearContent();
                FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                fs.Close();
                name = name + ".docx";
                string chaineattach = "attachment;filename= " + name;//fichier.";//"attachment;filename= fichier.pdf";
                string ext = Path.GetExtension(name);
                string contenttype = "application/msword";
               
                controller.Response.ContentType = contenttype;
                controller.Response.Buffer = true;
                controller.Response.Charset = "";
                controller.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                controller.Response.AddHeader("content-disposition", chaineattach);
                controller.Response.BinaryWrite(bytes);
                if (System.IO.File.Exists(filename))
                    System.IO.File.Delete(filename);
                controller.Response.Flush();
                controller.Response.End();
            }

            //System.Diagnostics.Process.Start(filename);
        }

        //public void OnProvisionImage(object sender, ProvisionImageEventArgs e)
        //{
        //    string filename = Path.GetFileName(e.ImageUrl.OriginalString);
        //    if (!File.Exists(e.ImageUrl.OriginalString))
        //    {
        //        e.Cancel = true;
        //        return;
        //    }

        //    e.Provision(File.ReadAllBytes(e.ImageUrl.OriginalString));
        //}

        //static void AssertThatOpenXmlDocumentIsValid(WordprocessingDocument wpDoc)
        //{
        //    var validator = new OpenXmlValidator(FileFormatVersions.Office2010);
        //    var errors = validator.Validate(wpDoc);

        //    if (!errors.GetEnumerator().MoveNext())
        //        return;

        //    Console.ForegroundColor = ConsoleColor.Red;
        //    Console.WriteLine("The document doesn't look 100% compatible with Office 2010.\n");

        //    Console.ForegroundColor = ConsoleColor.Gray;
        //    foreach (ValidationErrorInfo error in errors)
        //    {
        //        Console.Write("{0}\n\t{1}", error.Path.XPath, error.Description);
        //        Console.WriteLine();
        //    }

        //    Console.ReadLine();
        //}
    }
}