﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PROJETMODELE.Tools
{
    public class Parametre
    {
        [DisplayName("Nom server * :")]
        [Required(ErrorMessage = "* Champs obligatoire")]
        public String server { get; set; }

        [DisplayName("Nom base de donnée * :")]
        [Required(ErrorMessage = "* Champs obligatoire")]
        public String bd { get; set; }

        [DisplayName("Nom utilisateur * :")]
        [Required(ErrorMessage = "* Champs obligatoire")]
        public String user { get; set; }

        [DisplayName("Mot de passe * :")]
        [Required(ErrorMessage = "* Champs obligatoire")]
        [DataType(DataType.Password)]
        public String pass { get; set; }
    }
}