﻿using Microsoft.Win32;
using PROJETMODELE.Models.DAL;
using System;
using System.Linq;
using System.Configuration;
using System.Management;
using System.Security.Cryptography;
using System.Text;

namespace PROJETMODELE.Tools
{
    public class Licence
    {
        private static string GetIDMachine()
        {
            var localHID = string.Empty;

            // Identifiant processeur
            var mbs = new ManagementObjectSearcher("SELECT ProcessorID FROM Win32_Processor");
            var mbsList = mbs.Get();
            foreach (ManagementObject mo in mbsList)
                localHID += mo["ProcessorID"].ToString();

            // identifiant bios
            mbs.Query.QueryString = "SELECT SerialNumber FROM Win32_BIOS";
            mbsList = mbs.Get();
            foreach (ManagementObject mo in mbsList)
                localHID += mo["SerialNumber"].ToString();

            // identifiant carte mere
            mbs.Query.QueryString = "SELECT SerialNumber FROM Win32_BaseBoard";
            mbsList = mbs.Get();
            foreach (ManagementObject mo in mbsList)
                localHID += mo["SerialNumber"].ToString();
          
            // Encodage en MRD
            MD5 md5Hash = MD5.Create();
            return GetMd5Hash(md5Hash, localHID);
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        static bool VerifyMd5Hash(string input, string hash)
        {
            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.Ordinal;
            if (0 == comparer.Compare(input, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool VerificationLicence(ref string Message)
        {
            // decriptage 
            TripleDESCryptoServiceProvider TDES = new TripleDESCryptoServiceProvider();
            byte[] iv = new byte[] { 160, 213, 74, 49, 168, 25, 28, 224 };
            byte[] key = new byte[] {34, 212, 195, 155, 123, 209, 103, 156, 23, 181, 213, 30, 175, 95, 218, 77, 7, 116, 223, 172, 56, 142, 175, 26 };
            
            string ID_Serveur;
            DateTime Date_Jour, Date_Fin;
            try
            {
                StoreEntities db = new StoreEntities();
                var data = db.TG_Variable.Select(p => p).FirstOrDefault();
                ID_Serveur = data.Cle0;
                Date_Jour = DateTime.Parse(Decryp(data.Cle1, key, iv));
                Date_Fin = DateTime.Parse(Decryp(data.Cle2, key, iv));
            }
            catch(Exception e)
            {
                Message = "L'application n'est pas activée. Veuillez contacter votre fournisseur!";
                return false;
            }
           
            //  On persiste la date de connection
            if (VerifyMd5Hash(ID_Serveur.ToString(), GetIDMachine()))
            {
                if (Date_Jour < DateTime.Now && DateTime.Now < Date_Fin)
                {
                    StoreEntities db = new StoreEntities();
                    var data = db.TG_Variable.Select(p => p).FirstOrDefault();
                    data.Cle1 = Crypt(DateTime.Now.ToString("dd/MM/yyyy"), key, iv);
                    db.SaveChanges();
                    JoursRestant();
                    return true;
                }
                else
                {
                    Message = "Votre licence d'exploitation a expiré. Veuillez contacter votre fournisseur!";
                    return false;
                }
            }
            else
            {
                Message = "La licence d'exploitation n'existe pas pour ce serveur. Veuillez contacter votre fournisseur!";
                return false;
            }
        }

        static string Decryp(byte[] cryptedTextAsByte, byte[] key, byte[] iv)
        {
            TripleDESCryptoServiceProvider TDES = new TripleDESCryptoServiceProvider();

            // Cet objet est utilisé pour déchiffrer les données.
            // Il reçoit les données chiffrées sous la forme d'un tableau de bytes.
            // Les données déchiffrées sont également retournées sous la forme d'un tableau de bytes
            var decryptor = TDES.CreateDecryptor(key, iv);

            byte[] decryptedTextAsByte = decryptor.TransformFinalBlock(cryptedTextAsByte, 0, cryptedTextAsByte.Length);

            return Encoding.Default.GetString(decryptedTextAsByte);
        }

        static byte[] Crypt(string text, byte[] key, byte[] iv)
        {
            byte[] textAsByte = Encoding.Default.GetBytes(text);

            TripleDESCryptoServiceProvider TDES = new TripleDESCryptoServiceProvider();

            // Cet objet est utilisé pour chiffrer les données.
            // Il reçoit en entrée les données en clair sous la forme d'un tableau de bytes.
            // Les données chiffrées sont également retournées sous la forme d'un tableau de bytes
            var encryptor = TDES.CreateEncryptor(key, iv);

            byte[] cryptedTextAsByte = encryptor.TransformFinalBlock(textAsByte, 0, textAsByte.Length);

            return cryptedTextAsByte;
        }

        public int JoursRestant()
        {
            // decriptage 
            TripleDESCryptoServiceProvider TDES = new TripleDESCryptoServiceProvider();

            byte[] iv = new byte[] { 160, 213, 74, 49, 168, 25, 28, 224 };
            byte[] key = new byte[] { 34, 212, 195, 155, 123, 209, 103, 156, 23, 181, 213, 30, 175, 95, 218, 77, 7, 116, 223, 172, 56, 142, 175, 26 };

            StoreEntities db = new StoreEntities();
            var data = db.TG_Variable.Select(p => p).FirstOrDefault();
            DateTime Date_Jour = DateTime.Parse(Decryp(data.Cle1, key, iv));
            DateTime Date_Fin = DateTime.Parse(Decryp(data.Cle2, key, iv));

            return (Date_Fin - Date_Jour).Days;
        }
    }
}