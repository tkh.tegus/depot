﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{

    [Bind(Exclude = "Code_Commande")]
    public class Commandes
    {
        public int Code_Commande { get; set; }
        
        [Display ( Name = "Menu Parent * : " )]
        public int Code_Commande_Parent { get; set; }

        [Required ( ErrorMessage = "Ce champs est obligatoire" ), Display ( Name = "Libelle * : " )]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        [DataType ( DataType.Text )]
        public string Libelle { get; set; }

        [Display ( Name = "Id_Controle * : " )]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        [DataType ( DataType.Text )]
        public string ID_Control { get; set; }

        [Display(Name = "Ordre * : ")]
        public int Ordre { get; set; }

        [Required ( ErrorMessage = "Ce champs est obligatoire" ), Display ( Name = "Dèscription * : " )]
        [StringLength ( 500, ErrorMessage = "Le libelle doit contenir au plus 500 caractere" )]
        [DataType ( DataType.Text )]
        public string Description { get; set; }
            
    }
}