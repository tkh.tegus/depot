﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    public class Login
    {
        [Required ( ErrorMessage = "Ce champs est obligatoire" ), Display ( Name = "Password *: " )]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        [DataType ( DataType.Password )]
        public string Passe { get; set; }


        [Required ( ErrorMessage = "Ce champs est obligatoire" ), Display ( Name = "Login *: " )]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        public string Libelle { get; set; }

        [Display(Name="Se Souvenir de Moi ?")]
        public bool check { get; set; }
    }
}