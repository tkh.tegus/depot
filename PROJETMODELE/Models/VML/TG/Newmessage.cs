﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    public class Newmessage
    {
        [Display(Name="Destinataire")]
        public int Code_User { get; set; }
        [Display(Name = "Priorité")]
        public string priorite { get; set; }
        [Required(ErrorMessage = "Ce champs est obligatoire")]
        [StringLength(50,ErrorMessage="50 caractères au maximun")]
        [Display(Name = "Objet")]
        public string Objet { get; set; }
        [Required(ErrorMessage = "Ce champs est obligatoire")]
        [Display(Name = "Message")]
        public string Contenu { get; set; }
    }
}