﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{

    public class Corbeille
    {
        public int ID_Corbeille { get; set; }
        public int ID_Table { get; set; }
        public int ID_Element { get; set; }
        public string Description { get; set; }
        public System.DateTime Date { get; set; }
        public string Code_User { get; set; }
        public bool Actif { get; set; }
    }
}