﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    public class Import
    {
        [Display(Name = "Fichier CSV * : ")]
        public string Logo { get; set; }
        [Display(Name = "Entitée à importer * : ")]
        public int id { get; set; }

        [Display(Name = "Liste Diffusion * : ")]
        public Int64 ID_Liste_Diffusion { get; set; }
    }
}