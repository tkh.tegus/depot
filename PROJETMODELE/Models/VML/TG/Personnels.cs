﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    [Bind ( Exclude = "ID_Personnel" )]
    public class Personnels
    {
        public int ID_Personnel { get; set; }

        [Display ( Name = "Ville * : " )]
        public int ID_Ville { get; set; }

        [Display(Name = "Horaire * : ")]
        public Int64 ID_Horaire { get; set; }

        [Display(Name = "Date Début * : ")]
        public DateTime DateDebut { get; set; }

        [Display(Name = "Date Fin * : ")]
        public DateTime DateFin { get; set; }

        [Display ( Name = "Poste * : " )]
        public int ID_Poste { get; set; }

        [Display(Name = "Nationalité * : ")]
        public int ID_Nationalite { get; set; }

        [Display(Name = "Responsable * : ")]
        public int? ID_Chef { get; set; }

        [Display ( Name = "Département * : " )]
        public int ID_Departement { get; set; }


        [Display(Name = "Banque * : ")]
        public int? ID_Banque { get; set; }
        public string Banque { get; set; }

        [Display(Name = "Type Contrat * : ")]
        public int? ID_Type_Contrat { get; set; }
        public string Type_Contrat { get; set; }

        [Display(Name = "Nbre de jour Congé : ")]
        public int? Nbjrconge { get; set; }

        [Display(Name = "Numero Compte Bancaire * : ")]
        public string Numero_Compte { get; set; }

        [Display(Name = "Salaire : ")]
        public double? Salaire { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Code Biostar * : ")]
        public int Code_biostar { get; set; }

        [Required ( ErrorMessage = "Ce champs est obligatoire" ), Display ( Name = "Nom * : " )]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        [DataType ( DataType.Text )]
        public string Nom { get; set; }

        [Display ( Name = "Prénom  : " )]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        [DataType ( DataType.Text )]
        public string Prenom { get; set; }

        [Required ( ErrorMessage = "Ce champs est obligatoire" ), Display ( Name = "Date De Naissance * : " )]
        public System.DateTime Date_Naissance { get; set; }

        [Display ( Name = "Date De Recrutement * : " )]
        public Nullable<System.DateTime> Date_Recrutement { get; set; }

        public string Sexe { get; set; }
        [Display ( Name = "Matricule * : " )]
        public string Matricule { get; set; }

        [Display(Name = "Téléphone * : ")]
        public string Telephone { get; set; }

        [DataType ( DataType.EmailAddress )]
        [Display ( Name = "Email  : " )]
        public string Mail { get; set; }

        [Display ( Name = "Adresse  : " )]
        [DataType ( DataType.Text )]
        public string Adresse { get; set; }

        [StringLength(150, ErrorMessage = "La CNSS doit contenir au plus 150 caractere")]
        [Display(Name = "CNSS  : ")]
        public string Cnss { get; set; }

        [StringLength(50, ErrorMessage = "La Picture doit contenir au plus 50 caractere")]
        [Display(Name = "Picture  : ")]
        public string Picture { get; set; }

        [StringLength(50, ErrorMessage = "La CNI doit contenir au plus 50 caractere")]
        [Display(Name = "CNI  : ")]
        public string CNI { get; set; }

        [StringLength(50, ErrorMessage = "Le lieu doit contenir au plus 50 caractere")]
        [Display(Name = "Lieu de naissance  : ")]
        public string Lieu_de_Naissance { get; set; }

        [StringLength(150, ErrorMessage = "Le nom du contact doit contenir au plus 150 caractere")]
        [Display(Name = "Nom du contact  : ")]
        public string Contact_Nom { get; set; }

        [StringLength(150, ErrorMessage = "Le prenom du contact doit contenir au plus 150 caractere")]
        [Display(Name = "Prenom du contact I : ")]
        public string Contact_Prenom { get; set; }

        [StringLength(150, ErrorMessage = "Le Telephone du contact doit contenir au plus 150 caractere")]
        [Display(Name = "Telephone  N°1 du contact I : ")]
        public string Contact_Telephone1 { get; set; }

        [StringLength(150, ErrorMessage = "Le Telephone du contact doit contenir au plus 150 caractere")]
        [Display(Name = "Telephone  N°2 du contact I : ")]
        public string Contact_Telephone { get; set; }

        [StringLength(150, ErrorMessage = "La filliation du contact doit contenir au plus 150 caractere")]
        [Display(Name = "Filliation du contact I : ")]
        public string Contact_Filiation { get; set; }


        [StringLength(150, ErrorMessage = "Le nom du contact doit contenir au plus 150 caractere")]
        [Display(Name = "Nom du contact II  : ")]
        public string Contact1_Nom { get; set; }

        [StringLength(150, ErrorMessage = "Le prenom du contact doit contenir au plus 150 caractere")]
        [Display(Name = "Prenom du contact II : ")]
        public string Contact1_Prenom { get; set; }

        [StringLength(150, ErrorMessage = "Le Telephone du contact doit contenir au plus 150 caractere")]
        [Display(Name = "Telephone N°1 du contact II : ")]
        public string Contact1_Telephone1 { get; set; }

        [StringLength(150, ErrorMessage = "Le Telephone du contact doit contenir au plus 150 caractere")]
        [Display(Name = "Telephone N°2 du contact II : ")]
        public string Contact1_Telephone { get; set; }

        [StringLength(150, ErrorMessage = "La filliation du contact doit contenir au plus 150 caractere")]
        [Display(Name = "Filliation du contact II  : ")]
        public string Contact1_Filiation { get; set; }

        [StringLength(150, ErrorMessage = "Le telephone personnel doit contenir au plus 150 caractere")]
        [Display(Name = "Telephone personnel : ")]
        public string Telephone_Personnel { get; set; }

        [StringLength(150, ErrorMessage = "L'adresse mail personnel doit contenir au plus 150 caractere")]
        [Display(Name = "Email personnel : ")]
        public string Mail_Personnel { get; set; }

        [Display(Name = "Montant du telephone prepayer : ")]
        public double? Montant_Telephone_Prpaid { get; set; }

        [StringLength(50, ErrorMessage = "La frequence du telephone prepayer doit contenir au plus 50 caractere")]
        [Display(Name = "Frequence telephone prepayer : ")]
        public string Frequence_Telephone_Prepaid { get; set; }


        public byte[] Scan_Signature{ get; set; }

        /*/   [Display(Name = "Signature  : ")]
           public <Array>byte Signature { get; set; }*/
    }
}