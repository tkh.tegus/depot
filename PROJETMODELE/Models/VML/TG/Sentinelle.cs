﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    public class Sentinelle
    {
      
        [Display(Name="Activer la sentinelle ?")]
        public bool Sentinelles { get; set; }

        [Display(Name = "Journaliser les connexions ?")]
        public bool SentinelleConnexion { get; set; }

        [Display(Name = "Journaliser les déconnexions ?")]
        public bool SentinelleEndConnexion { get; set; }

        [Display(Name = "Journaliser les insertions ?")]
        public bool SentinelleInsert { get; set; }

        [Display(Name = "Journaliser les modifications ?")]
        public bool SentinelleEdit { get; set; }

        [Display(Name = "Journaliser les suppressions ?")]
        public bool SentinelleDelete { get; set; }

        [Display(Name = "Journaliser les impressions ?")]
        public bool SentinellePrint { get; set; }

        [Display(Name = "Durée de vie du journal :")]
        [Required(ErrorMessage="Champs obligation")]
        public string SentinelleLife { get; set; }

    }
}