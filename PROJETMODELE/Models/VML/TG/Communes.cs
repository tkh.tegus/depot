﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    [Bind ( Exclude = "ID_Commune" )]
    public class Communes
    {
       /* public Communes ()
        {
            this.TG_Localites = new HashSet<TG_Localites>();
        }*/
        
        public int ID_Commune { get; set; }

        [Display ( Name = "Arrondissement * : " )]
        public int ID_Arrondissement { get; set; }

        [Required ( ErrorMessage = "Ce champs est obligatoire" )]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        [Display ( Name = "Libelle * : " )]
        public string Libelle { get; set; }
    }
}