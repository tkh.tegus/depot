﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TG
{
    [Bind(Exclude = "ID_Grade")]
    public class GradesVM
    {
        public int ID_Grade { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Libelle * :")]
        [StringLength(50, ErrorMessage = "Le libelle doit contenir au plus 50 caractères")]
        public string Libelle { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Abréviation * :")]
        [StringLength(50, ErrorMessage = "L'abregé doit contenir au plus 50caractères")]
        public string Abrege { get; set; }
    }
}