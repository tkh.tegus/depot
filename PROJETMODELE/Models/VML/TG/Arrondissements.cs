﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{

    [Bind ( Exclude = "ID_Arrondissement" )]
    public class Arrondissements
    {
        
        public int ID_Arrondissement { get; set; }


        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Libelle * :")]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        [DataType ( DataType.Text )]
        public string Libelle { get; set; }

    }


}