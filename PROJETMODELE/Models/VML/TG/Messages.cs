﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    public class Messages
    {
        public int ID_messages { get; set; }
        public int Nom { get; set; }
        public string Objet { get; set; }
        public DateTime Date_Envoie  { get; set; }
        public string Contenu { get; set; }
        public int Code_User_Emetteur { get; set; }
        public int Code_User_Destinataire { get; set; }
        public bool sens { get;set; }
    }
}