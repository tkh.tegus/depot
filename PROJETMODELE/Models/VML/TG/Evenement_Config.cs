﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    public class Evenement_Config
    {
        [Display ( Name = "Type évènement * : " )]
        public int ID_Type_Evenement { get; set; }

        [Display(Name = "Utilisateur * : ")]
        public int ID_Personnel { get; set; }

    }
}