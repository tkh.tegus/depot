﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;
using System.Web;

namespace PROJETMODELE.Models.VML.TG
{
    public class Filtre
    {
        private string datedebut;
        private string datefin;

        public Filtre(string datedebut, string datefin)
        {
            // TODO: Complete member initialization
            this.datedebut = datedebut;
            this.datefin = datefin;
        }
        [Display(Name = "Début : ")]
        public DateTime DateDebut {
            get { return DateTime.Parse(datedebut, System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR")); }
        }

        [Display(Name = "Fin : ")]
        public DateTime DateFin {
            get { return DateTime.Parse(datefin, System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR")); }
        }

        public int ID_Action { get; set; }

        public int ID_Personnel { get; set; }
        /// <summary>
        /// cet date de fin est utilisé pour filtrer au niveau du controleur
        /// on y ajoute un jour car les éléménts qui sont enregistrer sont du type tymedate donc le temps intervien pour orgneter la date d'un jour
        /// </summary>
        [Display(Name = "Date Fin * : ")]
        public DateTime CDateFin
        {
            get { return DateTime.Parse(datefin, System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR")).AddDays(1); }
        }
    }
}