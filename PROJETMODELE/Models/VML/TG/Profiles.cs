﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    [Bind(Exclude = "Code_Profile,Code_User_Out,Code_User_In,profile,ancient")]
  
    public class Profiles
    {
        public int Code_Profile { get; set; }

        [Required(AllowEmptyStrings = true)]
        [Display ( Name = "Utilisateurs Liés * : " )]
        public int Code_User_In { get; set; }

        public int profile { get; set; }
        public int ancient { get; set; }
        [Required(AllowEmptyStrings= true)]
        [Display ( Name = "Utilisateurs Non Liés * : " )]
        public int Code_User_Out { get; set; }


        [Required ( ErrorMessage = "Ce champs est obligatoire" ), Display ( Name = "libelle * : " )]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        public string Libelle { get; set; }


        public bool Display { get; set; }
    }
}