﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    [Bind(Exclude = "ID_Personnel")]
    public class Compte
    {
        public int ID_Personnel { get; set; }
         [Display(Name = "Nom  :")]
        public string Nom { get; set; }
          [Display(Name = "Poste  :")]
        public string poste { get; set; }
          [Display(Name = "Nationalité  :")]
        public string nationalite { get; set; }
          [Display(Name = "Ville  :")]
          public string ville { get; set; }
          [Display(Name = "Login  :")]
        public string login { get; set; }
          [Display(Name = "Ancien Mot de passe  :")]
        public string pass { get; set; }
          [Display(Name = "Nouveau mot de passe  :")]
        public string newpass { get; set; }
          [Display(Name = "Confirmer Nouveau mot de passe  :")]
        public string retapnewpass { get; set; }
    }
}