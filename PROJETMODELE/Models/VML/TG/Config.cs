﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    public class Config
    {
        public int ID_Configuration { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Remise Personnel (%) * :")]

        public byte? Remise_Personnel { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Remise Patient (%) * :")]

        public byte? Remise_Patient { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Taux de Remboursement Patient (%) * :")]

        public byte? Taux_Credit_Patient { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Taux de Remboursement Personnel (%) * :")]
        public byte? Taux_Credit_Personnel { get; set; }

        [Display(Name = "Payer Total avant d'être servit ? * :")]
        public bool Payer_Totalite { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Délai pour payer les dettes (jours) * :")]
        public int Delais_Dette { get; set; }


    }
}