﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.VML.TG
{
    public class VariableVM
    {
        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Libelle * :")]
        [StringLength(50, ErrorMessage = "Le Libelle doit contenir au plus 50 caractères")]
        public string Nom_Societe { get; set; }

        [Display(Name = "Numéro Contribuable  :")]
        [StringLength(50, ErrorMessage = "Le Libelle doit contenir au plus 50 caractères")]
        public string Numero_Contribuable { get; set; }
        public string Registre_Commerce { get; set; }

        [Display(Name = "Téléphone  :")]
        [StringLength(50, ErrorMessage = "Le Libelle doit contenir au plus 50 caractères")]
        public string Telephone { get; set; }

        [Display(Name = "Adresse  :")]
        [StringLength(50, ErrorMessage = "Le Libelle doit contenir au plus 50 caractères")]
        public string Adresse { get; set; }
        public string Ville { get; set; }
        public string Description { get; set; }

        public Byte[] Logo { get; set; }

        [Display(Name = "Appliquer TVA  :")]
        public bool Appliquer_TVA { get; set; }

        [Display(Name = "Valeur TVA  :")]
        public double TVA { get; set; }

        [Display(Name = "Cocher cette case s'il faut vérifier l'état du stock avant de permettre la facturation")]
        public bool Verifier_Etat_Stock { get; set; }

        [Display(Name = "Dossier Sauvegarde  :")]
        public string Dossier_Sauvegarde { get; set; }

        [Display(Name = "Password Sauvegarde  :")]
        public string Password_Sauvegarde { get; set; }
        public bool Edit_Password_Sauvegarde { get; set; }
    }
}