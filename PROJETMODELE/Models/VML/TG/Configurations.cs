﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    public class Configurations
    {
        public int ID_Configuration { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"),Display(Name="Non Universite * :")]
        [StringLength(50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere")]
        public string Libelle { get; set; }

        [StringLength(350, ErrorMessage = "Le slogan doit contenir au plus 100 caractere"), Display(Name = "Slogan  :")]
        [DataType(DataType.Text)]
        public string Slogan { get; set; }

        [StringLength(350, ErrorMessage = "Le pays doit contenir au plus 50 caractere"), Display(Name = "Pays  :")]
        public string Pays { get; set; }

         [StringLength(350, ErrorMessage = "La ville doit contenir au plus 50 caractere"), Display(Name = "Ville  :")]
        public string Ville { get; set; }

         [StringLength(350, ErrorMessage = "Le Boîte postal doit contenir au plus 50 caractere"), Display(Name = "Boîte postal  :")]
        public string BP { get; set;}
               
        public string Logo { get; set; }

        [StringLength(350, ErrorMessage = "Le serveur SMTP doit contenir au plus 50 caractere"), Display(Name = "Serveur SMTP  :")]
        public string Serveur_SNTP { get; set; }

         [StringLength(350, ErrorMessage = "Le mail de l'expéditeur doit contenir au plus 50 caractere"), Display(Name = "Email expéditeur  :")]
        public string Mail_Expediteur { get; set; }

         [StringLength(350, ErrorMessage = "Le mot de passe pour l'email doit contenir au plus 50 caractere"), Display(Name = "Mot de passe  :")]
        public string Mot_Passe { get; set; }

         [Display(Name = "Distinction Entre Etrangés et Nationaux * :")]
         public string DistinctionNationauxEtranger { get; set; }
                 
        [Display(Name = "Modifier une note après sa validation * :")]
        public string UpdateNoteAfterValider { get; set; }

        [Display(Name = "Indiquez le systeme notation utilisé * :")]
        public string SystemAppliedLmdOrCoef { get; set; }

        [StringLength(350, ErrorMessage = "Le nom de la république doit contenir au plus 150 caractere"), Display(Name = "République  :")]
        public string republique { get; set; }

        [StringLength(350, ErrorMessage = "La device doit contenir au plus 150 caractere"), Display(Name = "Device  :")]
        public string device { get; set; }

        [StringLength(350, ErrorMessage = "Le ministére doit contenir au plus 150 caractere"), Display(Name = "Ministére  :")]
        public string ministere { get; set; }

        [StringLength(350, ErrorMessage = "La Division doit contenir au plus 150 caractere"), Display(Name = "division  :")]
        public string division { get; set; }
        
    }
}