﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    [Bind(Exclude = "Code_User,ancient,profile")]
    public class Utilisateurs
    {
        public int Code_User { get; set; }

        [Required ( ErrorMessage = "Ce champs est obligatoire" )]
        [Display ( Name = "Personnel  * : " )]
        public int ID_Personnel { get; set; }

        [Display ( Name = "Profils Non Liés * : " )]
        public int Code_Profile_Out { get; set; }

        [Display ( Name = "Profils Liés * : " )]
        public int Code_Profile_In { get; set; }

        public int profile { get; set; }
        public int ancient { get; set; }
        
        [Required( ErrorMessage = "Ce champs est obligatoire" ), Display( Name = "Login * : " )]
        [StringLength( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        [DataType( DataType.Text )]
        public string Login { get; set; }


        [Display(Name = "Sexe * : ")]
        public string Sexe { get; set; }

        [Display(Name = "Matricule  : ")]
        public string Matricule { get; set; }

        [Display(Name = "Téléphone : ")]
        public string Telephone { get; set; }

        [Display(Name = "Téléphone  : ")]
        public string Mail { get; set; }

        [Display(Name = "Adresse  : ")]
        [DataType(DataType.Text)]
        public string Adresse { get; set; }

        [Display(Name = "Grade * : ")]
        public int ID_Grade { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Nom * : ")]
        [StringLength(50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere")]
        public string Nom { get; set; }

        [Display(Name = "Prénom  : ")]
        [StringLength(50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere")]
        public string Prenom { get; set; }

        [Display(Name = "Client  : ")]
        public string CC_ID_Client { get; set; }

        [Display(Name = "Caisse  : ")]
        public int ID_Caisse { get; set; }
    }
}