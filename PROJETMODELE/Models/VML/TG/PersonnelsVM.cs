﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;
using PROJETMODELE.Models.DAL;

namespace PROJETMODELE.Models.VML.TG
{
    [Bind ( Exclude = "ID_Personnel" )]
    public class PersonnelsVM
    {
        public int ID_Personnel { get; set; }

        //[Display(Name = "Localite * : ")]
        //public int ID_Localite { get; set; }

        [Display(Name = "Grade * : ")]
        public int ID_Grade { get; set; }

        public int ID_Caisse { get; set; }

        [Required ( ErrorMessage = "Ce champs est obligatoire" ), Display ( Name = "Nom * : " )]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        public string Nom { get; set; }

        [Display ( Name = "Prénom  : " )]
        [StringLength ( 50, ErrorMessage = "Le libelle doit contenir au plus 50 caractere" )]
        public string Prenom { get; set; }

        [Display ( Name = "Date De Naissance * : " )]
        public DateTime? Date_Naissance { get; set; }

        [Display(Name = "Date De Recrutement * : ")]
        public DateTime? Date_Recrutement { get; set; }
    
        [Display(Name = "Sexe * : ")]
        public string Sexe { get; set; }

        [Display ( Name = "Matricule  : " )]
        public string Matricule { get; set; }

        [Display(Name = "Téléphone : ")]
        public string Telephone { get; set; }

      
        [Display ( Name = "Téléphone  : " )]
        public string Mail { get; set; }

        [Display ( Name = "Adresse  : " )]
        [DataType ( DataType.Text )]
        public string Adresse { get; set; }

        [Display(Name = "Display  : ")]
        public bool Display { get; set; }  
    }
}