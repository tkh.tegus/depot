﻿using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude = "ID_Vente")]
    public class VenteVM
    {
        public Int64 ID_Vente { get; set; }

        [Display(Name = "Date :")]
        public DateTime Date { get; set; }

        public string NUM_DOC { get; set; }

        [Required, Display(Name = "Client * :")]
        public int ID_Client { get; set; }

        public int ID_Caisse { get; set; }

        [Display(Name = "Nom Client :")]
        public string Nom_Client { get; set; }

        public string Utilisateur { get; set; }
        public string Statut { get; set; }
        public byte Etat { get; set; }

        [Display(Name = "Versement ")]
        public double Versement_Liquide { get; set; }
        public string Versement_Liquide_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Versement_Liquide);
            }
            set { }
        }

        [Display(Name = "Consigne ")]
        public double Consigne_Emballage { get; set; }
        public string Consigne_Emballage_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Consigne_Emballage);
            }
            set { }
        }
        public double Net_A_Payer_ { get; set; }
        public string Net_A_Payer { get; set; }
        public string Net_A_Payer_Lettre { get; set; }
        public string Montant_Verser
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Versement_Liquide + Consigne_Emballage);
            }
            set { }
        }
        public string Reste_A_Apyer
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Net_A_Payer_ - Versement_Liquide + Consigne_Emballage);
            }
            set { }
        }
        #region Tab Liquide
        [Display(Name = "Produit ")]
        public int ID_Produit { get; set; }

        [Display(Name = "Casier ")]
        public int? Quantite_Casier_Liquide { get; set; }

        [Display(Name = "Bouteille ")]
        public int? Quantite_Bouteille_Liquide { get; set; }

        [Display(Name = "Casier ")]
        public int? Quantite_Casier_Emballage { get; set; }

        [Display(Name = "Plastique ")]
        public int? Quantite_Plastique_Emballage { get; set; }

        [Display(Name = "Bouteille ")]
        public int? Quantite_Bouteille_Emballage { get; set; }
        #endregion

        #region Tab Emballage
        [Display(Name = "Emballage ")]
        public int ID_Emballage { get; set; }

        [Display(Name = "Casier ")]
        public int? Quantite_Casier_Emballage_Retour { get; set; }

        [Display(Name = "Plastique ")]
        public int? Quantite_Plastique_Emballage_Retour { get; set; }

        [Display(Name = "Bouteille ")]
        public int? Quantite_Bouteille_Emballage_Retour { get; set; }
        #endregion

        #region Vue Consulter
        public List<Detail_Vente> Liste_Liquide { get; set; }
        public List<Detail_Vente> Liste_Emballage { get; set; }

        public string Montant_Facture_Liquide { get; set; }
        public string Montant_Facture_Emballage { get; set; }
        public string Montant_Facture_Retour_Emballage { get; set; }
        #endregion
        public int? Nombre_Colis_Casier { get; set; }
        public int? Nombre_Colis_Bouteille { get; set; }

        public double? Solde_Liquide { get; set; }
        public double? Solde_Emballage { get; set; }

        public double? Solde_Final_Liquide { get; set; }
        public double? Solde_Final_Emballage{ get; set; }

        
        
        public int? Casier_12 { get; set; }
        public int? Plastique_12 { get; set; }
        public int? Bouteille_12 { get; set; }
        public int? Casier_15 { get; set; }
        public int? Plastique_15 { get; set; }
        public int? Bouteille_15 { get; set; }
        public int? Casier_24 { get; set; }
        public int? Plastique_24 { get; set; }
        public int? Bouteille_24 { get; set; }
        public string Montant_C12 { get; set; }
        public string Montant_C15 { get; set; }
        public string Montant_C24 { get; set; }


    }

    public class Detail_Vente
    {
        public string Code { get; set; }
        public string Libelle { get; set; }
        public int Qte_Casier { get; set; }
        public int Qte_Bouteille { get; set; }
        public int Qte_Casier_Emballage { get; set; }
        public int Qte_Plastique_Emballage { get; set; }
        public int Qte_Bouteille_Emballage { get; set; }
        public double PU_Casier { get; set; }
        public double PU_Plastique_Emballage { get; set; }
        public double PU_Bouteille_Emballage { get; set; }
        public int Conditionnement { get; set; }
        public string Montant_Liquide
        {
            get
            {
                Services service = new Services();
                double mt = Qte_Casier * PU_Casier + Qte_Bouteille * (PU_Casier/Conditionnement);
                return service.SeparateurMillier(mt);
            }
            set { }
        }
        public string Montant_Emballage
        {
            get
            {
                Services service = new Services();
                double mt = Qte_Casier_Emballage*(PU_Bouteille_Emballage*Conditionnement+PU_Plastique_Emballage) + Qte_Plastique_Emballage*PU_Plastique_Emballage + Qte_Bouteille_Emballage * PU_Bouteille_Emballage;
                return service.SeparateurMillier(mt);
            }
            set { }
        }
        public string Montant
        {
            get
            {
                Services service = new Services();
                double mt1 = Qte_Casier_Emballage * (PU_Bouteille_Emballage * Conditionnement + PU_Plastique_Emballage) + Qte_Plastique_Emballage * PU_Plastique_Emballage + Qte_Bouteille * PU_Bouteille_Emballage;
                double mt2 = Qte_Casier * PU_Casier + Qte_Bouteille * (PU_Casier / Conditionnement);
                return service.SeparateurMillier((mt1+mt2));
            }
            set { }
        }
    }
}