﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude = "ID_Client")]
    public class ClientVM
    {
        public int ID_Client { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Nom * :")]
        [StringLength(150, ErrorMessage = "Le nom doit contenir au plus 150 caractères")]
        public string Nom { get; set; }

        [Display(Name = "Adresse :")]
        [StringLength(50, ErrorMessage = "L'adresse doit contenir au plus 50 caractères")]
        public string Adresse { get; set; }

        [Display(Name = "Téléphone :")]
        [StringLength(50, ErrorMessage = "Le téléphone doit contenir au plus 50 caractères")]
        public string Telephone { get; set; }

        public bool Actif { get; set; }
        public string Statut { get; set; }
    }
}