﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.VML.TP
{
    public class Retour_EmballageVM
    {
        public Int64 ID_Retour_Emballage { get; set; }

        public byte Type_Element { get; set; }

        public int ID_Element { get; set; }
           
        [Display(Name = "Casier :")]
        public int Qte_Casier_12 { get; set; }

        [Display(Name = "Plastique :")]
        public int Qte_Plastique_12 { get; set; }

        [Display(Name = "Bouteille :")]
        public int Qte_Bouteille_12 { get; set; }

        [Display(Name = "Casier :")]
        public int Qte_Casier_15 { get; set; }

        [Display(Name = "Plastique :")]
        public int Qte_Plastique_15 { get; set; }

        [Display(Name = "Bouteille :")]
        public int Qte_Bouteille_15 { get; set; }

        [Display(Name = "Casier :")]
        public int Qte_Casier_24 { get; set; }

        [Display(Name = "Plastique :")]
        public int Qte_Plastique_24 { get; set; }

        [Display(Name = "Bouteille :")]
        public int Qte_Bouteille_24 { get; set; }
        public string Montant_C12 { get; set; }
        public string Montant_C15 { get; set; }
        public string Montant_C24 { get; set; }
        public string Montant { get; set; }
        public DateTime Date { get; set; }

        public byte Etat { get; set; }

        public string Motif { get; set; }

        public string Statut { get; set; }

        public string Client_Fournisseur { get; set; }

        public string Utilisateur { get; set; }

        public string NUM_DOC { get; set; }

        public List<Detail> Liste_Emallage { get; set; }
    }

    public class Detail
    {
        public int ID_Emballage { get; set; }
        public string Emballage { get; set; }
        public int Qte_Casier { get; set; }
        public int Qte_Plastique { get; set; }
        public int Qte_Bouteille { get; set; }
    }
}