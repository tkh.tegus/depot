﻿using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude = "ID_Encaissement")]
    public class EncaissementVM
    {
        public Int64 ID_Encaissement { get; set; }

        [Display(Name = "Date * :")]
        public DateTime Date { get; set; }

        [Required, Display(Name = "Client * :")]
        public int ID_Client { get; set; }
        public string Client { get; set; }

        [Required, Display(Name = "Caisse * :")]
        public int ID_Caisse { get; set; }
        public string Caisse { get; set; }

        [Required, Display(Name = "Montant * :")]
        [Range(1, double.MaxValue, ErrorMessage = "Le montant doit être supérieur au 0!")]
        public double Montant { get; set; }
        public string Montant_Lettre
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant);
            }
            set { }
        }

        [Display(Name = "Motif :")]
        [StringLength(100, ErrorMessage = "Le motif doit contenir au plus 100 caractères")]
        public string Motif { get; set; }

        public string NUM_DOC { get; set; }

        public string Utilisateur { get; set; }

        public string Statut { get; set; }

        public byte Etat { get; set; }
    }
}