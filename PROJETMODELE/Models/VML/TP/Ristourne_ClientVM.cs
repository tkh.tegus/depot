﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.VML.TP
{
    public class Ristourne_ClientVM
    {
        public int ID_Ristourne_Client { get; set; }

        [Required, Display(Name = "Date * :")]
        public DateTime Date { get; set; }

        [Required, Display(Name = "Client * :")]
        public int ID_Client { get; set; }
        public string Client { get; set; }

        [Required, Display(Name = "Produit * :")]
        public int ID_Produit { get; set; }
        public string Produit { get; set; }

        [Required, Display(Name = "Ristourne * :")]
        public double Ristourne { get; set; }

        [Required, Display(Name = "Cumuler? * :")]
        public bool Cumuler_Ristourne { get; set; }

        public bool Actif { get; set; }

        public string Statut { get; set; }
    }
}