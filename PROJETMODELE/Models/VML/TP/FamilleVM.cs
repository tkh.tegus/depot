﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude = "ID_Famille")]
    public class FamilleVM
    {
        public int ID_Famille { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Libelle * :")]
        [StringLength(50, ErrorMessage = "Le Libelle doit contenir au plus 50 caractères")]
        public string Libelle { get; set; }

        public bool Actif { get; set; }
        public string Statut { get; set; }
    }
}