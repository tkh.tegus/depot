﻿using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude = "ID_Regulation_Solde")]
    public class Regulation_SoldeVM
    {
        public Int64 ID_Regulation_Solde { get; set; }

        [Required, Display(Name = "Client * :")]
        public int ID_Element { get; set; }
        public string Element { get; set; }

        public byte Type_Element { get; set; }

        [Required, Display(Name = "Montant * :")]
        [Range(double.MinValue, double.MaxValue, ErrorMessage = "Le montant doit être différent de 0!")]
        public double Montant { get; set; }
        public string Montant_Lettre
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant);
            }
            set { }
        }

        [Display(Name = "Motif * :")]
        public string Motif { get; set; }

        public DateTime Date { get; set; }

        public string Utilisateur { get; set; }

        public string NUM_DOC { get; set; }

        public double Solde_Client_Avant { get; set; }

        public double Solde_Client_Emballage_Avant { get; set; }

        public byte Etat { get; set; }
        public string Statut { get; set; }
    }
}