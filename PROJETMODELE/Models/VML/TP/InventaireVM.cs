﻿using PROJETMODELE.Models.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude = "ID_Inventaire")]
    public class InventaireVM
    {
        public Int64 ID_Inventaire { get; set; }

        [Display(Name = "Date Creation * :")]
        public DateTime Date_Creation { get; set; }
        public byte Type { get; set; }


        [Required, Display(Name = "Motif *:")]
        [StringLength(500, ErrorMessage = "Ce champs doit contenir au plus 500 caractères")]
        public string Motif { get; set; }

        public bool Cloturer { get; set; }

        public byte Etat { get; set; }

        public string Statut { get; set; }

        public string NUM_DOC { get; set; }

        public string Utilisateur { get; set; }

        public List<Detail_Inventaire> Liste_Produit { get; set; }
    }
    public class Detail_Inventaire
    {
        public string Famille { get; set; }
        public string Produit { get; set; }
        public int Qte_Casier_Theorique { get; set; }
        public int Qte_Casier_Reel { get; set; }
        public int Qte_Plastique_Theorique { get; set; }
        public int Qte_Plastique_Reel { get; set; }
        public int Qte_Bouteille_Theorique { get; set; }
        public int Qte_Bouteille_Reel { get; set; }
        public double PU_Casier_Bouteille { get; set; }
        public double PU_Plastique { get; set; }
        public int Conditionnement { get; set; }
        public double Montant
        {
            get
            {
                StoreEntities db = new StoreEntities();
                double? Montant = db.Database.SqlQuery<double?>("SELECT [dbo].[FN_Prix_Vente_Achat_Detail](" + Qte_Casier_Reel + "," + Qte_Bouteille_Reel + "," + Conditionnement + "," + PU_Casier_Bouteille + ",0,0)").FirstOrDefault();
                if(Montant == null)
                {
                    return 0;
                }
                return Montant.Value;
            }
            set { }
        }

        public double Montant_Emballage
        {
            get
            {
                
                double Montant = Qte_Casier_Reel * (PU_Plastique + PU_Casier_Bouteille * Conditionnement) + Qte_Plastique_Reel * PU_Plastique + Qte_Bouteille_Reel * PU_Casier_Bouteille;               
                return Montant;
            }
            set { }
        }
    }
}