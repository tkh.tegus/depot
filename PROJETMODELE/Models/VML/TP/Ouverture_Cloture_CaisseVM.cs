﻿using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.VML.TP
{
    public class Ouverture_Cloture_CaisseVM
    {
        public int ID_Ouverture_Cloture_Caisse { get; set; }
        [Display(Name = "Date Ouverture * :")]
        public DateTime Date_Ouverture { get; set; }
        [Display(Name = "Date Clôture * :")]
        public DateTime? Date_Cloture { get; set; }
        public String Date_Cloture_Str
        {
            get
            {
                if(Date_Cloture == null)
                {
                    return "__/__/____";
                }
                else
                {
                    return ((DateTime)Date_Cloture).ToString("dd/MM/yyyy HH:mm");
                }
            }
            set { }
        }
        public byte Etat { get; set; }
        public double Solde_Caisse { get; set; }
        public double Inital_Solde_Caisse { get; set; }
        public double Solde_Client { get; set; }
        public double Solde_Fournisseur { get; set; }
        public double Solde_Client_Initial { get; set; }
        public double Solde_Fournisseur_Initial { get; set; }
        [Display(Name = "Caisse :")]
        public int ID_Caisse { get; set; }
        public string Caisse { get; set; }

        [Display(Name = "Caissier(e) :")]
        public int ID_Personnel { get; set; }
        public string Personnel { get; set; }

        public string Statut { get; set; }
        public string Utilisateur { get; set; }

        public double Montant_Ventes { get; set; }
        public string Montant_Ventes_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant_Ventes);
            }
            set { }
        }
        public double Benefice_Brut { get; set; }
        public string Benefice_Brut_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Benefice_Brut);
            }
            set { }
        }
        public int Quantite_Casier { get; set; }
        public int Quantite_Bouteille { get; set; }
        public double Encaissement { get; set; }
        public double Montant_Reglement_Fournisseur { get; set; }
        public string Montant_Reglement_Fournisseur_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant_Reglement_Fournisseur);
            }
            set { }
        }
        public double Montant_Ristourne_Client { get; set; }
        public string Montant_Ristourne_Client_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant_Ristourne_Client);
            }
            set { }
        }
        public double Montant_Reglement_Client { get; set; }
        public string Montant_Reglement_Client_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant_Reglement_Client);
            }
            set { }
        }
        public double Montant_Regulation_Solde { get; set; }
        public string Montant_Regulation_Solde_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant_Regulation_Solde);
            }
            set { }
        }
        public double Montant_Depense { get; set; }
        public string Montant_Depense_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant_Depense);
            }
            set { }
        }
        public double Valeur_Stock_Liquide { get; set; }
        public double Valeur_Stock_Liquide_Achat { get; set; }
        public double Valeur_Stock_Emballage { get; set; }
        public int Quantite_Stock_Casier { get; set; }
        public int Quantite_Stock_Bouteille { get; set; }
        public List<Rapport_Cloture_Facture> Facture_Emise { get; set; }
        public List<Rapport_Cloture_Mouvement_Stock> Mouvement_Stock { get; set; }
        public List<Rapport_Cloture_Mouvement_Stock> Mouvement_Stock_Emballage { get; set; }
    }

    public class Rapport_Cloture_Facture
    {
        public string NUM_DOC { get; set; }
        public string Client { get; set; }
        public double Solde_Initial { get; set; }
        public string Solde_Initial_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Solde_Initial);
            }
            set { }
        }
        public double Montant_Produit { get; set; }
        public double Montant_Produit_PU_Achat { get; set; }
        public string Montant_Produit_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant_Produit);
            }
            set { }
        }
        public double Montant_Emballage { get; set; }
        public string Montant_Emballage_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant_Emballage);
            }
            set { }
        }
        public double Montant_Retour_Emballage { get; set; }
        public string Montant_Retour_Emballage_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Montant_Retour_Emballage);
            }
            set { }
        }
        public double Consigne { get; set; }
        public string Consigne_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Consigne);
            }
            set { }
        }
        public double Versement { get; set; }
        public string Versement_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier(Versement);
            }
            set { }
        }
        public double Net_a_Payer
        {
            get
            {
                Services service = new Services();
                double val = Montant_Produit + Montant_Emballage - Montant_Retour_Emballage;
                return val;
            }
            set { }
        }
        public string Net_a_Payer_Str
        {
            get
            {
                Services service = new Services();
                double val = Montant_Produit + Montant_Emballage - Montant_Retour_Emballage;
                return service.SeparateurMillier(val);
            }
            set { }
        }
        public int Quantite_Casier { get; set; }
        public int Quantite_Bouteille { get; set; }
        public double Benefice_Brut
        {
            get
            {
                Services service = new Services();
                return (Montant_Produit - Montant_Produit_PU_Achat);
            }
            set { }
        }
        public string Benefice_Brut_Str
        {
            get
            {
                Services service = new Services();
                return service.SeparateurMillier((Montant_Produit - Montant_Produit_PU_Achat));
            }
            set { }
        }



        public double Solde_Final
        {
            get
            {
                Services service = new Services();
                double val = Solde_Initial + Montant_Produit + Montant_Emballage - Montant_Retour_Emballage  - Versement - Consigne;
                return val;
            }
            set { }
        }
        public string Solde_Final_Str
        {
            get
            {
                Services service = new Services();
                double val = Solde_Initial + Montant_Produit + Montant_Emballage - Montant_Retour_Emballage - Versement - Consigne;
                return service.SeparateurMillier(val);
            }
            set { }
        }

    }

    public class Rapport_Cloture_Mouvement_Stock
    {
        public string Code { get; set; }
        public string Designation { get; set; }

        public int Quantite_Casier_Initial { get; set; }
        public int Quantite_Bouteille_Initial { get; set; }
        public int Quantite_Plastique_Initial { get; set; }
        
        public double Valeur_Initial { get; set; }

        public int Quantite_Casier_Entree { get; set; }
        public int Quantite_Bouteille_Entree { get; set; }
        public double Valeur_Entree { get; set; }

        public int Quantite_Casier_Sotie { get; set; }
        public int Quantite_Bouteille_Sotie { get; set; }
        public double Valeur_Sotie { get; set; }

        public int Quantite_Casier_Stock { get; set; }
        public int Quantite_Plastique_Stock { get; set; }
        public int Quantite_Bouteille_Stock { get; set; }
        public double Valeur_Stock { get; set; }
    }
}