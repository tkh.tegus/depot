﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude = "ID_Regulation_Stock")]
    public class Regulation_StockVM
    {
        public Int64 ID_Regulation_Stock { get; set; }

        [Required, Display(Name = "Produit * :")]
        public int ID_Element { get; set; }
        public string Element { get; set; }

        public byte Type_Element { get; set; }

        [Display(Name = "Casier * :")]
        public int Quantite_Casier { get; set; }

        [Display(Name = "Plastique * :")]
        public int Quantite_Plastique { get; set; }

        [Display(Name = "Bouteille * :")]
        public int Quantite_Bouteille { get; set; }

        [Display(Name = "Motif :")]
        public string Motif { get; set; }

        public string NUM_DOC { get; set; }

        public DateTime Date { get; set; }

        public string Utilisateur { get; set; }

        public byte Etat { get; set; }
        public string Statut { get; set; }
    }
}