﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude = "ID_Caisse")]
    public class CaisseVM
    {
        public int ID_Caisse { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Libelle * :")]
        [StringLength(150, ErrorMessage = "Le Libelle doit contenir au plus 150 caractères")]
        public string Libelle { get; set; }
        
        public bool Actif { get; set; }

        public string Statut { get; set; }
    }
}