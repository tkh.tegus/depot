﻿using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude="ID_Commande")]
    public class CommandeVM
    {
        public Int64 ID_Commande { get; set; }

        [Display(Name = "Fournisseur *:")]
        public int ID_Fournisseur { get; set; }
        public string Fournisseur { get; set; }

        [Display(Name = "Date *:")]
        public DateTime Date { get; set; }

        public string NUM_DOC { get; set; }

        [Display(Name = "Produit ")]
        public int ID_Produit { get; set; }

        [Display(Name = "Casier ")]
        public int? Quantite_Casier_Liquide { get; set; }

        [Display(Name = "Bouteille ")]
        public int? Quantite_Bouteille_Liquide { get; set; }

        

        [Display(Name = "Emballage ")]
        public int ID_Emballage { get; set; }

        [Display(Name = "Casier ")]
        public int? Quantite_Casier_Emballage { get; set; }

        [Display(Name = "Plastique ")]
        public int? Quantite_Plastique_Emballage { get; set; }

        [Display(Name = "Bouteille ")]
        public int? Quantite_Bouteille_Emballage { get; set; }

        [Display(Name = "Casier ")]
        public int? Quantite_Casier_Emballage_Retour { get; set; }

        [Display(Name = "Plastique ")]
        public int? Quantite_Plastique_Emballage_Retour { get; set; }

        [Display(Name = "Bouteille ")]
        public int? Quantite_Bouteille_Emballage_Retour { get; set; }

        public string Montant_Commande_Liquide { get; set; }
        public string Montant_Commande_Emballage { get; set; }
        public string Montant_Commande_Retour_Emballage { get; set; }
        public string Net_A_Payer { get; set; }
        public string Net_A_Payer_Lettre { get; set; }
        public string Utilisateur { get; set; }
        public string Statut { get; set; }
        public byte Etat { get; set; }

        public int? Casier_12 { get; set; }
        public int? Plastique_12 { get; set; }
        public int? Bouteille_12 { get; set; }
        public int? Casier_15 { get; set; }
        public int? Plastique_15 { get; set; }
        public int? Bouteille_15 { get; set; }
        public int? Casier_24 { get; set; }
        public int? Plastique_24 { get; set; }
        public int? Bouteille_24 { get; set; }
        public string Montant_C12 { get; set; }
        public string Montant_C15 { get; set; }
        public string Montant_C24 { get; set; }

        public List<Detail_Commande> Liste_Liquide { get; set; }
        public List<Detail_Commande> Liste_Emballage { get; set; }

    }

    public class Detail_Commande
    {
        public string Code { get; set; }
        public string Libelle { get; set; }
        public int Qte_Casier { get; set; }
        public int Qte_Bouteille { get; set; }
        public int Qte_Casier_Emballage { get; set; }
        public int Qte_Plastique_Emballage { get; set; }
        public int Qte_Bouteille_Emballage { get; set; }
        public int PU_Casier { get; set; }
        public int PU_Plastique_Emballage { get; set; }
        public int PU_Bouteille_Emballage { get; set; }
        public int Conditionnement { get; set; }
        public string Montant_Liquide
        {
            get
            {
                Services service = new Services();
                double mt = Qte_Casier * PU_Casier + Qte_Bouteille * (PU_Casier / Conditionnement);
                return service.SeparateurMillier(mt);
            }
            set { }
        }
        public string Montant_Emballage
        {
            get
            {
                Services service = new Services();
                double mt = Qte_Casier_Emballage * (PU_Bouteille_Emballage * Conditionnement + PU_Plastique_Emballage) + Qte_Plastique_Emballage * PU_Plastique_Emballage + Qte_Bouteille_Emballage * PU_Bouteille_Emballage;
                return service.SeparateurMillier(mt);
            }
            set { }
        }
        public string Montant
        {
            get
            {
                Services service = new Services();
                double mt1 = Qte_Casier_Emballage * (PU_Bouteille_Emballage * Conditionnement + PU_Plastique_Emballage) + Qte_Plastique_Emballage * PU_Plastique_Emballage + Qte_Bouteille * PU_Bouteille_Emballage;
                double mt2 = Qte_Casier * PU_Casier + Qte_Bouteille * (PU_Casier / Conditionnement);
                return service.SeparateurMillier((mt1 + mt2));
            }
            set { }
        }
    }
}