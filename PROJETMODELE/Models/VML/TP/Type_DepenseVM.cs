﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude = "ID_Type_Depense")]
    public class Type_DepenseVM
    {
        public int ID_Type_Depense { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Libelle * :")]
        [StringLength(50, ErrorMessage = "Le libelle doit contenir au plus 50 caractères")]
        public string Libelle { get; set; }
      
        public bool Actif { get; set; }

        public string Statut { get; set; }
    }
}