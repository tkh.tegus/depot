﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.Models.VML.TP
{
    [Bind(Exclude = "ID_Emballage")]
    public class EmballageVM
    {
        public int ID_Emballage { get; set; }
      
        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Code * :")]
        [StringLength(50, ErrorMessage = "Ce code doit contenir au plus 50 caractères")]
        public string Code { get; set; }

        [Required(ErrorMessage = "Ce champs est obligatoire"), Display(Name = "Description * :")]
        [StringLength(150, ErrorMessage = "La description doit contenir au plus 150 caractères")]
        public string Description { get; set; }

        [Required, Display(Name = "Conditionnement * :")]
        [Range(0, double.MaxValue, ErrorMessage = "Le conditionnement doit être positif!")]
        public int Conditionnement { get; set; }

        [Required, Display(Name = "PU Plastique * :")]
        [Range(0, double.MaxValue, ErrorMessage = "Le PU Bouteille doit être positif!")]
        public double PU_Plastique { get; set; }

        [Required, Display(Name = "PU Bouteille * :")]
        [Range(0, double.MaxValue, ErrorMessage = "Le PU Bouteille doit être positif!")]
        public double PU_Bouteille { get; set; }
       
        public bool Actif { get; set; }

        public string Statut { get; set; }
       
    }
}