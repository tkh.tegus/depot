//------------------------------------------------------------------------------
// <auto-generated>
//    Ce code a été généré à partir d'un modèle.
//
//    Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//    Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PROJETMODELE.Models.DAL
{
    using System;
    
    public partial class FN_Recapitulatif_Commande_Result
    {
        public int Code_Commande { get; set; }
        public string Commande { get; set; }
        public string Profil { get; set; }
        public bool Executer { get; set; }
        public bool Disponible { get; set; }
    }
}
