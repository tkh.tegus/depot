//------------------------------------------------------------------------------
// <auto-generated>
//    Ce code a été généré à partir d'un modèle.
//
//    Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//    Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PROJETMODELE.Models.DAL
{
    using System;
    
    public partial class FN_Chiffre_Affaire_Par_Produit_Result
    {
        public int ID_Produit { get; set; }
        public string Famille { get; set; }
        public string Produit { get; set; }
        public int Conditionnement { get; set; }
        public int Caiser { get; set; }
        public int Bouteille { get; set; }
        public double Montant_Vente { get; set; }
        public double Montant_Achat { get; set; }
        public double Ristourne_Client { get; set; }
    }
}
