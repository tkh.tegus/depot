//------------------------------------------------------------------------------
// <auto-generated>
//    Ce code a été généré à partir d'un modèle.
//
//    Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//    Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PROJETMODELE.Models.DAL
{
    using System;
    
    public partial class FN_Historique_Stock_Result
    {
        public string Type { get; set; }
        public System.DateTime Date { get; set; }
        public string Mouvement { get; set; }
        public int Rang { get; set; }
        public string Numero { get; set; }
        public int Plastique { get; set; }
        public int Plastique_Livrer { get; set; }
        public int Plastique_Porgressif { get; set; }
        public int Bouteille { get; set; }
        public int Bouteille_Livrer { get; set; }
        public int Bouteille_Porgressif { get; set; }
        public int Casier { get; set; }
        public int Casier_Livrer { get; set; }
        public int Casier_Porgressif { get; set; }
        public int Conditionnement { get; set; }
    }
}
