//------------------------------------------------------------------------------
// <auto-generated>
//    Ce code a été généré à partir d'un modèle.
//
//    Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//    Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PROJETMODELE.Models.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class TG_Commandes
    {
        public TG_Commandes()
        {
            this.TG_Hierarchie_Commande = new HashSet<TG_Hierarchie_Commande>();
            this.TG_Droits = new HashSet<TG_Droits>();
        }
    
        public int Code_Commande { get; set; }
        public int Code_Commande_Parent { get; set; }
        public string Libelle { get; set; }
        public string ID_Control { get; set; }
        public string Description { get; set; }
        public int Ordre { get; set; }
        public string url { get; set; }
    
        public virtual ICollection<TG_Hierarchie_Commande> TG_Hierarchie_Commande { get; set; }
        public virtual ICollection<TG_Droits> TG_Droits { get; set; }
    }
}
