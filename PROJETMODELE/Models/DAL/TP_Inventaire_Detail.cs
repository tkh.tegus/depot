//------------------------------------------------------------------------------
// <auto-generated>
//    Ce code a été généré à partir d'un modèle.
//
//    Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//    Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PROJETMODELE.Models.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class TP_Inventaire_Detail
    {
        public long ID_Inventaire_Detail { get; set; }
        public long ID_Inventaire { get; set; }
        public int ID_Element { get; set; }
        public int Quantite_Casier_Theorique { get; set; }
        public int Quantite_Plastique_Theorique { get; set; }
        public int Quantite_Bouteille_Theorique { get; set; }
        public int Quantite_Casier_Reel { get; set; }
        public int Quantite_Plastique_Reel { get; set; }
        public int Quantite_Bouteille_Reel { get; set; }
        public double PU_Casier_Bouteille { get; set; }
        public double PU_Plastique { get; set; }
        public int Conditionnement { get; set; }
    
        public virtual TP_Inventaire TP_Inventaire { get; set; }
    }
}
