//------------------------------------------------------------------------------
// <auto-generated>
//    Ce code a été généré à partir d'un modèle.
//
//    Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//    Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PROJETMODELE.Models.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class TP_Client
    {
        public TP_Client()
        {
            this.TP_Encaissement = new HashSet<TP_Encaissement>();
            this.TP_Ristourne_Client = new HashSet<TP_Ristourne_Client>();
            this.TP_Vente_Liquide_Temp = new HashSet<TP_Vente_Liquide_Temp>();
            this.TP_Vente = new HashSet<TP_Vente>();
        }
    
        public int ID_Client { get; set; }
        public string Nom { get; set; }
        public string Adresse { get; set; }
        public string Telephone { get; set; }
        public System.DateTime Create_Date { get; set; }
        public System.DateTime Edit_Date { get; set; }
        public int Create_Code_User { get; set; }
        public int Edit_Code_User { get; set; }
        public bool Actif { get; set; }
    
        public virtual ICollection<TP_Encaissement> TP_Encaissement { get; set; }
        public virtual ICollection<TP_Ristourne_Client> TP_Ristourne_Client { get; set; }
        public virtual ICollection<TP_Vente_Liquide_Temp> TP_Vente_Liquide_Temp { get; set; }
        public virtual ICollection<TP_Vente> TP_Vente { get; set; }
    }
}
