﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class ProduitBM
    {
        private TP_Produit Local_Var = new TP_Produit();
        StoreEntities db = new StoreEntities();
       
        public void Save(ProduitVM Param_Var, string Code_User)
        {
            Local_Var.ID_Emballage = Param_Var.ID_Emballage;
            Local_Var.ID_Famille = Param_Var.ID_Famille;
            Local_Var.Code = Param_Var.Code;
            Local_Var.Libelle = Param_Var.Libelle;
            Local_Var.PU_Achat = Param_Var.PU_Achat;
            Local_Var.PU_Vente = Param_Var.PU_Vente;
            Local_Var.Valeur_Emballage = Param_Var.Valeur_Emballage;
            Local_Var.Produit_Decomposer = Param_Var.Produit_Decomposer;
            if(Param_Var.Produit_Decomposer)
            {
                Local_Var.ID_Produit_Base = Param_Var.ID_Produit_Base;
                Local_Var.ID_Plastique_Base = Param_Var.ID_Plastique_Base;
                Local_Var.ID_Bouteille_Base = Param_Var.ID_Bouteille_Base;
            }
            else
            {
                Local_Var.ID_Produit_Base = null;
                Local_Var.ID_Plastique_Base = null;
                Local_Var.ID_Bouteille_Base = null;
            }
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            Local_Var.Actif = true;
            db.TP_Produit.Add(Local_Var);
            db.SaveChanges();
        }

        public void Edit(int id, ProduitVM Param_Var, string Code_User)
        {
            Local_Var = db.TP_Produit.Find(id);
            Local_Var.ID_Emballage = Param_Var.ID_Emballage;
            Local_Var.ID_Famille = Param_Var.ID_Famille;
            Local_Var.Libelle = Param_Var.Libelle;
            Local_Var.Code = Param_Var.Code;
            Local_Var.PU_Achat = Param_Var.PU_Achat;
            Local_Var.PU_Vente = Param_Var.PU_Vente;
            Local_Var.Valeur_Emballage = Param_Var.Valeur_Emballage;
            Local_Var.Produit_Decomposer = Param_Var.Produit_Decomposer;
            if (Param_Var.Produit_Decomposer)
            {
                Local_Var.ID_Produit_Base = Param_Var.ID_Produit_Base;
                Local_Var.ID_Plastique_Base = Param_Var.ID_Plastique_Base;
                Local_Var.ID_Bouteille_Base = Param_Var.ID_Bouteille_Base;
            }
            else
            {
                Local_Var.ID_Produit_Base = null;
                Local_Var.ID_Plastique_Base = null;
                Local_Var.ID_Bouteille_Base = null;
            }
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Activer_Suspendre(int id, string Code_User)
        {
            Local_Var = db.TP_Produit.Find(id);
            Local_Var.Actif = !Local_Var.Actif;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

      
     
        public ProduitVM GetByID(int id)
        {
            TP_Produit LocalObjet = db.TP_Produit.Find(id);
            ProduitVM MidelObjet = new ProduitVM();
            if (LocalObjet == null)
            {
                return null;
            }

            MidelObjet.ID_Emballage = LocalObjet.ID_Emballage;
            MidelObjet.ID_Famille = LocalObjet.ID_Famille;
            MidelObjet.Famille = LocalObjet.TP_Famille.Libelle;
            MidelObjet.Libelle = LocalObjet.Libelle;
            MidelObjet.Code = LocalObjet.Code;
            MidelObjet.PU_Achat = LocalObjet.PU_Achat;
            MidelObjet.PU_Vente = LocalObjet.PU_Vente;
            MidelObjet.Valeur_Emballage = LocalObjet.Valeur_Emballage;
            MidelObjet.Produit_Decomposer = LocalObjet.Produit_Decomposer;
            MidelObjet.ID_Produit_Base = LocalObjet.ID_Produit_Base;
            MidelObjet.ID_Plastique_Base = LocalObjet.ID_Plastique_Base;
            MidelObjet.ID_Bouteille_Base = LocalObjet.ID_Bouteille_Base;
            MidelObjet.Actif = LocalObjet.Actif;
            MidelObjet.Statut = (LocalObjet.Actif) ? "Actif" : "Suspendu";           
            return MidelObjet;
        }

      
        public bool IsNotExist(ProduitVM Param_Var, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TP_Produit.Where(p => p.Libelle == Param_Var.Libelle).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TP_Produit.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Le produit {0} existe déjà", Param_Var.Libelle);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Le produit {0} existe déjà mais est actuellement supprimé, veuillez contacter un adminstrateur pour le restorer");
                return false;
            }
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle 
        /// et l'identifiant different
        /// </summary>
        /// <param name="libelle"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsNotExist(ProduitVM Param_Var, int id, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TP_Produit.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.ID_Produit != id).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TP_Produit.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.ID_Produit != id).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Le produit {0} existe déjà", Param_Var.Libelle);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Le produit  {0} existe déjà mais est actuellement supprimé, veuillez contacter un adminstrateur pour le restorer");
                return false;
            }
        }

        public Boolean VerifieEtatStock(int ID_Produit, int Casier,int Bouteille, ref string MessageRetour)
        {
            var produit = db.TP_Produit.Find(ID_Produit);
            if(produit.Produit_Decomposer == true)
            {
                produit = db.TP_Produit.Find(produit.ID_Produit_Base);
            }
            int quantite = Casier * produit.TP_Emballage.Conditionnement + Bouteille;
            var stock = db.FN_Etat_Stock(0, DateTime.Now, DateTime.Now).Where(p => p.ID_Element == ID_Produit).FirstOrDefault();
            if(quantite > (stock.Stock_Casier * stock.Conditionnement + stock.Stock_Bouteille))
            {
                MessageRetour = string.Format("Quantité en stock insuffisante. Casier : {0}, Bouteille {1} ", stock.Stock_Casier, stock.Stock_Bouteille);
                return false;
            }
            return true;
        }

    }
}