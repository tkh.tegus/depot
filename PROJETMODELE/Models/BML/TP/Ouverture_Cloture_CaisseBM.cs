﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE.Tools;

namespace PROJETMODELE.Models.BML.TP
{
    public class Ouverture_Cloture_CaisseBM
    {
        private TP_Ouverture_Cloture_Caisse Local_Var = new TP_Ouverture_Cloture_Caisse();
        StoreEntities db = new StoreEntities();

        public void Save(Ouverture_Cloture_CaisseVM Param_Var, string Code_User)
        {
            Local_Var.Date_Ouverture = Param_Var.Date_Ouverture;
            Local_Var.Date_Cloture = null;
            Local_Var.ID_Personnel = Param_Var.ID_Personnel;
            Local_Var.Solde_Caisse = db.FN_Etat_Solde_Caisse(DateTime.Now, DateTime.Now).Where(p=>p.ID_Caisse == Param_Var.ID_Caisse).FirstOrDefault().Solde;
            Local_Var.ID_Caisse = Param_Var.ID_Caisse;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            Local_Var.Etat = 0;
            db.TP_Ouverture_Cloture_Caisse.Add(Local_Var);
            db.SaveChanges();
        }

        public void Valider(int id, string Code_User)
        {
            Local_Var = db.TP_Ouverture_Cloture_Caisse.Find(id);
            Local_Var.Etat = 1;
            
            Local_Var.Date_Cloture = DateTime.Now;
            Local_Var.Edit_Code_User = int.Parse(Code_User);          
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();          
        }

        public void Annuler(int id, string Code_User)
        {
            Local_Var = db.TP_Ouverture_Cloture_Caisse.Find(id);
            Local_Var.Etat = 2;
            Local_Var.Date_Cloture = DateTime.Now;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public Ouverture_Cloture_CaisseVM GetEtatClorureById(int id)
        {
            Local_Var = db.TP_Ouverture_Cloture_Caisse.Find(id);
            Ouverture_Cloture_CaisseVM MidelObjet = new Ouverture_Cloture_CaisseVM();
            if (Local_Var.Date_Cloture == null)
            {
                Local_Var.Date_Cloture = DateTime.Now;
            }


            MidelObjet.Solde_Caisse = Local_Var.Solde_Caisse;

            MidelObjet.Personnel = db.Vue_Personnel.Where(p => p.ID_Personnel == Local_Var.ID_Personnel).Select(p => p.Libelle).FirstOrDefault();
            MidelObjet.Statut = (Local_Var.Etat == 0 ? "Ouverte" : (Local_Var.Etat == 1 ? "Clôturer" : "Annuler"));
            MidelObjet.Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == Local_Var.Create_Code_User).Select(p => p.Libelle).FirstOrDefault();
            MidelObjet.Etat = Local_Var.Etat;

            MidelObjet.Date_Ouverture = Local_Var.Date_Ouverture;
            MidelObjet.Date_Cloture = Local_Var.Date_Cloture;
            MidelObjet.ID_Personnel = Local_Var.ID_Personnel;
            MidelObjet.ID_Caisse = Local_Var.ID_Caisse;
            MidelObjet.Caisse = Local_Var.TP_Caisse.Libelle;
            MidelObjet.Personnel = db.Vue_Personnel.Where(p => p.ID_Personnel == Local_Var.ID_Personnel).Select(p => p.Libelle).FirstOrDefault();
            var vente = db.FN_Vente().Where(p => p.ID_Caisse == Local_Var.ID_Caisse && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).ToList();
            var stock = db.FN_Etat_Stock(0, Local_Var.Date_Cloture, Local_Var.Date_Cloture);
            var stock_emballage = db.FN_Etat_Stock(1, Local_Var.Date_Cloture, Local_Var.Date_Cloture);
            var solde_Caisse = db.FN_Etat_Solde_Caisse(Local_Var.Date_Ouverture, Local_Var.Date_Cloture).Where(p=>p.ID_Caisse == Local_Var.ID_Caisse).FirstOrDefault();
            var solde_client = db.FN_Etat_Solde_Client(2, Local_Var.Date_Ouverture, Local_Var.Date_Cloture).ToList();
            var solde_fournisseur = db.FN_Etat_Solde_Fournisseur(2, Local_Var.Date_Ouverture, Local_Var.Date_Cloture).ToList();
            

            if (vente != null)
            {
                MidelObjet.Montant_Ventes = (double)vente.Select(p => p.Montant_Liquide).Sum();
                MidelObjet.Benefice_Brut = (double)vente.Select(p => (p.Montant_Liquide - p.Montant_Liquide_PU_Achat)).Sum();
                MidelObjet.Quantite_Casier = vente.Select(p => p.Quantite_Casier).Sum();
                MidelObjet.Quantite_Bouteille = vente.Select(p => p.Quantite_Bouteille).Sum();
            }
            else
            {
                MidelObjet.Montant_Ventes = 0;
                MidelObjet.Benefice_Brut = 0;
                MidelObjet.Quantite_Casier = 0;
                MidelObjet.Quantite_Bouteille = 0;
            }
            
            if(stock != null)
            {
                MidelObjet.Valeur_Stock_Liquide = (double)stock.Select(p => p.Valeur_Final_Vente).Sum();
                MidelObjet.Valeur_Stock_Emballage = (double)stock_emballage.Select(p => p.Valeur_Final_Emballage).Sum();
                MidelObjet.Valeur_Stock_Liquide_Achat = (double)stock.Select(p => p.Valeur_Final_Achat).Sum();
                MidelObjet.Quantite_Stock_Casier = stock.Select(p => p.Stock_Casier).Sum();
                MidelObjet.Quantite_Stock_Bouteille = stock.Select(p => p.Stock_Plastique).Sum();
            }
            else
            {
                MidelObjet.Valeur_Stock_Liquide = 0;
                MidelObjet.Valeur_Stock_Emballage = 0;
                MidelObjet.Valeur_Stock_Liquide_Achat = 0;
                MidelObjet.Quantite_Stock_Casier = 0;
                MidelObjet.Quantite_Stock_Bouteille = 0;
            }

            ////Dépense
            //if (db.TP_Decaissement.Where(p => p.ID_Caisse == Local_Var.ID_Caisse && p.Type_Element == 2 && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).FirstOrDefault() != null)
            //{
            //    MidelObjet.Montant_Depense = db.TP_Decaissement.Where(p => p.ID_Caisse == Local_Var.ID_Caisse && p.Type_Element == 2 && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).Select(p => p.Montant).Sum();
            //}
            //else
            //{
            //    MidelObjet.Montant_Depense = 0;
            //}

            //Solde client
            if (solde_client != null)
            {
                MidelObjet.Solde_Client = solde_client.Select(p=>p.Solde).Sum();
                MidelObjet.Solde_Client_Initial = solde_client.Select(p => p.Initial).Sum();
            }
            else
            {
                MidelObjet.Solde_Client = 0;
                MidelObjet.Solde_Client_Initial = 0;
            }

            //Solde fournisseur
            if (solde_fournisseur != null)
            {
                MidelObjet.Solde_Fournisseur = solde_fournisseur.Select(p => p.Solde).Sum();
                MidelObjet.Solde_Fournisseur_Initial = solde_fournisseur.Select(p => p.Initial).Sum();
            }
            else
            {
                MidelObjet.Solde_Fournisseur = 0;
                MidelObjet.Solde_Fournisseur_Initial = 0;

            }
           
            //Solde Caisse
            if (solde_Caisse != null)
            {
                MidelObjet.Inital_Solde_Caisse = solde_Caisse.Initial;
                MidelObjet.Solde_Caisse = solde_Caisse.Solde;
            }
            else
            {
                MidelObjet.Inital_Solde_Caisse = 0;
                MidelObjet.Solde_Caisse = 0;
            }

            //Regulation Solde
            if (db.TP_Regulation_Solde.Where(p => p.Type_Element ==2 && p.ID_Element == Local_Var.ID_Caisse && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).FirstOrDefault() != null)
            {
                MidelObjet.Montant_Regulation_Solde = db.TP_Regulation_Solde.Where(p => p.Type_Element == 2 && p.ID_Element == Local_Var.ID_Caisse && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).Select(p => p.Montant).Sum();
            }
            else
            {
                MidelObjet.Montant_Regulation_Solde = 0;
            }

            //if (db.TP_Encaissement.Where(p => p.ID_Caissse == Local_Var.ID_Caisse && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).FirstOrDefault() != null)
            //{
            //    //MidelObjet.Montant_Reglement_Client = db.TP_Encaissement.Where(p => p.ID_Caissse == Local_Var.ID_Caisse &&  p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).Select(p => p.Montant).Sum();
            //    var versement = db.FN_Etat_Solde_Caisse(Local_Var.Date_Ouverture, Local_Var.Date_Cloture).Where(p => p.ID_Caisse == Local_Var.ID_Caisse).FirstOrDefault();
            //    MidelObjet.Montant_Reglement_Client = versement.Encaissement + versement;
            //}
            //else
            //{
            //    MidelObjet.Montant_Reglement_Client = 0;
            //}

            var decaissement = db.FN_Etat_Solde_Caisse(Local_Var.Date_Ouverture, Local_Var.Date_Cloture).Where(p => p.ID_Caisse == Local_Var.ID_Caisse).FirstOrDefault();
            if (decaissement != null)
            {
                MidelObjet.Montant_Reglement_Fournisseur = decaissement.Decaissement;
                MidelObjet.Montant_Ristourne_Client = decaissement.Ristourne;
                MidelObjet.Montant_Depense = decaissement.Depense;
                MidelObjet.Montant_Reglement_Client = decaissement.Encaissement + decaissement.Vente;
            }
            else
            {
                MidelObjet.Montant_Reglement_Fournisseur = 0;
                MidelObjet.Montant_Ristourne_Client = 0;
                MidelObjet.Montant_Depense = 0;
                MidelObjet.Montant_Reglement_Client = 0;
            }
            //Reglement Fournisseur
            //if (db.TP_Decaissement.Where(p => p.ID_Caisse == Local_Var.ID_Caisse && p.Type_Element == 0 && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).FirstOrDefault() != null)
            //{
            //    MidelObjet.Montant_Reglement_Fournisseur = db.TP_Decaissement.Where(p => p.ID_Caisse == Local_Var.ID_Caisse && p.Type_Element == 0 && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).Select(p => p.Montant).Sum();
            //}
            //else
            //{
            //    MidelObjet.Montant_Reglement_Fournisseur = 0;
            //}

            //Reglement ristourne client
            //if (db.TP_Decaissement.Where(p => p.ID_Caisse == Local_Var.ID_Caisse && p.Type_Element == 1 && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).FirstOrDefault() != null)
            //{
            //    MidelObjet.Montant_Ristourne_Client = db.TP_Decaissement.Where(p => p.ID_Caisse == Local_Var.ID_Caisse && p.Type_Element == 1 && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).Select(p => p.Montant).Sum();
            //}
            //else
            //{
            //    MidelObjet.Montant_Ristourne_Client = 0;
            //}

            //Reglement client
            

            //Regulation caisse
            if (db.TP_Regulation_Solde.Where(p => p.ID_Element == Local_Var.ID_Caisse && p.Type_Element == 2 && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).FirstOrDefault() != null)
            {
                MidelObjet.Montant_Regulation_Solde = db.TP_Regulation_Solde.Where(p => p.ID_Element == Local_Var.ID_Caisse && p.Type_Element == 2 && p.Date >= Local_Var.Date_Ouverture && p.Date <= Local_Var.Date_Cloture).Select(p => p.Montant).Sum();
            }
            else
            {
                MidelObjet.Montant_Regulation_Solde = 0;
            }

            MidelObjet.Facture_Emise = (from c in vente
                                        where c.ID_Caisse == Local_Var.ID_Caisse && c.Date >= Local_Var.Date_Ouverture && c.Date <= Local_Var.Date_Cloture
                                        orderby c.Date
                                        select new Rapport_Cloture_Facture
                                        {
                                            NUM_DOC = c.NUM_DOC,
                                            Client = c.Client,
                                            Montant_Produit = c.Montant_Liquide,
                                            Montant_Emballage = c.Montant_Emballage,
                                            Montant_Retour_Emballage = c.Montant_Retour_Emballage,
                                            Versement = c.Montant_Verser,
                                            Consigne = c.Montant_Consigne_Emballage,
                                            Solde_Initial = c.Solde_Liquide + c.Solde_Emballage,
                                            Quantite_Casier = c.Quantite_Casier,
                                            Quantite_Bouteille = c.Quantite_Bouteille,
                                            Montant_Produit_PU_Achat = c.Montant_Liquide_PU_Achat
                                        }).ToList();

            MidelObjet.Mouvement_Stock = (from c in db.FN_Stat_Mouvement_Stock(Local_Var.Date_Ouverture, Local_Var.Date_Cloture)
                                          select new Rapport_Cloture_Mouvement_Stock
                                          {
                                              Code = c.Code,
                                              Designation = c.Produit,
                                              Quantite_Casier_Entree = c.Quantite_Casier_Cmd,
                                              Quantite_Bouteille_Entree = c.Quantite_Bouteille_Cmd,
                                              Valeur_Entree = c.Valeur_Cmd,
                                              Quantite_Casier_Sotie = c.Quantite_Casier_Vnt,
                                              Quantite_Bouteille_Sotie = c.Quantite_Bouteille_Vnt,
                                              Valeur_Sotie = c.Valeur_Vnt,
                                              Quantite_Casier_Stock = c.Quantite_Casier_Stk,
                                              Quantite_Bouteille_Stock = c.Quantite_Bouteille_Stk,
                                              Valeur_Stock = c.Valeur_Stk,
                                              Quantite_Casier_Initial = c.Innitial_Quantite_Casier_Stk,
                                              Quantite_Bouteille_Initial = c.Innitial_Quantite_Bouteille_Stk,
                                              Valeur_Initial = c.Innitial_Valeur_Stk
                                          }).ToList();

            MidelObjet.Mouvement_Stock_Emballage = (from c in db.FN_Stat_Mouvement_Stock_Emballage(Local_Var.Date_Ouverture, Local_Var.Date_Cloture)
                                          select new Rapport_Cloture_Mouvement_Stock
                                          {
                                              Code = c.Element,
                                              Designation = c.Element,
                                              Quantite_Casier_Stock = c.Quantite_Casier_Stk,
                                              Quantite_Plastique_Stock = c.Quantite_Plastique_Stk,
                                              Quantite_Bouteille_Stock = c.Quantite_Bouteille_Stk,
                                              Valeur_Stock = c.Valeur_Stk,
                                              Quantite_Casier_Initial = c.Innitial_Quantite_Casier_Stk,
                                              Quantite_Plastique_Initial = c.Innitial_Quantite_Plastique_Stk,
                                              Quantite_Bouteille_Initial = c.Innitial_Quantite_Bouteille_Stk,
                                              Valeur_Initial = c.Innitial_Valeur_Stk
                                          }).ToList();
           
            return MidelObjet;
        }

        public bool IsCaisseOpen(int ID_Caisse)
        {
            if (db.TP_Ouverture_Cloture_Caisse.Where(p => p.ID_Caisse == ID_Caisse && p.Etat == 0).FirstOrDefault() == null)
            {
                return false;
            }
            return true;
        }

        public Ouverture_Cloture_CaisseVM GetByID(Int64 id)
        {
            TP_Ouverture_Cloture_Caisse LocalObjet = db.TP_Ouverture_Cloture_Caisse.Find(id);
            Ouverture_Cloture_CaisseVM MidelObjet = new Ouverture_Cloture_CaisseVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.Date_Ouverture = LocalObjet.Date_Ouverture;
            MidelObjet.Date_Cloture = LocalObjet.Date_Cloture;
            MidelObjet.Caisse = LocalObjet.TP_Caisse.Libelle;
            MidelObjet.ID_Caisse = LocalObjet.ID_Caisse;
            MidelObjet.Caisse = LocalObjet.TP_Caisse.Libelle;
            MidelObjet.Solde_Caisse = LocalObjet.Solde_Caisse;
            MidelObjet.ID_Personnel = LocalObjet.ID_Personnel;
            MidelObjet.Personnel = db.Vue_Personnel.Where(p => p.ID_Personnel == LocalObjet.ID_Personnel).Select(p => p.Libelle).FirstOrDefault();
            MidelObjet.Statut = (LocalObjet.Etat == 0 ? "Ouverte" : (LocalObjet.Etat == 1 ? "Clôturer" : "Annuler"));
            MidelObjet.Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == LocalObjet.Create_Code_User).Select(p => p.Libelle).FirstOrDefault();
            MidelObjet.Etat = LocalObjet.Etat;
            return MidelObjet;
        }
    }
}