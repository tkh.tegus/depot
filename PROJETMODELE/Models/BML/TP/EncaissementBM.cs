﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class EncaissementBM
    {
        private TP_Encaissement Local_Var = new TP_Encaissement();        
        StoreEntities db = new StoreEntities();      
        public Int64 Save(EncaissementVM Param_Var, string Code_User)
        {           
            Local_Var.ID_Client = Param_Var.ID_Client;
            Local_Var.ID_Caissse = Param_Var.ID_Caisse;
            Local_Var.Date = DateTime.Now;
            Local_Var.Montant = Param_Var.Montant;                    
            Local_Var.Motif = Param_Var.Motif;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            Local_Var.Etat = 0;
            db.TP_Encaissement.Add(Local_Var);
            db.SaveChanges();
            return Local_Var.ID_Encaissement;
        }

        public void Edit(Int64 id, EncaissementVM Param_Var, string Code_User)
        {
            Local_Var = db.TP_Encaissement.Find(id);
            Local_Var.ID_Client = Param_Var.ID_Client;      
            Local_Var.Montant = Param_Var.Montant;             
            Local_Var.Motif = Param_Var.Motif;          
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Valider(Int64 id, int Code_User)
        {
            Local_Var = db.TP_Encaissement.Find(id);
            Local_Var.Etat = 1;          
            Local_Var.Edit_Code_User = Code_User;
            Local_Var.Edit_Date = DateTime.Now;                       
            db.SaveChanges();            
        }

        public void Annuler(Int64 id, int Code_User)
        {
            Local_Var = db.TP_Encaissement.Find(id);
            Local_Var.Etat = 2;
            Local_Var.Edit_Code_User = Code_User;
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public EncaissementVM GetByID(Int64 id)
        {
            TP_Encaissement LocalObjet = db.TP_Encaissement.Find(id);
            EncaissementVM MidelObjet = new EncaissementVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.ID_Encaissement = id;
            MidelObjet.ID_Client = LocalObjet.ID_Client;
            MidelObjet.Client = LocalObjet.TP_Client.Nom;
            MidelObjet.ID_Caisse = LocalObjet.ID_Caissse;
            MidelObjet.Caisse = LocalObjet.TP_Caisse.Libelle;
            MidelObjet.Montant = LocalObjet.Montant;         
            MidelObjet.Date = LocalObjet.Date;
            MidelObjet.Etat = LocalObjet.Etat;
            MidelObjet.Statut = (LocalObjet.Etat == 0) ? "En Attente de validation" : (LocalObjet.Etat == 1) ? "Valider" : "Annuler";
            MidelObjet.Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == LocalObjet.Create_Code_User).Select(p => p.Libelle).FirstOrDefault();
            MidelObjet.Motif = LocalObjet.Motif;           
            MidelObjet.NUM_DOC = LocalObjet.NUM_DOC;          
            return MidelObjet;
        }
    }
}