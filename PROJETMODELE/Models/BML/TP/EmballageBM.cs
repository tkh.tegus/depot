﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class EmballageBM
    {
        private TP_Emballage Local_Var = new TP_Emballage();
        private StoreEntities db = new StoreEntities();

       
        public void Save(EmballageVM Param_Var, string Code_User)
        {
           
            Local_Var.Code = Param_Var.Code;
            Local_Var.Description = Param_Var.Description;
            Local_Var.Conditionnement = Param_Var.Conditionnement;
            Local_Var.PU_Plastique = Param_Var.PU_Plastique;
            Local_Var.PU_Bouteille = Param_Var.PU_Bouteille;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            Local_Var.Actif = true;
            db.TP_Emballage.Add(Local_Var);
            db.SaveChanges();
        }

        public void Edit(int id, EmballageVM Param_Var, string Code_User)
        {
            this.Local_Var = db.TP_Emballage.Find(id);
            Local_Var.Code = Param_Var.Code;
            Local_Var.Description = Param_Var.Description;
            Local_Var.Conditionnement = Param_Var.Conditionnement;
            Local_Var.PU_Plastique = Param_Var.PU_Plastique;
            Local_Var.PU_Bouteille = Param_Var.PU_Bouteille;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Activer_Suspendre(int id, string Code_User)
        {
            this.Local_Var = db.TP_Emballage.Find(id);
            this.Local_Var.Actif = !this.Local_Var.Actif;
            this.Local_Var.Edit_Code_User = int.Parse(Code_User);
            this.Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        
        public EmballageVM GetByID(int id)
        {
            TP_Emballage LocalObjet = db.TP_Emballage.Find(id);
            EmballageVM MidelObjet = new EmballageVM();
            if (LocalObjet == null)
            {
                return null;
            }
         
            MidelObjet.Code = LocalObjet.Code;
            MidelObjet.Description = LocalObjet.Description;
            MidelObjet.Conditionnement = LocalObjet.Conditionnement;
            MidelObjet.PU_Plastique = LocalObjet.PU_Plastique;
            MidelObjet.PU_Bouteille = LocalObjet.PU_Bouteille;
            MidelObjet.Actif = LocalObjet.Actif;
            MidelObjet.Statut = (LocalObjet.Actif) ? "Actif" : "Suspendu";
            return MidelObjet;
        }

      
        public bool IsNotExist(EmballageVM Param_Var, ref string MessagesRetour)
        {          
            if (db.TP_Emballage.Where(p => p.Code == Param_Var.Code).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TP_Emballage.Where(p => p.Code == Param_Var.Code).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("L'emballage de code {0} existe déja", Param_Var.Code);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("L'emballage de code {0} existe déja mais est actuelement supprimé, veuillez contacter un adminstrateur pour le restorer", Param_Var.Code);
                return false;
            }
        }
      
        public bool IsNotExist(EmballageVM Param_Var, int id, ref string MessagesRetour)
        {
            //string producteur = db.TP_Producteur.Where(p => p.ID_Producteur == Param_Var.ID_Producteur).Select(p => p.Libelle).FirstOrDefault();
            // si l'élément n'existe pas
            if (db.TP_Emballage.Where(p => p.Code == Param_Var.Code).Where(p => p.ID_Emballage != id).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TP_Emballage.Where(p => p.Code == Param_Var.Code).Where(p => p.Actif == true).Where(p => p.ID_Emballage != id).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("L'emballage de code {0} existe déja", Param_Var.Code);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("L'emballage de code {0} existe déja mais est actuelement supprimé, veuillez contacter un adminstrateur pour le restorer", Param_Var.Code);
                return false;
            }
        }          
    }
}