﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class InventaireBM
    {
        private TP_Inventaire Local_Var = new TP_Inventaire();
        private TP_Inventaire_Detail Local_Var_Detail = new TP_Inventaire_Detail();
        private StoreEntities db = new StoreEntities();

        public Int64 Save(InventaireVM Param_Var, string Code_User)
        {
            this.Local_Var.Date_Debut = DateTime.Now;
            this.Local_Var.Type = Param_Var.Type;
            this.Local_Var.Motif = Param_Var.Motif;
            this.Local_Var.Etat = 0;         
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            db.TP_Inventaire.Add(Local_Var);
            db.SaveChanges();
            return this.Local_Var.ID_Inventaire;
        }

        public void Edit(Int64 id, InventaireVM Param_Var, string Code_User)
        {
            this.Local_Var = db.TP_Inventaire.Find(id);
            this.Local_Var.Motif = Param_Var.Motif;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Valider(Int64 id,  string Code_User)
        {
            this.Local_Var = db.TP_Inventaire.Find(id);
            this.Local_Var.Etat = 1;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Annuler(Int64 id, string Code_User)
        {
            this.Local_Var = db.TP_Inventaire.Find(id);
            this.Local_Var.Etat = 3;

            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Reguler(Int64 id,  string Code_User)
        {
            this.Local_Var = db.TP_Inventaire.Find(id);
            this.Local_Var.Etat = 2;

            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }





        public InventaireVM GetByID(Int64 id)
        {
            TP_Inventaire LocalObjet = db.TP_Inventaire.Find(id);
            InventaireVM MidelObjet = new InventaireVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.Date_Creation = LocalObjet.Date_Debut;
            MidelObjet.Type = LocalObjet.Type;
            MidelObjet.NUM_DOC = LocalObjet.NUM_DOC;
            MidelObjet.Motif = LocalObjet.Motif;
            MidelObjet.Etat = LocalObjet.Etat;
            MidelObjet.Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == LocalObjet.Create_Code_User).Select(p => p.Libelle).FirstOrDefault();
            MidelObjet.Liste_Produit = (from c in db.TP_Inventaire_Detail
                                        where c.ID_Inventaire == id
                                        select new Detail_Inventaire
                                        {
                                            Famille = (LocalObjet.Type == 0 ? db.TP_Produit.Where(p=>p.ID_Produit == c.ID_Element).Select(p=>p.TP_Famille.Libelle).FirstOrDefault() : "" ),
                                            Produit = (LocalObjet.Type == 0 ? db.TP_Produit.Where(p=>p.ID_Produit == c.ID_Element).FirstOrDefault().Libelle : db.TP_Emballage.Where(p => p.ID_Emballage == c.ID_Element).FirstOrDefault().Code),
                                            Qte_Casier_Theorique = c.Quantite_Casier_Theorique,
                                            Qte_Casier_Reel = c.Quantite_Casier_Reel,
                                            Qte_Plastique_Theorique = c.Quantite_Plastique_Theorique,
                                            Qte_Plastique_Reel = c.Quantite_Plastique_Reel,
                                            Qte_Bouteille_Theorique = c.Quantite_Bouteille_Theorique,
                                            Qte_Bouteille_Reel = c.Quantite_Bouteille_Reel,
                                            PU_Casier_Bouteille = c.PU_Casier_Bouteille,
                                            PU_Plastique = c.PU_Plastique,
                                            Conditionnement = c.Conditionnement
                                        }).ToList();
            MidelObjet.Statut = (LocalObjet.Etat == 0 ? "En cours" : (LocalObjet.Etat == 1 ? "Valider" : (LocalObjet.Etat == 2 ? "Stock Réguler" : "Annuler")));
            return MidelObjet;
        }

        public void Edit_Detail_Grille_Inventaire(Int64 id, int key, int value)
        {
            this.Local_Var_Detail = db.TP_Inventaire_Detail.Find(id);
            if (key == 1)
            {
                this.Local_Var_Detail.Quantite_Casier_Reel = value;
            }
            else if (key == 2)
            {
                this.Local_Var_Detail.Quantite_Plastique_Reel = value;
            }else if(key == 3)
            {
                this.Local_Var_Detail.Quantite_Bouteille_Reel = value;

            }
            db.SaveChanges();
        }
        
    }
}