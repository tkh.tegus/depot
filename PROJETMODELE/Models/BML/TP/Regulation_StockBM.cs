﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class Regulation_StockBM
    {
        private TP_Regulation_Stock Local_Var = new TP_Regulation_Stock();
        private StoreEntities db = new StoreEntities();

        public Int64 Save(Regulation_StockVM Param_Var, string Code_User)
        {
            Local_Var.ID_Element = Param_Var.ID_Element;
            Local_Var.Type_Element = Param_Var.Type_Element;
            Local_Var.Quantite_Casier = Param_Var.Quantite_Casier;
            Local_Var.Quantite_Plastique = Param_Var.Quantite_Plastique;
            Local_Var.Quantite_Bouteille = Param_Var.Quantite_Bouteille;
           
            Local_Var.Motif = Param_Var.Motif;
            Local_Var.Date = DateTime.Now;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            Local_Var.Etat = 0;
            db.TP_Regulation_Stock.Add(Local_Var);
            db.SaveChanges();
            return Local_Var.ID_Regulation_Stock;
        }

        public void Edit(Int64 id, Regulation_StockVM Param_Var, string Code_User)
        {
            Local_Var = db.TP_Regulation_Stock.Find(id);
            Local_Var.ID_Element = Param_Var.ID_Element;
            Local_Var.Type_Element = Param_Var.Type_Element;
            Local_Var.Quantite_Casier = Param_Var.Quantite_Casier;
            Local_Var.Quantite_Plastique = Param_Var.Quantite_Plastique;
            Local_Var.Quantite_Bouteille = Param_Var.Quantite_Bouteille;

            Local_Var.Motif = Param_Var.Motif;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Valider(Int64 id, string Code_User)
        {
            Local_Var = db.TP_Regulation_Stock.Find(id);
            Local_Var.Etat = 1;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Annuler(Int64 id, string Code_User)
        {
            Local_Var = db.TP_Regulation_Stock.Find(id);
            Local_Var.Etat = 2;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }


        public Regulation_StockVM GetByID(Int64 id)
        {
            TP_Regulation_Stock LocalObjet = db.TP_Regulation_Stock.Find(id);
            Regulation_StockVM MidelObjet = new Regulation_StockVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.ID_Element = LocalObjet.ID_Element;
            MidelObjet.Type_Element = (byte)LocalObjet.Type_Element;
            if (LocalObjet.Type_Element == 0)
            {
                MidelObjet.Element = db.TP_Produit.Find(LocalObjet.ID_Element).Libelle;
            }
            else
            {
                MidelObjet.Element = db.TP_Emballage.Find(LocalObjet.ID_Element).Code;
            }
            
            MidelObjet.NUM_DOC = LocalObjet.NUM_DOC;
            MidelObjet.Quantite_Casier = LocalObjet.Quantite_Casier;
            MidelObjet.Quantite_Plastique = LocalObjet.Quantite_Plastique;
            MidelObjet.Quantite_Bouteille = LocalObjet.Quantite_Bouteille;
            MidelObjet.Date = LocalObjet.Date;
            MidelObjet.Motif = LocalObjet.Motif;
            MidelObjet.Etat = LocalObjet.Etat;
            MidelObjet.Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == LocalObjet.Create_Code_User).Select(p => p.Libelle).FirstOrDefault();
            MidelObjet.Statut = (LocalObjet.Etat == 0) ? "En Attente de validation" : (LocalObjet.Etat == 1) ? "Valider" : "Annuler";
            return MidelObjet;
        }
    }
}