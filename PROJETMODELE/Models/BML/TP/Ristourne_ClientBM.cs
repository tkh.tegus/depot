﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class Ristourne_ClientBM
    {
        private TP_Ristourne_Client Local_Var = new TP_Ristourne_Client();
        StoreEntities db = new StoreEntities();
        /// <summary>
        /// Enregistre un article dans la base de donnée
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        public void Save(Ristourne_ClientVM Param_Var, string Code_User)
        {
            Local_Var.ID_Client = Param_Var.ID_Client;
            Local_Var.ID_Produit = Param_Var.ID_Produit;
            Local_Var.Date = Param_Var.Date;
            Local_Var.Ristourne = Param_Var.Ristourne;
            Local_Var.Cumuler_Ristourne = Param_Var.Cumuler_Ristourne;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            Local_Var.Actif = true;
            db.TP_Ristourne_Client.Add(Local_Var);
            db.SaveChanges();
        }

        public void Edit(int id, Ristourne_ClientVM Param_Var, string Code_User)
        {
            Local_Var = db.TP_Ristourne_Client.Find(id);
            Local_Var.ID_Client = Param_Var.ID_Client;
            Local_Var.ID_Produit = Param_Var.ID_Produit;
            Local_Var.Cumuler_Ristourne = Param_Var.Cumuler_Ristourne;
            Local_Var.Date = Param_Var.Date;
            Local_Var.Ristourne = Param_Var.Ristourne;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Activer_Suspendre(int id, string Code_User)
        {
            Local_Var = db.TP_Ristourne_Client.Find(id);
            Local_Var.Actif = !Local_Var.Actif;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Delete(int id, string Code_User)
        {
            Local_Var = db.TP_Ristourne_Client.Find(id);
            db.TP_Ristourne_Client.Remove(Local_Var);
            
            db.SaveChanges();
        }

        /// <summary>
        /// Obtient un arrondisseent a partir de son identifiants
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Ristourne_ClientVM GetByID(int id)
        {
            TP_Ristourne_Client LocalObjet = db.TP_Ristourne_Client.Find(id);
            Ristourne_ClientVM MidelObjet = new Ristourne_ClientVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.ID_Client = LocalObjet.ID_Client;
            MidelObjet.Client = LocalObjet.TP_Client.Nom;
            MidelObjet.ID_Produit = LocalObjet.ID_Produit;
            MidelObjet.Produit = LocalObjet.TP_Produit.Libelle;
            MidelObjet.Date = LocalObjet.Date;
            MidelObjet.Ristourne = LocalObjet.Ristourne;
            MidelObjet.Actif = LocalObjet.Actif;
            MidelObjet.Cumuler_Ristourne = LocalObjet.Cumuler_Ristourne;
            MidelObjet.Statut = (LocalObjet.Actif) ? "Actif" : "Suspendu";
            return MidelObjet;
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle
        /// cette fonction renvoie true si 'element existe 
        /// et false dans le cas contraire
        /// </summary>
        /// <param name="libelle"></param>
        /// <returns></returns>
        public bool IsNotExist(Ristourne_ClientVM Param_Var, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TP_Ristourne_Client.Where(p => p.ID_Client == Param_Var.ID_Client && p.ID_Produit == Param_Var.ID_Produit).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TP_Ristourne_Client.Where(p => p.ID_Client == Param_Var.ID_Client && p.ID_Produit == Param_Var.ID_Produit).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Ce cLient possède déjà de ristourne sur ce produit");
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Ce cLient possède déjà de ristourne sur ce produit mais est actuelement supprimé, veillez contacter un adminstrateur pour le restorer");
                return false;
            }
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle 
        /// et l'identifiant different
        /// </summary>
        /// <param name="libelle"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsNotExist(Ristourne_ClientVM Param_Var, int id, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TP_Ristourne_Client.Where(p => p.ID_Client == Param_Var.ID_Client && p.ID_Produit == Param_Var.ID_Produit).Where(p => p.ID_Ristourne_Client != id).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TP_Ristourne_Client.Where(p => p.ID_Client == Param_Var.ID_Client && p.ID_Produit == Param_Var.ID_Produit).Where(p => p.ID_Ristourne_Client != id).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Ce cLient possède déjà de ristourne sur ce produit");
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Ce cLient possède déjà de ristourne sur ce produit mais est actuelement supprimé, veillez contacter un adminstrateur pour le restorer");
                return false;
            }
        }
    }
}