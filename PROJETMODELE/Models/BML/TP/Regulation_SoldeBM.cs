﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class Regulation_SoldeBM
    {
        private TP_Regulation_Solde Local_Var = new TP_Regulation_Solde();
        private StoreEntities db = new StoreEntities();

        public Int64 Save(Regulation_SoldeVM Param_Var, string Code_User)
        {

            Local_Var.ID_Element = Param_Var.ID_Element;
            Local_Var.Type_Element = Param_Var.Type_Element;
            Local_Var.Montant = Param_Var.Montant;
            Local_Var.Motif = Param_Var.Motif;
            Local_Var.Date = DateTime.Now;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            Local_Var.Etat = 0;
            db.TP_Regulation_Solde.Add(Local_Var);
            db.SaveChanges();
            return Local_Var.ID_Regulation_Solde;
        }

        public void Edit(Int64 id, Regulation_SoldeVM Param_Var, string Code_User)
        {
            Local_Var = db.TP_Regulation_Solde.Find(id);
            Local_Var.ID_Element = Param_Var.ID_Element;
            Local_Var.Type_Element = Param_Var.Type_Element;
            Local_Var.Montant = Param_Var.Montant;
            Local_Var.Motif = Param_Var.Motif;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Valider(Int64 id, string Code_User)
        {
            Local_Var = db.TP_Regulation_Solde.Find(id);
            Local_Var.Etat = 1;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Annuler(Int64 id, string Code_User)
        {
            Local_Var = db.TP_Regulation_Solde.Find(id);
            Local_Var.Etat = 2;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }


        public Regulation_SoldeVM GetByID(Int64 id)
        {
            TP_Regulation_Solde LocalObjet = db.TP_Regulation_Solde.Find(id);
            Regulation_SoldeVM MidelObjet = new Regulation_SoldeVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.ID_Element = LocalObjet.ID_Element;
            MidelObjet.Type_Element = (byte)LocalObjet.Type_Element;
            if (LocalObjet.Type_Element == 0)
            {
                MidelObjet.Element = db.TP_Fournisseur.Find(LocalObjet.ID_Element).Nom;
            }
            else if (LocalObjet.Type_Element == 1)
            {
                MidelObjet.Element = db.TP_Client.Find(LocalObjet.ID_Element).Nom;
            }
            else
            {
                MidelObjet.Element = db.TP_Caisse.Find(LocalObjet.ID_Element).Libelle;
            }
            MidelObjet.NUM_DOC = LocalObjet.NUM_DOC;
            MidelObjet.Montant = LocalObjet.Montant;
            MidelObjet.Date = LocalObjet.Date;
            MidelObjet.Motif = LocalObjet.Motif;
            MidelObjet.Etat = LocalObjet.Etat;
            MidelObjet.Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == LocalObjet.Create_Code_User).Select(p => p.Libelle).FirstOrDefault();
            MidelObjet.Statut = (LocalObjet.Etat == 0) ? "En Attente de validation" : (LocalObjet.Etat == 1) ? "Valider" : "Annuler";
            return MidelObjet;
        }
    }
}