﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class VenteBM
    {
        private TP_Vente Local_Var = new TP_Vente();
        private TP_Vente_Liquide Vente_Liquide = new TP_Vente_Liquide();
        private TP_Vente_Liquide_Temp Vente_Liquide_temp = new TP_Vente_Liquide_Temp();
        private TP_Vente_Emballage Vente_Emballage = new TP_Vente_Emballage();
        private TP_Vente_Emballage_Temp Vente_Emballage_temp = new TP_Vente_Emballage_Temp();
        StoreEntities db = new StoreEntities();

        public Int64 Save(VenteVM Param_var, int Code_User)
        {
            if (String.IsNullOrWhiteSpace(Param_var.Nom_Client))
            {
                var client = db.TP_Client.Find(Param_var.ID_Client);
                this.Local_Var.Nom_Client = client.Nom;
            }
            else
            {
                this.Local_Var.Nom_Client = Param_var.Nom_Client;
            }
            this.Local_Var.ID_Client = Param_var.ID_Client;
            this.Local_Var.ID_Caisse = Param_var.ID_Caisse;
            this.Local_Var.Montant = (double)Param_var.Versement_Liquide;
            this.Local_Var.Montant_Consigne_Emballage = (double)Param_var.Consigne_Emballage;
            this.Local_Var.Solde_Liquide = db.FN_Etat_Solde_Client(0, DateTime.Now, DateTime.Now).Where(p => p.ID_Client == Param_var.ID_Client).FirstOrDefault().Solde;
            this.Local_Var.Solde_Emballage = db.FN_Etat_Solde_Client(1, DateTime.Now, DateTime.Now).Where(p => p.ID_Client == Param_var.ID_Client).FirstOrDefault().Solde;
            this.Local_Var.Date = DateTime.Now;
            this.Local_Var.Create_Date = DateTime.Now;
            this.Local_Var.Edit_Date = DateTime.Now;
            this.Local_Var.Create_Code_User = Code_User;
            this.Local_Var.Edit_Code_User = Code_User;
            this.Local_Var.Etat = 1;
            db.TP_Vente.Add(this.Local_Var);
            db.SaveChanges();
            return this.Local_Var.ID_Vente;
        }

        public void Edit(Int64 id, VenteVM Param_var, int Code_User)
        {
            this.Local_Var = db.TP_Vente.Find(id);
            this.Local_Var.Montant = (double)Param_var.Versement_Liquide;
            this.Local_Var.Montant_Consigne_Emballage = (double)Param_var.Consigne_Emballage;
            this.Local_Var.Edit_Code_User = Code_User;
            this.Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Save_Detail_Liquide(VenteVM Param_var, int Code_User)
        {
            //si le produit n'existe pas!
            if (db.TP_Vente_Liquide_Temp.Where(p => p.ID_Produit == Param_var.ID_Produit && p.Create_Code_User == Code_User).FirstOrDefault() == null)
            {
                this.Vente_Liquide_temp.ID_Client = Param_var.ID_Client;
                this.Vente_Liquide_temp.ID_Produit = Param_var.ID_Produit;
                this.Vente_Liquide_temp.Quantite_Casier_Liquide = (Param_var.Quantite_Casier_Liquide == null ? 0 : Param_var.Quantite_Casier_Liquide.Value);
                this.Vente_Liquide_temp.Quantite_Bouteille_Liquide =(Param_var.Quantite_Bouteille_Liquide == null ? 0 : Param_var.Quantite_Bouteille_Liquide.Value);
                this.Vente_Liquide_temp.Create_Code_User = Code_User;
                //si l'emballage du produit a une valeur financière!
                if (db.TP_Produit.Where(p=>p.ID_Produit == Param_var.ID_Produit).Select(p=>p.Valeur_Emballage).FirstOrDefault())
                {
                    this.Vente_Liquide_temp.Quantite_Casier_Emballage = (Param_var.Quantite_Casier_Emballage == null ? 0 : Param_var.Quantite_Casier_Emballage.Value);
                    this.Vente_Liquide_temp.Quantite_Plastique_Emballage = (Param_var.Quantite_Plastique_Emballage == null ? 0 : Param_var.Quantite_Plastique_Emballage.Value);
                    this.Vente_Liquide_temp.Quantite_Bouteille_Emballage = (Param_var.Quantite_Bouteille_Emballage == null ? 0 : Param_var.Quantite_Bouteille_Emballage.Value);
                }
                else
                {
                    this.Vente_Liquide_temp.Quantite_Casier_Emballage = 0;
                    this.Vente_Liquide_temp.Quantite_Plastique_Emballage = 0;
                    this.Vente_Liquide_temp.Quantite_Bouteille_Emballage = 0;
                }
                db.TP_Vente_Liquide_Temp.Add(this.Vente_Liquide_temp);
                db.SaveChanges();
            }
            else
            {
                this.Vente_Liquide_temp = db.TP_Vente_Liquide_Temp.Where(p => p.ID_Produit == Param_var.ID_Produit && p.Create_Code_User == Code_User).FirstOrDefault();
                this.Vente_Liquide_temp.Quantite_Casier_Liquide = (Param_var.Quantite_Casier_Liquide == null ? 0 : Param_var.Quantite_Casier_Liquide.Value);
                this.Vente_Liquide_temp.Quantite_Bouteille_Liquide = (Param_var.Quantite_Bouteille_Liquide == null ? 0 : Param_var.Quantite_Bouteille_Liquide.Value);
                //si l'emballage du produit a une valeur financière!
                if (db.TP_Produit.Where(p => p.ID_Produit == Param_var.ID_Produit).Select(p => p.Valeur_Emballage).FirstOrDefault())
                {
                    this.Vente_Liquide_temp.Quantite_Casier_Emballage = (Param_var.Quantite_Casier_Emballage == null ? 0 : Param_var.Quantite_Casier_Emballage.Value);
                    this.Vente_Liquide_temp.Quantite_Plastique_Emballage = (Param_var.Quantite_Plastique_Emballage == null ? 0 : Param_var.Quantite_Plastique_Emballage.Value);
                    this.Vente_Liquide_temp.Quantite_Bouteille_Emballage = (Param_var.Quantite_Bouteille_Emballage == null ? 0 : Param_var.Quantite_Bouteille_Emballage.Value);
                }
                else
                {
                    this.Vente_Liquide_temp.Quantite_Casier_Emballage = 0;
                    this.Vente_Liquide_temp.Quantite_Plastique_Emballage = 0;
                    this.Vente_Liquide_temp.Quantite_Bouteille_Emballage = 0;
                }
                db.SaveChanges();
            }
        }

        public void Supprimer_Detail_Liquide(Int64 id)
        {
            this.Vente_Liquide_temp = db.TP_Vente_Liquide_Temp.Find(id);
            db.TP_Vente_Liquide_Temp.Remove(this.Vente_Liquide_temp);
            db.SaveChanges();
        }

        public void Save_Detail_Emballage(VenteVM Param_var, int Code_User)
        {
            //si il n'existe pas d'emballage
            if (db.TP_Vente_Emballage_Temp.Where(p => p.ID_Emballage == Param_var.ID_Emballage && p.Create_Code_User == Code_User).Count() == 0)
            {
                this.Vente_Emballage_temp.ID_Emballage = Param_var.ID_Emballage;
                this.Vente_Emballage_temp.Quantite_Casier = (Param_var.Quantite_Casier_Emballage_Retour == null ? 0 : Param_var.Quantite_Casier_Emballage_Retour.Value);
                this.Vente_Emballage_temp.Quantite_Plastique = (Param_var.Quantite_Plastique_Emballage_Retour == null ? 0 : Param_var.Quantite_Plastique_Emballage_Retour.Value);
                this.Vente_Emballage_temp.Quantite_Bouteille = (Param_var.Quantite_Bouteille_Emballage_Retour == null ? 0 : Param_var.Quantite_Bouteille_Emballage_Retour.Value);
                this.Vente_Emballage_temp.Create_Code_User = Code_User;
                db.TP_Vente_Emballage_Temp.Add(this.Vente_Emballage_temp);
                db.SaveChanges();
            }
            else
            {
                this.Vente_Emballage_temp = db.TP_Vente_Emballage_Temp.Where(p => p.ID_Emballage == Param_var.ID_Emballage && p.Create_Code_User == Code_User).FirstOrDefault();
                this.Vente_Emballage_temp.Quantite_Casier = (Param_var.Quantite_Casier_Emballage_Retour == null ? 0 : Param_var.Quantite_Casier_Emballage_Retour.Value);
                this.Vente_Emballage_temp.Quantite_Plastique = (Param_var.Quantite_Plastique_Emballage_Retour == null ? 0 : Param_var.Quantite_Plastique_Emballage_Retour.Value);
                this.Vente_Emballage_temp.Quantite_Bouteille = (Param_var.Quantite_Bouteille_Emballage_Retour == null ? 0 : Param_var.Quantite_Bouteille_Emballage_Retour.Value);
                db.SaveChanges();
            }
        }

        public void Supprimer_Detail_Emballage(Int64 id)
        {
            this.Vente_Emballage_temp = db.TP_Vente_Emballage_Temp.Find(id);
            db.TP_Vente_Emballage_Temp.Remove(this.Vente_Emballage_temp);
            db.SaveChanges();
        }

        public void Edit_Detail_Liquide(Int64 id, VenteVM Param_var, string Code_User)
        {
            //si le produit n'existe pas
            if (db.TP_Vente_Liquide.Where(p => p.ID_Vente == id && p.ID_Produit == Param_var.ID_Produit).FirstOrDefault() == null)
            {
                this.Vente_Liquide.ID_Vente = id;
                this.Vente_Liquide.ID_Produit = Param_var.ID_Produit;
                this.Vente_Liquide.Quantite_Casier_Liquide = (Param_var.Quantite_Casier_Liquide == null ? 0 : Param_var.Quantite_Casier_Liquide.Value);
                this.Vente_Liquide.Quantite_Bouteille_Liquide = (Param_var.Quantite_Bouteille_Liquide == null ? 0 : Param_var.Quantite_Bouteille_Liquide.Value);

                //si l'emballage a une valeur financière
                if (db.TP_Produit.Find(Param_var.ID_Produit).Valeur_Emballage)
                {
                    this.Vente_Liquide.Quantite_Casier_Emballage = (Param_var.Quantite_Casier_Emballage == null ? 0 : Param_var.Quantite_Casier_Emballage.Value);
                    this.Vente_Liquide.Quantite_Plastique_Emballage = (Param_var.Quantite_Plastique_Emballage == null ? 0 : Param_var.Quantite_Plastique_Emballage.Value);
                    this.Vente_Liquide.Quantite_Bouteille_Emballage = (Param_var.Quantite_Bouteille_Emballage == null ? 0 : Param_var.Quantite_Bouteille_Emballage.Value);
                }
                else
                {
                    this.Vente_Liquide.Quantite_Casier_Emballage = 0;
                    this.Vente_Liquide.Quantite_Plastique_Emballage = 0;
                    this.Vente_Liquide.Quantite_Bouteille_Emballage = 0;
                }
                db.TP_Vente_Liquide.Add(this.Vente_Liquide);
                db.SaveChanges();
            }
            else
            {
                this.Vente_Liquide = db.TP_Vente_Liquide.Where(p => p.ID_Vente == id && p.ID_Produit == Param_var.ID_Produit).FirstOrDefault();
                this.Vente_Liquide.Quantite_Casier_Liquide = (Param_var.Quantite_Casier_Liquide == null ? 0 : Param_var.Quantite_Casier_Liquide.Value);
                this.Vente_Liquide.Quantite_Bouteille_Liquide = (Param_var.Quantite_Bouteille_Liquide == null ? 0 : Param_var.Quantite_Bouteille_Liquide.Value);

                //si l'emballage a une valeur financière
                if (db.TP_Produit.Find(Param_var.ID_Produit).Valeur_Emballage)
                {
                    this.Vente_Liquide.Quantite_Casier_Emballage = (Param_var.Quantite_Casier_Emballage == null ? 0 : Param_var.Quantite_Casier_Emballage.Value);
                    this.Vente_Liquide.Quantite_Plastique_Emballage = (Param_var.Quantite_Plastique_Emballage == null ? 0 : Param_var.Quantite_Plastique_Emballage.Value);
                    this.Vente_Liquide.Quantite_Bouteille_Emballage = (Param_var.Quantite_Bouteille_Emballage == null ? 0 : Param_var.Quantite_Bouteille_Emballage.Value);
                }
                else
                {
                    this.Vente_Liquide.Quantite_Casier_Emballage = 0;
                    this.Vente_Liquide.Quantite_Plastique_Emballage = 0;
                    this.Vente_Liquide.Quantite_Bouteille_Emballage = 0;
                }
                db.SaveChanges();
            }
        }

        public Int64 Supprimer_Detail_Edit_Liquide(Int64 id)
        {
            this.Vente_Liquide = db.TP_Vente_Liquide.Find(id);
            Int64 ID_Vente = this.Vente_Liquide.ID_Vente;
            db.TP_Vente_Liquide.Remove(this.Vente_Liquide);
            db.SaveChanges();
            return ID_Vente;
        }

        public void Edit_Detail_Emballage(Int64 id, VenteVM Param_var, string Code_User)
        {
            //si l'emballage n'existe pas
            if (db.TP_Vente_Emballage.Where(p => p.ID_Vente == id && p.ID_Emballage == Param_var.ID_Emballage).FirstOrDefault() == null)
            {
                this.Vente_Emballage.ID_Vente = id;
                this.Vente_Emballage.ID_Emballage = Param_var.ID_Emballage;
                this.Vente_Emballage.Quantite_Casier = (Param_var.Quantite_Casier_Emballage_Retour == null ? 0 : Param_var.Quantite_Casier_Emballage_Retour.Value);
                this.Vente_Emballage.Quantite_Plastique = (Param_var.Quantite_Plastique_Emballage_Retour == null ? 0 : Param_var.Quantite_Plastique_Emballage_Retour.Value);
                this.Vente_Emballage.Quantite_Bouteille = (Param_var.Quantite_Bouteille_Emballage_Retour == null ? 0 : Param_var.Quantite_Bouteille_Emballage_Retour.Value);
                db.TP_Vente_Emballage.Add(this.Vente_Emballage);
                db.SaveChanges();
            }
            else
            {
                this.Vente_Emballage = db.TP_Vente_Emballage.Where(p => p.ID_Vente == id && p.ID_Emballage == Param_var.ID_Emballage).FirstOrDefault();
                this.Vente_Emballage.Quantite_Casier = (Param_var.Quantite_Casier_Emballage_Retour == null ? 0 : Param_var.Quantite_Casier_Emballage_Retour.Value);
                this.Vente_Emballage.Quantite_Plastique = (Param_var.Quantite_Plastique_Emballage_Retour == null ? 0 : Param_var.Quantite_Plastique_Emballage_Retour.Value);
                this.Vente_Emballage.Quantite_Bouteille = (Param_var.Quantite_Bouteille_Emballage_Retour == null ? 0 : Param_var.Quantite_Bouteille_Emballage_Retour.Value);
                db.SaveChanges();
            }
        }

        public Int64 Supprimer_Detail_Edit_Emballage(Int64 id)
        {
            this.Vente_Emballage = db.TP_Vente_Emballage.Find(id);
            Int64 ID_Vente = this.Vente_Emballage.ID_Vente;
            db.TP_Vente_Emballage.Remove(this.Vente_Emballage);
            db.SaveChanges();
            return ID_Vente;
        }

        public VenteVM GetByCmdID(Int64 id)
        {
            TP_Vente LocalObjet = db.TP_Vente.Find(id);
            VenteVM MidelObjet = new VenteVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.NUM_DOC = LocalObjet.NUM_DOC;
            MidelObjet.Nom_Client = LocalObjet.Nom_Client;
            MidelObjet.ID_Client = LocalObjet.ID_Client;
            MidelObjet.Date = LocalObjet.Date;
            MidelObjet.Solde_Liquide = LocalObjet.Solde_Liquide;
            MidelObjet.Solde_Emballage = LocalObjet.Solde_Emballage;
            MidelObjet.Versement_Liquide = LocalObjet.Montant;
            MidelObjet.Consigne_Emballage = LocalObjet.Montant_Consigne_Emballage;
            return MidelObjet;
        }

        public VenteVM GetByID(Int64 id)
        {
            TP_Vente LocalObjet = db.TP_Vente.Find(id);
            VenteVM MidelObjet = new VenteVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.Nom_Client = LocalObjet.Nom_Client;
            MidelObjet.ID_Client = LocalObjet.ID_Client;
            MidelObjet.NUM_DOC = LocalObjet.NUM_DOC;
            MidelObjet.Date = LocalObjet.Date;
            MidelObjet.Liste_Liquide = (from c in db.TP_Vente_Liquide
                                        where c.ID_Vente == id
                                        orderby c.TP_Produit.Libelle
                                        select new Detail_Vente
                                        {
                                            Code = c.TP_Produit.Code,
                                            Libelle = c.TP_Produit.Libelle,
                                            Qte_Casier = c.Quantite_Casier_Liquide,
                                            Qte_Bouteille = c.Quantite_Bouteille_Liquide,
                                            PU_Casier = ((bool)c.Cumuler_Ristourne ? (double)c.PU_Vente : (double)(c.PU_Vente - c.Ristourne_Client)) ,
                                            Conditionnement = (int)c.Conditionnement,
                                            Qte_Casier_Emballage = c.Quantite_Casier_Emballage,
                                            Qte_Plastique_Emballage = c.Quantite_Plastique_Emballage,
                                            Qte_Bouteille_Emballage = c.Quantite_Bouteille_Emballage,
                                            PU_Plastique_Emballage = (int)c.PU_Plastique,
                                            PU_Bouteille_Emballage = (int)c.PU_Bouteille,

                                        }).ToList();
            MidelObjet.Liste_Emballage = (from c in db.TP_Vente_Emballage
                                        where c.ID_Vente == id
                                        orderby c.TP_Emballage.Code
                                        select new Detail_Vente
                                        {
                                            Code = c.TP_Emballage.Code,
                                            Libelle = c.TP_Emballage.Description,
                                            Qte_Casier_Emballage = c.Quantite_Casier,
                                            Qte_Plastique_Emballage = c.Quantite_Plastique,
                                            Qte_Bouteille_Emballage = c.Quantite_Bouteille,
                                            PU_Plastique_Emballage = c.PU_Plastique,
                                            PU_Bouteille_Emballage = c.PU_Bouteille,
                                            Conditionnement = c.Conditionnement,
                                            Qte_Casier = 0,
                                            Qte_Bouteille = 0,
                                            PU_Casier = 0,
                                        }).ToList();
            var Qte_12 = MidelObjet.Liste_Emballage.Where(p => p.Conditionnement == 12).FirstOrDefault();
            MidelObjet.Casier_12 = (Qte_12 == null ? 0 : Qte_12.Qte_Casier_Emballage);
            MidelObjet.Plastique_12 = (Qte_12 == null ? 0 : Qte_12.Qte_Plastique_Emballage);
            MidelObjet.Bouteille_12 = (Qte_12 == null ? 0 : Qte_12.Qte_Bouteille_Emballage);
            MidelObjet.Montant_C12 = (Qte_12 == null ? "0" : Qte_12.Montant_Emballage);

            var Qte_15 = MidelObjet.Liste_Emballage.Where(p => p.Conditionnement == 15).FirstOrDefault();
            MidelObjet.Casier_15 = (Qte_15 == null ? 0 : Qte_15.Qte_Casier_Emballage);
            MidelObjet.Plastique_15 = (Qte_15 == null ? 0 : Qte_15.Qte_Plastique_Emballage);
            MidelObjet.Bouteille_15 = (Qte_15 == null ? 0 : Qte_15.Qte_Bouteille_Emballage);
            MidelObjet.Montant_C15 = (Qte_15 == null ? "0" : Qte_15.Montant_Emballage);

            var Qte_24 = MidelObjet.Liste_Emballage.Where(p => p.Conditionnement == 24).FirstOrDefault();
            MidelObjet.Casier_24 = (Qte_24 == null ? 0 : Qte_24.Qte_Casier_Emballage);
            MidelObjet.Plastique_24 = (Qte_24 == null ? 0 : Qte_24.Qte_Plastique_Emballage);
            MidelObjet.Bouteille_24 = (Qte_24 == null ? 0 : Qte_24.Qte_Bouteille_Emballage);
            MidelObjet.Montant_C24 = (Qte_24 == null ? "0" : Qte_24.Montant_Emballage);


            var pro = db.TP_Vente_Liquide.Where(p => p.ID_Vente == id).ToList();
            Services service = new Services();
            var Montant_Facture = db.FN_Montant_Facture(id).FirstOrDefault();
            MidelObjet.Montant_Facture_Liquide = service.SeparateurMillier(Montant_Facture == null ? 0 : Montant_Facture.Montant_Liquide);
            MidelObjet.Montant_Facture_Emballage = service.SeparateurMillier(Montant_Facture == null ? 0 : Montant_Facture.Montant_Emballage);
            MidelObjet.Montant_Facture_Retour_Emballage = service.SeparateurMillier(Montant_Facture == null ? 0 : Montant_Facture.Montant_Retour_Emballage);
            MidelObjet.Net_A_Payer = service.SeparateurMillier(Montant_Facture == null ? 0 : (Montant_Facture.Montant_Liquide + Montant_Facture.Montant_Emballage - Montant_Facture.Montant_Retour_Emballage));
            MidelObjet.Net_A_Payer_ = (Montant_Facture == null ? 0 : (Montant_Facture.Montant_Liquide + Montant_Facture.Montant_Emballage - Montant_Facture.Montant_Retour_Emballage));
            MidelObjet.Versement_Liquide = LocalObjet.Montant;
            MidelObjet.Consigne_Emballage = LocalObjet.Montant_Consigne_Emballage;
            Convertisseur convertir = new Convertisseur();
            MidelObjet.Net_A_Payer_Lettre = convertir.convertir(Montant_Facture == null ? 0 : (Montant_Facture.Montant_Liquide + Montant_Facture.Montant_Emballage - Montant_Facture.Montant_Retour_Emballage));
            MidelObjet.Utilisateur = db.Vue_Personnel.FirstOrDefault(p => p.ID_Personnel == LocalObjet.Create_Code_User).Libelle;
            var liq = (Montant_Facture == null ? 0 : Montant_Facture.Montant_Liquide);
            var emb = (Montant_Facture == null ? 0 : Montant_Facture.Montant_Emballage);
            var retour_emb = (Montant_Facture == null ? 0 :  Montant_Facture.Montant_Retour_Emballage);

            MidelObjet.Solde_Liquide = LocalObjet.Solde_Liquide;
            MidelObjet.Solde_Emballage = LocalObjet.Solde_Emballage;
            MidelObjet.Solde_Final_Liquide = LocalObjet.Solde_Liquide + liq - LocalObjet.Montant;
            MidelObjet.Solde_Final_Emballage = LocalObjet.Solde_Emballage + emb - retour_emb - LocalObjet.Montant_Consigne_Emballage; 
            return MidelObjet;

        }

        public void Valider(Int64 id, int Code_User)
        {
            this.Local_Var = db.TP_Vente.Find(id);
            Local_Var.Etat = 1;
            Local_Var.Edit_Code_User = Code_User;
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Annuler(Int64 id, int Code_User)
        {
            this.Local_Var = db.TP_Vente.Find(id);
            Local_Var.Etat = 2;
            Local_Var.Edit_Code_User = Code_User;
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Edit_Grille(Int64 id, int key, int value)
        {
            this.Vente_Liquide_temp = db.TP_Vente_Liquide_Temp.Find(id);
            if (key == 1)
            {
                this.Vente_Liquide_temp.Quantite_Casier_Liquide = value;
            }
            else if (key == 2)
            {
                this.Vente_Liquide_temp.Quantite_Bouteille_Liquide = value;
            }
            else if (key == 3)
            {
                this.Vente_Liquide_temp.Quantite_Casier_Emballage = value;
            }
            else if (key == 4)
            {
                this.Vente_Liquide_temp.Quantite_Plastique_Emballage = value;
            }
            else
            {
                this.Vente_Liquide_temp.Quantite_Bouteille_Emballage = value;
            }
            db.SaveChanges();
        }

        public Int64 Edit_Grille_1(Int64 id, int key, int value)
        {
            this.Vente_Liquide = db.TP_Vente_Liquide.Find(id);
            if (key == 1)
            {
                this.Vente_Liquide.Quantite_Casier_Liquide = value;
            }
            else if (key == 2)
            {
                this.Vente_Liquide.Quantite_Bouteille_Liquide = value;
            }
            else if (key == 3)
            {
                this.Vente_Liquide.Quantite_Casier_Emballage = value;
            }
            else if (key == 4)
            {
                this.Vente_Liquide.Quantite_Plastique_Emballage = value;
            }
            else
            {
                this.Vente_Liquide.Quantite_Bouteille_Emballage = value;
            }
            db.SaveChanges();
            return this.Vente_Liquide.ID_Vente;
        }
    }
}