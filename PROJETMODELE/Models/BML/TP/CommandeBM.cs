﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class CommandeBM
    {
        private TP_Commande Local_Var = new TP_Commande();
        private TP_Commande_Liquide_Temp Commande_Liquide_temp = new TP_Commande_Liquide_Temp();
        private TP_Commande_Emballage_Temp Commande_Emballage_temp = new TP_Commande_Emballage_Temp();
        private TP_Commande_Liquide Commande_Liquide = new TP_Commande_Liquide();
        private TP_Commande_Emballage Commande_Emballage = new TP_Commande_Emballage();
        StoreEntities db = new StoreEntities();

        public Int64 Save(CommandeVM Param_var, int Code_User)
        {
            this.Local_Var.ID_Fournisseur = Param_var.ID_Fournisseur;
            this.Local_Var.Date = DateTime.Now; 
            this.Local_Var.Create_Date = DateTime.Now;
            this.Local_Var.Edit_Date = DateTime.Now;
            this.Local_Var.Create_Code_User = Code_User;
            this.Local_Var.Edit_Code_User = Code_User;
            this.Local_Var.Etat = 0;
            db.TP_Commande.Add(this.Local_Var);
            db.SaveChanges();
            return this.Local_Var.ID_Commande;
        }

        public void Edit(Int64 id, CommandeVM Param_var, int Code_User)
        {
            this.Local_Var = db.TP_Commande.Find(id);
            this.Local_Var.ID_Fournisseur = Param_var.ID_Fournisseur;
            this.Local_Var.Edit_Date = DateTime.Now;
            this.Local_Var.Edit_Code_User = Code_User;
            db.SaveChanges();
        }

        public void Save_Detail_Liquide(CommandeVM Param_var, int Code_User)
        {
            //si le produit n'existe pas!
            if (db.TP_Commande_Liquide_Temp.Where(p => p.ID_Produit == Param_var.ID_Produit && p.Create_Code_User == Code_User).FirstOrDefault() == null)
            {
                this.Commande_Liquide_temp.ID_Produit = Param_var.ID_Produit;
                this.Commande_Liquide_temp.Quantite_Casier_Liquide = (Param_var.Quantite_Casier_Liquide == null ? 0 : Param_var.Quantite_Casier_Liquide.Value);
                this.Commande_Liquide_temp.Quantite_Bouteille_Liquide = (Param_var.Quantite_Bouteille_Liquide == null ? 0 : Param_var.Quantite_Bouteille_Liquide.Value);
                this.Commande_Liquide_temp.Create_Code_User = Code_User;
                //si l'emballage du produit a une valeur financière!
                if (db.TP_Produit.Find(Param_var.ID_Produit).Valeur_Emballage == true)
                {
                    this.Commande_Liquide_temp.Quantite_Casier_Emballage = (Param_var.Quantite_Casier_Emballage == null ? 0 : Param_var.Quantite_Casier_Emballage.Value);
                    this.Commande_Liquide_temp.Quantite_Plastique_Emballage = (Param_var.Quantite_Plastique_Emballage == null ? 0 : Param_var.Quantite_Plastique_Emballage.Value);
                    this.Commande_Liquide_temp.Quantite_Bouteille_Emballage = (Param_var.Quantite_Bouteille_Emballage == null ? 0 : Param_var.Quantite_Bouteille_Emballage.Value);
                }
                else
                {
                    this.Commande_Liquide_temp.Quantite_Casier_Emballage = 0;
                    this.Commande_Liquide_temp.Quantite_Plastique_Emballage = 0;
                    this.Commande_Liquide_temp.Quantite_Bouteille_Emballage = 0;
                }
                db.TP_Commande_Liquide_Temp.Add(this.Commande_Liquide_temp);
                db.SaveChanges();
            }
            else
            {
                this.Commande_Liquide_temp = db.TP_Commande_Liquide_Temp.Where(p => p.ID_Produit == Param_var.ID_Produit && p.Create_Code_User == Code_User).FirstOrDefault();
                this.Commande_Liquide_temp.Quantite_Casier_Liquide = (Param_var.Quantite_Casier_Liquide == null ? 0 : Param_var.Quantite_Casier_Liquide.Value);
                this.Commande_Liquide_temp.Quantite_Bouteille_Liquide = (Param_var.Quantite_Bouteille_Liquide == null ? 0 : Param_var.Quantite_Bouteille_Liquide.Value);
                //si l'emballage du produit a une valeur financière!
                if (db.TP_Produit.Find(Param_var.ID_Produit).Valeur_Emballage == true)
                {
                    this.Commande_Liquide_temp.Quantite_Casier_Emballage = (Param_var.Quantite_Casier_Emballage == null ? 0 : Param_var.Quantite_Casier_Emballage.Value);
                    this.Commande_Liquide_temp.Quantite_Plastique_Emballage = (Param_var.Quantite_Plastique_Emballage == null ? 0 : Param_var.Quantite_Plastique_Emballage.Value);
                    this.Commande_Liquide_temp.Quantite_Bouteille_Emballage = (Param_var.Quantite_Bouteille_Emballage == null ? 0 : Param_var.Quantite_Bouteille_Emballage.Value);
                }
                else
                {
                    this.Commande_Liquide_temp.Quantite_Casier_Emballage = 0;
                    this.Commande_Liquide_temp.Quantite_Plastique_Emballage = 0;
                    this.Commande_Liquide_temp.Quantite_Bouteille_Emballage = 0;
                }
                db.SaveChanges();
            }
        }

        public void Supprimer_Detail_Liquide(Int64 id)
        {
            this.Commande_Liquide_temp = db.TP_Commande_Liquide_Temp.Find(id);
            int Create_Code_User = this.Commande_Liquide_temp.Create_Code_User;
            db.TP_Commande_Liquide_Temp.Remove(this.Commande_Liquide_temp);
            db.SaveChanges();
        }

        public void Save_Detail_Emballage(CommandeVM Param_var, int Code_User)
        {
            var emballage = db.TP_Emballage.Where(p => p.ID_Emballage == Param_var.ID_Emballage).FirstOrDefault();
            //si il n'existe pas d'emballage
            if (db.TP_Commande_Emballage_Temp.Where(p => p.ID_Emballage == Param_var.ID_Emballage && p.Create_Code_User == Code_User).Count() == 0)
            {
                this.Commande_Emballage_temp.ID_Emballage = Param_var.ID_Emballage;
                this.Commande_Emballage_temp.Quantite_Casier = (Param_var.Quantite_Casier_Emballage_Retour == null ? 0 : Param_var.Quantite_Casier_Emballage_Retour.Value);
                this.Commande_Emballage_temp.Quantite_Plastique = (Param_var.Quantite_Plastique_Emballage_Retour == null ? 0 : Param_var.Quantite_Plastique_Emballage_Retour.Value);
                this.Commande_Emballage_temp.Quantite_Bouteille = (Param_var.Quantite_Bouteille_Emballage_Retour == null ? 0 : Param_var.Quantite_Bouteille_Emballage_Retour.Value);
                this.Commande_Emballage_temp.Create_Code_User = Code_User;
                db.TP_Commande_Emballage_Temp.Add(this.Commande_Emballage_temp);
                db.SaveChanges();
            }
            else
            {
                this.Commande_Emballage_temp = db.TP_Commande_Emballage_Temp.Where(p => p.ID_Emballage == Param_var.ID_Emballage && p.Create_Code_User == Code_User).FirstOrDefault();
                this.Commande_Emballage_temp.Quantite_Casier = (Param_var.Quantite_Casier_Emballage_Retour == null ? 0 : Param_var.Quantite_Casier_Emballage_Retour.Value);
                this.Commande_Emballage_temp.Quantite_Plastique = (Param_var.Quantite_Plastique_Emballage_Retour == null ? 0 : Param_var.Quantite_Plastique_Emballage_Retour.Value);
                this.Commande_Emballage_temp.Quantite_Bouteille = (Param_var.Quantite_Bouteille_Emballage_Retour == null ? 0 : Param_var.Quantite_Bouteille_Emballage_Retour.Value);
                db.SaveChanges();
            }
        }

        public void Supprimer_Detail_Emballage(Int64 id)
        {
            this.Commande_Emballage_temp = db.TP_Commande_Emballage_Temp.Find(id);
            int Create_Code_User = this.Commande_Emballage_temp.Create_Code_User;
            db.TP_Commande_Emballage_Temp.Remove(this.Commande_Emballage_temp);
            db.SaveChanges();
        }

        public void Edit_Detail_Liquide(Int64 id, CommandeVM Param_var, string Code_User)
        {
            //si le produit n'existe pas
            if (db.TP_Commande_Liquide.Where(p => p.ID_Commande == id && p.ID_Produit == Param_var.ID_Produit).FirstOrDefault() == null)
            {
                this.Commande_Liquide.ID_Commande = Param_var.ID_Commande;
                this.Commande_Liquide.ID_Produit = Param_var.ID_Produit;
                this.Commande_Liquide.Quantite_Casier_Liquide = (Param_var.Quantite_Casier_Liquide == null ? 0 : Param_var.Quantite_Casier_Liquide.Value);
                this.Commande_Liquide.Quantite_Bouteille_Liquide = (Param_var.Quantite_Bouteille_Liquide == null ? 0 : Param_var.Quantite_Bouteille_Liquide.Value);
                //si l'emballage a une valeur financière
                if (db.TP_Produit.Find(Param_var.ID_Produit).Valeur_Emballage == true)
                {
                    this.Commande_Liquide.Quantite_Casier_Emballage = (Param_var.Quantite_Casier_Emballage == null ? 0 : Param_var.Quantite_Casier_Emballage.Value);
                    this.Commande_Liquide.Quantite_Plastique_Emballage = (Param_var.Quantite_Plastique_Emballage == null ? 0 : Param_var.Quantite_Plastique_Emballage.Value);
                    this.Commande_Liquide.Quantite_Bouteille_Emballage = (Param_var.Quantite_Bouteille_Emballage == null ? 0 : Param_var.Quantite_Bouteille_Emballage.Value);
                }
                else
                {
                    this.Commande_Liquide.Quantite_Casier_Emballage = 0;
                    this.Commande_Liquide.Quantite_Plastique_Emballage = 0;
                    this.Commande_Liquide.Quantite_Bouteille_Emballage = 0;
                }
                db.TP_Commande_Liquide.Add(this.Commande_Liquide);
                db.SaveChanges();
            }
            else
            {
                this.Commande_Liquide = db.TP_Commande_Liquide.Where(p => p.ID_Commande == id && p.ID_Produit == Param_var.ID_Produit).FirstOrDefault();
                this.Commande_Liquide.Quantite_Casier_Liquide = (Param_var.Quantite_Casier_Liquide == null ? 0 : Param_var.Quantite_Casier_Liquide.Value);
                this.Commande_Liquide.Quantite_Bouteille_Liquide = (Param_var.Quantite_Bouteille_Liquide == null ? 0 : Param_var.Quantite_Bouteille_Liquide.Value);
                //si l'emballage a une valeur financière
                if (db.TP_Produit.Find(Param_var.ID_Produit).Valeur_Emballage == true)
                {
                    this.Commande_Liquide.Quantite_Casier_Emballage = (Param_var.Quantite_Casier_Emballage == null ? 0 : Param_var.Quantite_Casier_Emballage.Value); 
                    this.Commande_Liquide.Quantite_Plastique_Emballage = (Param_var.Quantite_Plastique_Emballage == null ? 0 : Param_var.Quantite_Plastique_Emballage.Value); 
                    this.Commande_Liquide.Quantite_Bouteille_Emballage = (Param_var.Quantite_Bouteille_Emballage == null ? 0 : Param_var.Quantite_Bouteille_Emballage.Value); 
                }
                else
                {
                    this.Commande_Liquide.Quantite_Casier_Emballage = 0;
                    this.Commande_Liquide.Quantite_Plastique_Emballage = 0;
                    this.Commande_Liquide.Quantite_Bouteille_Emballage = 0;
                }
                db.SaveChanges();
            }          
        }

        public Int64 Supprimer_Detail_Edit_Liquide(Int64 id)
        {
            this.Commande_Liquide = db.TP_Commande_Liquide.Find(id);
            Int64 ID_Commande = this.Commande_Liquide.ID_Commande;
            db.TP_Commande_Liquide.Remove(this.Commande_Liquide);
            db.SaveChanges();

            return ID_Commande;
        }

        public void Edit_Detail_Emballage(Int64 id, CommandeVM Param_var, string Code_User)
        {
            //si l'emballage n'existe pas
            if (db.TP_Commande_Emballage.Where(p => p.ID_Commande == id && p.ID_Emballage == Param_var.ID_Emballage).FirstOrDefault() == null)
            {
                this.Commande_Emballage.ID_Commande = id;
                this.Commande_Emballage.ID_Emballage = Param_var.ID_Emballage;
                this.Commande_Emballage.Quantite_Casier = (Param_var.Quantite_Casier_Emballage_Retour == null ? 0 : Param_var.Quantite_Casier_Emballage_Retour.Value);
                this.Commande_Emballage.Quantite_Plastique = (Param_var.Quantite_Plastique_Emballage_Retour == null ? 0 : Param_var.Quantite_Plastique_Emballage_Retour.Value);
                this.Commande_Emballage.Quantite_Bouteille = (Param_var.Quantite_Bouteille_Emballage_Retour == null ? 0 : Param_var.Quantite_Bouteille_Emballage_Retour.Value);
                db.TP_Commande_Emballage.Add(this.Commande_Emballage);
                db.SaveChanges();
            }
            else
            {
                this.Commande_Emballage = db.TP_Commande_Emballage.Where(p => p.ID_Commande == id && p.ID_Emballage == Param_var.ID_Emballage).FirstOrDefault();
                this.Commande_Emballage.Quantite_Casier = (Param_var.Quantite_Casier_Emballage_Retour == null ? 0 : Param_var.Quantite_Casier_Emballage_Retour.Value);
                this.Commande_Emballage.Quantite_Plastique = (Param_var.Quantite_Plastique_Emballage_Retour == null ? 0 : Param_var.Quantite_Plastique_Emballage_Retour.Value);
                this.Commande_Emballage.Quantite_Bouteille = (Param_var.Quantite_Bouteille_Emballage_Retour == null ? 0 : Param_var.Quantite_Bouteille_Emballage_Retour.Value);
                db.SaveChanges();
            }          
        }

        public Int64 Supprimer_Detail_Edit_Emballage(Int64 id)
        {
            this.Commande_Emballage = db.TP_Commande_Emballage.Find(id);
            Int64 ID_Commande = this.Commande_Emballage.ID_Commande;
            db.TP_Commande_Emballage.Remove(this.Commande_Emballage);
            db.SaveChanges();

            return ID_Commande;
        }

        public CommandeVM GetByCmdID(Int64 id, int Code_User)
        {
            TP_Commande LocalObjet = db.TP_Commande.Find(id);
            CommandeVM MidelObjet = new CommandeVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.ID_Commande = LocalObjet.ID_Commande;
            MidelObjet.Fournisseur = db.TP_Fournisseur.Where(p=>p.ID_Fournisseur == LocalObjet.ID_Fournisseur).Select(p=>p.Nom).FirstOrDefault();
            MidelObjet.ID_Fournisseur = LocalObjet.ID_Fournisseur;
            MidelObjet.Date = LocalObjet.Date;
            MidelObjet.NUM_DOC = LocalObjet.NUM_DOC;
            return MidelObjet;
        }

        public CommandeVM GetByID(Int64 id, int Code_User)
        {
            TP_Commande LocalObjet = db.TP_Commande.Find(id);
            CommandeVM MidelObjet = new CommandeVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.ID_Commande = LocalObjet.ID_Commande;
            MidelObjet.Fournisseur = db.TP_Fournisseur.Where(p => p.ID_Fournisseur == LocalObjet.ID_Fournisseur).Select(p => p.Nom).FirstOrDefault();
            MidelObjet.ID_Fournisseur = LocalObjet.ID_Fournisseur;
            MidelObjet.Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == LocalObjet.Create_Code_User).Select(p => p.Libelle).FirstOrDefault();
            MidelObjet.Etat = LocalObjet.Etat;
            MidelObjet.NUM_DOC = LocalObjet.NUM_DOC;
            MidelObjet.Statut = (LocalObjet.Etat == 0) ? "En Attente de Validation" : (LocalObjet.Etat == 1) ? "Valider" : "Annuler";
            MidelObjet.Liste_Liquide = (from c in db.TP_Commande_Liquide
                                        where c.ID_Commande == id
                                        orderby c.TP_Produit.Libelle
                                        select new Detail_Commande
                                        {
                                            Code = c.TP_Produit.Code,
                                            Libelle = c.TP_Produit.Libelle,
                                            Qte_Casier = c.Quantite_Casier_Liquide,
                                            Qte_Bouteille = c.Quantite_Bouteille_Liquide,
                                            PU_Casier = (int)c.PU_Achat,
                                            Conditionnement = (int)c.Conditionnement,
                                            Qte_Casier_Emballage = c.Quantite_Casier_Emballage,
                                            Qte_Plastique_Emballage = c.Quantite_Plastique_Emballage,
                                            Qte_Bouteille_Emballage = c.Quantite_Bouteille_Emballage,
                                            PU_Plastique_Emballage = (int)c.PU_Plastique,
                                            PU_Bouteille_Emballage = (int)c.PU_Bouteille,

                                        }).ToList();
            MidelObjet.Liste_Emballage = (from c in db.TP_Commande_Emballage
                                          where c.ID_Commande == id
                                          orderby c.TP_Emballage.Code
                                          select new Detail_Commande
                                          {
                                              Code = c.TP_Emballage.Code,
                                              Libelle = c.TP_Emballage.Description,
                                              Qte_Casier_Emballage = c.Quantite_Casier,
                                              Qte_Plastique_Emballage = c.Quantite_Plastique,
                                              Qte_Bouteille_Emballage = c.Quantite_Bouteille,
                                              PU_Plastique_Emballage = (int)c.PU_Plastique,
                                              PU_Bouteille_Emballage = (int)c.PU_Bouteille,
                                              Conditionnement = (int)c.Conditionnement,
                                              Qte_Casier = 0,
                                              Qte_Bouteille = 0,
                                              PU_Casier = 0,
                                          }).ToList();
            var Qte_12 = MidelObjet.Liste_Emballage.Where(p => p.Conditionnement == 12).FirstOrDefault();
            MidelObjet.Casier_12 = (Qte_12 == null ? 0 : Qte_12.Qte_Casier_Emballage);
            MidelObjet.Plastique_12 = (Qte_12 == null ? 0 : Qte_12.Qte_Plastique_Emballage);
            MidelObjet.Bouteille_12 = (Qte_12 == null ? 0 : Qte_12.Qte_Bouteille_Emballage);
            MidelObjet.Montant_C12 = (Qte_12 == null ? "0" : Qte_12.Montant_Emballage);

            var Qte_15 = MidelObjet.Liste_Emballage.Where(p => p.Conditionnement == 15).FirstOrDefault();
            MidelObjet.Casier_15 = (Qte_15 == null ? 0 : Qte_15.Qte_Casier_Emballage);
            MidelObjet.Plastique_15 = (Qte_15 == null ? 0 : Qte_15.Qte_Plastique_Emballage);
            MidelObjet.Bouteille_15 = (Qte_15 == null ? 0 : Qte_15.Qte_Bouteille_Emballage);
            MidelObjet.Montant_C15 = (Qte_15 == null ? "0" : Qte_15.Montant_Emballage);

            var Qte_24 = MidelObjet.Liste_Emballage.Where(p => p.Conditionnement == 24).FirstOrDefault();
            MidelObjet.Casier_24 = (Qte_24 == null ? 0 : Qte_24.Qte_Casier_Emballage);
            MidelObjet.Plastique_24 = (Qte_24 == null ? 0 : Qte_24.Qte_Plastique_Emballage);
            MidelObjet.Bouteille_24 = (Qte_24 == null ? 0 : Qte_24.Qte_Bouteille_Emballage);
            MidelObjet.Montant_C24 = (Qte_24 == null ? "0" : Qte_24.Montant_Emballage);

            var pro = db.TP_Commande_Liquide.Where(p => p.ID_Commande == id).ToList();
            Services service = new Services();
            var Montant_Facture = db.FN_Montant_Commande(id).FirstOrDefault();
            MidelObjet.Montant_Commande_Liquide = service.SeparateurMillier(Montant_Facture == null ? 0 : Montant_Facture.Montant_Liquide);
            MidelObjet.Montant_Commande_Emballage = service.SeparateurMillier(Montant_Facture == null ? 0 : Montant_Facture.Montant_Emballage);
            MidelObjet.Montant_Commande_Retour_Emballage = service.SeparateurMillier(Montant_Facture == null ? 0 : Montant_Facture.Montant_Retour_Emballage);
            MidelObjet.Net_A_Payer = service.SeparateurMillier(Montant_Facture == null ? 0 : (Montant_Facture.Montant_Liquide + Montant_Facture.Montant_Emballage - Montant_Facture.Montant_Retour_Emballage));
            Convertisseur convertir = new Convertisseur();
            MidelObjet.Net_A_Payer_Lettre = convertir.convertir(Montant_Facture == null ? 0 : (Montant_Facture.Montant_Liquide + Montant_Facture.Montant_Emballage - Montant_Facture.Montant_Retour_Emballage));
            return MidelObjet;
        }

        public void Edit_Grille(Int64 id, int key, int value)
        {
            this.Commande_Liquide_temp = db.TP_Commande_Liquide_Temp.Find(id);
            if (key == 1)
            {
                this.Commande_Liquide_temp.Quantite_Casier_Liquide = value;
            }
            else if (key == 2)
            {
                this.Commande_Liquide_temp.Quantite_Bouteille_Liquide = value;
            }
            else if (key == 3)
            {
                this.Commande_Liquide_temp.Quantite_Casier_Emballage = value;
            }
            else if (key == 4)
            {
                this.Commande_Liquide_temp.Quantite_Plastique_Emballage = value;
            }
            else
            {
                this.Commande_Liquide_temp.Quantite_Bouteille_Emballage = value;
            }
            db.SaveChanges();
        }

        public void Valider(Int64 id, int Code_User)
        {
            this.Local_Var = db.TP_Commande.Find(id);
            Local_Var.Etat = 1;
            Local_Var.Edit_Code_User = Code_User;
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges(); 
        }
       
        public void Annuler(Int64 id, int Code_User)
        {
            this.Local_Var = db.TP_Commande.Find(id);
            Local_Var.Etat = 2;
            Local_Var.Edit_Code_User = Code_User;
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }
    }
}