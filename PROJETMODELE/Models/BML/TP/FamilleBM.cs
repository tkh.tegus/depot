﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class FamilleBM
    {
        private TP_Famille Local_Var = new TP_Famille();
        private StoreEntities db = new StoreEntities();
       
        public void Save(FamilleVM Param_Var, string Code_User)
        {
            Local_Var.Libelle = Param_Var.Libelle.Trim();
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            Local_Var.Actif = true;
            db.TP_Famille.Add(Local_Var);
            db.SaveChanges();
        }

        public void Edit(int id, FamilleVM Param_Var, string Code_User)
        {
            this.Local_Var = db.TP_Famille.Find(id);
            this.Local_Var.Libelle = Param_Var.Libelle.Trim();
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Activer_Suspendre(int id, string Code_User)
        {
            this.Local_Var = db.TP_Famille.Find(id);
            this.Local_Var.Actif = !this.Local_Var.Actif;
            this.Local_Var.Edit_Code_User = int.Parse(Code_User);
            this.Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

      
        public FamilleVM GetByID(int id)
        {
            TP_Famille LocalObjet = db.TP_Famille.Find(id);
            FamilleVM MidelObjet = new FamilleVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.Libelle = LocalObjet.Libelle;
            MidelObjet.Actif = LocalObjet.Actif;
            MidelObjet.Statut = (LocalObjet.Actif) ? "Actif" : "Suspendu";
            return MidelObjet;
        }

        
        public bool IsNotExist(FamilleVM Param_Var, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TP_Famille.Where(p => p.Libelle == Param_Var.Libelle).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TP_Famille.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("La famille {0} existe déja", Param_Var.Libelle);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("La famille  {0} existe déja mais est actuellement supprimé, veuillez contacter un adminstrateur pour le restorer", Param_Var.Libelle);
                return false;
            }
        }

       
        public bool IsNotExist(FamilleVM Param_Var, int id, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TP_Famille.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.ID_Famille != id).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TP_Famille.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.Actif == true).Where(p => p.ID_Famille != id).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("La famille {0} existe déja", Param_Var.Libelle);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("La famille {0} existe déja mais est actuellement supprimé, veuillez contacter un adminstrateur pour le restorer", Param_Var.Libelle);
                return false;
            }
        }
    }
}