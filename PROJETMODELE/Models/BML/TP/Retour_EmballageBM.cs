﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using PROJETMODELE.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class Retour_EmballageBM
    {
        private TP_Retour_Emballage Local_Var = new TP_Retour_Emballage();
        private TP_Retour_Emballage_Detail Local_Var_Detail = new TP_Retour_Emballage_Detail();
       
        StoreEntities db = new StoreEntities();

        public Int64 Save(Retour_EmballageVM Param_Var, string Code_User)
        {
            this.Local_Var.Type_Element = Param_Var.Type_Element;
            this.Local_Var.ID_Element = Param_Var.ID_Element;
            this.Local_Var.Motif = Param_Var.Motif;
            this.Local_Var.Date = DateTime.Now;
            this.Local_Var.Etat = 0;
            this.Local_Var.Edit_Code_User = int.Parse(Code_User);
            this.Local_Var.Create_Code_User = int.Parse(Code_User);
            this.Local_Var.Create_Date = DateTime.Now;
            this.Local_Var.Edit_Date = DateTime.Now;
            db.TP_Retour_Emballage.Add(this.Local_Var);
            db.SaveChanges();
            

            var Emballage_12 = db.TP_Emballage.Where(p=>p.ID_Emballage == 1).FirstOrDefault();
            var Emballage_15 = db.TP_Emballage.Where(p=>p.ID_Emballage == 2).FirstOrDefault();
            var Emballage_24 = db.TP_Emballage.Where(p=>p.ID_Emballage == 3).FirstOrDefault();

            this.Local_Var_Detail.ID_Retour_Emballage = Local_Var.ID_Retour_Emballage;
            this.Local_Var_Detail.ID_Emballage = 1;
            this.Local_Var_Detail.PU_Plastique = Emballage_12.PU_Plastique;
            this.Local_Var_Detail.PU_Bouteille = Emballage_12.PU_Bouteille;
            this.Local_Var_Detail.Conditionnement = Emballage_12.Conditionnement;
            this.Local_Var_Detail.Quantite_Casier = Param_Var.Qte_Casier_12;
            this.Local_Var_Detail.Quantite_Plastique = Param_Var.Qte_Plastique_12;
            this.Local_Var_Detail.Quantite_Bouteille = Param_Var.Qte_Bouteille_12;
            db.TP_Retour_Emballage_Detail.Add(Local_Var_Detail);
            db.SaveChanges();

            this.Local_Var_Detail.ID_Retour_Emballage = Local_Var.ID_Retour_Emballage;
            this.Local_Var_Detail.ID_Emballage = 2;
            this.Local_Var_Detail.PU_Plastique = Emballage_15.PU_Plastique;
            this.Local_Var_Detail.PU_Bouteille = Emballage_15.PU_Bouteille;
            this.Local_Var_Detail.Conditionnement = Emballage_15.Conditionnement;
            this.Local_Var_Detail.Quantite_Casier = Param_Var.Qte_Casier_15;
            this.Local_Var_Detail.Quantite_Plastique = Param_Var.Qte_Plastique_15;
            this.Local_Var_Detail.Quantite_Bouteille = Param_Var.Qte_Bouteille_15;
            db.TP_Retour_Emballage_Detail.Add(Local_Var_Detail);
            db.SaveChanges();

            this.Local_Var_Detail.ID_Retour_Emballage = Local_Var.ID_Retour_Emballage;
            this.Local_Var_Detail.ID_Emballage = 3;
            this.Local_Var_Detail.PU_Plastique = Emballage_24.PU_Plastique;
            this.Local_Var_Detail.PU_Bouteille = Emballage_24.PU_Bouteille;
            this.Local_Var_Detail.Conditionnement = Emballage_24.Conditionnement;
            this.Local_Var_Detail.Quantite_Casier = Param_Var.Qte_Casier_24;
            this.Local_Var_Detail.Quantite_Plastique = Param_Var.Qte_Plastique_24;
            this.Local_Var_Detail.Quantite_Bouteille = Param_Var.Qte_Bouteille_24;
            db.TP_Retour_Emballage_Detail.Add(Local_Var_Detail);
            db.SaveChanges();

            return Local_Var.ID_Retour_Emballage;

        }
        public void Edit(Int64 id, Retour_EmballageVM Param_Var, string Code_User)
        {
            this.Local_Var = db.TP_Retour_Emballage.Find(id);
            Local_Var.Type_Element = Param_Var.Type_Element;
            Local_Var.ID_Element = Param_Var.ID_Element;
            Local_Var.Motif = Param_Var.Motif;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();


            
            this.Local_Var_Detail = db.TP_Retour_Emballage_Detail.Where(p=>p.ID_Retour_Emballage == id && p.ID_Emballage == 1).FirstOrDefault();     
            this.Local_Var_Detail.Quantite_Casier = Param_Var.Qte_Casier_12;
            this.Local_Var_Detail.Quantite_Plastique = Param_Var.Qte_Plastique_12;
            this.Local_Var_Detail.Quantite_Bouteille = Param_Var.Qte_Bouteille_12;          
            db.SaveChanges();

            this.Local_Var_Detail = db.TP_Retour_Emballage_Detail.Where(p => p.ID_Retour_Emballage == id && p.ID_Emballage == 2).FirstOrDefault(); 
            this.Local_Var_Detail.Quantite_Casier = Param_Var.Qte_Casier_15;
            this.Local_Var_Detail.Quantite_Plastique = Param_Var.Qte_Plastique_15;
            this.Local_Var_Detail.Quantite_Bouteille = Param_Var.Qte_Bouteille_15;          
            db.SaveChanges();

            this.Local_Var_Detail = db.TP_Retour_Emballage_Detail.Where(p => p.ID_Retour_Emballage == id && p.ID_Emballage == 3).FirstOrDefault(); 
            this.Local_Var_Detail.Quantite_Casier = Param_Var.Qte_Casier_24;
            this.Local_Var_Detail.Quantite_Plastique = Param_Var.Qte_Plastique_24;
            this.Local_Var_Detail.Quantite_Bouteille = Param_Var.Qte_Bouteille_24;
            db.SaveChanges();
        }

        public void Valider(Int64 id, String Code_User)
        {
            Local_Var = db.TP_Retour_Emballage.Find(id);
            Local_Var.Etat = 1;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Annuler(Int64 id, String Code_User)
        {
            Local_Var = db.TP_Retour_Emballage.Find(id);
            Local_Var.Etat = 2;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public Retour_EmballageVM GetByID(Int64 id)
        {
            var LocalObjet = db.FN_Retour_Emballage().Where(p => p.ID_Retour_Emballage == id).FirstOrDefault();// .Find(id);
            Retour_EmballageVM MidelObjet = new Retour_EmballageVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.Type_Element = (byte)LocalObjet.Type_Element;
            MidelObjet.ID_Element = LocalObjet.ID_Element;
            if (LocalObjet.Type_Element == 0)
            {
                MidelObjet.Client_Fournisseur = db.TP_Fournisseur.Where(p => p.ID_Fournisseur == LocalObjet.ID_Element).Select(p => p.Nom).FirstOrDefault();
            }
            else
            {
                MidelObjet.Client_Fournisseur = db.TP_Client.Where(p => p.ID_Client == LocalObjet.ID_Element).Select(p => p.Nom).FirstOrDefault();
            }
            Services service = new Services();
            MidelObjet.Date = LocalObjet.Date;
            MidelObjet.Etat = (byte)LocalObjet.Etat;
            MidelObjet.Utilisateur = db.Vue_Personnel.Where(p => p.ID_Personnel == LocalObjet.Code_User).Select(p => p.Libelle).FirstOrDefault();
            MidelObjet.Statut = (LocalObjet.Etat == 0 ? "En attente de validation" : (LocalObjet.Etat == 1 ? "Valider" : "Annuler"));
            //MidelObjet.Motif = LocalObjet.Motif;
            MidelObjet.Montant = service.SeparateurMillier(LocalObjet.Montant);
            MidelObjet.NUM_DOC = LocalObjet.NUM_DOC;
            var emballage_12 = db.TP_Retour_Emballage_Detail.Where(p => p.ID_Retour_Emballage == id && p.ID_Emballage == 1).FirstOrDefault();
            var emballage_15 = db.TP_Retour_Emballage_Detail.Where(p => p.ID_Retour_Emballage == id && p.ID_Emballage == 2).FirstOrDefault();
            var emballage_24 = db.TP_Retour_Emballage_Detail.Where(p => p.ID_Retour_Emballage == id && p.ID_Emballage == 3).FirstOrDefault();

            MidelObjet.Qte_Casier_12 = emballage_12.Quantite_Casier;
            MidelObjet.Qte_Plastique_12 = emballage_12.Quantite_Plastique;
            MidelObjet.Qte_Bouteille_12 = emballage_12.Quantite_Bouteille;
            
            MidelObjet.Montant_C12 = service.SeparateurMillier(((emballage_12.Quantite_Casier * (emballage_12.PU_Plastique+emballage_12.PU_Bouteille*emballage_12.Conditionnement)) + (emballage_12.PU_Plastique*emballage_12.Quantite_Plastique)+(emballage_12.PU_Bouteille*emballage_12.Quantite_Bouteille)));

            MidelObjet.Qte_Casier_15 = emballage_15.Quantite_Casier;
            MidelObjet.Qte_Plastique_15 = emballage_15.Quantite_Plastique;
            MidelObjet.Qte_Bouteille_15 = emballage_15.Quantite_Bouteille;
            MidelObjet.Montant_C15 = service.SeparateurMillier(((emballage_15.Quantite_Casier * (emballage_15.PU_Plastique + emballage_15.PU_Bouteille * emballage_15.Conditionnement)) + (emballage_15.PU_Plastique * emballage_15.Quantite_Plastique) + (emballage_15.PU_Bouteille * emballage_15.Quantite_Bouteille)));

            MidelObjet.Qte_Casier_24 = emballage_24.Quantite_Casier;
            MidelObjet.Qte_Plastique_24 = emballage_24.Quantite_Plastique;
            MidelObjet.Qte_Bouteille_24 = emballage_24.Quantite_Bouteille;
            MidelObjet.Montant_C24 = service.SeparateurMillier(((emballage_24.Quantite_Casier * (emballage_24.PU_Plastique + emballage_24.PU_Bouteille * emballage_24.Conditionnement)) + (emballage_24.PU_Plastique * emballage_24.Quantite_Plastique) + (emballage_24.PU_Bouteille * emballage_24.Quantite_Bouteille)));
            return MidelObjet;
        }
    }
}