﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TP
{
    public class FournisseurBM
    {
        private TP_Fournisseur Local_Var = new TP_Fournisseur();
        private StoreEntities db = new StoreEntities();
       
        public void Save(FournisseurVM Param_Var, string Code_User)
        {
            Local_Var.Nom = Param_Var.Nom;
            Local_Var.Adresse = Param_Var.Adresse;
            Local_Var.Telephone = Param_Var.Telephone;        
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            Local_Var.Actif = true;
            db.TP_Fournisseur.Add(Local_Var);
            db.SaveChanges();
        }

        public void Edit(int id, FournisseurVM Param_Var, string Code_User)
        {
            Local_Var = db.TP_Fournisseur.Find(id);
            Local_Var.Nom = Param_Var.Nom;
            Local_Var.Adresse = Param_Var.Adresse;
            Local_Var.Telephone = Param_Var.Telephone;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Activer_Suspendre(int id, string Code_User)
        {
            Local_Var = db.TP_Fournisseur.Find(id);
            Local_Var.Actif = !Local_Var.Actif;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }
      
        public FournisseurVM GetByID(int id)
        {
            TP_Fournisseur LocalObjet = db.TP_Fournisseur.Find(id);
            FournisseurVM MidelObjet = new FournisseurVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.Nom = LocalObjet.Nom;
            MidelObjet.Adresse = LocalObjet.Adresse;
            MidelObjet.Telephone = LocalObjet.Telephone;
            MidelObjet.Actif = LocalObjet.Actif;         
            MidelObjet.Statut = (LocalObjet.Actif) ? "Actif" : "Suspendu";
            return MidelObjet;
        }                   
    }
}