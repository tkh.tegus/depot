﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;
using PROJETMODELE.Tools;

namespace PROJETMODELE.Models.BML.TG
{
    public class GradesBM
    {
        /*
        private TG_Grades Local_Var = new TG_Grades();
        StoreEntities db = new StoreEntities();
        /// <summary>
        /// Enregistre un Ville dans la base de donnée
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        public void Save(GradesVM Param_Var, string Code_User)
        {
            Local_Var.Libelle = Param_Var.Libelle;
            Local_Var.Abrege = Param_Var.Abrege;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Create_Code_User = int.Parse(Code_User);
            Local_Var.Create_Date = DateTime.Now;
            Local_Var.Edit_Date = DateTime.Now;
            Local_Var.Actif = true;
            db.TG_Grades.Add(Local_Var);
            db.SaveChanges();

        }

        public void Edit(Int64 id, GradesVM Param_Var, string Code_User)
        {
            Local_Var = db.TG_Grades.Find(id);
            Local_Var.Libelle = Param_Var.Libelle;
            Local_Var.Abrege = Param_Var.Abrege;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Delete(Int64 id, string Code_User)
        {
            this.Local_Var = db.TG_Grades.Find(id);
            this.Local_Var.Actif = false;
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public void Activer_Suspendre(int id, string Code_User)
        {
            this.Local_Var = db.TG_Grades.Find(id);
            this.Local_Var.Actif = !this.Local_Var.Actif;
            this.Local_Var.Edit_Code_User = int.Parse(Code_User);
            this.Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }


        /// <summary>
        /// Obtient un Objet a partir de son identifiants
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GradesVM GetByID(Int64 id)
        {
            TG_Grades ReturnObjet = db.TG_Grades.Find(id);
            GradesVM MidelObjet = new GradesVM();
            if (ReturnObjet == null)
            {
                return null;
            }
            MidelObjet.ID_Grade = ReturnObjet.ID_Grade;
            MidelObjet.Libelle = ReturnObjet.Libelle;
            MidelObjet.Abrege = ReturnObjet.Abrege;
            return MidelObjet;
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle
        /// cette fonction renvoie true si 'element existe 
        /// et false dans le cas contraire
        /// </summary>
        /// <param name="libelle"></param>
        /// <returns></returns>
        public bool IsNotExist(GradesVM Param_Var, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Grades.Where(p => p.Libelle == Param_Var.Libelle).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Grades.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Le grade {0} existe déja  ", Param_Var.Libelle);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Le grade {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", Param_Var.Libelle);
                return false;
            }
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle 
        /// et l'identifiant different
        /// </summary>
        /// <param name="libelle"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsNotExist(GradesVM Param_Var, int id, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Grades.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.ID_Grade != id).FirstOrDefault() == null)
            {

                return true;
            }
            else if (db.TG_Grades.Where(p => p.Libelle == Param_Var.Libelle).Where(p => p.ID_Grade != id).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Le grade {0} existe déja ", Param_Var.Libelle);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Le grade {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", Param_Var.Libelle);
                return false;
            }
        }
         * */
    }
}