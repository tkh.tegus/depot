﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;
using PROJETMODELE.Tools;
using System.Web.Mvc;


namespace PROJETMODELE.Models.BML.TG
{
   
    public class Locked_RecordsBM
    {
        Services services_message = new Services ( );
        private TG_Locked_Records veroux = new TG_Locked_Records ( );
        StoreEntities db = new StoreEntities();
        /// <summary>
        /// permet d'enregistrer un veroux sur une element d'une table
        /// id = identifiant de l'element    verouller
        /// table =  nom de la table contenant l'element
        /// </summary>
        /// <param name="id"></param>
        /// <param name="table"></param>
        public void Save(Int64 id, string table, string Code_User)
        {
            veroux.Code_User = int.Parse(Code_User);
            veroux.Computer_Name = System.Net.Dns.GetHostName();
            veroux.Lock_Date_Time = DateTime.Now;
            veroux.Record_ID = (int)id;
            veroux.Table_Name = table;
            db.TG_Locked_Records.Add(veroux);
            db.SaveChanges();
        }

        /// <summary>
        /// libere le veroux pauser par un utilisateur
        /// </summary>
        /// <param name="id"></param>
        /// <param name="table"></param>
        public void Liberer(Int64 id, string table)
        {
            veroux = db.TG_Locked_Records.Where ( p => p.Record_ID == id ).Where ( p => p.Table_Name == table ).FirstOrDefault ( );
            if (veroux != null)
            {
                db.TG_Locked_Records.Remove(veroux);
                db.SaveChanges();
            }
        }


        /// <summary>
        /// permet de supprmer tous les veroux pausé par un utilisateur
        /// </summary>
        /// <param name="id"></param>
        /// <param name="table"></param>
        public void LibererTous ( int code_user )
        {
            foreach (TG_Locked_Records ver in db.TG_Locked_Records.Where ( p => p.Code_User == code_user ).ToList ( ))
            {
                db.TG_Locked_Records.Remove ( ver );                
            }
            db.SaveChanges ( );
        }

        /// <summary>
        /// retourne true si l'element est verouller dans le cas contraire il verouille simplement l'element
        /// </summary>
        /// <param name="id"></param>
        /// <param name="table"></param>
        /// <param name="message"></param>
        /// <param name="Controlcontex"></param>
        /// <param name="tempData"></param>
        /// <returns></returns>
        public bool IsVerouller(Int64 id, string table, ref string message, ControllerContext Controlcontex, TempDataDictionary tempData, string Code_User)
        {
            // verifier s'il existe dans la table de veroullage ( TG_Locked_Records) un element de la table et du meme id
            if (db.TG_Locked_Records.Where(p => p.Record_ID == id).Where(p => p.Table_Name == table).FirstOrDefault() != null)
            {
                // recupération de l'élément veroullé
                veroux = db.TG_Locked_Records.Where(p => p.Record_ID == id).Where(p => p.Table_Name == table).FirstOrDefault();

                // recuperation des informations sur le personnel ayant pausé le veroux
                TG_Personnels Veroux_User_Name = new TG_Personnels();
                Veroux_User_Name = db.TG_Personnels.Where(p => p.ID_Personnel == veroux.Code_User).FirstOrDefault();

                // verifier si l'utilisateur qui accede à l'élément est celui qui l'avait veroullé au paravent
                if (Veroux_User_Name.ID_Personnel != int.Parse(Code_User))
                {
                    string Nom_Prenom = Veroux_User_Name.Nom;
                    //tester si  le prenom est vide ou null
                    if (!string.IsNullOrEmpty(Veroux_User_Name.Prenom))
                    {
                        Nom_Prenom = Nom_Prenom + " " + Veroux_User_Name.Prenom; // concatener le nom et le prenom
                    }

                    tempData["messagenotification"] = string.Format("Cet enregistrement à été véroullé par l'utilisateur {0} depuis le poste {1} en date du {2}", Nom_Prenom, veroux.Computer_Name, veroux.Lock_Date_Time.ToString());
                    message = services_message.GetHtmlByView(Controlcontex, tempData, "~/Views/Shared/_Message.cshtml", null);
                    return true;
                }
                else
                {
                    tempData["messagenotification"] = string.Format("Cet enregistrement avais été véroullé par vous depuis le poste {0} en date du {1}", veroux.Computer_Name, veroux.Lock_Date_Time.ToString());
                    message = services_message.GetHtmlByView(Controlcontex, tempData, "~/Views/Shared/_Message.cshtml", null);
                    return false;
                }
            }
            else
            { // si l'élément n'est pas veroullé , le vérouller
                this.Save(id, table, Code_User);
                return false;
            }
        }
    }
}