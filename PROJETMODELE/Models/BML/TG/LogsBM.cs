﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;
using PROJETMODELE.Tools;
using System.Web.Mvc;

namespace PROJETMODELE.Models.BML.TG
{
    public class LogsBM
    {
        private TG_Logs Log = new TG_Logs ( );
        StoreEntities db = new StoreEntities();
        /// <summary>
        /// permet de journaliser les erreurs qui arrive dans l'application
        /// 
        /// </summary>
        /// <param name="ex :  exception intercepter" ></param>
        /// <param name="route : route d'acces a la fonction ( controleur ) qui a levé l'exeption"> </param>
        public void Save(Controller controller, Exception ex, string route, string Code_User)
        {
         
            
            if (!ex.Source.Contains("EntityFramework"))
            {
               
                Log.Code_user = int.Parse(Code_User);
                Log.Date = DateTime.Now;
                Log.Error_value = ex.Message.ToString();
                Log.Fonction = route;
                Log.Nom_Poste_Travail = System.Net.Dns.GetHostName();

                db.TG_Logs.Add(Log);
                db.SaveChanges();
            }
            else//Erreur SQL
            {
                if (controller != null)
                {
                    Services service = new Services();
                    service.WriteLog(controller, route, ex, Code_User);
                }
            }
        }
    }
}