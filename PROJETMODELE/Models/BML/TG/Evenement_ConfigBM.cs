﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class Evenement_ConfigBM
    {
        /*
        private TG_Evenement_Config Commande = new TG_Evenement_Config ( );
        StoreEntities db = new StoreEntities();

        /// <summary>
        /// Enregistre un grade dans la base de donnée
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        public void Save(Evenement_Config Commande_Var, string Code_User)
        {
            this.Commande.ID_Personnel = Commande_Var.ID_Personnel;
            this.Commande.ID_Type_Evenement = Commande_Var.ID_Type_Evenement;
            this.Commande.Edit_Code_User = int.Parse ( Code_User );
            this.Commande.Create_Code_User = int.Parse ( Code_User );
            this.Commande.Create_Date = DateTime.Now;
            this.Commande.Edit_Date = DateTime.Now;
            this.Commande.Actif = true;
            db.TG_Evenement_Config.Add ( Commande );
            db.SaveChanges ( );
        }

        public void Edit(int id, Evenement_Config Commande_Var, string Code_User)
        {
            this.Commande = db.TG_Evenement_Config.Find(id);
            this.Commande.ID_Personnel = Commande_Var.ID_Personnel;
            this.Commande.ID_Type_Evenement = Commande_Var.ID_Type_Evenement;
            this.Commande.Edit_Date =  DateTime.Now;
            this.Commande.Edit_Code_User = int.Parse ( Code_User );
            db.SaveChanges ( );
        }

        public void Delete(int id, string Code_User)
        {
            this.Commande = db.TG_Evenement_Config.Find ( id );
            db.TG_Evenement_Config.Remove(Commande);
            db.SaveChanges();
            
        }


        /// <summary>
        /// verifie s'il existe un element avec le meme libelle
        /// cette fonction renvoie true si 'element existe 
        /// et false dans le cas contraire
        /// </summary>
        /// <param name="libelle"></param>
        /// <returns></returns>
        public bool IsNotExist(int ID_Personnel, int Type_Evenement)
        {
            return (db.TG_Evenement_Config.Where(p => p.ID_Personnel == ID_Personnel).Where(p => p.ID_Type_Evenement == Type_Evenement).FirstOrDefault() == null);
        }
        */
    }
}