﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class MessagesBM
    {
        /*
        private TG_Message_Echange me = new TG_Message_Echange();
        private TG_Messages m = new TG_Messages();
        StoreEntities db = new StoreEntities();

        /// <summary>
        /// Enregistre un nouveau messages
        /// </summary>
        /// <param name="message"></param>
        public void Save(Newmessage message, string Code_User)
        {
            int code_personnel = int.Parse(Code_User);
            var user = db.TG_Utilisateurs.Where(p => p.ID_Personnel == code_personnel).FirstOrDefault();
            this.m.Code_User_Destinataire = message.Code_User;
            this.m.Code_User_Emetteur = user.Code_User;
            this.m.Priorite = message.priorite;
            db.TG_Messages.Add(this.m);
            db.SaveChanges();

            this.me.ID_Message = this.m.ID_Message;
            this.me.Code_User_Destinataire = message.Code_User;
            this.me.Code_User_Emetteur = user.Code_User;
            this.me.Contenu = message.Contenu;
            this.me.Date_Envoie = DateTime.Now;
            this.me.Lu = false;
            this.me.Objet = message.Objet;
            this.me.Sens = true;
            try
            {
                db.TG_Message_Echange.Add(this.me);
                db.SaveChanges();
            }
            catch (Exception )
            {
                
            }
        }
        /// <summary>
        /// Enrefistre la reponse a un messages
        /// </summary>
        /// <param name="message"></param>
        public void SaveReponse(Newmessage message, int idmessage, string Code_User)
        {

            int code_personnel = int.Parse(Code_User);
            var user = db.TG_Utilisateurs.Where(p => p.ID_Personnel == code_personnel).FirstOrDefault();
            this.me.ID_Message = idmessage;
            this.me.Code_User_Destinataire = message.Code_User;
            this.me.Code_User_Emetteur = user.Code_User;
            this.me.Contenu = message.Contenu;
            this.me.Date_Envoie = DateTime.Now;
            this.me.Lu = false;
            this.me.Objet = message.Objet;
            this.me.Sens = false;
            db.TG_Message_Echange.Add(this.me);
            db.SaveChanges();
        }


        /// <summary>
        /// indique que le message a déjà été lu
        /// </summary>
        /// <param name="message"></param>
        public void PutRead(int idmessage)
        {

            var all = (from p in db.TG_Message_Echange  where p.ID_Message == idmessage select p);
            foreach (var item in all)
            {
                TG_Message_Echange Mechange = (from p in db.TG_Message_Echange where p.ID_Echange == item.ID_Echange select p).FirstOrDefault();
                Mechange.Lu = true;
              
            }
            db.SaveChanges();
            
        }

        /// <summary>
        /// Nombre 
        /// </summary>
        /// <param name="message"></param>
        public int NumberOfMessage(int iduser)
        {

         return (from M in db.TG_Messages
                join ME in db.TG_Message_Echange on M.ID_Message equals ME.ID_Message
                where ME.Lu == false && M.Code_User_Destinataire == iduser
                select (new { ME.ID_Message, ME.Contenu, ME.Sens, ME.Objet, ME.Date_Envoie, ME.Code_User_Emetteur, ME.Code_User_Destinataire }
                )).Count();

        }
         * */
    }
}