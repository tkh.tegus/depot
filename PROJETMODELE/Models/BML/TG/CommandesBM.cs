﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class CommandesBM
    {
        private TG_Commandes Commande = new TG_Commandes();
        StoreEntities db = new StoreEntities();
        /// <summary>
        /// Enregistre un grade dans la base de donnée
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        public void Save(Commandes Commande_Var)
        {
            this.Commande.Code_Commande_Parent = Commande_Var.Code_Commande_Parent;
            this.Commande.Description = Commande_Var.Description;
            this.Commande.ID_Control = Commande_Var.ID_Control;
            this.Commande.Libelle = Commande_Var.Libelle;
            this.Commande.Ordre = Commande_Var.Ordre;
            db.TG_Commandes.Add(Commande);
            db.SaveChanges();
        }

        public void Edit(int id, Commandes Commande_Var)
        {
            this.Commande = db.TG_Commandes.Find(id);
            this.Commande.Code_Commande_Parent = Commande_Var.Code_Commande_Parent;
            this.Commande.Description = Commande_Var.Description;
            this.Commande.ID_Control = Commande_Var.ID_Control;
            this.Commande.Libelle = Commande_Var.Libelle;
            this.Commande.Ordre = Commande_Var.Ordre;
            db.SaveChanges();
        }

        /// <summary>
        /// permet d'éditer les droits d'un profile
        /// </summary>
        /// <param name="Code_Commande"> Code de la commande  </param>
        /// <param name="Code_Profile"> code du profil dont on veut modifier le droit</param>
        /// <param name="Executer"> True si on veut lui attribuer le droit false si on veut le lui refuser</param>
        public void EditDroit(int Code_Commande, int Code_Profile, int Executer)
        {
            db.Affecter_Droit(Code_Commande, Code_Profile, Executer);
        }

        public void Delete(int id)
        {
            this.Commande = db.TG_Commandes.Find(id);
            db.TG_Commandes.Remove(Commande);
            db.SaveChanges();
        }

        public Commandes GetByID(int id)
        {
            TG_Commandes LocalObjet = db.TG_Commandes.Find(id);
            Commandes MidelObjet = new Commandes();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.Code_Commande_Parent = LocalObjet.Code_Commande_Parent;
            MidelObjet.Description = LocalObjet.Description;
            MidelObjet.ID_Control = LocalObjet.ID_Control;
            MidelObjet.Libelle = LocalObjet.Libelle;
            MidelObjet.Ordre = LocalObjet.Ordre;
            return MidelObjet;
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle
        /// cette fonction renvoie true si 'element existe 
        /// et false dans le cas contraire
        /// </summary>
        /// <param name="libelle"></param>
        /// <returns></returns>
        public bool IsNotExist(string libelle)
        {
            return (db.TG_Commandes.Where(p => p.Libelle == libelle).FirstOrDefault() == null);
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle 
        /// et l'identifiant different
        /// </summary>
        /// <param name="libelle"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsNotExist(string libelle, int id)
        {
            return (db.TG_Commandes.Where(p => p.Libelle == libelle).Where(p => p.Code_Commande != id).FirstOrDefault() == null);
        }

        public void creer_noeud(int Code_Commande, string Libelle, string description, ref string Liste, int Code_Profile)
        {
            IQueryable<TG_Commandes> retour;

            string check = Profile_Can_Execute_Commande(Code_Commande, Code_Profile);
            while (true)
            {

                Liste = Liste + string.Format("<li  title={2} id='{1}'>  <img class='ribbon-icon ribbon-normal' src='/Images/{3}.png' /> {0} <ul>", Libelle, Code_Commande, '"' + description + '"', check);
                retour = selection_noeud(Code_Commande); // selection de toutes les commandes ayant pour parent le Code_Commande
                if (retour != null) // si on a retourner des commande
                {
                    foreach (TG_Commandes code_com in retour)
                    {
                        creer_noeud(code_com.Code_Commande, code_com.Libelle, code_com.Description, ref Liste, Code_Profile);
                    }
                    Liste = Liste + "</ul></li>";
                    return;
                }
                else
                {
                    Liste = Liste + string.Format("<li  title={2} id='{1}'> <img class='ribbon-icon ribbon-normal' src='/Images/{3}.png' /> {0} </li>", Libelle, Code_Commande, '"' + description + '"', check);
                    return;
                }
            }
        }

        private IQueryable<TG_Commandes> selection_noeud(int Code_Commande)
        {
            return (from Commande in db.TG_Commandes where (Commande.Code_Commande_Parent == Code_Commande) orderby Commande.Ordre select (Commande));
        }

        /// <summary>
        /// retourne le libelle de l'iconne a afficher pour indiquer l'etat de la commande
        /// </summary>
        /// <param name="Code_Commande"> code de la commande dont on veu verifier le droit du profil</param>
        /// <param name="Code_Profile"> profile dont on veut verifier le droit</param>
        /// <returns></returns>
        private string Profile_Can_Execute_Commande(int Code_Commande, int Code_Profile)
        {
            TG_Droits droit = (from Droits in db.TG_Droits where (Droits.Code_Profile == Code_Profile && Droits.Code_Commande == Code_Commande) select (Droits)).FirstOrDefault();

            if (droit.Disponible) // si la commande est disponible pour ce profil
            {
                if (droit.Executer) // s'il a le droit d'executer
                {
                    return "icn_alert_success";
                }
                else // s'il n'a pas le droit d'executer
                {
                    return "icn_alert_error";
                }
            }
            else
            { // si la commabnde n'est pas disponible
                return "pwd";
            }

        }


    }
}