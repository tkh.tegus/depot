﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class PersonnelsBM
    {
        private TG_Personnels Personnel = new TG_Personnels ( );
        StoreEntities db = new StoreEntities();
        /// <summary>
        /// Enregistre un grade dans la base de donnée
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        public int Save(PersonnelsVM Personnel_Var, string Code_User)
        {            
            this.Personnel.Nom = Personnel_Var.Nom;
            //this.Personnel.Mail = Personnel_Var.Mail;
            //this.Personnel.Sexe = true; // (Personnel_Var.Sexe.Equals("0")) ? false : true;
            this.Personnel.ID_Caisse = Personnel_Var.ID_Caisse;
            this.Personnel.Edit_Code_User = int.Parse ( Code_User );
            this.Personnel.Create_Code_User = int.Parse ( Code_User );
            this.Personnel.Create_Date = DateTime.Now;
            this.Personnel.Edit_Date = DateTime.Now;
            this.Personnel.Actif = true;
            this.Personnel.Display = true;
            db.TG_Personnels.Add ( Personnel );
            db.SaveChanges ( );
            return Personnel.ID_Personnel;
        }

        public void Edit(int id, PersonnelsVM Personnel_Var, string Code_User)
        {
            this.Personnel = db.TG_Personnels.Find ( id );            
            this.Personnel.Nom = Personnel_Var.Nom;
            this.Personnel.ID_Caisse = Personnel_Var.ID_Caisse;
            //this.Personnel.Mail = Personnel_Var.Mail;
            //this.Personnel.Sexe = (Personnel_Var.Sexe.Equals("0")) ? false : true;
            this.Personnel.Edit_Code_User = int.Parse ( Code_User );
            this.Personnel.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public void Delete(int id, string Code_User)
        {
            this.Personnel = db.TG_Personnels.Find ( id );
            this.Personnel.Actif = false;
            this.Personnel.Edit_Code_User = int.Parse ( Code_User );
            this.Personnel.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public void Activer(int id, string Code_User)
        {
            this.Personnel = db.TG_Personnels.Find ( id );
            this.Personnel.Actif = true;
            this.Personnel.Edit_Code_User = int.Parse ( Code_User );
            this.Personnel.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public PersonnelsVM GetByID(int id)
        {
            TG_Personnels LocalObjet = db.TG_Personnels.Find ( id );
            PersonnelsVM MidelObjet = new PersonnelsVM();
            if (LocalObjet == null)
            {
                return null;
            }           
            MidelObjet.Nom = LocalObjet.Nom;
            MidelObjet.Prenom = LocalObjet.Prenom;
            //MidelObjet.Sexe = (LocalObjet.Sexe) ? "1" : "0";      
            //MidelObjet.Mail = LocalObjet.Mail;          
            return MidelObjet;
        }

    }
}