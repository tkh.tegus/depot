﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;
using  PROJETMODELE.Tools;

namespace PROJETMODELE.Models.BML.TG
{
    public class ArrondissementsBM
    {
        /*
        private TG_Arrondissements ArrondissementLocal = new TG_Arrondissements ( );
        StoreEntities db = new StoreEntities();
        Services service = new Services();
        /// <summary>
        /// Enregistre un Arrondissements dans la base de donnée
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        public void Save(Arrondissements Arrondissement_Var, string Code_User)
        {
            
            ArrondissementLocal.Libelle = Arrondissement_Var.Libelle;
            ArrondissementLocal.Edit_Code_User = int.Parse ( Code_User );
            ArrondissementLocal.Create_Code_User = int.Parse ( Code_User );
            ArrondissementLocal.Create_Date = DateTime.Now;
            ArrondissementLocal.Edit_Date = DateTime.Now;
            ArrondissementLocal.Actif = true;
            db.TG_Arrondissements.Add ( ArrondissementLocal );
            db.SaveChanges ( );
        }

        public void Edit(int id, Arrondissements Arrondissement_Var, string Code_User)
        {
            this.ArrondissementLocal = db.TG_Arrondissements.Find ( id );
            this.ArrondissementLocal.Libelle = Arrondissement_Var.Libelle;
            ArrondissementLocal.Edit_Code_User = int.Parse ( Code_User );
            ArrondissementLocal.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public void Delete(int id, string Code_User)
        {
            this.ArrondissementLocal = db.TG_Arrondissements.Find ( id );
            this.ArrondissementLocal.Actif = false;
            ArrondissementLocal.Edit_Code_User = int.Parse ( Code_User );
            ArrondissementLocal.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public void Activer(int id, string Code_User)
        {
            this.ArrondissementLocal = db.TG_Arrondissements.Find ( id );
            this.ArrondissementLocal.Actif = true;
            ArrondissementLocal.Edit_Code_User = int.Parse ( Code_User );
            ArrondissementLocal.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        /// <summary>
        /// Obtient un arrondisseent a partir de son identifiants
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Arrondissements GetByID ( int id )
        {
            TG_Arrondissements ReturnObjet = db.TG_Arrondissements.Find ( id );
            Arrondissements MidelObjet = new Arrondissements ( );
            if (ReturnObjet == null)
            {
                return null;
            }
            MidelObjet.ID_Arrondissement = ReturnObjet.ID_Arrondissement;
            MidelObjet.Libelle = ReturnObjet.Libelle;

            return MidelObjet;
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle
        /// cette fonction renvoie true si 'element existe 
        /// et false dans le cas contraire
        /// </summary>
        /// <param name="libelle"></param>
        /// <returns></returns>
        public bool IsNotExist(string libelle, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Arrondissements.Where(p => p.Libelle == libelle).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Arrondissements.Where(p => p.Libelle == libelle).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("L'arrondissement de libelle {0} existe déja", libelle);
                // l'élément existe et es consultable
                return false;
            }
            else {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("L'arrondissement de libelle {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", libelle);
                return false;
            }
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle 
        /// et l'identifiant different
        /// </summary>
        /// <param name="libelle"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsNotExist ( string libelle, int id, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Arrondissements.Where(p => p.Libelle == libelle).Where(p => p.ID_Arrondissement != id).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Arrondissements.Where(p => p.Libelle == libelle).Where(p => p.ID_Arrondissement != id).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("L'arrondissement de libelle {0} existe déja", libelle);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("L'arrondissement de libelle {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", libelle);
                return false;
            }
        }
        */
    }
}