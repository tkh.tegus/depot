﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;
using System.Text;
using System.Web.Security;

namespace PROJETMODELE.Models.BML.TG
{
    public class LoginBM
    {
        private TG_Utilisateurs Utilisateur = new TG_Utilisateurs ( );
        StoreEntities db = new StoreEntities();

        public bool UserLogin(string Login, String passe, bool persist, ref string message,ref string Code_User, ref int ID_Personnel)
        {
            StoreEntities db = new StoreEntities();
            Utilisateur = db.TG_Utilisateurs.Where(p => p.Login == Login).FirstOrDefault();
            if (Utilisateur == null) // le login est incorrect
            {
                message = "Mot de passe ou Login Incorrect ";
                return false;
            }
            else
            { // login correct
                if (Utilisateur.Actif == false)
                {
                    message = "Vous avez été désactivé ";
                    return false;
                }
                // reccupere les profil de l'utilisateur
                TG_Profiles_Utilisateurs profile = db.TG_Profiles_Utilisateurs.Where(p => p.Code_User == Utilisateur.Code_User).FirstOrDefault();

                if (profile != null) // si l'utilisateur  n'est associer a aucun profil
                {
                    string passecrypter = cryptage(passe);
                    if (Utilisateur.Somme_Ctrl.Equals("-1")) // si l'utilisateur n'a pas encore de passe donc c'est la premiere connexion
                    { // dans ce cas il faut enregistrer le login 
                        message = "Première connexion";
                        Code_User = Utilisateur.ID_Personnel.ToString(); // Utilisateur.Code_User;
                        var chaine = Utilisateur.TG_Personnels.Nom;
                        if (Utilisateur.TG_Personnels.Prenom != null)
                            chaine = chaine + " " + Utilisateur.TG_Personnels.Prenom;
                        FormsAuthentication.SetAuthCookie(chaine, persist);
                        // met a jour le pass de l'utilisateur
                        Edit_Passe(Utilisateur.Code_User, passecrypter);
                        ID_Personnel = Utilisateur.TG_Personnels.ID_Personnel;
                        return true;
                    }
                    else if (!Utilisateur.Somme_Ctrl.Equals(passecrypter)) // si l'utilisateur a un mot de passe mais qui est different de celui renseigner
                    {
                        message = "Mot de passe ou Login Incorrect ";
                        return false;
                    }
                    else if (Utilisateur.Somme_Ctrl.Equals(passecrypter)) // si l'utilisateur a un mot de passe qui est identique à celui renseigner
                    {
                        message = "Connexion réussit";
                        Code_User = Utilisateur.ID_Personnel.ToString();
                        var chaine = Utilisateur.TG_Personnels.Nom;
                        if (Utilisateur.TG_Personnels.Prenom != null)
                            chaine = chaine + " " + Utilisateur.TG_Personnels.Prenom;
                        FormsAuthentication.SetAuthCookie(chaine, persist);
                        // journalise la connexion
                        Edit_Last_Connexion(Utilisateur.Code_User);
                        ID_Personnel = Utilisateur.TG_Personnels.ID_Personnel;
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    message = "L'utilisateur n'est associé à aucun profile";
                    return false;
                }
            }
        }


        public string cryptage (string stringToHash)
        {
            System.Security.Cryptography.MD5 md5HashAlgo = System.Security.Cryptography.MD5.Create ( );
            // Place le texte à hacher dans un tableau d'octets 
            byte[] byteArrayToHash = System.Text.Encoding.UTF8.GetBytes ( stringToHash );
            // Hash le texte et place le résulat dans un tableau d'octets 
            byte[] hashResult = md5HashAlgo.ComputeHash ( byteArrayToHash );
            StringBuilder result = new StringBuilder ( );
            for (int i = 0; i < hashResult.Length; i++)
            {
                // Affiche le Hash en hexadecimal 
                result.Append ( hashResult[i].ToString ( "X2" ) );
            }
            return  result.ToString ( );
        }


        /// <summary>
        /// enregistrer la premiere connexion de l'utilisateur avec le passe
        /// </summary>
        /// <param name="Code_User"></param>
        /// <param name="PasseCrypter"></param>
        public void Edit_Passe ( int Code_User, string PasseCrypter)
        {
            this.Utilisateur = db.TG_Utilisateurs.Find ( Code_User );
            this.Utilisateur.Somme_Ctrl = PasseCrypter;
            this.Utilisateur.Last_Connexion = DateTime.Now;
            this.Utilisateur.Edit_Date = DateTime.Now;
            db.SaveChanges ( );

        }

        /// <summary>
        /// enregistrer la derniere connexion de l'utilisateur
        /// </summary>
        /// <param name="Code_User"></param>
        public void Edit_Last_Connexion ( int Code_User )
        {
            this.Utilisateur = db.TG_Utilisateurs.Find ( Code_User );
            this.Utilisateur.Last_Connexion = DateTime.Now;
            this.Utilisateur.Edit_Date = DateTime.Now;
            db.SaveChanges ( );

        }

        public Compte GetCompte(int idpersonnel)
        {
            var personnel = db.TG_Personnels.Find(idpersonnel);
             var utilisateur = db.TG_Utilisateurs.Where(p => p.ID_Personnel == idpersonnel).FirstOrDefault();
            Compte compte = new Compte();
            compte.Nom = db.Vue_Personnel.Where(p => p.ID_Personnel == idpersonnel).FirstOrDefault().Libelle;
            compte.login = utilisateur.Login;       
            compte.nationalite = "";
            return compte;
        }

    }
}