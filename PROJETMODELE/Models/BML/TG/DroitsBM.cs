﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;
using PROJETMODELE.Tools;
using System.Web.Mvc;

namespace PROJETMODELE.Models.BML.TG
{
    public class DroitsBM
    {

        private TG_Droits Droit = new TG_Droits();
        StoreEntities db = new StoreEntities();

        /// <summary>
        /// retourne true si un utilisateur a le doit d'executer une commande 
        ///  et false dans le cas contraire
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public bool CanExecuteCommande(string ID_Controle, string Code_User)
        {
            try
            {
                int code_user = int.Parse(Code_User);

                // selection de tous les profiles de l'utilisateur
                IEnumerable<TG_Profiles_Utilisateurs> AllProdile = from ProfileUser in db.TG_Profiles_Utilisateurs where (ProfileUser.TG_Utilisateurs.ID_Personnel == code_user) select (ProfileUser);

                // selection du code de la commande dont on a passé l'identifaint
                int Code_commande_Local = (from Commande in db.TG_Commandes
                                           where (Commande.Libelle == ID_Controle)
                                           select (Commande.Code_Commande)).FirstOrDefault();

                // selection de tous les droits de l'utilisateur pour ses profiles sur la commande
                IQueryable<TG_Droits> AllDroitDeny = from Droit in db.TG_Droits
                                                     join Profile in AllProdile on Droit.Code_Profile equals Profile.Code_Profile
                                                     where (Droit.Code_Commande == Code_commande_Local)
                                                     select (Droit);

                // si le nombre d'élément retourné est different de 0 alors il n'a pas le droit (retour false) sinon il a le droit ( retour true)
                return (AllDroitDeny.Distinct().Count() == 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// retourne la liste d'identifiants des controles ou l'outilisateur a les doits
        /// </summary>
        /// <returns></returns>
        public List<string> CommandeExecutableUser(string Code_User)
        {
            try
            {
                int code_user = int.Parse(Code_User);
                // selection de tous les profiles de l'utilisateur
                var AllProdile = from ProfileUser in db.TG_Profiles_Utilisateurs where (ProfileUser.TG_Utilisateurs.ID_Personnel == code_user && ProfileUser.TG_Profiles.Actif==true) select (ProfileUser.Code_Profile);

                // selection de toutes les commandes
                IQueryable<TG_Commandes> Allcommande = from Commande in db.TG_Commandes select (Commande);

                // selection de tous les droits de l'utilisateur ou un de ses profiles lui donne le droit et qui est disponible
                IQueryable<TG_Droits> AllDroitAllow = from Droit in db.TG_Droits
                                                      where AllProdile.Contains(Droit.Code_Profile) == true  && (Droit.Disponible == true && Droit.Executer == true)
                                                      select (Droit);

                // selection de tous les droits de l'utilisateur ou un de ses profiles lui refude le droit et qui est disponible
                IQueryable<TG_Droits> AllDroitDeny = from Droit in db.TG_Droits
                                                     where AllProdile.Contains(Droit.Code_Profile) == true && (Droit.Disponible == false)
                                                     select (Droit);

                // selection des commandes attribuer n'apparaisant pas parmi les commandes refusés
                IQueryable<TG_Droits> AllDroitReel = from DroitAttribuer in AllDroitAllow // cmd attribuer 
                                                     let DroitRefuser = from AllCmdDeny in AllDroitDeny select (AllCmdDeny.Code_Commande)  // cmd refuser
                                                     where (DroitRefuser.Contains(DroitAttribuer.Code_Commande) == false)
                                                     select (DroitAttribuer); // selection des cmd attribués n'apparaissant pas ds les cmd refusés

                // selection des commandes proprement dite ou l'utilisateur a le droit
                IQueryable<string> AllcommandeAttibuer = (from com in Allcommande join Droitcmd in AllDroitReel on com.Code_Commande equals Droitcmd.Code_Commande select (com.ID_Control)).Distinct();

                return AllcommandeAttibuer.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool ExecuteCommande(string Code_User, string Commande)
        {
            DroitsBM droitUtilisateur = new DroitsBM();
            List<string> cmd = droitUtilisateur.CommandeExecutableUser(Code_User);
            return cmd.Contains(Commande);
        }
    }
}