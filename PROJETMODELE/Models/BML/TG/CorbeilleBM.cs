﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class CorbeilleBM
    {
        private TG_Corbeille CorbeilleLocal = new TG_Corbeille();
        StoreEntities db = new StoreEntities();

        public void Delete(int id)
        {
            
            this.CorbeilleLocal = db.TG_Corbeille.Find(id);
            this.CorbeilleLocal.Actif = false;
            db.SaveChanges();
        }

        public void RestorerElement(int id)
        {
           // db.Restorer_Corbeille(id);
        }

        public void Vider()
        {
        }
    }
}