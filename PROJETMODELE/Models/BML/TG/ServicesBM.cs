﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class ServicesBM
    {
        /*
        private TG_Services Service = new TG_Services ( );
        StoreEntities db = new StoreEntities();
        /// <summary>
        /// Enregistre un element dans la base de donnée
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        public void Save(ServicesVM Service_Var, string Code_User)
        {
            this.Service.Libelle = Service_Var.Libelle;
            this.Service.Edit_Code_User = int.Parse ( Code_User );
            this.Service.Create_Code_User = int.Parse ( Code_User );
            this.Service.Create_Date = DateTime.Now;
            this.Service.Edit_Date = DateTime.Now;
            this.Service.Actif = true;
            db.TG_Services.Add ( Service );
            db.SaveChanges ( );
        }

        public void Edit(int id, ServicesVM Service_Var, string Code_User)
        {
            this.Service = db.TG_Services.Find ( id );
            this.Service.Libelle = Service_Var.Libelle;
            this.Service.Edit_Code_User = int.Parse ( Code_User );
            this.Service.Edit_Date = DateTime.Now;
             db.SaveChanges ( );
        }

        public void Delete(int id, string Code_User)
        {
            this.Service = db.TG_Services.Find ( id );
            this.Service.Actif = false;
            this.Service.Edit_Code_User = int.Parse ( Code_User );
            this.Service.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public void Activer(int id, string Code_User)
        {
            this.Service = db.TG_Services.Find ( id );
            this.Service.Actif = true;
            this.Service.Edit_Code_User = int.Parse ( Code_User );
            this.Service.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public ServicesVM GetByID ( int id )
        {
            TG_Services LocalObjet = db.TG_Services.Find ( id );
            ServicesVM MidelObjet = new ServicesVM ( );
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.Libelle = LocalObjet.Libelle;
            return MidelObjet;
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle
        /// cette fonction renvoie true si 'element existe 
        /// et false dans le cas contraire
        /// </summary>
        /// <param name="libelle"></param>
        /// <returns></returns>
        public bool IsNotExist(string libelle, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Services.Where(p => p.Libelle == libelle).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Services.Where(p => p.Libelle == libelle).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Le service de libelle {0} existe déja", libelle);
                // l'élément existe et es consultable
                return false;
            }
            else {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Le service de libelle {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", libelle);
                return false;
            }
        }
        /// <summary>
        /// verifie s'il existe un element avec le meme libelle 
        /// et l'identifiant different
        /// </summary>
        /// <param name="libelle"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsNotExist ( string libelle, int id, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Services.Where(p => p.Libelle == libelle).Where(p => p.ID_Service != id).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Services.Where(p => p.Libelle == libelle).Where(p => p.ID_Service != id).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Le service de libelle {0} existe déja", libelle);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Le service de libelle {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", libelle);
                return false;
            }
        }
         * */
    }
}