﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class ConfigurationBM
    {
        /*
        private TG_Variables Variables = new TG_Variables();
        StoreEntities db = new StoreEntities();

        public void Edit(int id, Configurations Configuration_Var)
        {
            Enregistre("Libelle", Configuration_Var.Libelle);
            Enregistre("Slogan", Configuration_Var.Slogan);
            Enregistre("BP", Configuration_Var.BP);
            Enregistre("device", Configuration_Var.device);
            Enregistre("DistinctionNationauxEtranger", Configuration_Var.DistinctionNationauxEtranger.ToString());
            Enregistre("division", Configuration_Var.division);
            Enregistre("Mail_Expediteur", Configuration_Var.Mail_Expediteur);
            Enregistre("ministere", Configuration_Var.ministere);
            Enregistre("Mot_Passe", Configuration_Var.Mot_Passe);
            Enregistre("Pays", Configuration_Var.Pays);
            Enregistre("republique", Configuration_Var.republique);
            Enregistre("Serveur_SNTP", Configuration_Var.Serveur_SNTP);
            Enregistre("SystemAppliedLmdOrCoef", Configuration_Var.SystemAppliedLmdOrCoef.ToString());
            Enregistre("UpdateNoteAfterValider", Configuration_Var.UpdateNoteAfterValider.ToString());
            Enregistre("Ville", Configuration_Var.Ville);
            Enregistre("Logo", Configuration_Var.Logo);
            db.SaveChanges();
        }

        public void Enregistre(string Libelle , string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                Variables = db.TG_Variables.Where(p => p.Libelle == Libelle).FirstOrDefault();
                Variables.Valeur = value;
            }
        }
        public string GetByName(string Libelle)
        {
            if (db.TG_Variables.Where(p => p.Libelle == Libelle).Count() != 0)
            {
                return db.TG_Variables.Where(p => p.Libelle == Libelle).FirstOrDefault().Valeur;
            }
            else {
                return null;
            }
        }
        public Configurations GetByID()
        {
            Configurations MidelObjet = new Configurations();
            MidelObjet.Libelle = GetByName("Libelle");
            MidelObjet.Logo = GetByName("Logo");
            MidelObjet.Mail_Expediteur = GetByName("Mail_Expediteur");
            MidelObjet.Serveur_SNTP = GetByName("Serveur_SNTP");
            MidelObjet.Mot_Passe = GetByName("Mot_Passe");
            MidelObjet.Slogan = GetByName("Slogan");
            MidelObjet.Pays = GetByName("Pays");
            MidelObjet.Ville = GetByName("Ville");
            MidelObjet.UpdateNoteAfterValider = GetByName("UpdateNoteAfterValider");
            MidelObjet.SystemAppliedLmdOrCoef = GetByName("SystemAppliedLmdOrCoef");
            MidelObjet.DistinctionNationauxEtranger = GetByName("DistinctionNationauxEtranger");
            MidelObjet.republique = GetByName("republique");
            MidelObjet.device = GetByName("device");
            MidelObjet.ministere = GetByName("ministere");
            MidelObjet.division = GetByName("division");
            MidelObjet.BP = GetByName("BP");
            return MidelObjet;
        }
        */
    }
}