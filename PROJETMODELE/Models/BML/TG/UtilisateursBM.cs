﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class UtilisateursBM
    {
        private TG_Utilisateurs Local_Var = new TG_Utilisateurs ( );
        StoreEntities db = new StoreEntities();
      
        public int Save(Utilisateurs Param_Var, string Liste_ID_Profile, string Code_User)
        {
            PersonnelsVM Personnel = new PersonnelsVM();
            Personnel.ID_Grade = Param_Var.ID_Grade;           
            Personnel.Nom = Param_Var.Nom;
            Personnel.Sexe = Param_Var.Sexe;
            Personnel.Mail = Param_Var.Mail;
            Personnel.ID_Caisse = Param_Var.ID_Caisse;
            PersonnelsBM ObjectBM = new PersonnelsBM();
            int ID_Personnel = ObjectBM.Save(Personnel, Code_User);
            // enregstrement de l'utilisateur
            this.Local_Var.ID_Personnel = ID_Personnel;// Param_Var.ID_Personnel;
            this.Local_Var.Login = Param_Var.Login;
            this.Local_Var.Edit_Code_User = int.Parse ( Code_User );
            this.Local_Var.Create_Code_User = int.Parse ( Code_User );
            this.Local_Var.Create_Date = DateTime.Now;
            this.Local_Var.Edit_Date = DateTime.Now;
            this.Local_Var.Actif = true;
            this.Local_Var.Somme_Ctrl = "-1";
            this.Local_Var.Display = true;
            this.Local_Var.Last_Connexion = DateTime.Now;
            db.TG_Utilisateurs.Add(this.Local_Var);
            db.SaveChanges ( );
            // fin

            // enregistrement des profiles de l'utilisateur
            Profiles_UtilisateursBM Profiles_Utilisateurs_Var = new Profiles_UtilisateursBM ( );
            Profiles_Utilisateurs_Var.Save(this.Local_Var.Code_User, Liste_ID_Profile, true,Code_User);
            //Create_Caisse_Utilisateur(ID_Personnel, int.Parse(Code_User));
            return ID_Personnel;
            //fin
        }
      

        public int Edit(int id, Utilisateurs Param_Var, string ancient, string profile, string Code_User)
        {
            this.Local_Var = db.TG_Utilisateurs.Find(id);
            PersonnelsVM Personnel = new PersonnelsVM();
            Personnel.ID_Grade = Param_Var.ID_Grade;
            //Personnel.ID_Localite = Param_Var.ID_Localite;
            Personnel.Nom = Param_Var.Nom;
            Personnel.Sexe = Param_Var.Sexe;
            Personnel.Mail = Param_Var.Mail;
            Personnel.ID_Caisse = Param_Var.ID_Caisse;
            PersonnelsBM ObjectBM = new PersonnelsBM();
            ObjectBM.Edit(Local_Var.ID_Personnel,Personnel, Code_User);
            this.Local_Var.Login = Param_Var.Login;
            this.Local_Var.Edit_Code_User = int.Parse ( Code_User );
            this.Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
            // enregistrement des profiles de l'utilisateur
            Profiles_UtilisateursBM Profiles_Utilisateurs_Var = new Profiles_UtilisateursBM ( );
            Profiles_Utilisateurs_Var.Edit ( id, ancient, profile,true ,Code_User);
            return Local_Var.ID_Personnel;
            //fin
        }

        public void Delete(int id, string Code_User)
        {
            this.Local_Var = db.TG_Utilisateurs.Find ( id );
            this.Local_Var.Actif = !this.Local_Var.Actif;
            Local_Var.Edit_Code_User = int.Parse ( Code_User );
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public void Activer(int id, string Code_User)
        {
            this.Local_Var = db.TG_Utilisateurs.Find ( id );
            this.Local_Var.Actif = true;
            Local_Var.Edit_Code_User = int.Parse ( Code_User );
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public void Reinitialiser(int id, string Code_User)
        {
            this.Local_Var = db.TG_Utilisateurs.Find(id);
            Local_Var.Somme_Ctrl = "-1";
            Local_Var.Edit_Code_User = int.Parse(Code_User);
            Local_Var.Edit_Date = DateTime.Now;
            db.SaveChanges();
        }

        public Utilisateurs GetByID ( int id )
        {
            TG_Utilisateurs LocalObjet = db.TG_Utilisateurs.Find ( id );
            Utilisateurs MidelObjet = new Utilisateurs ( );
            if (LocalObjet == null)
            {
                return null;
            }         
            MidelObjet.Nom = LocalObjet.TG_Personnels.Nom;          
            MidelObjet.ID_Personnel = LocalObjet.ID_Personnel;
            MidelObjet.Login = LocalObjet.Login;
            MidelObjet.ID_Caisse = LocalObjet.TG_Personnels.ID_Caisse;           
            string str = MidelObjet.CC_ID_Client;
            return MidelObjet;
        }
      
        public bool IsNotExist(string Login, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Utilisateurs.Where(p => p.Login == Login).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Utilisateurs.Where(p => p.Login == Login).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Le login  {0} existe déja", Login);
                // l'élément existe et es consultable
                return false;
            }
            else {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Le login {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", Login);
                return false;
            }
        }

       
        public bool IsNotExist(string Login, int id, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Utilisateurs.Where(p => p.Login == Login).Where(p => p.Code_User != id).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Utilisateurs.Where(p => p.Login == Login).Where(p => p.Code_User != id).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Le login {0} existe déja", Login);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Le login {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", Login);
                return false;
            }
        }
    }
}