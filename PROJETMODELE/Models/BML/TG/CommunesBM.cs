﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class CommunesBM
    {
        /*
        private TG_Communes Commune = new TG_Communes();
        StoreEntities db = new StoreEntities();
        /// <summary>
        /// Enregistre un grade dans la base de donnée
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        public void Save(Communes Commune_Var, string Code_User)
        {
            this.Commune.ID_Arrondissement = Commune_Var.ID_Arrondissement;
            this.Commune.Libelle = Commune_Var.Libelle;
            this.Commune.Edit_Code_User = int.Parse ( Code_User );
            this.Commune.Create_Code_User = int.Parse ( Code_User );
            this.Commune.Create_Date = DateTime.Now;
            this.Commune.Edit_Date = DateTime.Now;
            this.Commune.Actif = true;
            db.TG_Communes.Add ( Commune );
            db.SaveChanges ( );
        }

        public void Edit(int id, Communes Commune_Var, string Code_User)
        {
            this.Commune = db.TG_Communes.Find ( id );
            this.Commune.ID_Arrondissement = Commune_Var.ID_Arrondissement;
            this.Commune.Libelle = Commune_Var.Libelle;
            this.Commune.Edit_Code_User = int.Parse ( Code_User );
            this.Commune.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public void Delete(int id, string Code_User)
        {
            this.Commune = db.TG_Communes.Find ( id );
            this.Commune.Actif = false;
            this.Commune.Edit_Code_User = int.Parse ( Code_User );
            this.Commune.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public void Activer(int id, string Code_User)
        {
            this.Commune = db.TG_Communes.Find ( id );
            this.Commune.Actif = true;
            this.Commune.Edit_Code_User = int.Parse ( Code_User );
            this.Commune.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }


        public Communes GetByID ( int id )
        {
            TG_Communes LocalObjet = db.TG_Communes.Find ( id );
            Communes MidelObjet = new Communes ( );
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.ID_Arrondissement = LocalObjet.ID_Arrondissement;
            MidelObjet.Libelle = LocalObjet.Libelle;

            return MidelObjet;
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle
        /// cette fonction renvoie true si 'element existe 
        /// et false dans le cas contraire
        /// </summary>
        /// <param name="libelle"></param>
        /// <returns></returns>
        public bool IsNotExist(string libelle, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Communes.Where(p => p.Libelle == libelle).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Communes.Where(p => p.Libelle == libelle).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("La commune de libelle {0} existe déja", libelle);
                // l'élément existe et es consultable
                return false;
            }
            else {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("La commune de libelle {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", libelle);
                return false;
            }
        }
        /// <summary>
        /// verifie s'il existe un element avec le meme libelle 
        /// et l'identifiant different
        /// </summary>
        /// <param name="libelle"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsNotExist ( string libelle, int id, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Communes.Where(p => p.Libelle == libelle).Where(p => p.ID_Commune != id).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Communes.Where(p => p.Libelle == libelle).Where(p => p.ID_Commune != id).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("La commune de libelle {0} existe déja", libelle);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("La commune de libelle {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", libelle);
                return false;
            }
        }
        */
    }
}