﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class ProfilesBM
    {
        private TG_Profiles Profile = new TG_Profiles();
        StoreEntities db = new StoreEntities();
        /// <summary>
        /// Enregistre un élémént dans la base de donnée
        /// </summary>
        /// <param name="Profile"></param>
        /// <returns></returns>
        public void Save(Profiles Profiles, string Liste_ID_User, string Code_User)
        {
            this.Profile.Libelle = Profiles.Libelle;
            this.Profile.Edit_Code_User = int.Parse ( Code_User );
            this.Profile.Create_Code_User = int.Parse ( Code_User );
            this.Profile.Create_Date = DateTime.Now;
            this.Profile.Edit_Date = DateTime.Now;
            this.Profile.Actif = true;
            this.Profile.Display = true;
            db.TG_Profiles.Add(this.Profile);
            db.SaveChanges ( );

            // enregistrement des utilisateur du profile

            //Profiles_UtilisateursBM Profiles_Utilisateurs_Var = new Profiles_UtilisateursBM ( );

            //Profiles_Utilisateurs_Var.Save(this.Profile.Code_Profile, Liste_ID_User, false,Code_User);

            //fin
        }

        public void Edit(int id, Profiles Profiles, string liste_ancient_code_user, string liste_nouveau_code_user, string Code_User)
        {
            this.Profile = db.TG_Profiles.Find ( id );
            this.Profile.Libelle = Profiles.Libelle;
            this.Profile.Edit_Code_User = int.Parse ( Code_User );
            this.Profile.Edit_Date = DateTime.Now;
            db.SaveChanges ( );

            // enregistrement des profiles de l'utilisateur

            //Profiles_UtilisateursBM Profiles_Utilisateurs_Var = new Profiles_UtilisateursBM ( );

            //Profiles_Utilisateurs_Var.Edit ( id, liste_ancient_code_user, liste_nouveau_code_user, false,Code_User );

            //fin
        }

        public void Delete(int id, string Code_User)
        {
            this.Profile = db.TG_Profiles.Find ( id );
            this.Profile.Actif = !this.Profile.Actif;
            this.Profile.Edit_Code_User = int.Parse ( Code_User );
            this.Profile.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public void Activer(int id, string Code_User)
        {
            this.Profile = db.TG_Profiles.Find ( id );
            this.Profile.Actif = true;
            this.Profile.Edit_Code_User = int.Parse ( Code_User );
            this.Profile.Edit_Date = DateTime.Now;
            db.SaveChanges ( );
        }

        public Profiles GetByID ( int id )
        {
            TG_Profiles LocalObjet = db.TG_Profiles.Find ( id );
            Profiles MidelObjet = new Profiles ( );
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.Libelle = LocalObjet.Libelle;

            return MidelObjet;
        }

        /// <summary>
        /// verifie s'il existe un element avec le meme libelle
        /// cette fonction renvoie true si 'element existe 
        /// et false dans le cas contraire
        /// </summary>
        /// <param name="libelle"></param>
        /// <returns></returns>
        public bool IsNotExist(string libelle, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Profiles.Where(p => p.Libelle == libelle).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Profiles.Where(p => p.Libelle == libelle).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Le profile de libelle {0} existe déja", libelle);
                // l'élément existe et es consultable
                return false;
            }
            else {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Le profile de libelle {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", libelle);
                return false;
            }
        }
        /// <summary>
        /// verifie s'il existe un element avec le meme libelle 
        /// et l'identifiant different
        /// </summary>
        /// <param name="libelle"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool IsNotExist ( string libelle, int id, ref string MessagesRetour)
        {
            // si l'élément n'existe pas
            if (db.TG_Profiles.Where(p => p.Libelle == libelle).Where(p => p.Code_Profile != id).FirstOrDefault() == null)
            {
                return true;
            }
            else if (db.TG_Profiles.Where(p => p.Libelle == libelle).Where(p => p.Code_Profile != id).Where(p => p.Actif == true).FirstOrDefault() != null)
            {
                MessagesRetour = string.Format("Le profile de libelle {0} existe déja", libelle);
                // l'élément existe et es consultable
                return false;
            }
            else
            {
                // si l'élément existe et est dans la corbeille
                MessagesRetour = string.Format("Le profile de libelle {0} existe déja mais est actuellement supprimé, veillez contacter un adminstrateur pour le restorer", libelle);
                return false;
            }
        }
    }
}