﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROJETMODELE;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System.Data;
using System.Data.Entity;

namespace PROJETMODELE.Models.BML.TG
{
    public class Profiles_UtilisateursBM
    {
        private TG_Profiles_Utilisateurs Profiles_UtilisateursLocal = new TG_Profiles_Utilisateurs ( );
        StoreEntities db = new StoreEntities();
        /// <summary>
        /// lier les elements a lier dans la table TG_Profiles_Utilisateurs
        /// </summary>
        /// <param name="Element_a_lier"> element a lier soit un utilisateur soit un profil </param>
        /// <param name="Liste_a_lier"> liste a d'element a lier soit une liste de code profil soit une liste de code user</param>
        /// <param name="type"> true il s'agit de lier un utilisateur aux profil et un on lie un profile aux utilisateur</param>
        public void Save(int Element_a_lier, string Liste_a_lier, bool type, string Code_User)
        {

            string[] nouveau = Liste_a_lier.Split ( ';' );
            Profiles_UtilisateursBM Profiles_Utilisateurs_Var = new Profiles_UtilisateursBM ( );
            foreach (string ID_Profil in nouveau)
            {
                if (!string.IsNullOrWhiteSpace ( ID_Profil )) // s'assurer que l'element n'est pas vide
                {
                    if (type) // on lie un utilisateur aux profils
                    {
                        Profiles_UtilisateursLocal.Code_User = Element_a_lier;
                        Profiles_UtilisateursLocal.Code_Profile = int.Parse ( ID_Profil );
                    }
                    else {  // on lie un profil aux utilisateurs
                        Profiles_UtilisateursLocal.Code_Profile = Element_a_lier;
                        Profiles_UtilisateursLocal.Code_User = int.Parse ( ID_Profil );
                    }


                    Profiles_UtilisateursLocal.Edit_Code_User = int.Parse ( Code_User );
                    Profiles_UtilisateursLocal.Create_Code_User = int.Parse ( Code_User );
                    Profiles_UtilisateursLocal.Create_Date = DateTime.Now;
                    Profiles_UtilisateursLocal.Edit_Date = DateTime.Now;
                    Profiles_UtilisateursLocal.Actif = true;
                    db.TG_Profiles_Utilisateurs.Add ( Profiles_UtilisateursLocal );
                    db.SaveChanges ( );
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">identifiant de l'élément a lier</param>
        /// <param name="liste_ancient">liste des encients élément lier </param>
        /// <param name="liste_nouveau">liste des nouveaux elements à lier</param>
        /// <param name="type"> true il s'agit de lier un utilisateur aux profil et un on lie un profile aux utilisateur</param>
        public void Edit(int id, string liste_ancient, string liste_nouveau, bool type, string Code_User)
        {

            string[] ancien = liste_ancient.Split ( ';' ), nouveau = liste_nouveau.Split ( ';' );
            string[] supprimer = new string[ancien.Length], ajouter = new string[nouveau.Length];

            Gerer_liste ( ancien, nouveau, ajouter, supprimer );
            
            // supprimer ceux qui ne sont plus associer au profil
            for (int j = 0; j < supprimer.Length; j++)
            {
                if (!supprimer[j].Equals ( "#" ))
                {
                    int code_pro = int.Parse ( supprimer[j] );
                    if (type) // on lie un utilisateur aux profils
                    {
                        Profiles_UtilisateursLocal = db.TG_Profiles_Utilisateurs.Where ( p => p.Code_User == id ).Where ( p => p.Code_Profile == code_pro ).FirstOrDefault ( );
                    }
                    else
                    {  // on lie un profil aux utilisateurs
                        Profiles_UtilisateursLocal = db.TG_Profiles_Utilisateurs.Where ( p => p.Code_Profile == id ).Where ( p => p.Code_User == code_pro ).FirstOrDefault ( );
                    }

                    
                    db.TG_Profiles_Utilisateurs.Remove ( Profiles_UtilisateursLocal );
                    db.SaveChanges ( );
                }
            }

            // ajoute ceux qui sont associer au profil
            for (int k = 0; k < ajouter.Length; k++)
            {
                if (!ajouter[k].Equals ( "#" ))
                {
                    if (type) // on lie un utilisateur aux profils
                    {
                        Profiles_UtilisateursLocal.Code_User = id;
                        Profiles_UtilisateursLocal.Code_Profile = int.Parse ( ajouter[k] );
                    }
                    else
                    {  // on lie un profil aux utilisateurs
                        Profiles_UtilisateursLocal.Code_Profile = id;
                        Profiles_UtilisateursLocal.Code_User = int.Parse ( ajouter[k] );
                    }

                    Profiles_UtilisateursLocal.Edit_Code_User = int.Parse ( Code_User );
                    Profiles_UtilisateursLocal.Create_Code_User = int.Parse ( Code_User );
                    Profiles_UtilisateursLocal.Create_Date = DateTime.Now;
                    Profiles_UtilisateursLocal.Edit_Date = DateTime.Now;
                    Profiles_UtilisateursLocal.Actif = true;
                    db.TG_Profiles_Utilisateurs.Add ( Profiles_UtilisateursLocal );
                    db.SaveChanges ( );
                }
            }
        }



        /// <summary>
        /// cette fonction recheche dans deux liste ceux qui ne sont pas 
        /// </summary>
        /// <param name="anciene_liste"></param>
        /// <param name="nouveau_liste"></param>
        /// <param name="liste_inserer"></param>
        /// <param name="liste_supprimer"></param>
        public void Gerer_liste ( string[] anciene_liste, string[] nouveau_liste, string[] liste_inserer, string[] liste_supprimer )
        {
            // retrouver ceux qui seront supprimé
            int i = 0;
            for (int j = 0; j < anciene_liste.Length; j++)
            {
                liste_supprimer[j] = "#";
                if (!nouveau_liste.Contains ( anciene_liste[j] ))
                {
                    liste_supprimer[i++] = anciene_liste[j];
                }
            }

            //retrouve ceux qui seront inséré
            i = 0;
            for (int j = 0; j < nouveau_liste.Length; j++)
            {
                liste_inserer[j] = "#";
                if (!anciene_liste.Contains ( nouveau_liste[j] ))
                {
                    liste_inserer[i++] = nouveau_liste[j];
                }
            }

        }


    }
}