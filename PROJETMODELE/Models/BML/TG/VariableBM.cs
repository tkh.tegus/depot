﻿using PROJETMODELE.Models.DAL;
using PROJETMODELE.Models.VML.TG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROJETMODELE.Models.BML.TG
{
    public class VariableBM
    {
        private TG_Variable Local_Var = new TG_Variable();
        StoreEntities db = new StoreEntities();

        public void Edit(VariableVM Param_Var, int Code_User)
        {
            this.Local_Var = db.TG_Variable.FirstOrDefault();
            this.Local_Var.Nom_Societe = Param_Var.Nom_Societe;
            this.Local_Var.Numero_Contribuable = Param_Var.Numero_Contribuable;
            this.Local_Var.Registre_Commerce = Param_Var.Registre_Commerce;
            this.Local_Var.Telephone = Param_Var.Telephone;
            this.Local_Var.Adresse = Param_Var.Adresse;
            this.Local_Var.Ville = Param_Var.Ville;
            this.Local_Var.Description = Param_Var.Description;
            this.Local_Var.Logo = Param_Var.Logo;
            this.Local_Var.Verifier_Etat_Stock = Param_Var.Verifier_Etat_Stock;
            this.Local_Var.Appliquer_TVA = Param_Var.Appliquer_TVA;
            this.Local_Var.TVA = Param_Var.TVA;
            this.Local_Var.Dossier_Sauvegarde = Param_Var.Dossier_Sauvegarde;
            if (Param_Var.Edit_Password_Sauvegarde && Code_User == 1)
            {
                this.Local_Var.Password_Sauvegarde = Param_Var.Password_Sauvegarde;
            }
            db.SaveChanges();
        }

        public VariableVM GetByID()
        {
            TG_Variable LocalObjet = db.TG_Variable.FirstOrDefault();
            VariableVM MidelObjet = new VariableVM();
            if (LocalObjet == null)
            {
                return null;
            }
            MidelObjet.Nom_Societe = LocalObjet.Nom_Societe;
            MidelObjet.Numero_Contribuable = LocalObjet.Numero_Contribuable;
            MidelObjet.Registre_Commerce = LocalObjet.Registre_Commerce;
            MidelObjet.Telephone = LocalObjet.Telephone;
            MidelObjet.Adresse = LocalObjet.Adresse;
            MidelObjet.Ville = LocalObjet.Ville;
            MidelObjet.Description = LocalObjet.Description;
            MidelObjet.Logo = LocalObjet.Logo;
            MidelObjet.Verifier_Etat_Stock = (bool)LocalObjet.Verifier_Etat_Stock;
            MidelObjet.Appliquer_TVA = (bool)LocalObjet.Appliquer_TVA;
            MidelObjet.TVA = (LocalObjet.TVA == null ? 0 : LocalObjet.TVA.Value);
            MidelObjet.Dossier_Sauvegarde = LocalObjet.Dossier_Sauvegarde;
            return MidelObjet;
        }
    }
}