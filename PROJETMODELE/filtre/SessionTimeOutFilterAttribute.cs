﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.filtre
{
    public class SessionTimeOutFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            PROJETMODELE.Controllers.LoginController aLogin = new Controllers.LoginController();
            bool nonajax = filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[1] == "" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[1] == "home" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[1] == "commandes" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[1] == "Base" || (filterContext.HttpContext.Request.Url.AbsolutePath.Split('/').Length > 2 && (filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "parametre" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "GetFile" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "UploadFile" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "AllFiche" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "SendEmailToChief" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "SendEmail" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "EmployeRetardDay" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "ExportEmploye" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "Login" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "Timezone" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "Logout" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "pdfallpointage" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "pdfallpointagechef" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "pdfpointage" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "pdfpassage" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "ExportPDFMarketeur" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "TimeoutRedirect" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "modele" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "Index" || filterContext.HttpContext.Request.Url.AbsolutePath.Split('/')[2] == "Confirm"));
            if (!nonajax)
            {
                if (!filterContext.HttpContext.Request.IsAjaxRequest() || !filterContext.HttpContext.Request.IsAuthenticated)
                {

                    filterContext.Result = new HttpNotFoundResult();
                    filterContext.HttpContext.Response.StatusCode = 404;
                }
                else
                {
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }
}