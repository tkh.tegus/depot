﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PROJETMODELE.filtre
{
    public class DroitFilterAttribute : ActionFilterAttribute
    {
        public string Droit { get; set; }
        public string RedirectUrl { get; set; }
        public override void OnActionExecuting(ActionExecutingContext actionExecutedContext)
        {
            if (Droit == "herve")
            {
                //actionExecutedContext.Result = new RedirectResult(RedirectUrl);
                actionExecutedContext.Result = new  HttpUnauthorizedResult();
                actionExecutedContext.HttpContext.Response.StatusCode = 401;
            }
            base.OnActionExecuting(actionExecutedContext);
        }
    }
}