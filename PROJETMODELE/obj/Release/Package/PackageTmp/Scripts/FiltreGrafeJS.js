﻿function InitialiseFiltre() {
    if ($("#DateDebut").attr('value') != null && $("#DateFin").attr('value') != null) {
        var DateDebut = $("#DateDebut").attr('value').split(' ')[0].split('/');
        var DateFin = $("#DateFin").attr('value').split(' ')[0].split('/');
        $.getScript('/Content/jqwidgets/globalization/globalize.culture.fr-FR.js', function () {
            $("#startdate").jqxDateTimeInput({ readonly: true, width: '120px', height: '30px', theme: "ui-redmond", culture: 'fr-FR', formatString: 'd/M/yyyy' });
            $("#enddate").jqxDateTimeInput({ readonly: true, width: '120px', height: '30px', theme: "ui-redmond", culture: 'fr-FR', formatString: 'd/M/yyyy' })
        });
        if (DateDebut.length >= 2) {
            $("#startdate").val(DateDebut[2] + "-" + DateDebut[1] + "-" + DateDebut[0])
        }
        if (DateFin.length >= 2) {
            $("#enddate").val(DateFin[2] + "/" + DateFin[1] + "/" + DateFin[0])
        }
        $("#DateDebut").css('display', 'none'); $("#DateDebut").css('visibility', 'hidden');
        $("#DateFin").css('display', 'none'); $("#DateFin").css('visibility', 'hidden')
    }
} $('#startdate').on('valueChanged', function (event) {
    var textstart = $('#startdate').jqxDateTimeInput('getText'); $('#DateDebut').attr('value', textstart)
});
$('#enddate').on('valueChanged', function (event) {
    var textend = $('#enddate').jqxDateTimeInput('getText');
    $('#DateFin').attr('value', textend);
});