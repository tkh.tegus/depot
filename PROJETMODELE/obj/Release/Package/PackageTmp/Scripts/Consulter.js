﻿$('#formulaire').jqxWindow({ height: "95%", width: "95%", isModal: true, showCloseButton: true, theme: "ui-redmond" });
$(".SansIdentifiantConsultation").off('click');
$(".SansIdentifiantConsultation").on('click', function () {
    $("#loaderSpace,#loader").show();
    var url = $(this).attr('id') + "Url"; $.ajax({
        type: 'GET', url: $("#" + url).attr("value"), success: function (html) {
            $("#loaderSpace,#loader").hide();
            $(".toast-item").remove();
            if (html.length == 2) {
                $("#grades").jqxGrid('updatebounddata', 'cells'); $("#grades").jqxGrid('clearselection');
                $('body').append(html)
                var url = $("#OpenconsulterUrl").attr('value');
                if ($("#OpenconsulterUrl").length != 0) {
                    OpenConsulter(url + html[1]);
                }
            } else
            { $("#grades").jqxGrid('updatebounddata', 'cells'); $("#grades").jqxGrid('clearselection'); $('body').append(html) } $('#formulaire').jqxWindow("destroy")
        }, error: function (jqXHR, exception) {
            $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) { alert('Not connect.\n Verify Network.') } else if (jqXHR.status == 404) { alert('Requested page not found. [404]') } else if (jqXHR.status == 500) { alert('Internal Server Error [500].') } else if (exception === 'parsererror') { alert('Requested JSON parse failed.') } else if (exception === 'timeout') { alert('Time out error.') } else if (exception === 'abort') { alert('Ajax request aborted.') } else { alert('Uncaught Error.\n' + jqXHR.responseText) }
        }
    }); return false
});

$(".FermerConsultation").off('click');
$(".FermerConsultation").on('click', function () {
    $("#loaderSpace,#loader").show();
    var url = $(this).attr('id') + "Url";
    $.ajax({
        type: 'GET', url: $("#" + url).attr("value"), success: function (response) {
            $("#loaderSpace,#loader").hide();
            $('#formulaire').jqxWindow("destroy");
            $.each(response, function (i, item) {
                $("#popupform").html(item)
            });
            $('#formulaire').jqxWindow({ height: "auto", width: "auto", isModal: true, minHeight: "20%", showCloseButton: true, theme: "ui-redmond" });
        }, error: function (jqXHR, exception) {
            console.log(2); $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) { alert('Not connect.\n Verify Network.') }
            else if (jqXHR.status == 404) { alert('Requested page not found. [404]') }
            else if (jqXHR.status == 500) { alert('Internal Server Error [500].') }
            else if (exception === 'parsererror') { alert('Requested JSON parse failed.') }
            else if (exception === 'timeout') { alert('Time out error.') }
            else if (exception === 'abort') { alert('Ajax request aborted.') }
            else { alert('Uncaught Error.\n' + jqXHR.responseText) }
        }
    }); return false
});

$('#formulaire').on('close', function (event) {
    $('#formulaire').jqxValidator('hide');
    $('#formulaire').jqxWindow("destroy");
    //$.ajax({
    //    url: '/grades/deverouiller', type: 'GET',
    //    success: function (a) {
    //        $('#formulaire').jqxValidator('hide');
    //        $('#formulaire').jqxWindow("destroy");
    //        //$("#grades").jqxGrid('updatebounddata', 'cells');
    //        //$("#grades").jqxGrid('clearselection');
    //    }
    //}); 
    return false
});

$(".RedirigerFermerConsultation").off('click');
$(".RedirigerFermerConsultation").on('click', function () {
    $("#loaderSpace,#loader").show();
    var url = $(this).attr('id') + "Url"; $.ajax({
        type: 'GET', url: $("#" + url).attr("value"), success: function (response) {
            $('#formulaire').jqxWindow("destroy");
            $.each(response, function (i, item) {
                if (i == 0) { $('#contentajax').html(item) }
                else if (i == 1) $('.tabs_involved').html(item);
                else $(".breadcrumbs").html(item)
            }); $("#loaderSpace,#loader").hide()
        }, error: function (jqXHR, exception) {
            $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) { alert('Not connect.\n Verify Network.') } else if (jqXHR.status == 404) { alert('Requested page not found. [404]') } else if (jqXHR.status == 500) { alert('Internal Server Error [500].') } else if (exception === 'parsererror') { alert('Requested JSON parse failed.') } else if (exception === 'timeout') { alert('Time out error.') } else if (exception === 'abort') { alert('Ajax request aborted.') } else { alert('Uncaught Error.\n' + jqXHR.responseText) }
        }
    })
});

$(".Fermer").off('click');
$(".Fermer").on('click', function () {
    $.ajax({
        url: '/grades/deverouiller', type: 'GET',
        success: function (a) {
            $('#formulaire').jqxValidator('hide');
            $('#formulaire').jqxWindow("destroy");
            $("#grades").jqxGrid('updatebounddata', 'cells');
            $("#grades").jqxGrid('clearselection');
        }
    }); return false
});

function OpenConsulter(url) {
    $.ajax({
        type: 'GET',
        url: url,
        success: function (response) {
            $(".toast-item").remove();
            $("#loaderSpace,#loader").hide();
            $.each(response, function (i, item) {
                $("#popupform").html(item);
            });
            $('#formulaire').jqxWindow({ height: "95%", width: "95%", isModal: true, minHeight: "20%", showCloseButton: true, theme: "ui-redmond" });
        },
        error: function (jqXHR, exception) {
            $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });

}