﻿//fonctionement du menu contextuelle
//quant on click sur un boutton du menu contextuel on cherche son correspondant dans 
// les boutton du haut et on genere le clieck sur ce correspondant
$('#confirm,#messages').css("display", "none");
$("#Menu").on('itemclick', function (event) {
    var args = event.args;
    var rowindex = $("#grades").jqxGrid('getselectedrowindex');
    var texte = $.trim($(args).text());
    //var selector = ".boutons text-right button[title*=" + texte + "]";
    var selector = "div.boutons button[title='" + texte + "']";
    $(selector).click();

});

//$("input[type='button'],button.oufbutton").jqxButton({ width: 'auto', height: 'auto', theme: "ui-redmond" });
$('#confirm').jqxWindow({ height: "120px", width: "350px", isModal: true, theme: "ui-redmond" });
$('#messages').jqxWindow({ height: "auto", width: "auto", isModal: true, theme: "ui-redmond" });
$('#confirm,#messages').jqxWindow("close");

$("#ok").off('click');
$("#ok").on('click', function () {
    $("#loaderSpace,#loader").show();
    $('#confirm').jqxWindow("close");
    if (id != "") {
        // appel Ajax
        $.ajax({
            url: $("#DeleteUrl").attr("value") + id, // le nom du fichier indiqué dans le formulaire
            type: 'GET', // la méthode indiquée dans le formulaire (get ou post)
        }).success(function (html) {
            $(".toast-item").remove();
            $("#grades").jqxGrid('updatebounddata', 'cells');
            $("#grades").jqxGrid('clearselection');
            $('body').append(html);
            $("#loaderSpace,#loader").hide();
        }).error(function () {
            $("#loaderSpace,#loader").hide();
            alert('Echek de suppression');
        })
    }
    else
        $("#loaderSpace,#loader").hide();
});

$("#annuler").off('click');
$("#annuler").on('click', function () {
    $('#confirm').jqxWindow("close");
    id = 0;
});

var datafields = [];
var columnsData = [ ];
for (var i = 0; i < documentName.length; i++) { // on les colonnes
    var element = documentName[i].split(':'); // element contient trois element 0 = libelle reel , 1 = libelle a afficher 2 = type
    datafields.push({ name: element[0], type: element[2] });
    if (i == 0) { // traitement de la premiere colonne qui represente l'identifiant de la grille
        columnsData.push({ text: element[1], editable: false, filtertype: 'textbox', datafield: element[0], hidden: true });
    }
    else { // traitement des autres colones
        //si element contient 4 element alors on a precisé la taille de la collone comme paramettre
        if (element.length == 4 || element.length == 5) {
            if (element[2] == 'number') {
                if (element[4] == 'sum') { // si on n'a pas passé de fonction d'agrégation
                    columnsData.push({ text: element[1], editable: false, datafield: element[0], width: element[3], filtertype: 'number', cellsalign: 'right', cellsformat: 'd', aggregates: ["sum"] });
                }
                else if (element[4] == 'count') { // si on n'a pas passé de fonction d'agrégation
                    columnsData.push({ text: element[1], editable: false, datafield: element[0], width: element[3], filtertype: 'number', cellsalign: 'right', cellsformat: 'd', aggregates: ["count"] });
                }
                else {
                    columnsData.push({ text: element[1], editable: false, datafield: element[0], width: element[3], filtertype: 'number', cellsalign: 'right', cellsformat: 'd' });
                }
            }
            else if (element[2] === 'date') {
                columnsData.push({ text: element[1], editable: false, datafield: element[0], width: element[3], filtertype: 'date', cellsformat: 'dd-MM-yyyy HH:mm' });
            }
            else if (element[2] === 'datetime') {
                columnsData.push({ text: element[1], editable: false, datafield: element[0], width: element[3], filtertype: 'date', cellsformat: 'dd-MM-yyyy HH:mm' });
            }
            else if (element[2] === 'bool') {
                columnsData.push({ text: element[1], editable: false, datafield: element[0], width: element[3], cellsalign: 'center', columntype: 'checkbox', filtertype: 'bool' });
            }
            else if (element[2] === 'string') {
                columnsData.push({ text: element[1], editable: false, datafield: element[0], width: element[3] });
            } else if (element[2] === 'stringdrop') {
                columnsData.push({ text: element[1], editable: false, datafield: element[0], width: element[3], filtertype: 'checkedlist', });
            } 
            else // on considere que c'est un string et on le traite comme tel
            {
                alert("Le type n'est pas prit en charge");
            }
        }
        else {
            alert("Le format des la colonne n'a pas été respecté");
        }
    }
};
//grid
var source =
{
    datatype: "json",
    url: $("#LoardUrl").attr("value"),
    datafields: datafields,
    cache: false,
    updaterow: function (rowid, rowdata, commit) {
        // synchronize with the server - send update command
        // call commit with parameter true if the synchronization with the server is successful 
        // and with parameter false if the synchronization failed.
        commit(true);
    },
    deleterow: function (rowid, commit) {
        // synchronize with the server - send delete command
        // call commit with parameter true if the synchronization with the server is successful 
        // and with parameter false if the synchronization failed.
        commit(true);
    }
};
var columnCheckBox = null;
var updatingCheckState = false;
var dataadapter = new $.jqx.dataAdapter(source, {
    loadError: function (xhr, status, error) {
        alert(error);
    }
});

// initialize grades
$("#grades").jqxGrid(
{
    columnsresize: true,
    editable: true,
    width: '100%',
    groupable: false,
    source: dataadapter,
    selectionmode: 'none',
    showfilterrow: true,
    sortable: false,
    autoheight: true,
    filterable: true,
    pageable: true,
    enabletooltips: true,
    showstatusbar: true,
    statusbarheight: 25,
    showaggregates: true,
    autoshowfiltericon: true,
    altrows: true,
    selectionmode: 'checkbox',
    theme: "ui-overcast",
    localization: getLocalization(),
    columns: [{ text: 'id', editable: false, filtertype: 'textbox', datafield: 'id', hidden: true }]
});

// change columns collection.
$("#grades").on('bindingcomplete', function (event) {
    $("#grades").jqxGrid({ columns: columnsData });
    $('#grades').jqxGrid({ pagesizeoptions: ['10', '20', '50', '100', '500'] });
    $('#grades').jqxGrid('pagesize', 50);
});

// select or unselect rows when a checkbox is checked or unchecked. 
$("#grades").on('cellvaluechanged', function (event) {
    if (event.args.value) {
        $("#grades").jqxGrid('selectrow', event.args.rowindex);
    }
    else {
        $("#grades").jqxGrid('unselectrow', event.args.rowindex);
    }

    // update the state of the column's checkbox. When all checkboxes on the displayed page are checked, we need to check column's checkbox. We uncheck it,
    // when there are no checked checkboxes on the page and set it to intederminate state when there is at least one checkbox checked on the page.  
    if (columnCheckBox) {
        var datainfo = $("#grades").jqxGrid('getdatainformation');
        var pagesize = datainfo.paginginformation.pagesize;
        var pagenum = datainfo.paginginformation.pagenum;
        var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
        var state = false;
        var count = 0;
        $.each(selectedRows, function () {
            if (pagenum * pagesize <= this && this < pagenum * pagesize + pagesize) {
                count++;
            }
        });

        if (count != 0) state = null;
        if (count == pagesize) state = true;
        if (count == 0) state = false;

        updatingCheckState = true;
        $(columnCheckBox).jqxCheckBox({ checked: state });

        updatingCheckState = false;
    }
});

// create context menu
var contextMenu = $("#Menu").jqxMenu({ width: "140", height: "auto", autoOpenPopup: false, mode: 'popup', theme: "ui-redmond" });
$("#grades").on('contextmenu', function () {
    return false;
});

$("#grades").on('rowclick', function (event) {
    if (event.args.rightclick) {
        $("#grades").jqxGrid('selectrow', event.args.rowindex);
        var scrollTop = $(window).scrollTop();
        var scrollLeft = $(window).scrollLeft();
        contextMenu.jqxMenu('open', parseInt(event.args.originalEvent.clientX) + 5 + scrollLeft, parseInt(event.args.originalEvent.clientY) + 5 + scrollTop);
        return false;
    } else {

    }
});

$(".exportgrille").on('click', function (event) {
    var textData = "";
    var cols = $("#grades").jqxGrid("columns");
    for (var i = 2; i < cols.records.length; i++) {
        if (i == 2) {
            textData = cols.records[i].text;
        } else {
            textData = textData + ";" + cols.records[i].text;
        }
    }
    var json = JSON.stringify($('#grades').jqxGrid('getrows'));
    var pramas = $(this).attr("id");
    console.log(textData);
    console.log(json);
    $.ajax({
        url: '/Base/GetDataGrid/' + pramas,
        type: 'post',
        data: {
            json: json,
            titre: textData
        },
        success: function (data) {
            window.location = "/Base/GetFile/" + pramas;
            //réponse du serveur
        }
    });
});

$("#sup").off('click');
$("#sup").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var tab = '';
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);
        if (tab == '')
            tab = ligne.id;
        else
            tab = tab + ';' + ligne.id;
    });
    id = tab;
    $('#confirm').jqxWindow("open");
});

$(".SansIdentifiant").off('click');
$(".SansIdentifiant").on('click', function () {
    $("#loaderSpace,#loader").show();
    var url = $(this).attr('id') + "Url";
    $.ajax({
        type: 'GET',
        url: $("#" + url).attr("value"),
        success: function (response) {
            $(".toast-item").remove();
            $("#loaderSpace,#loader").hide();
            // alert($("#loginpage", response).length);
            $.each(response, function (i, item) {
                $("#popupform").html(item);
            });
            if ($("#afficherclosebutton").attr("value") == 0) {
                $('#formulaire').jqxWindow({ height: "auto", width: "auto", isModal: true, minHeight: "20%", showCloseButton: true, theme: "ui-redmond" });
                $("#afficherclosebutton").remove();
            }
            else {
                $('#formulaire').jqxWindow({ height: "auto", width: "auto", isModal: true, minHeight: "10%", showCloseButton: false, theme: "ui-redmond" });
            }
        },
        error: function (jqXHR, exception) {
            $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });
    return false;
});

$(".AvecIdentifiant").off('click');
$(".AvecIdentifiant").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var compte = 0;
    var idselected = "";
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);
        compte = compte + 1;
        idselected = ligne.id;
    });
    if (compte == 1) {
       
        $("#loaderSpace,#loader").show();
        var url = $(this).attr('id') + "Url";
        $.ajax({
            type: 'GET',
            url: $("#" + url).attr("value") + idselected,
            success: function (response) {
                $(".toast-item").remove();
                $("#loaderSpace,#loader").hide();
                $.each(response, function (i, item) {
                    $("#popupform").html(item);
                });
                // les page index qui veulent afficher le popup avec le boutton de fermeture doivent positioner un controle hidden field avec la valeur 0 
                // Exemple <input id="afficherclosebutton" value="0" type="hidden" />
                if ($("#afficherclosebutton").attr("value") == 0) {
                    $('#formulaire').jqxWindow({ height: "auto", width: "auto", isModal: true, minHeight: "20%", showCloseButton: true, theme: "ui-redmond" });
                    $("#afficherclosebutton").remove();
                }
                else {
                    $('#formulaire').jqxWindow({ height: "auto", width: "auto", isModal: true, minHeight: "20%", showCloseButton: false, theme: "ui-redmond" });
                }
                //permet de rafraichir la grille dans le cas ou on n'affiche pas de po pup
                // Exemple <input id="refreshgrid" value="0" type="hidden" />
                //if ($("#refreshgrid")) {
                //    $("#grades").jqxGrid('updatebounddata', 'cells');
                //    $("#grades").jqxGrid('clearselection');
                //    $("#refreshgrid").remove();
                //}

            },
            error: function (jqXHR, exception) {
                $("#loaderSpace,#loader").hide();
                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    }
    else
        $('#messages').jqxWindow("open");
});


$(".CloseAvecIdentifiant").off('click');
$(".CloseAvecIdentifiant").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var compte = 0;
    var idselected = "";
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);
        compte = compte + 1;
        idselected = ligne.id;
    });
    if (compte == 1) {
        $("#loaderSpace,#loader").show();
        var url = $(this).attr('id') + "Url";
        $.ajax({
            type: 'GET',
            url: $("#" + url).attr("value") + idselected,
            success: function (response) {
                $(".toast-item").remove();
                $("#loaderSpace,#loader").hide();
                $.each(response, function (i, item) {
                    $("#popupform").html(item);
                });
                $('#formulaire').jqxWindow({ height: "95%", width: "95%", isModal: true, minHeight: "20%", showCloseButton: true, theme: "ui-redmond" });
                //permet de rafraichir la grille dans le cas ou on n'affiche pas de po pup
                // Exemple <input id="refreshgrid" value="0" type="hidden" />
                //if ($("#refreshgrid")) {
                //    $("#grades").jqxGrid('updatebounddata', 'cells');
                //    $("#grades").jqxGrid('clearselection');
                //    $("#refreshgrid").remove();
                //}

            },
            error: function (jqXHR, exception) {
                $("#loaderSpace,#loader").hide();
                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    }
    else
        $('#messages').jqxWindow("open");
});

$(".RedirigerAvecid").off('click');
$(".RedirigerAvecid").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var compte = 0;
    var idselected = "";
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);
        compte = compte + 1;
        idselected = ligne.id;
    });
    if (compte == 1) {
        $("#loaderSpace,#loader").show();
        var url = $(this).attr('id') + "Url";
        Afficher($("#" + url).attr("value") + idselected);
    }
    else
        $('#messages').jqxWindow("open");
});

$(".RedirigerSansid").off('click');
$(".RedirigerSansid").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');

    $("#loaderSpace,#loader").show();$("#" + url).attr("value")
    var url = $(this).attr('id') + "Url";
    Afficher($("#" + url).attr("value"));

});

$(".AvecIdentifiantSanspopup").off('click');
$(".AvecIdentifiantSanspopup").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var tab = '';
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);
        if (tab == '')
            tab = ligne.id;
        else
            tab = tab + ';' + ligne.id;
    });
    if (tab == '')
        return;
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    $("#loaderSpace,#loader").show();
    var url = $(this).attr('id') + "Url";
    $.ajax({
        type: 'GET',
        url: $("#" + url).attr("value") + tab,
        success: function (response) {
            $(".toast-item").remove();
            $("#loaderSpace,#loader").hide();
            $.each(response, function (i, item) {
                $("#popupform").html(item);
            });
            var grids = $("#grades");
            if (grids.length > 0) {
                grids.jqxGrid('updatebounddata', 'cells');
                grids.jqxGrid('clearselection');
            }
        },
        error: function (jqXHR, exception) {
            $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });
});

$(".AvecIdentifiantOrSanspopup").off('click');
$(".AvecIdentifiantOrSanspopup").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var compte = 0;
    var idselected = "";
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);
        compte = compte + 1;
        idselected = ligne.id;
    });
    if (compte == 1) {
        $("#loaderSpace,#loader").show();
        var url = $(this).attr('id') + "Url";
        $.ajax({
            type: 'GET',
            url: $("#" + url).attr("value") + idselected,
            success: function (response) {
                $(".toast-item").remove();
                $("#loaderSpace,#loader").hide();
                if (response.length == 1) {
                    $("body").append(response);
                } else {
                    $.each(response, function (i, item) {
                        $("#popupform").append(item);
                    });
                    $('#formulaire').jqxWindow({ height: "auto", width: "auto", isModal: true, minHeight: "20%", showCloseButton: false, theme: "ui-redmond" });
                }
            },
            error: function (jqXHR, exception) {
                $("#loaderSpace,#loader").hide();
                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    }
    else
        $('#messages').jqxWindow("open");
});

$(".AvecIdentifiantChat").off('click');
$(".AvecIdentifiantChat").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var tab = '';
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);
        tab = ligne.id;
    });
    if (tab == '')
        return;
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    $("#loaderSpace,#loader").show();
    var url = $(this).attr('id') + "Url";
    $.ajax({
        type: 'GET',
        url: $("#" + url).attr("value") + tab,
        success: function (response) {
            $(".toast-item").remove();
            $("#chat").remove();
            $("#loaderSpace,#loader").hide();
            $.each(response, function (i, item) {
                $("body").append(item);
            });
            var grids = $("#grades");
            if (grids.length > 0) {
                grids.jqxGrid('updatebounddata', 'cells');
                grids.jqxGrid('clearselection');
            }
        },
        error: function (jqXHR, exception) {
            $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });
});


$(".AvecPlusieursIdentifiant").off('click');
$(".AvecPlusieursIdentifiant").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var compte = '';
    var idselected = "";
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);

        if (compte == '')
            compte = ligne.id;
        else
            compte = compte + ';' + ligne.id;
    });

    if (compte == '')
    {
        $('#messages').jqxWindow("open");
        return;
    }
    else if (compte != '') {
        $("#loaderSpace,#loader").show();
        var url = $(this).attr('id') + "Url";
        $.ajax({
            type: 'GET',
            url: $("#" + url).attr("value") + compte,
            success: function (response) {
                $(".toast-item").remove();
                $("#loaderSpace,#loader").hide();
                $.each(response, function (i, item) {
                    $("#popupform").html(item);
                });
                // les page index qui veulent afficher le popup avec le boutton de fermeture doivent positioner un controle hidden field avec la valeur 0 
                // Exemple <input id="afficherclosebutton" value="0" type="hidden" />
                if ($("#afficherclosebutton").attr("value") == 0) {
                    $('#formulaire').jqxWindow({ height: "auto", width: "auto", isModal: true, minHeight: "20%", showCloseButton: true, theme: "ui-redmond" });
                    $("#afficherclosebutton").remove();
                }
                else {
                    $('#formulaire').jqxWindow({ height: "auto", width: "auto", isModal: true, minHeight: "20%", showCloseButton: false, theme: "ui-redmond" });
                }
            },
            error: function (jqXHR, exception) {
                $("#loaderSpace,#loader").hide();
                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    }
    else
        $('#messages').jqxWindow("open");
});


$(".AvecIdentifiantRedirect").off('click');
$(".AvecIdentifiantRedirect").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var compte = 0;
    var idselected = "";
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);
        compte = compte + 1;
        idselected = ligne.id;
    });
    if (compte == 1) {
        $("#loaderSpace,#loader").show();
        var url = $(this).attr('id') + "Url";
        $.ajax({
            type: 'GET',
            url: $("#" + url).attr("value") + idselected,
            success: function (response) {
                $(".toast-item").remove();
                $("#loaderSpace,#loader").hide();
                if (response.length == 1) {//page de page
                    $("#popupform").html(response);
                } else {
                    window.location = "/Base/pagebopi/" + idselected;
                }
            },
            error: function (jqXHR, exception) {
                $("#loaderSpace,#loader").hide();
                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    }
    else
        $('#messages').jqxWindow("open");
});


$(".AvecIdentifiantEnvoieFichier").off('click');
$(".AvecIdentifiantEnvoieFichier").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var tab = '';
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);
        if (tab == '')
            tab = ligne.id;
        else
            tab = tab + ';' + ligne.id;
    });
    if (tab == '') {
        $('#messages').jqxWindow("open");
        return;
    }
    var url = $(this).attr('id') + "Url";
    $("#loaderSpace,#loader").show();
    $.ajax({
        url: '/Base/SetTitre',
        type: 'POST',
        data: 'q='+tab,
        dataType: 'json',
        success: function (response) {
            $(".toast-item").remove();
            $("#loaderSpace,#loader").hide();
            window.location = $("#" + url).attr("value") ;
        },
        error: function (jqXHR, exception) {
            $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });
    return false;
});
