﻿
var checkTimeout = function () {
    //var thereIsStillTime = true;
    //$.ajax({
    //    url: "/Login/CheckTimeout/",
    //    type: 'GET',
    //    dataType: 'json',
    //    contentType: 'application/json; charset=utf-8',
    //    async: false,
    //    complete: function (data) {
    //        if ( data.responseText.indexOf("_Logon_") > -1)
    //            thereIsStillTime = false;
    //        if (!thereIsStillTime) {
    //            window.location.href = "/Login/TimeoutRedirect";
    //        }
    //    }
    //});

    return thereIsStillTime;
}

var OufPrinter = function (response) {

    var content = response;
    var newWindow = window.open('', "", 'width=1000, height=500,scrollbars=yes'),
   document = newWindow.document.open(),
    pageContent =
        '<!DOCTYPE html>' +
        '<html>' +
        '<head>' +
        '<link rel="stylesheet" type="text/css"  href="/Content/css/layout.css"  />' +
        '<link rel="stylesheet" type="text/css"  href="/Content/css/print.css" media="print" />' +
       ' <link rel="stylesheet" href="/Content/font/css/font-awesome.css">' +
        '<!--[if IE 7]>' +
       ' <link rel="stylesheet" href="/Content/font/css/font-awesome-ie7.css">' +
        '<![endif]-->' +
        '<meta charset="utf-8" />' +
        '<title></title>' +
        '</head>' +
        '<body>' +
       '<button title="Imprimer" type="button" id="print" onclick="window.print();" class="oufbutton"><i class="icon-print"></i> Imprimer</button>'
        + content +
        '<style> #print{display:block;margin:0 auto;} .jqx-tabs-title-container,.jqx-tabs-headerWrapper{display:none;visibility:hidden;} </style>'
    '</body></html>';
    document.write(pageContent);
    document.close();


}

$(document).ready(function () {
    $(".tablesorter").tablesorter();
    //When page loads...
    $(".tab_content").hide(); //Hide all content
    $("ul.tabs li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content

    //On Click Event
    $("ul.tabs li").click(function () {

        $("ul.tabs li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".tab_content").hide(); //Hide all tab content

        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
        $(activeTab).fadeIn(); //Fade in the active ID content
        return false;
    });

    //$('.column').equalHeight();


    $('#ribbon').ribbon();

    $('#enable-btn').click(function () {
        $('#del-table-btn').enable();
        $('#del-page-btn').enable();
        $('#save-btn').enable();
        $('#other-btn-2').enable();

        $('#enable-btn').hide();
        $('#disable-btn').show();
    });
    $('#disable-btn').click(function () {
        $('#del-table-btn').disable();
        $('#del-page-btn').disable();
        $('#save-btn').disable();
        $('#other-btn-2').disable();

        $('#disable-btn').hide();
        $('#enable-btn').show();
    });

    $('.ribbon-button').click(function () {
        if (this.isEnabled()) {
        }
    });


});
