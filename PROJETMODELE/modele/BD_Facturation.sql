USE [BD_Facturation]
GO
/****** Object:  StoredProcedure [dbo].[Affecter_Droit]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TEG-FACTURATION
-- Create date: 26 / 01 / 2014
-- Description:	Permet de gerer l'attibution des droits
-- =============================================
CREATE PROCEDURE [dbo].[Affecter_Droit]
	-- Add the parameters for the stored procedure here
	@Code_Commande as int , @Code_Profile as int , @Executer as int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@Executer = 0) -- refuser droit
	begin 
		-- retirer le droit d'execution pour lui et ses sous commandes							
		update TG_Droits set Executer = 0 where Code_Profile = @Code_Profile and Code_Commande in ( select Code_Commande from FN_Enfent_Commande(@Code_Commande))
	end 
	else IF (@Executer = 1) -- autoriser 
	begin 
		-- attribuer le droit pour la commande et les commandes parent							
		update TG_Droits set Executer = 1 where Code_Profile = @Code_Profile and Code_Commande in  (select Code_Commande from FN_Parent_Commande(@Code_Commande))
	end
	else IF (@Executer = 2) -- controle total
	begin 
	
		-- attribuer le droit aux commandes enfent
		update TG_Droits set Executer = 1 where Code_Profile = @Code_Profile and Code_Commande in ( select Code_Commande from FN_Enfent_Commande(@Code_Commande))
	
		-- attribuer le droit aux commandes parent
		update TG_Droits set Executer = 1 where Code_Profile = @Code_Profile and Code_Commande in  (select Code_Commande from FN_Parent_Commande(@Code_Commande))
	end
	else IF (@Executer = 3) -- Disponible
	begin 
		-- rendre le droit disponible pour la commande et les commandes parent						
		update TG_Droits set Disponible = 1 where Code_Profile = @Code_Profile and Code_Commande in  (select Code_Commande from FN_Parent_Commande(@Code_Commande))
	end
	else IF (@Executer = 4) -- Indisponible
	begin 
		-- rendre le droit indisponible pour lui et ses enfents							
		update TG_Droits set Disponible = 0 where Code_Profile = @Code_Profile and Code_Commande in ( select Code_Commande from FN_Enfent_Commande(@Code_Commande))
	end
	else IF (@Executer = 5) -- tous disponible
	begin 
	
		-- rendre le droit disponible aux commandes enfent
		update TG_Droits set Disponible = 1 where Code_Profile = @Code_Profile and Code_Commande in ( select Code_Commande from FN_Enfent_Commande(@Code_Commande))
	
		-- rendre le droit disponible aux commandes parent
		update TG_Droits set Disponible = 1 where Code_Profile = @Code_Profile and Code_Commande in (select Code_Commande from FN_Parent_Commande(@Code_Commande))
	end
END






GO
/****** Object:  StoredProcedure [dbo].[DELETE_LOG_DI]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		BD_Facturation
-- Create date: 01 02 2014
-- Description:	PERMET DE LIBERER LES LOGS QUI ONT DEPASSE LA VALEUR LIFE
-- =============================================
CREATE PROCEDURE [dbo].[DELETE_LOG_DI]
	-- Add the parameters for the stored procedure here
	@ID_Corbeille as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @life AS INT;
	
    -- selection de l'élément à restorer à partir de la corbeille
	SELECT @life = SentinelleLife FROM TG_Sentinelles_Options  WHERE ID_Sentinelle = 1
	
	DELETE FROM TG_Sentinelles 
	WHERE  DATEDIFF(DAY,Date, GETDATE()) >= @life
	
	
END






GO
/****** Object:  StoredProcedure [dbo].[Restorer_Corbeille]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TEG-FACTURATION
-- Create date: 01 02 2014
-- Description:	PERMET DE RESTORER LES ELEMENTS DE LA CORBEILLE
-- =============================================
CREATE PROCEDURE [dbo].[Restorer_Corbeille]
	-- Add the parameters for the stored procedure here
	@ID_Corbeille as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @ID_Table AS INT , @ID_Element AS INT , @LibelleReel as nvarchar(50), @Query  as nvarchar(500), @Libelle_ID as nvarchar(50),@ParmDefinition nvarchar(500);
	
    -- selection de l'élément à restorer à partir de la corbeille
	SELECT @ID_Table = ID_Table ,@ID_Element = ID_Element  FROM TG_Corbeille  WHERE ID_Corbeille = @ID_Corbeille
	
	--selection de la table de restoration
	SELECT @LibelleReel = LibelleReel, @Libelle_ID = Libelle_PK FROM TG_Tables WHERE ID_Table = @ID_Table

	-- restoration de l'élément

	SET @Query =N'UPDATE '+@LibelleReel+' SET Actif = 1 WHERE ' + @Libelle_ID +' = ' + convert (nvarchar(50),@ID_Element);

	print @Query;

	EXEC (@Query)
	
	-- supprimer l'element restorer de la corbeille
	DELETE FROM TG_Corbeille WHERE ID_Corbeille = @ID_Corbeille 
END






GO
/****** Object:  StoredProcedure [dbo].[Vider_Commande_Emballage_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	supprime les transations d'un utilisateur
-- =============================================
CREATE PROCEDURE [dbo].[Vider_Commande_Emballage_Temp] 
	-- Add the parameters for the stored procedure here
	@Code_User int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		DELETE FROM TP_Commande_Emballage_Temp WHERE Create_Code_User = @Code_User ;
END










GO
/****** Object:  StoredProcedure [dbo].[Vider_Commande_Liquide_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	supprime les transations d'un utilisateur
-- =============================================
CREATE PROCEDURE [dbo].[Vider_Commande_Liquide_Temp] 
	-- Add the parameters for the stored procedure here
	@Code_User int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		DELETE FROM TP_Commande_Liquide_Temp WHERE Create_Code_User = @Code_User ;
END









GO
/****** Object:  StoredProcedure [dbo].[Vider_Encaissement_Emballage_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	supprime les transations d'un utilisateur
-- =============================================
CREATE PROCEDURE [dbo].[Vider_Encaissement_Emballage_Temp] 
	-- Add the parameters for the stored procedure here
	@Code_User int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		DELETE FROM TP_Encaissement_Emballage_Temp WHERE Create_Code_User = @Code_User ;
END










GO
/****** Object:  StoredProcedure [dbo].[Vider_Retour_Emballage_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	supprime les transations d'un utilisateur
-- =============================================
CREATE PROCEDURE [dbo].[Vider_Retour_Emballage_Temp] 
	-- Add the parameters for the stored procedure here
	@Code_User int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		DELETE FROM TP_Retour_Emballage_Temp WHERE Create_Code_User = @Code_User ;
END









GO
/****** Object:  StoredProcedure [dbo].[Vider_Vente_Emballage_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	supprime les transations d'un utilisateur
-- =============================================
CREATE PROCEDURE [dbo].[Vider_Vente_Emballage_Temp] 
	-- Add the parameters for the stored procedure here
	@Code_User int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		DELETE FROM TP_Vente_Emballage_Temp WHERE Create_Code_User = @Code_User ;
END









GO
/****** Object:  StoredProcedure [dbo].[Vider_Vente_Liquide_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	supprime les transations d'un utilisateur
-- =============================================
CREATE PROCEDURE [dbo].[Vider_Vente_Liquide_Temp] 
	-- Add the parameters for the stored procedure here
	@Code_User int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		DELETE FROM TP_Vente_Liquide_Temp WHERE Create_Code_User = @Code_User ;
END








GO
/****** Object:  UserDefinedFunction [dbo].[FN_Chiffre_Affaire_Client]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Chiffre_Affaire_Client]
(
   @Date_Debut date, @Date_Fin date
)
RETURNS @FN_Stat_Vente_Produit TABLE
(
	ID_Client int NOT NULL,
	Client nvarchar(50) NOT NULL,
    Montant_Vente float  NULL,
    Montant_Achat float  NULL,
	Quantite_Casier int  NOT NULL,
	Quantite_Bouteille int  NOT NULL,
	Quantite_Casier_Temp int NOT NULL,
	Quantite_Bouteille_Temp int NOT NULL,
	Conditionnement int NOT NULL,
	Rang int NOT NULL
)
AS
BEGIN 
    INSERT INTO @FN_Stat_Vente_Produit (ID_Client, Client, Montant_Vente, Montant_Achat, Quantite_Casier, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Bouteille_Temp, Rang)
    
    SELECT ID_Client, Client, Montant_Vente, Montant_Achat, Quantite_Casier, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Bouteille_Temp, Rang          
    FROM		
		(	
		SELECT RESULTAT.ID_Client ID_Client, RESULTAT.Conditionnement Conditionnement, RESULTAT.Rang Rang,
		(SELECT Nom FROM TP_Client WHERE ID_Client = RESULTAT.ID_Client) AS Client, 
	    CASE WHEN (RESULTAT.Montant_Vente IS NULL) THEN 0 ELSE RESULTAT.Montant_Vente END AS Montant_Vente,
        CASE WHEN (RESULTAT.Montant_Achat IS NULL) THEN 0 ELSE RESULTAT.Montant_Achat END AS Montant_Achat,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier_Temp,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille_Temp
		FROM (SELECT ID_Client, SUM(Montant_Vente) AS Montant_Vente, SUM(Montant_Achat) AS Montant_Achat, SUM(Quantite_Casier) AS Quantite_Casier, SUM(Quantite_Bouteille) AS Quantite_Bouteille, Conditionnement, ROW_NUMBER() OVER (ORDER BY ID_Client) AS Rang
			FROM (					
					--VENTE LIQUIDE
					SELECT       ID_Client, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Vente, Ristourne_Client, Cumuler_Ristourne) Montant_Vente, 
					             dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Achat, 0, 0) Montant_Achat,
					             Quantite_Casier_Liquide Quantite_Casier, Quantite_Bouteille_Liquide Quantite_Bouteille, Conditionnement
					FROM         TP_Vente Vente INNER JOIN TP_Vente_Liquide Detail
					ON           Vente.ID_Vente = Detail.ID_Vente
					WHERE Etat = 1
					AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))
				)
			AS RESULTAT_1
			GROUP BY ID_Client, Conditionnement) AS RESULTAT 
   ) AS derivedtbl_2  
   
    DECLARE @ID AS INT, @Casier AS INT, @Bouteille AS INT, @Contenance AS INT, @Nombre_Total_Bouteil AS INT, @NombreCasierPlein AS INT, @NombreBouteil_Restant AS INT
    DECLARE Stock_Cursor CURSOR FOR
	SELECT Rang, Quantite_Casier_Temp, Quantite_Bouteille_Temp, Conditionnement FROM @FN_Stat_Vente_Produit;
	OPEN Stock_Cursor;
	FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
	WHILE @@FETCH_STATUS = 0
		BEGIN
		SET @Nombre_Total_Bouteil = @Casier * @Contenance + @Bouteille;
		SET @NombreCasierPlein = @Nombre_Total_Bouteil /  @Contenance ;
		SET @NombreBouteil_Restant = @Nombre_Total_Bouteil %  @Contenance ;
		update @FN_Stat_Vente_Produit set Quantite_Casier = @NombreCasierPlein, Quantite_Bouteille = @NombreBouteil_Restant   where Rang = @ID;		
		FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
		END;
	CLOSE Stock_Cursor;
	DEALLOCATE Stock_Cursor;                
RETURN ;
END














GO
/****** Object:  UserDefinedFunction [dbo].[FN_Chiffre_Affaire_Famille]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Chiffre_Affaire_Famille]
(
 -- type = 0 vente : type = 1 commande
   @Date_Debut date, @Date_Fin date, @Type int
)
RETURNS @FN_Stat_Vente_Produit TABLE
(
	ID_Famille int NOT NULL,
	Famille nvarchar(50) NOT NULL,
    Montant_Vente float  NULL,
    Montant_Achat float  NULL,
	Quantite_Casier int  NOT NULL,
	Quantite_Bouteille int  NOT NULL,
	Quantite_Casier_Temp int NOT NULL,
	Quantite_Bouteille_Temp int NOT NULL,
	Conditionnement int NOT NULL,
	Rang int NOT NULL
)
AS
BEGIN 
    INSERT INTO @FN_Stat_Vente_Produit (ID_Famille, Famille, Montant_Vente, Montant_Achat, Quantite_Casier, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Bouteille_Temp, Rang)
    
    SELECT ID_Famille, Famille, Montant_Vente, Montant_Achat, Quantite_Casier, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Bouteille_Temp, Rang        
    FROM		
		(	
		SELECT RESULTAT.ID_Famille ID_Famille, RESULTAT.Conditionnement Conditionnement, RESULTAT.Rang Rang,
		(SELECT Libelle FROM TP_Famille WHERE ID_Famille = RESULTAT.ID_Famille) AS Famille, 
	    CASE WHEN (RESULTAT.Montant_Vente IS NULL) THEN 0 ELSE RESULTAT.Montant_Vente END AS Montant_Vente,
		CASE WHEN (RESULTAT.Montant_Achat IS NULL) THEN 0 ELSE RESULTAT.Montant_Achat END AS Montant_Achat,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier_Temp,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille_Temp
		FROM (SELECT ID_Famille, SUM(Montant_Vente) AS Montant_Vente, SUM(Montant_Achat) AS Montant_Achat, SUM(Quantite_Casier) AS Quantite_Casier, SUM(Quantite_Bouteille) AS Quantite_Bouteille, Conditionnement, ROW_NUMBER() OVER (ORDER BY ID_Famille) AS Rang
			FROM (					
					--VENTE
					SELECT       ID_Famille, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, Detail.PU_Vente, Ristourne_Client, Cumuler_Ristourne) Montant_Vente,
					             dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, Detail.PU_Achat, 0, 0) Montant_Achat,
					             Quantite_Casier_Liquide Quantite_Casier, Quantite_Bouteille_Liquide Quantite_Bouteille, Conditionnement
					FROM         TP_Vente Vente INNER JOIN TP_Vente_Liquide Detail
					ON           Vente.ID_Vente = Detail.ID_Vente INNER JOIN TP_Produit Produit
					ON           Detail.ID_Produit = Produit.ID_Produit 
					WHERE Etat = (CASE @Type WHEN 0 THEN 1 ELSE 10 END)
					AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))

					UNION ALL 

					--COMMANDES
					SELECT       ID_Famille, 0 Montant_Vente, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, Commande_Detail.PU_Achat, 0, 0)  Montant_Achat,
					             Quantite_Casier_Liquide Quantite_Casier, Quantite_Bouteille_Liquide Quantite_Bouteille, Conditionnement
					FROM         TP_Commande Commande INNER JOIN TP_Commande_Liquide Commande_Detail 
					ON Commande.ID_Commande = Commande_Detail.ID_Commande INNER JOIN TP_Produit Produit 
					ON Commande_Detail.ID_Produit = Produit.ID_Produit
					WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END)
					AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))
				)
			AS RESULTAT_1
			GROUP BY ID_Famille, Conditionnement) AS RESULTAT 
   ) AS derivedtbl_2  
   
    DECLARE @ID AS INT, @Casier AS INT, @Bouteille AS INT, @Contenance AS INT, @Nombre_Total_Bouteil AS INT, @NombreCasierPlein AS INT, @NombreBouteil_Restant AS INT
    DECLARE Stock_Cursor CURSOR FOR
	SELECT Rang, Quantite_Casier_Temp, Quantite_Bouteille_Temp, Conditionnement FROM @FN_Stat_Vente_Produit;
	OPEN Stock_Cursor;
	FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
	WHILE @@FETCH_STATUS = 0
		BEGIN
		SET @Nombre_Total_Bouteil = @Casier * @Contenance + @Bouteille;
		SET @NombreCasierPlein = @Nombre_Total_Bouteil /  @Contenance ;
		SET @NombreBouteil_Restant = @Nombre_Total_Bouteil %  @Contenance ;
		update @FN_Stat_Vente_Produit set Quantite_Casier = @NombreCasierPlein, Quantite_Bouteille = @NombreBouteil_Restant   where Rang = @ID;		
		FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
		END;
	CLOSE Stock_Cursor;
	DEALLOCATE Stock_Cursor;                
RETURN ;
END








GO
/****** Object:  UserDefinedFunction [dbo].[FN_Chiffre_Affaire_Fournisseur]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Chiffre_Affaire_Fournisseur]
(
   @Date_Debut date, @Date_Fin date
)
RETURNS @FN_Stat_Vente_Produit TABLE
(
	ID_Fournisseur int NOT NULL,
	Fournisseur nvarchar(50) NOT NULL,
    Montant float  NULL,
	Rang int NOT NULL,
	Quantite_Casier int  NOT NULL,
	Quantite_Bouteille int  NOT NULL,
	Quantite_Casier_Temp int NOT NULL,
	Quantite_Bouteille_Temp int NOT NULL,
	Conditionnement int NOT NULL
)
AS
BEGIN 
    INSERT INTO @FN_Stat_Vente_Produit (ID_Fournisseur, Fournisseur, Montant, Rang, Quantite_Casier, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Bouteille_Temp)
    
    SELECT ID_Fournisseur, Fournisseur, Montant, Rang, Quantite_Casier, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Bouteille_Temp          
    FROM		
		(	
		SELECT RESULTAT.ID_Fournisseur ID_Fournisseur, RESULTAT.Conditionnement Conditionnement,
		(SELECT Nom FROM TP_Fournisseur WHERE ID_Fournisseur = RESULTAT.ID_Fournisseur) AS Fournisseur, 
	    CASE WHEN (RESULTAT.Montant IS NULL) THEN 0 ELSE RESULTAT.Montant END AS Montant,
	    CASE WHEN (RESULTAT.Rang IS NULL) THEN 0 ELSE RESULTAT.Rang END AS Rang,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier_Temp,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille_Temp
		FROM (SELECT ID_Fournisseur, SUM(Montant) AS Montant, SUM(Quantite_Casier) AS Quantite_Casier, SUM(Quantite_Bouteille) AS Quantite_Bouteille, Conditionnement, ROW_NUMBER() OVER (ORDER BY ID_Fournisseur) AS Rang
			FROM (					
					--VENTE COMMANDES
					SELECT       ID_Fournisseur, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Achat, 0, 0) Montant,
					             Quantite_Casier_Liquide Quantite_Casier, Quantite_Bouteille_Liquide Quantite_Bouteille, Conditionnement
					FROM         TP_Commande Commande INNER JOIN TP_Commande_Liquide Detail
					ON           Commande.ID_Commande = Detail.ID_Commande
					WHERE Etat = 1
					AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))
				)
			AS RESULTAT_1
			GROUP BY ID_Fournisseur, Conditionnement) AS RESULTAT 
   ) AS derivedtbl_2  
   
    DECLARE @ID AS INT, @Casier AS INT, @Bouteille AS INT, @Contenance AS INT, @Nombre_Total_Bouteil AS INT, @NombreCasierPlein AS INT, @NombreBouteil_Restant AS INT
    DECLARE Stock_Cursor CURSOR FOR
	SELECT Rang, Quantite_Casier_Temp, Quantite_Bouteille_Temp, Conditionnement FROM @FN_Stat_Vente_Produit;
	OPEN Stock_Cursor;
	FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
	WHILE @@FETCH_STATUS = 0
		BEGIN
		SET @Nombre_Total_Bouteil = @Casier * @Contenance + @Bouteille;
		SET @NombreCasierPlein = @Nombre_Total_Bouteil /  @Contenance ;
		SET @NombreBouteil_Restant = @Nombre_Total_Bouteil %  @Contenance ;
		update @FN_Stat_Vente_Produit set Quantite_Casier = @NombreCasierPlein, Quantite_Bouteille = @NombreBouteil_Restant   where Rang = @ID;		
		FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
		END;
	CLOSE Stock_Cursor;
	DEALLOCATE Stock_Cursor;                
RETURN ;
END














GO
/****** Object:  UserDefinedFunction [dbo].[FN_Chiffre_Affaire_Produit]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Chiffre_Affaire_Produit]
(
   -- Type == 0 Liquide, Type == 1 Emballage
   @Date_Debut date, @Date_Fin date, @Type int
)
RETURNS @FN_Stat_Vente_Produit TABLE
(
	ID_Produit int NOT NULL,
	Produit nvarchar(50) NOT NULL,
    Montant_Vente float NOT NULL,
    Montant_Achat float NOT NULL,
	Quantite_Casier int  NOT NULL,
	Quantite_Plastique int  NOT NULL,
	Quantite_Bouteille int  NOT NULL,
	Quantite_Casier_Temp int NOT NULL,
	Quantite_Plastique_Temp int NOT NULL,
	Quantite_Bouteille_Temp int NOT NULL,
	Conditionnement int NOT NULL
)
AS
BEGIN 
    INSERT INTO @FN_Stat_Vente_Produit (ID_Produit, Produit, Montant_Vente, Montant_Achat, Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Plastique_Temp, Quantite_Bouteille_Temp)
    
    SELECT ID_Produit, Produit, Montant_Vente, Montant_Achat, Quantite_Casier,  Quantite_Plastique, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Plastique_Temp, Quantite_Bouteille_Temp          
    FROM		
		(	
		SELECT RESULTAT.ID_Produit ID_Produit, RESULTAT.Conditionnement Conditionnement,
		(CASE WHEN (@Type = 0) THEN (SELECT Libelle FROM TP_Produit WHERE ID_Produit = RESULTAT.ID_Produit) 
	     ELSE (SELECT Description FROM TP_Emballage WHERE ID_Emballage = RESULTAT.ID_Produit) END) AS Produit, 
	    CASE WHEN (RESULTAT.Montant_Vente IS NULL) THEN 0 ELSE RESULTAT.Montant_Vente END AS Montant_Vente,
		CASE WHEN (RESULTAT.Montant_Achat IS NULL) THEN 0 ELSE RESULTAT.Montant_Achat END AS Montant_Achat,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier,
		CASE WHEN (RESULTAT.Quantite_Plastique IS NULL) THEN 0 ELSE RESULTAT.Quantite_Plastique END AS Quantite_Plastique,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier_Temp,
		CASE WHEN (RESULTAT.Quantite_Plastique IS NULL) THEN 0 ELSE RESULTAT.Quantite_Plastique END AS Quantite_Plastique_Temp,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille_Temp
		FROM (SELECT ID_Produit, SUM(Montant_Vente) AS Montant_Vente, SUM(Montant_Achat) AS Montant_Achat, SUM(Quantite_Casier) AS Quantite_Casier, SUM(Quantite_Plastique) AS Quantite_Plastique, SUM(Quantite_Bouteille) AS Quantite_Bouteille, Conditionnement
			FROM (					
					--VENTE LIQUIDE
					SELECT       ID_Produit, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Vente, Ristourne_Client, Cumuler_Ristourne) Montant_Vente,
					             dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Achat, 0, 0) Montant_Achat,
					             Quantite_Casier_Liquide Quantite_Casier, 0 Quantite_Plastique, Quantite_Bouteille_Liquide Quantite_Bouteille, Conditionnement
					FROM         TP_Vente Vente INNER JOIN TP_Vente_Liquide Detail
					ON           Vente.ID_Vente = Detail.ID_Vente
					WHERE Etat = (CASE @Type WHEN 0 THEN 1 ELSE 10 END)
					AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))
				)
			AS RESULTAT_1
			GROUP BY ID_Produit, Conditionnement) AS RESULTAT 
   ) AS derivedtbl_2  
   
    DECLARE @ID AS INT, @Casier AS INT, @Bouteille AS INT, @Contenance AS INT, @Nombre_Total_Bouteil AS INT, @NombreCasierPlein AS INT, @NombreBouteil_Restant AS INT
    DECLARE Stock_Cursor CURSOR FOR
	SELECT ID_Produit, Quantite_Casier_Temp, Quantite_Bouteille_Temp, Conditionnement FROM @FN_Stat_Vente_Produit;
	OPEN Stock_Cursor;
	FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
	WHILE @@FETCH_STATUS = 0
		BEGIN
		SET @Nombre_Total_Bouteil = @Casier * @Contenance + @Bouteille;
		SET @NombreCasierPlein = @Nombre_Total_Bouteil / @Contenance;
		SET @NombreBouteil_Restant = @Nombre_Total_Bouteil % @Contenance;
		update @FN_Stat_Vente_Produit set Quantite_Casier = @NombreCasierPlein, Quantite_Bouteille = @NombreBouteil_Restant   where ID_Produit = @ID;		
		FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
		END;
	CLOSE Stock_Cursor;
	DEALLOCATE Stock_Cursor;                
RETURN ;
END









GO
/****** Object:  UserDefinedFunction [dbo].[FN_Commande]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Commande]
(	
)
RETURNS @FN_Vente TABLE
(
	ID_Commande bigint NOT NULL,
	ID_Fournisseur int NOT NULL,
	Fournisseur nvarchar(200),
	NUM_DOC nvarchar(50),
    Date  Datetime NOT NULL,
    Etat  int NOT NULL,
    Versement float NOT NULL,
    Montant_Liquide float NOT NULL,
    Montant_Emballage float NOT NULL,
    Montant_Retour_Emballage float NOT NULL,
    Montant as (Montant_Liquide+Montant_Emballage-Montant_Retour_Emballage)
)
AS
BEGIN 
    INSERT INTO @FN_Vente (ID_Commande, NUM_DOC, Date, Etat, Versement, Montant_Liquide, Montant_Emballage, Montant_Retour_Emballage, ID_Fournisseur, Fournisseur)
    
    SELECT 
          ID_Commande,
          NUM_DOC,
          Date,
          Etat,
          Versement,
          Montant_Liquide,
          Montant_Emballage,
          Montant_Retour_Emballage, 
		  ID_Fournisseur,
		  Fournisseur
    FROM		
		(	
		SELECT StockReel.ID_Commande ID_Commande,StockReel.ID_Fournisseur ID_Fournisseur,
		StockReel.Nom Fournisseur,
		StockReel.NUM_DOC NUM_DOC,
		StockReel.Date Date, 
		StockReel.Etat Etat,   
	    CASE WHEN (StockReel.Montant IS NULL) THEN 0 ELSE StockReel.Montant END AS Versement,
	    CASE WHEN (EMBALLAGE.EMBALLAGE IS NULL) THEN 0 ELSE EMBALLAGE.EMBALLAGE END AS Montant_Emballage,
	    CASE WHEN (LIQUIDE.LIQUIDE IS NULL) THEN 0 ELSE LIQUIDE.LIQUIDE END AS Montant_Liquide,
	    CASE WHEN (RETOUR_EMBALLAGE.RETOUR_EMBALLAGE IS NULL) THEN 0 ELSE RETOUR_EMBALLAGE.RETOUR_EMBALLAGE END AS Montant_Retour_Emballage	
		FROM (SELECT ID_Fournisseur ,Montant, Date, Etat,Nom,NUM_DOC, ID_Commande
			FROM (				
					--Montant Emballage
					SELECT       commande.ID_Fournisseur ID_Fournisseur, NUM_DOC, Nom, Montant, Date, Etat, ID_Commande
					FROM         dbo.TP_Commande commande inner join TP_Fournisseur fournisseur on commande.ID_Fournisseur = fournisseur.ID_Fournisseur				
				 )
			AS derivedtbl_1) AS StockReel LEFT OUTER JOIN
			
			(  
			  SELECT ID_Commande, EMBALLAGE
			   FROM
			     (
			       SELECT ID_Commande ,SUM(Montant) AS EMBALLAGE
			       --VENTE EMBALLAGE 
						FROM (
						       SELECT      commande.ID_Commande ID_Commande, dbo.FN_Prix_Emballage(Quantite_Casier_Emballage, Quantite_Plastique_Emballage, Quantite_Bouteille_Emballage, PU_Plastique, PU_Bouteille, Conditionnement)  Montant
								FROM       dbo.TP_Commande commande inner join dbo.TP_Commande_Liquide emballage on commande.ID_Commande = emballage.ID_Commande					        
							 ) AS EMBALLAGE_2
						GROUP BY ID_Commande
			      ) AS EMBALLAGE_1
			) AS EMBALLAGE ON StockReel.ID_Commande  = EMBALLAGE.ID_Commande LEFT OUTER JOIN	
			
			(  
			  SELECT ID_Commande, LIQUIDE
			   FROM
			     (
			       SELECT ID_Commande ,SUM(Montant) AS LIQUIDE
			     --VENTE LIQUIDE 
						FROM (
						       SELECT       commande.ID_Commande ID_Commande, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide,Conditionnement, PU_Achat, 0, 0) Montant
								FROM        TP_Commande commande inner join TP_Commande_Liquide liquide
								on commande.ID_Commande = liquide.ID_Commande    
								) AS LIQUIDE_2
						GROUP BY ID_Commande
			      ) AS LIQUIDE_1
			) AS LIQUIDE ON StockReel.ID_Commande  = LIQUIDE.ID_Commande LEFT OUTER JOIN
			
			(  
			  SELECT ID_Commande, RETOUR_EMBALLAGE
			   FROM
			     (
			       SELECT ID_Commande ,SUM(Montant) AS RETOUR_EMBALLAGE
			     --RETOUR_EMBALLAGE 
						FROM (
						       SELECT      commande.ID_Commande ID_Commande ,dbo.FN_Prix_Emballage(Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille,Conditionnement) Montant
								FROM       dbo.TP_Commande commande inner join dbo.TP_Commande_Emballage emballage on commande.ID_Commande = emballage.ID_Commande        
								) AS RETOUR_EMBALLAGE_2
						GROUP BY ID_Commande
			      ) AS RETOUR_EMBALLAGE_1
			) AS RETOUR_EMBALLAGE ON StockReel.ID_Commande  = RETOUR_EMBALLAGE.ID_Commande
				
   ) AS derivedtbl_2               
RETURN ;
END








GO
/****** Object:  UserDefinedFunction [dbo].[FN_Enfent_Commande]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TEG-FACTURATION
-- Create date: 19-11-2015
-- Description:	cette fontion renvoie toutes les commandes enfents d'une commande passer en parametre
-- =============================================
CREATE FUNCTION [dbo].[FN_Enfent_Commande]
(
	@Code_Commande int
)
RETURNS 
@Tab_Enfent_Commande TABLE 
(
	Code_Commande int not null,
	Parcourru int not null -- si parcourru est a 0 alors on n'a pas encore enregistrer les fils direct de cette commande
)
AS
BEGIN
	declare @Code_Commande_Parent as int
	
	-- insertion de la commande elle meme comme etant son propore enfent
	insert into @Tab_Enfent_Commande values(@Code_Commande,0);
	
	select @Code_Commande_Parent = Code_Commande_Parent from TG_Commandes where Code_Commande = @Code_Commande;
	
	-- cette boucle permet de retrouver tous les enfents des dans l'hierarchie de la commande
	-- si on retrouve une commande dont on n'a pas encore enregistrer les enfents alors
	-- on enregistre ses enfents et on la marque comme deja parcourru
	WHILE(EXISTS(SELECT Code_Commande FROM @Tab_Enfent_Commande WHERE Parcourru = 0))
	BEGIN
		
		update @Tab_Enfent_Commande set Parcourru = 1 -- indiquer les commandes comme etant parcourru
		
		insert into @Tab_Enfent_Commande (Code_Commande,Parcourru)
		select Code_Commande ,0 from TG_Commandes  -- selection des commandes 
		where Code_Commande_Parent in (select Code_Commande from @Tab_Enfent_Commande ) -- dont les parents sont enregistrer dans la table
			and Code_Commande not in (select Code_Commande from @Tab_Enfent_Commande ) -- et sui n'ont pas encore été enregistré
		
	END
	
	RETURN 
END







GO
/****** Object:  UserDefinedFunction [dbo].[FN_Etat_Solde_Caisse]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Etat_Solde_Caisse]
(	
	@Date_Debut datetime, @Date_Fin datetime
)
RETURNS @Tab_Solde TABLE
(
	ID_Caisse int NOT NULL,
	Caisse nvarchar(100) NOT NULL,
    Initial  as (  Solde + Decaissement + Depense + Ristourne - Encaissement - Vente - Regulation_Caisse),
    Vente float NOT NULL,
    Encaissement float NOT NULL,
	Decaissement  float NOT NULL,	
    Depense  float NOT NULL,
	Ristourne  float NOT NULL,    
    Regulation_Caisse float NOT NULL,
    Solde float NOT NULL
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (ID_Caisse, Caisse, Decaissement, Depense, Ristourne, Encaissement,Vente, Regulation_Caisse, Solde)   
    SELECT ID_Caisse, Caisse, Decaissement, Depense, Ristourne, Encaissement, Vente, Regulation_Caisse, Solde  
    FROM		
		(	
		SELECT TAB_Caisse.ID_Caisse as ID_Caisse,TAB_Caisse.Libelle as Caisse, 	    
	    CASE WHEN (StockReel.Montant IS NULL) THEN 0 ELSE StockReel.Montant END AS Solde,
		CASE WHEN (ENCAISSEMENT.ENCAISSEMENT IS NULL) THEN 0 ELSE ENCAISSEMENT.ENCAISSEMENT END AS Encaissement,
		CASE WHEN (RISTOURNE.RISTOURNE IS NULL) THEN 0 ELSE RISTOURNE.RISTOURNE END AS Ristourne,
		CASE WHEN (DECAISSEMENT.DECAISSEMENT IS NULL) THEN 0 ELSE DECAISSEMENT.DECAISSEMENT END AS Decaissement,		
		CASE WHEN (DEPENSE.DEPENSE  IS NULL) THEN 0 ELSE DEPENSE.DEPENSE END AS Depense,
		CASE WHEN (REGULER.REGULER IS NULL) THEN 0 ELSE REGULER.REGULER END AS Regulation_Caisse,
		CASE WHEN (VENTE.VENTE IS NULL) THEN 0 ELSE VENTE.VENTE END AS Vente
		FROM (select ID_Caisse, Libelle  FROM TP_Caisse ) as TAB_Caisse LEFT OUTER JOIN
		(SELECT ID_Caisse, SUM(Montant) AS Montant
			FROM (				
					--Depense
					SELECT        ID_Caisse , - Montant  Montant
					FROM         dbo.TP_Decaissement
					WHERE (Etat = 1) AND Type_Element = 2 AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
					
					 UNION ALL
					 --Encaissement
				    SELECT       ID_Caissse AS ID_Caisse,  Montant
					FROM         dbo.TP_Encaissement 
					WHERE (Etat = 1) AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
					
					 UNION ALL
					 --Regulation
				    SELECT       ID_Element ID_Caisse,  Montant
					FROM         dbo.TP_Regulation_Solde 
					WHERE (Etat = 1) AND Type_Element = 2 AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
									 
					UNION ALL 
				     --Ristourne 
				    SELECT     ID_Caisse, - Montant Montant
					FROM         dbo.TP_Decaissement
					WHERE (Etat = 1) AND Type_Element = 1 AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
					
					UNION ALL 
				     --Decaissement 
				    SELECT     ID_Caisse, - Montant Montant
					FROM         dbo.TP_Decaissement
					WHERE (Etat = 1) AND Type_Element = 0 AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))

					UNION ALL 
				     --Vente  
				    SELECT       ID_Caisse, (Montant_Verser + Montant_Consigne_Emballage) Montant
					FROM         dbo.FN_Vente()
					WHERE (Etat = 1) AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
				)
			AS derivedtbl_1
			GROUP BY ID_Caisse) AS StockReel on TAB_Caisse.ID_Caisse = StockReel.ID_Caisse LEFT OUTER JOIN		
			(  
			  SELECT ID_Caisse, ENCAISSEMENT
			   FROM
			     (
			       SELECT ID_Caisse ,SUM(Montant) AS ENCAISSEMENT
			     --ENCAISSEMENT
						FROM (
								SELECT       ID_Caissse as ID_Caisse, Montant
								FROM         dbo.TP_Encaissement 
								WHERE (Etat = 1) AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
								) AS ENCAISSEMENT_2
						GROUP BY ID_Caisse
			      ) AS ENCAISSEMENT_1
			) AS ENCAISSEMENT ON StockReel.ID_Caisse  = ENCAISSEMENT.ID_Caisse LEFT OUTER JOIN
			(  
			  SELECT ID_Caisse, DECAISSEMENT
			   FROM
			     (
			       SELECT ID_Caisse ,SUM(Montant) AS DECAISSEMENT
			     --DECAISSEMENT
						FROM (
								SELECT       ID_Caisse,  Montant
								FROM         dbo.TP_Decaissement
								WHERE (Etat = 1) AND Type_Element = 0 AND   (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102))
								) AS DECAISSEMENT_2
						GROUP BY ID_Caisse
			      ) AS DECAISSEMENT_1
			) AS DECAISSEMENT ON StockReel.ID_Caisse  = DECAISSEMENT.ID_Caisse LEFT OUTER JOIN
			(  
			  SELECT ID_Caisse, RISTOURNE
			   FROM
			     (
			       SELECT ID_Caisse ,SUM(Montant) AS RISTOURNE
			     --RISTOURNE
						FROM (
								SELECT       ID_Caisse,  Montant
								FROM         dbo.TP_Decaissement
								WHERE (Etat = 1) AND Type_Element = 1 AND   (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102))
								) AS RISTOURNE_2
						GROUP BY ID_Caisse
			      ) AS RISTOURNE_1
			) AS RISTOURNE ON StockReel.ID_Caisse  = RISTOURNE.ID_Caisse LEFT OUTER JOIN
			(  
			  SELECT ID_Caisse, DEPENSE
			   FROM
			     (
			       SELECT ID_Caisse, SUM(Montant) AS DEPENSE
			     --DEPENSE
						FROM (
								SELECT     ID_Caisse, Montant
								FROM         dbo.TP_Decaissement
								WHERE (Etat = 1) AND Type_Element = 2 AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
								 AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
							 )	AS DEPENSE_2
						GROUP BY ID_Caisse
			      ) AS DEPENSE_1
			) AS DEPENSE ON StockReel.ID_Caisse  = DEPENSE.ID_Caisse LEFT OUTER JOIN
			(  
			  SELECT ID_Caisse, REGULER
			   FROM
			     (
			       SELECT ID_Caisse, SUM(Montant) AS REGULER
						---REGULER
						FROM (
								SELECT   ID_Element ID_Caisse, Montant
								FROM     dbo.TP_Regulation_Solde
								WHERE (Etat = 1) AND Type_Element = 2 AND  (CAST([Date] AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
								 AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
							 )	AS REGULER_2
						GROUP BY ID_Caisse
			      ) AS REGULER_1
			) AS REGULER ON StockReel.ID_Caisse  = REGULER.ID_Caisse LEFT OUTER JOIN
			(  
			  SELECT ID_Caisse, VENTE
			   FROM
			     (
			       SELECT ID_Caisse, SUM(Montant) AS VENTE
			     ---VENTE
						FROM (
								SELECT    ID_Caisse, (Montant_Verser+Montant_Consigne_Emballage) Montant
								FROM         dbo.FN_Vente() 
								WHERE (Etat = 1) AND  (CAST([Date] AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
								 AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
							 )	AS VENTE_2
						GROUP BY ID_Caisse
			      ) AS VENTE_1
			) AS VENTE ON StockReel.ID_Caisse  = VENTE.ID_Caisse
   ) AS derivedtbl_2               
RETURN ;
END








GO
/****** Object:  UserDefinedFunction [dbo].[FN_Etat_Solde_Client]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Etat_Solde_Client]
(	
    -- Type = 0 alors solde liquide, Type = 1 alors solde emballage
    -- Type = 2 solde client 
	@Type int, @Date_Debut datetime, @Date_Fin datetime
)
RETURNS @Tab_Solde TABLE
(
	ID_Client int NOT NULL,
	Client nvarchar(100) NOT NULL,
    Initial  as (  Solde + Encaissement + Consigne + Versement + Retour_emballage - Regulation - Vente),
	Vente  float NOT NULL,	
    Versement  float NOT NULL,
    Encaissement float NOT NULL,
    Regulation float NOT NULL,
	Consigne float NOT NULL,
    Retour_emballage float NOT NULL,
    Solde  float NOT NULL
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (ID_Client, Client, Vente, Versement, Encaissement, Regulation, Consigne, Solde, Retour_emballage)   
    SELECT 
     ID_Client, 
     Client, 
     Vente,
     Versement, 
     Encaissement,
     Regulation,
	 Consigne, 
     Solde,
     Retour_emballage  
    FROM		
		(	
		SELECT StockReel.ID_Client as ID_Client,
		(SELECT Nom FROM TP_Client WHERE ID_Client = StockReel.ID_Client) AS Client,
	    StockReel.Montant as Solde,
		CASE WHEN (ENCAISSEMENT.ENCAISSEMENT IS NULL) THEN 0 ELSE ENCAISSEMENT.ENCAISSEMENT END AS Encaissement,
		CASE WHEN (CONSIGNE.CONSIGNE IS NULL) THEN 0 ELSE CONSIGNE.CONSIGNE END AS Consigne,
		CASE WHEN (RETOUR_EMBALLAGE.RETOUR_EMBALLAGE IS NULL) THEN 0 ELSE RETOUR_EMBALLAGE.RETOUR_EMBALLAGE END AS Retour_emballage,		
		CASE WHEN (VERSEMENT.VERSEMENT  IS NULL) THEN 0 ELSE VERSEMENT.VERSEMENT END AS Versement,
		CASE WHEN (REGULER.REGULER IS NULL) THEN 0 ELSE REGULER.REGULER END AS Regulation,
		CASE WHEN (VENTE.VENTE IS NULL) THEN 0 ELSE VENTE.VENTE END AS Vente
		FROM (SELECT ID_Client, SUM(Montant) AS Montant
			FROM (	
			        --Client
					SELECT        ID_Client , 0 Montant
					FROM         dbo.TP_Client
					WHERE Actif = 1 
										
			        UNION ALL			
					--Encaissement
					SELECT        ID_Client , -Montant  Montant
					FROM         dbo.TP_Encaissement
					WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END)  
					     AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
					
					 UNION ALL
					 --Regulation
				    SELECT       ID_Element ID_Client,  Montant
					FROM         dbo.TP_Regulation_Solde 
					WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END)  
					AND Type_Element = 1 AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
									 
					UNION ALL 
				     --Versement 
				    SELECT     ID_Client, -Montant
					FROM         dbo.TP_Vente
					WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
					AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 				
					
					UNION ALL 
				     --Vente liquide   
				    SELECT       ID_Client,  (Montant_Liquide) AS Montant
					FROM         dbo.FN_Vente()
					WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
					 AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
					 
					UNION ALL 
					--Retour Emballage
					SELECT        ID_Element ID_Client ,  -Montant  Montant
					FROM         dbo.FN_Retour_Emballage()
					WHERE Etat = (CASE @Type WHEN 0 THEN  10  WHEN 1 THEN 1  ELSE  1 END) 
					AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) AND Type_Element = 1 
							
					UNION ALL 
				     --Vente emballage  
				    SELECT       ID_Client, (Montant_Emballage - Montant_Retour_Emballage) AS Montant
					FROM         dbo.FN_Vente()
					WHERE Etat = (CASE @Type WHEN 0 THEN  10  WHEN 1 THEN 1  ELSE  1 END)
					AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))

					UNION ALL 										
						--- Consigne
					SELECT     ID_Client, -Montant_Consigne_Emballage Montant
					FROM          dbo.TP_Vente
					WHERE  Etat = (CASE @Type WHEN 0 THEN  10  WHEN 1 THEN 1  ELSE 1 END)
							AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
				)
			AS derivedtbl_1
			GROUP BY ID_Client) AS StockReel LEFT OUTER JOIN
		   (  
			  SELECT ID_Client, ENCAISSEMENT
			   FROM
			     (
			       SELECT ID_Client ,SUM(Montant) AS ENCAISSEMENT
			     --ENCAISSEMENT
						FROM (
								SELECT       ID_Client, (CASE @Type WHEN 0 THEN  Montant WHEN 1 THEN 0  ELSE  Montant END) AS  Montant
								FROM         dbo.TP_Encaissement 
								WHERE (Etat = 1) AND   (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
								) AS ENCAISSEMENT_2
						GROUP BY ID_Client
			      ) AS ENCAISSEMENT_1
			) AS ENCAISSEMENT ON StockReel.ID_Client  = ENCAISSEMENT.ID_Client LEFT OUTER JOIN
			(  
			  SELECT ID_Client, RETOUR_EMBALLAGE
			   FROM
			     (
			       SELECT ID_Client ,SUM(Montant) AS RETOUR_EMBALLAGE
			     --Retour Emballage
						FROM (
								SELECT       ID_Element ID_Client, (CASE @Type WHEN 0 THEN  0 WHEN 1 THEN Montant ELSE  Montant END) Montant
								FROM         dbo.FN_Retour_Emballage() 
								WHERE (Etat = 1) AND   (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102))  AND Type_Element = 1
								) AS RETOUR_EMBALLAGE_2
						GROUP BY ID_Client
			      ) AS RETOUR_EMBALLAGE_1
			) AS RETOUR_EMBALLAGE ON StockReel.ID_Client  = RETOUR_EMBALLAGE.ID_Client LEFT OUTER JOIN
			(  
			  SELECT ID_Client, VERSEMENT
			   FROM
			     (
			       SELECT ID_Client, SUM(Montant) AS VERSEMENT
			     --VERSEMENT
						FROM (
								SELECT     ID_Client, (CASE @Type WHEN 0 THEN  Montant WHEN 1 THEN Montant_Consigne_Emballage ELSE ( Montant + Montant_Consigne_Emballage) END) Montant
								FROM         dbo.TP_Vente
								WHERE (Etat = 1) AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
								 AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
							 )	AS VERSEMENT_2
						GROUP BY ID_Client
			      ) AS VERSEMENT_1
			) AS VERSEMENT ON StockReel.ID_Client  = VERSEMENT.ID_Client LEFT OUTER JOIN
			(  
			  SELECT ID_Client, REGULER
			   FROM
			     (
			       SELECT ID_Client, SUM(Montant) AS REGULER
			     ---REGULER
						FROM (
								SELECT   ID_Element ID_Client, (CASE @Type WHEN 0 THEN  Montant WHEN 1 THEN 0 ELSE  Montant END) Montant
								FROM     dbo.TP_Regulation_Solde
								WHERE (Etat = 1) AND  (CAST([Date] AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
								AND Type_Element = 1 AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
							 )	AS REGULER_2
						GROUP BY ID_Client
			      ) AS REGULER_1
			) AS REGULER ON StockReel.ID_Client  = REGULER.ID_Client LEFT OUTER JOIN
			(  
			  SELECT ID_Client, CONSIGNE
			   FROM
			     (
			       SELECT ID_Client, SUM(Montant) AS CONSIGNE
			     ---CONSIGNE
						FROM (
								SELECT   ID_Client, (CASE @Type WHEN 2 THEN  Montant_Consigne_Emballage ELSE  0 END) Montant
								FROM     dbo.TP_Vente
								WHERE (Etat = 1) AND  (CAST([Date] AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
							    AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
							 )	AS CONSIGNE_2
						GROUP BY ID_Client
			      ) AS REGULER_1
			) AS CONSIGNE ON StockReel.ID_Client  = CONSIGNE.ID_Client LEFT OUTER JOIN
			(  
			  SELECT ID_Client, VENTE
			   FROM
			     (
			       SELECT ID_Client, SUM(Montant) AS VENTE
			     ---VENTE
						FROM (
								SELECT    ID_Client, (CASE @Type WHEN 0 THEN  Montant_Liquide  WHEN 1 THEN (Montant_Emballage - Montant_Retour_Emballage)  ELSE  Montant_Facture END) AS Montant
								FROM         dbo.FN_Vente() 
								WHERE (Etat = 1) AND  (CAST([Date] AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
								 AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
							 )	AS VENTE_2
						GROUP BY ID_Client
			      ) AS VENTE_1
			) AS VENTE ON StockReel.ID_Client  = VENTE.ID_Client
   ) AS derivedtbl_2               
RETURN ;
END










GO
/****** Object:  UserDefinedFunction [dbo].[FN_Etat_Solde_Fournisseur]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: 16/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Etat_Solde_Fournisseur]
(	
    -- Type = 0 alors solde liquide, Type = 1 alors solde emballage
    -- Type = 2 solde fournisseur 
	@Type int, @Date_Debut datetime, @Date_Fin datetime
)
RETURNS @Tab_Solde TABLE
(
	ID_Fournisseur int NOT NULL,
	Fournisseur nvarchar(100) NOT NULL,
    Initial AS (Solde + Retour_Emballage - Commande - Regulation_Liquide + Reglement),
	Commande  float NOT NULL,
    Reglement float NOT NULL,
    Retour_Emballage float NOT NULL,
    Regulation_Liquide float NOT NULL,
    Solde  float NOT NULL
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (ID_Fournisseur, Fournisseur, Commande, Reglement, Retour_Emballage, Regulation_Liquide, Solde)   
    SELECT 
          ID_Fournisseur, 
		  Fournisseur,
		  Commande,
          Reglement, 
          Retour_Emballage,     
          Regulation_Liquide,      
          Solde 
    FROM		
		(	
		SELECT StockReel.ID_Fournisseur as ID_Fournisseur,
		(SELECT Nom FROM TP_Fournisseur WHERE ID_Fournisseur = StockReel.ID_Fournisseur) AS Fournisseur,
	    StockReel.Montant AS Solde,
		CASE WHEN (DECAISSEMENT.DECAISSEMENT IS NULL) THEN 0 ELSE DECAISSEMENT.DECAISSEMENT END AS Reglement,		
		CASE WHEN (RETOUR_EMBALLAGE.RETOUR_EMBALLAGE  IS NULL) THEN 0 ELSE RETOUR_EMBALLAGE.RETOUR_EMBALLAGE END AS Retour_Emballage,
		CASE WHEN (REGULER_LIQUIDE.REGULER_LIQUIDE IS NULL) THEN 0 ELSE REGULER_LIQUIDE.REGULER_LIQUIDE END AS Regulation_Liquide,
		CASE WHEN (COMMANDE.COMMANDE IS NULL) THEN 0 ELSE COMMANDE.COMMANDE END AS Commande
		FROM (SELECT ID_Fournisseur, SUM(Montant) AS Montant
			FROM (	
			        --Fournisseur  				     
				    SELECT ID_Fournisseur, 0 AS Montant
					FROM dbo.TP_Fournisseur
					WHERE Actif = 1
					
					UNION ALL  
				    --Commande  				     
				    SELECT ID_Fournisseur, Montant_Liquide AS Montant
					FROM dbo.FN_Commande()
					WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
					      AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
					
					UNION ALL		
					--Decaissement
					SELECT ID_Element ID_Fournisseur , - Montant  Montant
					FROM dbo.TP_Decaissement
					WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END)
					      AND Type_Element = 0 AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
					
					UNION ALL
					--Regulation
				    SELECT ID_Element ID_Fournisseur,  Montant
					FROM dbo.TP_Regulation_Solde 
					WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
					AND Type_Element = 0 AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
					
					UNION ALL
					-- Commande Emballage  
				    SELECT ID_Fournisseur, (Montant_Emballage - Montant_Retour_Emballage) AS Montant
					FROM  dbo.FN_Commande()
					WHERE Etat = (CASE @Type WHEN 0 THEN  10  WHEN 1 THEN 1  ELSE  1 END) 
					AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
					
					UNION ALL			
					 --Retour emballage
				    SELECT       ID_Element ID_Fournisseur,  -Montant Montant
					FROM         dbo.FN_Retour_Emballage()
					WHERE Etat = (CASE @Type WHEN 0 THEN  10  WHEN 1 THEN 1  ELSE  1 END) 
					AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) AND Type_Element = 0    
				)
			AS derivedtbl_1 
			GROUP BY ID_Fournisseur) AS StockReel  LEFT OUTER JOIN			
		    (  
			  SELECT ID_Fournisseur, COMMANDE
			   FROM
			     (
			       SELECT ID_Fournisseur ,SUM(Montant) AS COMMANDE
			     --COMMANDE
						FROM (
								SELECT  ID_Fournisseur, (CASE @Type WHEN 0 THEN  Montant_Liquide  WHEN 1 THEN (Montant_Emballage - Montant_Retour_Emballage)  ELSE  (Montant_Liquide + Montant_Emballage - Montant_Retour_Emballage) END) AS Montant
								FROM    dbo.FN_Commande()
								WHERE (Etat = 1) AND   (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
								) AS COMMANDE_2
						GROUP BY ID_Fournisseur
			      ) AS COMMANDE_1
			) AS COMMANDE ON StockReel.ID_Fournisseur  = COMMANDE.ID_Fournisseur LEFT OUTER JOIN
		   (  
			  SELECT ID_Fournisseur, DECAISSEMENT
			   FROM
			     (
			       SELECT ID_Fournisseur ,SUM(Montant) AS DECAISSEMENT
			     --DECAISSEMENT
						FROM (
								SELECT  ID_Element ID_Fournisseur, (CASE @Type WHEN 0 THEN  Montant  WHEN 1 THEN 0  ELSE  (Montant) END)  Montant
								FROM    dbo.TP_Decaissement
								WHERE (Etat = 1) AND   (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
						         AND Type_Element = 0 AND  (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
								) AS DECAISSEMENT_2
						GROUP BY ID_Fournisseur
			      ) AS DECAISSEMENT_1
			) AS DECAISSEMENT ON StockReel.ID_Fournisseur  = DECAISSEMENT.ID_Fournisseur LEFT OUTER JOIN								
			
			(  
			  SELECT ID_Fournisseur, REGULER_LIQUIDE
			   FROM
			     (
			       SELECT ID_Fournisseur, SUM(Montant) AS REGULER_LIQUIDE
			     ---REGULER
						FROM (
								SELECT   ID_Element ID_Fournisseur, (CASE @Type WHEN 0 THEN  Montant  WHEN 1 THEN 0  ELSE  (Montant) END)  Montant
								FROM     dbo.TP_Regulation_Solde
								WHERE (Etat = 1) AND  (CAST([Date] AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
								AND Type_Element = 0 AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
							 )	AS REGULER_LIQUIDE_2
						GROUP BY ID_Fournisseur
			      ) AS REGULER_LIQUIDE_1
			) AS REGULER_LIQUIDE ON StockReel.ID_Fournisseur  = REGULER_LIQUIDE.ID_Fournisseur LEFT OUTER JOIN	
						
			(  
			  SELECT ID_Fournisseur, RETOUR_EMBALLAGE
			   FROM
			     (
			       SELECT ID_Element ID_Fournisseur, SUM(Montant) AS RETOUR_EMBALLAGE
			     ---RETOUR_EMBALLAGE
						FROM (
								SELECT   ID_Element, (CASE @Type WHEN 0 THEN  0  WHEN 1 THEN Montant  ELSE  (Montant) END)  Montant
								FROM     dbo.FN_Retour_Emballage()
								WHERE (Etat = 1) AND  (CAST([Date] AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
								 AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) AND Type_Element = 0
							 )	AS RETOUR_EMBALLAGE_2
						GROUP BY ID_Element
			      ) AS RETOUR_EMBALLAGE_1
			) AS RETOUR_EMBALLAGE ON StockReel.ID_Fournisseur  = RETOUR_EMBALLAGE.ID_Fournisseur		
   ) AS derivedtbl_2               
RETURN ;
END












GO
/****** Object:  UserDefinedFunction [dbo].[FN_Etat_Solde_Ristourne_Client]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Etat_Solde_Ristourne_Client]
(	
	@Date_Debut datetime, @Date_Fin datetime
)
RETURNS @Tab_Solde TABLE
(
	ID_Client int NOT NULL,
	Client nvarchar(100) NOT NULL,
    Initial  as (Solde - Vente + Reglement),
    Vente float NOT NULL,   
    Reglement float NOT NULL,
    Solde float NOT NULL 
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (ID_Client, Client, Vente, Reglement, Solde)   
    SELECT ID_Client, Client, Vente, Reglement, Solde  
    FROM		
		(	
		SELECT TAB_Client.ID_Client as ID_Client, TAB_Client.Nom as Client, 	    
	    CASE WHEN (StockReel.Montant IS NULL) THEN 0 ELSE StockReel.Montant END AS Solde,
		CASE WHEN (DECAISSEMENT.DECAISSEMENT IS NULL) THEN 0 ELSE DECAISSEMENT.DECAISSEMENT END AS Reglement,
		CASE WHEN (VENTE.VENTE IS NULL) THEN 0 ELSE VENTE.VENTE END AS Vente
		FROM (select ID_Client, Nom  FROM TP_Client ) as TAB_Client LEFT OUTER JOIN
		(SELECT ID_Client, SUM(Montant) AS Montant
			FROM (				
					--Vente
					SELECT        ID_Client, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, Ristourne_Client , 0, 0) Montant
					FROM     dbo.TP_Vente Vente INNER JOIN TP_Vente_Liquide Vente_Detail
					ON Vente.ID_Vente = Vente_Detail.ID_Vente
					WHERE (Etat = 1) AND Cumuler_Ristourne = 1
					       AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
					
					 UNION ALL
					 --Reglement
				    SELECT      ID_Element AS ID_Client, - Montant Montant
					FROM         dbo.TP_Decaissement 
					WHERE (Etat = 1) AND Type_Element = 1 AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
		
				)
			AS derivedtbl_1
			GROUP BY ID_Client) AS StockReel on TAB_Client.ID_Client = StockReel.ID_Client LEFT OUTER JOIN		
			(  
			  SELECT ID_Client, VENTE
			   FROM
			     (
			       SELECT ID_Client ,SUM(Montant) AS VENTE
			     --VENTE
						FROM (
								SELECT       ID_Client, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, Ristourne_Client , 0, 0) Montant
								FROM         dbo.TP_Vente Vente INNER JOIN TP_Vente_Liquide Vente_Detail
				             	ON Vente.ID_Vente = Vente_Detail.ID_Vente 
								WHERE (Etat = 1) AND Cumuler_Ristourne = 1 AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102)) 
								) AS VENTE_2
						GROUP BY ID_Client
			      ) AS VENTE_1
			) AS VENTE ON StockReel.ID_Client  = VENTE.ID_Client LEFT OUTER JOIN
			(  
			  SELECT ID_Client, DECAISSEMENT
			   FROM
			     (
			       SELECT ID_Client ,SUM(Montant) AS DECAISSEMENT
			     --DECAISSEMENT
						FROM (
								SELECT       ID_Element AS ID_Client,  Montant
								FROM         dbo.TP_Decaissement
								WHERE (Etat = 1) AND Type_Element = 1 AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATETIME) >= CONVERT(DATETIME, @Date_Debut, 102))
								) AS DEClientMENT_2
						GROUP BY ID_Client
			      ) AS DECAISSEMENT_1
			) AS DECAISSEMENT ON StockReel.ID_Client  = DECAISSEMENT.ID_Client
			
   ) AS derivedtbl_2               
RETURN ;
END








GO
/****** Object:  UserDefinedFunction [dbo].[FN_Etat_Stock]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: 11/13/2017
-- Description:	Renvoi l'état du stock
-- =============================================
CREATE FUNCTION [dbo].[FN_Etat_Stock]
(	
    -- Type = 0 liquide, 1 Emballage
	@Type int, @Date_Debut datetime, @Date_Fin datetime
)
RETURNS @Tab_Solde TABLE
(
	ID_Element int NOT NULL,
	Libelle nvarchar(150) NOT NULL,
	Code nvarchar(50) NOT NULL,
	Stock_Casier int  NOT NULL,
	Stock_Plastique int NOT NULL,	   
	Stock_Bouteille int NOT NULL,
	Stock_Casier_Temp int NOT NULL,
	Stock_Plastique_Temp int NOT NULL,	   
	Stock_Bouteille_Temp int NOT NULL,
	PU_Bouteille_Vente int NOT NULL,
	PU_Bouteille_Achat int NOT NULL,	   
	PU_Plastique int NOT NULL,
	Conditionnement int  NULL,	
	Valeur_Final_Vente as  (Stock_Casier * PU_Bouteille_Vente + (Stock_Bouteille * (PU_Bouteille_Vente / Conditionnement))),
	Valeur_Final_Achat as  (Stock_Casier * PU_Bouteille_Achat + (Stock_Bouteille * (PU_Bouteille_Achat / Conditionnement))),
	Valeur_Final_Emballage as  (Stock_Casier * (PU_Plastique + PU_Bouteille_Achat * Conditionnement) + Stock_Plastique * PU_Plastique + Stock_Bouteille * PU_Bouteille_Achat)
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (
		ID_Element, 
		Libelle,
		Code,	
		Stock_Casier, 
		Stock_Plastique, 
		Stock_Bouteille,
		Stock_Casier_Temp, 
		Stock_Plastique_Temp, 
		Stock_Bouteille_Temp,
		Conditionnement,
		PU_Bouteille_Vente,
		PU_Bouteille_Achat,
		PU_Plastique
	)   
    SELECT ID_Element, 
		   Libelle,
		   Code,
		   Stock_Casier,
		   Stock_Plastique, 
		   Stock_Bouteille,
		   Stock_Casier_Temp, 
	       Stock_Plastique_Temp, 
		   Stock_Bouteille_Temp,
		   Conditionnement,
		   PU_Bouteille_Vente,
		   PU_Bouteille_Achat,
		   PU_Plastique  
    FROM		
	(		
		SELECT StockReel.ID_Element as ID_Element, 
		StockReel.Stock_Casier as Stock_Casier, 
		StockReel.Stock_Plastique as Stock_Plastique, 
		StockReel.Stock_Bouteille as Stock_Bouteille,

		StockReel.Stock_Casier as Stock_Casier_Temp, 
		StockReel.Stock_Plastique as Stock_Plastique_Temp, 
		StockReel.Stock_Bouteille as Stock_Bouteille_Temp,
			
		 (CASE WHEN (@Type = 0) THEN (SELECT Code FROM TP_Produit WHERE ID_Produit = StockReel.ID_Element) 
	     ELSE (SELECT Code FROM TP_Emballage WHERE ID_Emballage = StockReel.ID_Element) END) AS Code,

	     (CASE WHEN (@Type = 0) THEN (SELECT Libelle FROM TP_Produit WHERE ID_Produit = StockReel.ID_Element) 
	     ELSE (SELECT Description FROM TP_Emballage WHERE ID_Emballage = StockReel.ID_Element) END) AS Libelle,

		 (CASE WHEN (@Type = 0) THEN (SELECT Emballage.Conditionnement FROM TP_Produit AS Produit INNER JOIN TP_Emballage Emballage 
													                   ON Produit.ID_Emballage = Emballage.ID_Emballage 
																	   WHERE ID_Produit = StockReel.ID_Element) 
	     ELSE (SELECT Conditionnement FROM TP_Emballage WHERE ID_Emballage = StockReel.ID_Element) END) AS Conditionnement,

		 (CASE WHEN (@Type = 0) THEN (SELECT PU_Vente FROM TP_Produit AS Produit WHERE ID_Produit = StockReel.ID_Element) 
	     ELSE (SELECT PU_Bouteille  FROM TP_Emballage WHERE ID_Emballage = StockReel.ID_Element) END) AS PU_Bouteille_Vente,

		  (CASE WHEN (@Type = 0) THEN (SELECT PU_Achat FROM TP_Produit AS Produit WHERE ID_Produit = StockReel.ID_Element) 
	     ELSE (SELECT PU_Bouteille  FROM TP_Emballage WHERE ID_Emballage = StockReel.ID_Element) END) AS PU_Bouteille_Achat,

		  (CASE WHEN (@Type = 0) THEN (SELECT 0 FROM TP_Produit AS Produit WHERE ID_Produit = StockReel.ID_Element) 
	     ELSE (SELECT PU_Plastique  FROM TP_Emballage WHERE ID_Emballage = StockReel.ID_Element) END) AS PU_Plastique

	    FROM (SELECT ID_Element, SUM(Stock_Casier) AS Stock_Casier, SUM(Stock_Plastique) AS Stock_Plastique, SUM(Stock_Bouteille) AS Stock_Bouteille
			FROM (	
			        --Liquide  				     
				    SELECT ID_Produit AS ID_Element, 0 AS Stock_Casier, 0 AS Stock_Plastique, 0 AS Stock_Bouteille
					FROM dbo.TP_Produit
					WHERE Actif = (CASE @Type WHEN 0 THEN  1  ELSE NULL END)
					
					UNION ALL  				
					--Emballage  				     
				    SELECT ID_Emballage AS ID_Element, 0 AS Stock_Casier, 0 AS Stock_Plastique, 0 AS Stock_Bouteille
					FROM dbo.TP_Emballage
					WHERE Actif = (CASE @Type WHEN 0 THEN  NULL  ELSE 1 END) AND ID_Emballage <> 5
					
					UNION ALL  
				    -- Commande emballage  				     
					SELECT ID_Element, -Quantite_Casier AS Stock_Casier, -Quantite_Plastique AS Stock_Plastique, -Quantite_Bouteille AS Stock_Bouteille
					FROM dbo.TP_Commande AS Commande INNER JOIN (SELECT Emballage.ID_Commande AS ID_Commande, Emballage.ID_Emballage ID_Element, sum(Quantite_Casier) Quantite_Casier , sum(Quantite_Plastique) Quantite_Plastique, sum(Quantite_Bouteille) Quantite_Bouteille
																   FROM (
																				SELECT ID_Commande, ID_Emballage, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
																				FROM TP_Commande_Emballage
																				GROUP BY ID_Emballage, ID_Commande
																			   UNION ALL
																				SELECT ID_Commande, Produit.ID_Emballage, -SUM(Quantite_Casier_Emballage) Quantite_Casier, -SUM(Quantite_Plastique_Emballage) Quantite_Plastique, -SUM(Quantite_Bouteille_Emballage) Quantite_Bouteille
																				FROM TP_Commande_Liquide Commande_Liquide INNER JOIN TP_Produit Produit
																				ON Commande_Liquide.ID_Produit = Produit.ID_Produit
																				GROUP BY Produit.ID_Emballage, ID_Commande
																		) AS Emballage GROUP BY Emballage.ID_Emballage, Emballage.ID_Commande) AS Detail
					ON Commande.ID_Commande = Detail.ID_Commande
					WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END)
					      AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
					
					UNION ALL  
				    -- Commande liquide  				     
                    SELECT Detail.ID_Produit AS ID_Element, Quantite_Casier_Liquide AS Quantite_Casier, 0 AS Stock_Plastique, Quantite_Bouteille_Liquide AS Stock_Bouteille
					FROM dbo.TP_Commande AS Commande INNER JOIN (SELECT ID_Commande, ID_Produit, SUM(Quantite_Casier_Liquide) Quantite_Casier_Liquide, SUM(Quantite_Bouteille_Liquide) Quantite_Bouteille_Liquide
					                                             FROM TP_Commande_Liquide 
					                                             GROUP BY ID_Commande, ID_Produit) AS Detail
					ON Commande.ID_Commande = Detail.ID_Commande
					WHERE Etat = (CASE @Type WHEN 0 THEN 1 ELSE 10 END)
					      AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
					
					UNION ALL  
				    -- Vente amballage  				     
				    SELECT Detail.ID_Element AS ID_Element,  Quantite_Casier AS Stock_Casier,  Quantite_Plastique AS Stock_Plastique,  Quantite_Bouteille AS Stock_Bouteille
					FROM dbo.TP_Vente AS Vente INNER JOIN (SELECT Emballage.ID_Vente AS ID_Vente, Emballage.ID_Emballage ID_Element, sum(Quantite_Casier) Quantite_Casier , sum(Quantite_Plastique) Quantite_Plastique, sum(Quantite_Bouteille) Quantite_Bouteille
															FROM (
																		SELECT ID_Vente, ID_Emballage, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
																		FROM TP_Vente_Emballage
																		GROUP BY ID_Emballage, ID_Vente
																	UNION ALL
																		SELECT ID_Vente, Produit.ID_Emballage, -SUM(Quantite_Casier_Emballage) Quantite_Casier, -SUM(Quantite_Plastique_Emballage) Quantite_Plastique, -SUM(Quantite_Bouteille_Emballage) Quantite_Bouteille
																		FROM TP_Vente_Liquide Vente_Liquide INNER JOIN TP_Produit Produit
																		ON Vente_Liquide.ID_Produit = Produit.ID_Produit where Vente_Liquide.Produit_Decomposer = 0
																		GROUP BY Produit.ID_Emballage, ID_Vente

																	UNION ALL
																		SELECT ID_Vente, Produit.ID_Plastique_Base as ID_Emballage, 0 Quantite_Casier, -SUM(Quantite_Casier_Emballage+Quantite_Plastique_Emballage) Quantite_Plastique, 0 Quantite_Bouteille
																		FROM TP_Vente_Liquide Vente_Liquide INNER JOIN TP_Produit Produit
																		ON Vente_Liquide.ID_Produit = Produit.ID_Produit where Vente_Liquide.Produit_Decomposer = 1 
																		GROUP BY Produit.ID_Plastique_Base, ID_Vente
																	UNION ALL
																		SELECT ID_Vente, Produit.ID_Bouteille_Base as ID_Emballage, 0 Quantite_Casier, 0 Quantite_Plastique, -SUM(Quantite_Bouteille_Emballage+Quantite_Casier_Emballage*Conditionnement) Quantite_Bouteille
																		FROM TP_Vente_Liquide Vente_Liquide INNER JOIN TP_Produit Produit
																		ON Vente_Liquide.ID_Produit = Produit.ID_Produit where Vente_Liquide.Produit_Decomposer = 1 
																		GROUP BY Produit.ID_Bouteille_Base, ID_Vente
																) AS Emballage GROUP BY Emballage.ID_Emballage, Emballage.ID_Vente
															  ) AS Detail
					ON Vente.ID_Vente = Detail.ID_Vente
					WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END)
					      AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
					
					UNION ALL  
				    -- Vente liquide  				     
                    SELECT Detail.ID_Produit AS ID_Element, - Quantite_Casier_Liquide AS Quantite_Casier, 0 AS Stock_Plastique, - Quantite_Bouteille_Liquide AS Stock_Bouteille
					FROM dbo.TP_Vente AS Vente INNER JOIN (SELECT ID_Vente, ID_Produit, SUM(Quantite_Casier_Liquide) Quantite_Casier_Liquide, SUM(Quantite_Bouteille_Liquide) Quantite_Bouteille_Liquide
					                                             FROM TP_Vente_Liquide 
																 WHERE Produit_Decomposer = 0
					                                             GROUP BY ID_Vente, ID_Produit 
																 UNION ALL
																 SELECT ID_Vente, produit.ID_Produit_Base ID_Produit, 0 Quantite_Casier_Liquide, SUM(Quantite_Bouteille_Liquide+Quantite_Casier_Liquide*Conditionnement) Quantite_Bouteille_Liquide
																 FROM TP_Vente_Liquide vente_produit inner join TP_Produit produit on vente_produit.ID_Produit = produit.ID_Produit
																 WHERE vente_produit.Produit_Decomposer = 1
					                                             GROUP BY ID_Vente, ID_Produit_Base
																 ) AS Detail
					ON Vente.ID_Vente = Detail.ID_Vente
					WHERE Etat = (CASE @Type WHEN 0 THEN 1 ELSE 10 END)
					      AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
					      		        
					UNION ALL
					--Regulation emballage
				    SELECT ID_Element AS ID_Element,  Quantite_Casier, Quantite_Plastique, Quantite_Bouteille
					FROM dbo.TP_Regulation_Stock 
					WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END) 
					AND Type_Element = 1 AND (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) 
					
					UNION ALL
					-- Regularisation Liquide  
				    SELECT ID_Element,  Quantite_Casier, 0 AS Quantite_Plastique, Quantite_Bouteille
					FROM  dbo.TP_Regulation_Stock
					WHERE Etat = (CASE @Type WHEN 0 THEN 1 ELSE 10 END)  
					AND Type_Element = 0 AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
					
					UNION ALL			
					 --Retour emballage client
				    SELECT Detail.ID_Emballage AS ID_Element, Quantite_Casier AS Stock_Casier, Quantite_Plastique AS Stock_Plastique, Quantite_Bouteille AS Stock_Bouteille
					FROM dbo.TP_Retour_Emballage AS Retour INNER JOIN (SELECT ID_Retour_Emballage, ID_Emballage, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
					                                             FROM TP_Retour_Emballage_Detail
					                                             GROUP BY ID_Retour_Emballage, ID_Emballage) AS Detail
					ON Retour.ID_Retour_Emballage = Detail.ID_Retour_Emballage
					WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END)
					      AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) AND Type_Element = 1 
					      
					UNION ALL			
					 --Retour emballage fournisseur
				    SELECT Detail.ID_Emballage AS ID_Element, - Quantite_Casier AS Stock_Casier, - Quantite_Plastique AS Stock_Plastique, - Quantite_Bouteille AS Stock_Bouteille
					FROM dbo.TP_Retour_Emballage AS Retour INNER JOIN (SELECT ID_Retour_Emballage, ID_Emballage, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
					                                             FROM TP_Retour_Emballage_Detail
					                                             GROUP BY ID_Retour_Emballage, ID_Emballage) AS Detail
					ON Retour.ID_Retour_Emballage = Detail.ID_Retour_Emballage
					WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END)
					      AND  (CAST([Date]  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102)) AND Type_Element = 0

				   UNION ALL			
					 -- Inventaire Liquide
				    SELECT Detail.ID_Element AS ID_Element, Quantite_Casier AS Stock_Casier,  Quantite_Plastique AS Stock_Plastique,  Quantite_Bouteille AS Stock_Bouteille
					FROM dbo.TP_Inventaire AS Inventaire INNER JOIN (SELECT ID_Inventaire, ID_Element, SUM(Quantite_Casier_Reel - Quantite_Casier_Theorique) Quantite_Casier, 
					                                                  0 Quantite_Plastique, SUM(Quantite_Bouteille_Reel - Quantite_Bouteille_Theorique) Quantite_Bouteille
					                                             FROM TP_Inventaire_Detail
					                                             GROUP BY ID_Inventaire, ID_Element) AS Detail
					ON Inventaire.ID_Inventaire = Detail.ID_Inventaire
					WHERE  Type = 0 AND Etat = (CASE @Type WHEN 0 THEN 2 ELSE 10 END) 
					      AND  (CAST(Edit_Date  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))

					UNION ALL			
					 -- Inventaire Emballage
				    SELECT Detail.ID_Element AS ID_Element,  Quantite_Casier AS Stock_Casier,  Quantite_Plastique AS Stock_Plastique,  Quantite_Bouteille AS Stock_Bouteille
					FROM dbo.TP_Inventaire AS Inventaire INNER JOIN (SELECT ID_Inventaire, ID_Element, SUM(Quantite_Casier_Reel - Quantite_Casier_Theorique) Quantite_Casier, 
					                                                  SUM(Quantite_Plastique_Reel - Quantite_Plastique_Theorique) Quantite_Plastique, 
																	  SUM(Quantite_Bouteille_Reel - Quantite_Bouteille_Theorique) Quantite_Bouteille
					                                             FROM TP_Inventaire_Detail
					                                             GROUP BY ID_Inventaire, ID_Element) AS Detail
					ON Inventaire.ID_Inventaire = Detail.ID_Inventaire
					WHERE  Type = 1 AND Etat = (CASE @Type WHEN 0 THEN 10 ELSE 2 END) 
					      AND  (CAST(Edit_Date  AS DATETIME) <= CONVERT(DATETIME, @Date_Fin, 102))
				)
			AS derivedtbl_1 
			GROUP BY ID_Element) AS StockReel 		
   ) AS derivedtbl_2                
   
   DECLARE @ID AS INT, @Casier AS INT, @Plastique AS INT, @Bouteille AS INT, @Contenance AS INT, @C AS INT, @Nombre_Total_Plastique AS INT, @NombrePlastique_Restant AS INT, @Nombre_Total_Bouteil AS INT, @NombreCasierPlein AS INT, @NombreBouteil_Restant AS INT

   IF(@Type = 0)
   BEGIN
       DECLARE Stock_Cursor CURSOR FOR
	   SELECT ID_Element, Stock_Casier_Temp, Stock_Bouteille_Temp, Conditionnement FROM @Tab_Solde;
	   OPEN Stock_Cursor;
	   FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
		WHILE @@FETCH_STATUS = 0
		   BEGIN
			SET @Nombre_Total_Bouteil = @Casier * @Contenance + @Bouteille;
			SET @NombreCasierPlein = @Nombre_Total_Bouteil / @Contenance;
			SET @NombreBouteil_Restant = @Nombre_Total_Bouteil % @Contenance;
			update @Tab_Solde set Stock_Casier = @NombreCasierPlein, Stock_Bouteille = @NombreBouteil_Restant   where ID_Element = @ID;		
			FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
		   END;
		CLOSE Stock_Cursor;
		DEALLOCATE Stock_Cursor;   
   END
   ELSE
   BEGIN
     DECLARE Stock_Cursor CURSOR FOR
	   SELECT ID_Element, Stock_Casier_Temp, Stock_Plastique_Temp, Stock_Bouteille_Temp, Conditionnement FROM @Tab_Solde;
	   OPEN Stock_Cursor;
	   FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Plastique, @Bouteille, @Contenance;
		WHILE @@FETCH_STATUS = 0
		   BEGIN
			SET @Nombre_Total_Bouteil = @Casier * @Contenance + @Bouteille;
			SET @Nombre_Total_Plastique = @Casier + @Plastique;
			SET @NombreCasierPlein = @Nombre_Total_Bouteil / @Contenance;
			SET @NombrePlastique_Restant = @Nombre_Total_Plastique - @NombreCasierPlein;
			IF(@NombreCasierPlein > @Nombre_Total_Plastique)
			BEGIN
				SET @NombreCasierPlein = @Nombre_Total_Plastique
				SET @NombrePlastique_Restant = 0;
			END				
			SET @NombreBouteil_Restant = @Nombre_Total_Bouteil % @Contenance;
			update @Tab_Solde SET Stock_Casier = @NombreCasierPlein, Stock_Plastique = @NombrePlastique_Restant, Stock_Bouteille = @NombreBouteil_Restant   where ID_Element = @ID;		
			FETCH NEXT FROM Stock_Cursor into @ID, @Casier,@Plastique, @Bouteille, @Contenance;
		   END;
		CLOSE Stock_Cursor;
		DEALLOCATE Stock_Cursor; 
   END           
RETURN ;
END












GO
/****** Object:  UserDefinedFunction [dbo].[FN_Historique_Solde_Caisse]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FN_Historique_Solde_Caisse](@ID_Caisse int)
RETURNS @HistoriqueStock TABLE 
(
    [Type] nvarchar(10) NOT NULL, 
    [Date] datetime NOT NULL, 
    Mouvement nvarchar(50) NOT NULL, 
    Numero nvarchar(50)  NULL,
    Rang int NOT NULL ,
    Quantite float NOT NULL,
    QT_Livrer float NOT NULL,
    Stock_Porgressif float NOT NULL
)
AS
BEGIN
	insert into @HistoriqueStock

select [Type], [Date],Mouvement,Num_Bon,Rang, Quantite, QT_Livrer,0
from(
						SELECT   Quantite, QT_Livrer, Date, Num_Bon, Mouvement, Type, ROW_NUMBER() OVER (ORDER BY Date) AS Rang
						FROM         (
									--- Credite la caisse 
										SELECT     ID_Caissse ID_Caissse, Montant AS Quantite, Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Encaissement de '+TP_Client.Nom AS Mouvement ,CONVERT(nvarchar, ID_Encaissement)+';0' AS Type
										FROM         dbo.TP_Encaissement 
										INNER JOIN TP_Client ON TP_Client.ID_Client = TP_Encaissement.ID_Client
										WHERE (Etat = 1)  AND (ID_Caissse = @ID_Caisse)
										
										UNION ALL 
										
											--- Credite le compte 
										SELECT     ID_Caisse,- Montant AS Quantite, -Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Décaissement à '+ Fournisseur.Nom AS Mouvement ,CONVERT(nvarchar, ID_Decaissement)+';1' AS Type
										FROM         dbo.TP_Decaissement Decaissement INNER JOIN TP_Fournisseur Fournisseur 
										ON Decaissement.ID_Element = Fournisseur.ID_Fournisseur
										WHERE (Etat = 1) AND Type_Element = 0 AND  (ID_Caisse = @ID_Caisse)

										UNION ALL 
											--- Ristourne 
										SELECT     ID_Caisse,- Montant AS Quantite, -Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Ristourne à '+ Client.Nom AS Mouvement ,CONVERT(nvarchar, ID_Decaissement)+';1' AS Type
										FROM         dbo.TP_Decaissement Decaissement INNER JOIN TP_Client Client 
										ON Decaissement.ID_Element = Client.ID_Client
										WHERE (Etat = 1) AND Type_Element = 1 AND  (ID_Caisse = @ID_Caisse)
										
										UNION ALL 
										
											--- Regulation 
										SELECT    ID_Element ID_Caisse, Montant AS Quantite, Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Régularisation' AS Mouvement ,CONVERT(nvarchar, ID_Regulation_Solde)+';2' AS Type
										FROM         dbo.TP_Regulation_Solde 
										WHERE (Etat = 1) AND Type_Element = 2 AND (ID_Element = @ID_Caisse)
									
										UNION ALL 										
											--- Depense 
										SELECT     ID_Caisse, -Montant AS Quantite, -Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Dépense' AS Mouvement ,CONVERT(nvarchar, ID_Decaissement)+';1' AS Type
										FROM         dbo.TP_Decaissement 
										WHERE (Etat = 1) AND Type_Element = 2 AND (ID_Caisse = @ID_Caisse)
										
										UNION ALL 										
											--- Reglement 
										SELECT     ID_Caisse, (Montant_Verser + Montant_Consigne_Emballage) AS Quantite, (Montant_Verser + Montant_Consigne_Emballage) AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Versement de '+ Client.Nom AS Mouvement ,CONVERT(nvarchar, ID_Vente)+';3' AS Type
										FROM          dbo.FN_Vente() Vente INNER JOIN TP_Client Client 
										ON Vente.ID_Client = Client.ID_Client
										WHERE (Etat = 1) AND (ID_Caisse = @ID_Caisse)
										
					
	) AS derivedtbl_1) As EE order by Rang 


declare @Rang as int , @QT_Livrer as float ,@ID as Nvarchar(10), @Stock_Porgressif as float
                                             
DECLARE Historique_Cursor CURSOR FOR
SELECT [Type], Rang, QT_Livrer FROM @HistoriqueStock;

OPEN Historique_Cursor;
FETCH NEXT FROM Historique_Cursor into @ID,@Rang,@QT_Livrer;
WHILE @@FETCH_STATUS = 0
   BEGIN
	if (@Rang = 1)
		update @HistoriqueStock set Stock_Porgressif = QT_Livrer  where Rang = @Rang ;
	else
	begin
		select @Stock_Porgressif = Stock_Porgressif from @HistoriqueStock where Rang = @Rang - 1;
		update @HistoriqueStock set Stock_Porgressif = QT_Livrer + @Stock_Porgressif where Rang = @Rang ;
	end
	FETCH NEXT FROM Historique_Cursor into @ID,@Rang,@QT_Livrer;
   END;
CLOSE Historique_Cursor;
DEALLOCATE Historique_Cursor;



	RETURN;
END








GO
/****** Object:  UserDefinedFunction [dbo].[FN_Historique_Solde_Client]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FN_Historique_Solde_Client](@ID_Client int, @Type int)
RETURNS @HistoriqueStock TABLE 
(
    [Type] nvarchar(10) NOT NULL, 
    [Date] datetime NOT NULL, 
    Mouvement nvarchar(50) NOT NULL, 
    Numero nvarchar(50)  NULL,
    Rang int NOT NULL ,
    Quantite float NOT NULL,
    QT_Livrer float NOT NULL,
    Stock_Porgressif float NOT NULL
)
AS
BEGIN
	insert into @HistoriqueStock

select [Type], [Date],Mouvement,Num_Bon,Rang, Quantite, QT_Livrer,0
from(
						SELECT   Quantite, QT_Livrer, Date, Num_Bon, Mouvement, Type, ROW_NUMBER() OVER (ORDER BY Date) AS Rang
						FROM         (
									--- Credite la caisse 
										SELECT     TP_Encaissement.ID_Client ID_Client, -Montant AS Quantite, -Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Encaissement de '+TP_Client.Nom AS Mouvement ,CONVERT(nvarchar, ID_Encaissement)+';0' AS Type
										FROM         dbo.TP_Encaissement 
										INNER JOIN TP_Client ON TP_Client.ID_Client = TP_Encaissement.ID_Client
										WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
										        AND Montant <> 0 AND (TP_Encaissement.ID_Client = @ID_Client)
										
										UNION ALL 
										-- Regulation 
										SELECT    ID_Element ID_Client, Montant AS Quantite, Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Régularisation' AS Mouvement ,CONVERT(nvarchar, ID_Regulation_Solde)+';1' AS Type
										FROM         dbo.TP_Regulation_Solde 
										WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
										       AND Montant <> 0 AND Type_Element = 1 AND (ID_Element = @ID_Client)
									
										UNION ALL 										
											--- versement 
										SELECT     ID_Client, -Montant AS Quantite, -Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Versement' AS Mouvement ,CONVERT(nvarchar, ID_Vente)+';2' AS Type
										FROM         dbo.TP_Vente 
										WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
										    AND Montant <> 0 AND (ID_Client = @ID_Client)
										
										UNION ALL 										
											--- Vente 
										SELECT     ID_Client, Montant_Liquide AS Quantite, Montant_Liquide AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Vente' AS Mouvement ,CONVERT(nvarchar, ID_Vente)+';2' AS Type
										FROM          dbo.FN_Vente()
										WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
										     AND Montant_Liquide <> 0  AND (ID_Client = @ID_Client)

									    UNION ALL 										
											--- Retour emballage 
										SELECT     ID_Element ID_Client, -Montant AS Quantite, -Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Retour Emballage Client' AS Mouvement ,CONVERT(nvarchar, ID_Retour_Emballage)+';3' AS Type
										FROM          dbo.FN_Retour_Emballage()
										WHERE  Etat = (CASE @Type WHEN 0 THEN  10  WHEN 1 THEN 1  ELSE  1 END)
										      AND Montant <> 0 AND (ID_Element = @ID_Client) AND Type_Element = 1 

									    UNION ALL 										
									      --- Vente Emballage
										SELECT     ID_Client, (Montant_Emballage - Montant_Retour_Emballage) AS Quantite, (Montant_Emballage - Montant_Retour_Emballage) AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Vente Emballage' AS Mouvement ,CONVERT(nvarchar, ID_Vente)+';2' AS Type
										FROM          dbo.FN_Vente()
										WHERE  Etat = (CASE @Type WHEN 0 THEN  10  WHEN 1 THEN 1  ELSE  1 END)
										 AND (Montant_Emballage - Montant_Retour_Emballage) <> 0  AND (ID_Client = @ID_Client)

										UNION ALL 										
									      --- Vente Emballage
										SELECT     ID_Client, -Montant_Consigne_Emballage AS Quantite, -Montant_Consigne_Emballage AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Consigne Emballage' AS Mouvement ,CONVERT(nvarchar, ID_Vente)+';2' AS Type
										FROM          dbo.TP_Vente
										WHERE  Etat = (CASE @Type WHEN 0 THEN  10  WHEN 1 THEN 1  ELSE  1 END)
										     AND Montant_Consigne_Emballage <> 0 AND (ID_Client = @ID_Client)			
	) AS derivedtbl_1) As EE order by Rang 


declare @Rang as int , @QT_Livrer as float ,@ID as Nvarchar(10), @Stock_Porgressif as float
                                             
DECLARE Historique_Cursor CURSOR FOR
SELECT [Type], Rang, QT_Livrer FROM @HistoriqueStock;

OPEN Historique_Cursor;
FETCH NEXT FROM Historique_Cursor into @ID,@Rang,@QT_Livrer;
WHILE @@FETCH_STATUS = 0
   BEGIN
	if (@Rang = 1)
		update @HistoriqueStock set Stock_Porgressif = QT_Livrer  where Rang = @Rang ;
	else
	begin
		select @Stock_Porgressif = Stock_Porgressif from @HistoriqueStock where Rang = @Rang - 1;
		update @HistoriqueStock set Stock_Porgressif = QT_Livrer + @Stock_Porgressif where Rang = @Rang ;
	end
	FETCH NEXT FROM Historique_Cursor into @ID,@Rang,@QT_Livrer;
   END;
CLOSE Historique_Cursor;
DEALLOCATE Historique_Cursor;



	RETURN;
END





GO
/****** Object:  UserDefinedFunction [dbo].[FN_Historique_Solde_Fournisseur]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FN_Historique_Solde_Fournisseur](@ID_Fournisseur int, @Type int)
RETURNS @HistoriqueStock TABLE 
(
    [Type] nvarchar(10) NOT NULL, 
    [Date] datetime NOT NULL, 
    Mouvement nvarchar(50) NOT NULL, 
    Numero nvarchar(50)  NULL,
    Rang int NOT NULL ,
    Quantite float NOT NULL,
    QT_Livrer float NOT NULL,
    Stock_Porgressif float NOT NULL
)
AS
BEGIN
	insert into @HistoriqueStock

select [Type], [Date],Mouvement,Num_Bon,Rang, Quantite, QT_Livrer,0
from(
						SELECT   Quantite, QT_Livrer, Date, Num_Bon, Mouvement, Type, ROW_NUMBER() OVER (ORDER BY Date) AS Rang
						FROM         (
									--- Commande 
										SELECT    a.ID_Fournisseur AS ID_Fournisseur, + Montant_Liquide AS Quantite, + Montant_Liquide AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Commande chez '+b.Nom AS Mouvement ,CONVERT(nvarchar, ID_Commande)+';0' AS Type
										FROM         dbo.FN_Commande() as a
										INNER JOIN TP_Fournisseur AS b ON a.ID_Fournisseur = b.ID_Fournisseur
										WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
										        AND (a.ID_Fournisseur = @ID_Fournisseur) AND Montant_Liquide <> 0
										
										UNION ALL 										
											--- Decaissement 
										SELECT     ID_Element ID_Fournisseur, - Montant AS Quantite, - Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Decaissement' AS Mouvement ,CONVERT(nvarchar, ID_Decaissement)+';1' AS Type
										FROM         dbo.TP_Decaissement 
										WHERE  Type_Element = 0 AND Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
										    AND (ID_Element = @ID_Fournisseur) AND Montant <> 0
										
										UNION ALL 										
											--- Regularisation 
										SELECT    ID_Element ID_Fournisseur, Montant AS Quantite, Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Regularisation' AS Mouvement ,CONVERT(nvarchar, ID_Regulation_Solde)+';2' AS Type
										FROM          dbo.TP_Regulation_Solde
										WHERE Etat = (CASE @Type WHEN 0 THEN  1  WHEN 1 THEN  10  ELSE  1 END) 
										     AND Type_Element = 0 AND (ID_Element = @ID_Fournisseur) AND Montant <> 0

									    UNION ALL 										
											--- Retour emballage 
										SELECT     ID_Element ID_Fournisseur, - Montant AS Quantite, - Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Retour Emballage ' AS Mouvement ,CONVERT(nvarchar, ID_Retour_Emballage)+';3' AS Type
										FROM          dbo.FN_Retour_Emballage()
										WHERE  Etat = (CASE @Type WHEN 0 THEN  10  WHEN 1 THEN 1  ELSE  1 END)
										      AND (ID_Element = @ID_Fournisseur) AND Type_Element = 0  AND Montant <> 0

									    UNION ALL 										
									      --- Commande Emballage
										SELECT     ID_Fournisseur, (Montant_Emballage - Montant_Retour_Emballage) AS Quantite, (Montant_Emballage - Montant_Retour_Emballage) AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Commande Emballage' AS Mouvement ,CONVERT(nvarchar, ID_Commande)+';0' AS Type
										FROM          dbo.FN_Commande()
										WHERE  Etat = (CASE @Type WHEN 0 THEN  10  WHEN 1 THEN 1  ELSE  1 END)
										      AND (ID_Fournisseur = @ID_Fournisseur) AND (Montant_Emballage - Montant_Retour_Emballage) <> 0		
	) AS derivedtbl_1) As EE order by Rang 


declare @Rang as int , @QT_Livrer as float ,@ID as Nvarchar(10), @Stock_Porgressif as float
                                             
DECLARE Historique_Cursor CURSOR FOR
SELECT [Type], Rang, QT_Livrer FROM @HistoriqueStock;

OPEN Historique_Cursor;
FETCH NEXT FROM Historique_Cursor into @ID,@Rang,@QT_Livrer;
WHILE @@FETCH_STATUS = 0
   BEGIN
	if (@Rang = 1)
		update @HistoriqueStock set Stock_Porgressif = QT_Livrer  where Rang = @Rang ;
	else
	begin
		select @Stock_Porgressif = Stock_Porgressif from @HistoriqueStock where Rang = @Rang - 1;
		update @HistoriqueStock set Stock_Porgressif = QT_Livrer + @Stock_Porgressif where Rang = @Rang ;
	end
	FETCH NEXT FROM Historique_Cursor into @ID,@Rang,@QT_Livrer;
   END;
CLOSE Historique_Cursor;
DEALLOCATE Historique_Cursor;



	RETURN;
END








GO
/****** Object:  UserDefinedFunction [dbo].[FN_Historique_Solde_Ristourne_Client]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  UserDefinedFunction [dbo].[FN_Historique_Solde_Caisse]    Script Date: 17/02/2018 22:36:06 ******/

CREATE FUNCTION [dbo].[FN_Historique_Solde_Ristourne_Client](@ID_Client int)
RETURNS @HistoriqueStock TABLE 
(
    [Type] nvarchar(10) NOT NULL, 
    [Date] datetime NOT NULL, 
    Mouvement nvarchar(50) NOT NULL, 
    Numero nvarchar(50)  NULL,
    Rang int NOT NULL ,
    Quantite float NOT NULL,
    QT_Livrer float NOT NULL,
    Stock_Porgressif float NOT NULL
)
AS
BEGIN
	insert into @HistoriqueStock

select [Type], [Date],Mouvement,Num_Bon,Rang, Quantite, QT_Livrer,0
from(
						SELECT   Quantite, QT_Livrer, Date, Num_Bon, Mouvement, Type, ROW_NUMBER() OVER (ORDER BY Date) AS Rang
						FROM         (
									    --- 
										SELECT     ID_Client, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, Ristourne_Client , 0, 0) AS Quantite, 
										dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, Ristourne_Client , 0, 0) AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Vente ' AS Mouvement ,CONVERT(nvarchar, Vente.ID_Vente)+';0' AS Type
										FROM         TP_Vente Vente INNER JOIN TP_Vente_Liquide Vente_Detail 
										ON Vente.ID_Vente = Vente_Detail.ID_Vente
										WHERE (Etat = 1) AND Cumuler_Ristourne = 1 AND (ID_Client = @ID_Client)
										
										UNION ALL 
										
										--- Credite le compte 
										SELECT    ID_Element AS ID_Client,- Montant AS Quantite, - Montant AS QT_Livrer,Date as Date , NUM_DOC as Num_Bon, 
										'Décaissement ' AS Mouvement ,CONVERT(nvarchar, ID_Decaissement)+';1' AS Type
										FROM         dbo.TP_Decaissement 
										WHERE (Etat = 1) AND Type_Element = 1 AND  (ID_Element = @ID_Client)			
	) AS derivedtbl_1) As EE order by Rang 


declare @Rang as int , @QT_Livrer as float ,@ID as Nvarchar(10), @Stock_Porgressif as float
                                             
DECLARE Historique_Cursor CURSOR FOR
SELECT [Type], Rang, QT_Livrer FROM @HistoriqueStock;

OPEN Historique_Cursor;
FETCH NEXT FROM Historique_Cursor into @ID,@Rang,@QT_Livrer;
WHILE @@FETCH_STATUS = 0
   BEGIN
	if (@Rang = 1)
		update @HistoriqueStock set Stock_Porgressif = QT_Livrer  where Rang = @Rang ;
	else
	begin
		select @Stock_Porgressif = Stock_Porgressif from @HistoriqueStock where Rang = @Rang - 1;
		update @HistoriqueStock set Stock_Porgressif = QT_Livrer + @Stock_Porgressif where Rang = @Rang ;
	end
	FETCH NEXT FROM Historique_Cursor into @ID,@Rang,@QT_Livrer;
   END;
CLOSE Historique_Cursor;
DEALLOCATE Historique_Cursor;
	RETURN;
END








GO
/****** Object:  UserDefinedFunction [dbo].[FN_Historique_Stock]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FN_Historique_Stock]
(
     @ID_Element int, 
	 @Type int -- Type = 0 liquide, 1 Emballage
)

RETURNS @HistoriqueStock TABLE 
(
    [Type] nvarchar(10) NOT NULL, 
    [Date] datetime NOT NULL, 
    Mouvement nvarchar(50) NOT NULL,
    Rang int NOT NULL,
    Numero nvarchar(50) NOT NULL,
    
    Plastique int NOT NULL,
    Plastique_Livrer int NOT NULL,
    Plastique_Porgressif int NOT NULL,
    
    Bouteille int NOT NULL,
    Bouteille_Livrer int NOT NULL,
    Bouteille_Porgressif int NOT NULL,
    
    Casier int NOT NULL,
    Casier_Livrer int NOT NULL,
    Casier_Porgressif int NOT NULL,

	Conditionnement int NOT NULL
)
AS
BEGIN
	INSERT INTO @HistoriqueStock

SELECT [Type], [Date], Mouvement, Rang,  Numero, Plastique, Plastique_Livrer, 0, Bouteille, Bouteille_Livrer, 0, Casier, Casier_Livrer, 0, Conditionnement
FROM(
SELECT   Plastique, Plastique_Livrer, Bouteille, Numero, Bouteille_Livrer, Casier, Casier_Livrer,  Date, Conditionnement, Mouvement, Type, ROW_NUMBER() OVER (ORDER BY Date) AS Rang
FROM         (
			--- Commande Liquide
				SELECT      ID_Element, 0 Plastique,  0 Plastique_Livrer,  
						     Quantite_Bouteille_Liquide AS Bouteille, NUM_DOC Numero, 
						     Quantite_Bouteille_Liquide AS Bouteille_Livrer, 
						     Quantite_Casier_Liquide AS Casier, 
						     Quantite_Casier_Liquide AS Casier_Livrer, Date as Date,
					    	 Detail.Conditionnement AS Conditionnement, 
				(N'Commande chez '+ Nom) AS Mouvement ,CONVERT(nvarchar, Commande.ID_Commande)+';0' AS Type
				FROM TP_Commande AS Commande INNER JOIN (SELECT ID_Commande, Emballage.Conditionnement Conditionnement, Produit.ID_Produit AS ID_Element, SUM(Quantite_Casier_Liquide) Quantite_Casier_Liquide, SUM(Quantite_Bouteille_Liquide) Quantite_Bouteille_Liquide
					                                        FROM TP_Commande_Liquide Commande_Liquide INNER JOIN TP_Produit Produit
															ON Commande_Liquide.ID_Produit = Produit.ID_Produit INNER JOIN TP_Emballage Emballage
															ON Emballage.ID_Emballage = Produit.ID_Produit
					                                        GROUP BY ID_Commande, Produit.ID_Produit, Emballage.Conditionnement) AS Detail
				ON Commande.ID_Commande = Detail.ID_Commande INNER JOIN TP_Fournisseur Fournisseur 
				ON Commande.ID_Fournisseur = Fournisseur.ID_Fournisseur
				WHERE Etat = (CASE @Type WHEN 0 THEN 1 ELSE 10 END) AND ID_Element = @ID_Element
				AND (Quantite_Bouteille_Liquide + Quantite_Casier_Liquide * Conditionnement) <> 0
				 									
				UNION ALL 				
		   --- Commande emballage 
				SELECT     ID_Element, -Quantite_Plastique AS Plastique, 
						   -Quantite_Plastique AS Plastique_Livrer,
						   -Quantite_Bouteille AS Bouteille, NUM_DOC Numero, 
						   -Quantite_Bouteille AS Bouteille_Livrer,
						   -Quantite_Casier Casier, 
						   -Quantite_Casier Casier_Livrer, Date as Date,
						   Detail.Conditionnement Conditionnement,  
				(N'Commande chez '+ Nom) AS Mouvement ,CONVERT(nvarchar, Commande.ID_Commande)+';0'AS Type
				FROM dbo.TP_Commande AS Commande INNER JOIN (SELECT Emballage.ID_Commande AS ID_Commande, Emballage.ID_Emballage ID_Element, Conditionnement, SUM(Quantite_Casier) Quantite_Casier , sum(Quantite_Plastique) Quantite_Plastique, sum(Quantite_Bouteille) Quantite_Bouteille
																FROM (
																		SELECT ID_Commande, ID_Emballage, Conditionnement, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
																		FROM TP_Commande_Emballage Commande_Emballage
																		GROUP BY ID_Emballage, ID_Commande, Conditionnement
																	UNION ALL
																		SELECT ID_Commande, Produit.ID_Emballage, Conditionnement, -SUM(Quantite_Casier_Emballage) Quantite_Casier, -SUM(Quantite_Plastique_Emballage) Quantite_Plastique, -SUM(Quantite_Bouteille_Emballage) Quantite_Bouteille
																		FROM TP_Commande_Liquide Commande_Liquide INNER JOIN TP_Produit Produit
																		ON Commande_Liquide.ID_Produit = Produit.ID_Produit 
																		GROUP BY Produit.ID_Emballage, ID_Commande, Conditionnement
																	) AS Emballage GROUP BY Emballage.ID_Emballage, Emballage.ID_Commande, Conditionnement) AS Detail
				ON Commande.ID_Commande = Detail.ID_Commande INNER JOIN TP_Fournisseur Fournisseur 
				ON Commande.ID_Fournisseur = Fournisseur.ID_Fournisseur
				WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END) AND  (ID_Element = @ID_Element)
				AND (Quantite_Plastique + Quantite_Bouteille + Quantite_Casier * Conditionnement) <> 0
				 				
				UNION ALL 
				
			--- Vente Emballage
				SELECT     ID_Element,  Quantite_Plastique AS Plastique, 
						   Quantite_Plastique AS Plastique_Livrer, 
						   Quantite_Bouteille AS Bouteille, NUM_DOC Numero,
						   Quantite_Bouteille AS Bouteille_Livrer,
						   Quantite_Casier Casier, 
						   Quantite_Casier Casier_Livrer, Date as Date,
						    Detail.Conditionnement Conditionnement,   
				(N'Vente à '+ Nom) AS Mouvement ,CONVERT(nvarchar, Vente.ID_Vente)+';1' AS Type
				FROM dbo.TP_Vente AS Vente INNER JOIN (SELECT Emballage.ID_Vente AS ID_Vente, Emballage.ID_Emballage ID_Element, Conditionnement, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, sum(Quantite_Bouteille) Quantite_Bouteille
															FROM (
																	SELECT ID_Vente, ID_Emballage ID_Emballage, Conditionnement, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
																	FROM TP_Vente_Emballage Vente_Emballage 
																	GROUP BY ID_Emballage, ID_Vente, Conditionnement
																UNION ALL
																	SELECT ID_Vente, Produit.ID_Emballage, Conditionnement, -SUM(Quantite_Casier_Emballage) Quantite_Casier, -SUM(Quantite_Plastique_Emballage) Quantite_Plastique, -SUM(Quantite_Bouteille_Emballage) Quantite_Bouteille
																	FROM TP_Vente_Liquide Vente_Liquide INNER JOIN TP_Produit Produit
																	ON Vente_Liquide.ID_Produit = Produit.ID_Produit 
																	GROUP BY Produit.ID_Emballage, ID_Vente, Conditionnement

																UNION ALL
																		SELECT ID_Vente, Produit.ID_Plastique_Base as ID_Emballage, Conditionnement, 0 Quantite_Casier, -SUM(Quantite_Casier_Emballage+Quantite_Plastique_Emballage) Quantite_Plastique, 0 Quantite_Bouteille
																		FROM TP_Vente_Liquide Vente_Liquide INNER JOIN TP_Produit Produit
																		ON Vente_Liquide.ID_Produit = Produit.ID_Produit where Vente_Liquide.Produit_Decomposer = 1 
																		GROUP BY Produit.ID_Plastique_Base, ID_Vente, Conditionnement
																	UNION ALL
																		SELECT ID_Vente, Produit.ID_Bouteille_Base as ID_Emballage, Conditionnement, 0 Quantite_Casier, 0 Quantite_Plastique, -SUM(Quantite_Bouteille_Emballage+Quantite_Casier_Emballage*Conditionnement) Quantite_Bouteille
																		FROM TP_Vente_Liquide Vente_Liquide INNER JOIN TP_Produit Produit
																		ON Vente_Liquide.ID_Produit = Produit.ID_Produit where Vente_Liquide.Produit_Decomposer = 1 
																		GROUP BY Produit.ID_Bouteille_Base, ID_Vente, Conditionnement
																) AS Emballage GROUP BY Emballage.ID_Emballage, Emballage.ID_Vente, Conditionnement
																	) AS Detail
				ON Vente.ID_Vente = Detail.ID_Vente INNER JOIN TP_Client Client 
				ON Vente.ID_Client = Client.ID_Client
				WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END) AND ID_Element = @ID_Element
				AND (Quantite_Plastique + Quantite_Bouteille + Quantite_Casier * Conditionnement) <> 0

				UNION ALL 
				
			--- Vente Liquide
				SELECT     ID_Element, 0 AS Plastique, 0 AS Plastique_Livrer, 
						  - Quantite_Bouteille_Liquide AS Bouteille, NUM_DOC Numero,
						  - Quantite_Bouteille_Liquide AS Bouteille_Livrer,
						  - Quantite_Casier_Liquide Casier, 
						  - Quantite_Casier_Liquide Casier_Livrer, Date as Date,
						    Detail.Conditionnement Conditionnement,
				(N'Vente à '+ Nom) AS Mouvement ,CONVERT(nvarchar, Vente.ID_Vente)+';1' AS Type
				FROM dbo.TP_Vente AS Vente INNER JOIN (SELECT ID_Vente, ID_Produit AS ID_Element, Conditionnement, SUM(Quantite_Casier_Liquide) Quantite_Casier_Liquide, SUM(Quantite_Bouteille_Liquide) Quantite_Bouteille_Liquide
					                                             FROM TP_Vente_Liquide 
																 
					                                             GROUP BY ID_Vente, ID_Produit, Conditionnement
																 UNION ALL
																 SELECT ID_Vente, produit.ID_Produit_Base ID_Produit,Conditionnement, 0 Quantite_Casier_Liquide, SUM(Quantite_Bouteille_Liquide+Quantite_Casier_Liquide*Conditionnement) Quantite_Bouteille_Liquide
																 FROM TP_Vente_Liquide vente_produit inner join TP_Produit produit on vente_produit.ID_Produit = produit.ID_Produit
																 WHERE vente_produit.Produit_Decomposer = 1
					                                             GROUP BY ID_Vente, ID_Produit_Base, Conditionnement) AS Detail
				ON Vente.ID_Vente = Detail.ID_Vente INNER JOIN TP_Client Client 
				ON Vente.ID_Client = Client.ID_Client
				WHERE Etat = (CASE @Type WHEN 0 THEN 1 ELSE 10 END) AND ID_Element = @ID_Element
				AND (Quantite_Bouteille_Liquide + Quantite_Casier_Liquide * Conditionnement) <> 0
				
				UNION ALL 
				
			--- Reguarisation Stock Emballage
				SELECT     ID_Element ID_Element, Quantite_Plastique AS Plastique, 
						   Quantite_Plastique AS Plastique_Livrer, 
						   Quantite_Bouteille AS Bouteille, NUM_DOC Numero,
						   Quantite_Bouteille AS Bouteille_Livrer,
						   Quantite_Casier Casier, 
						   Quantite_Casier Casier_Livrer, Date as Date,
						   Emballage.Conditionnement Conditionnement,  
				(N'Regularisation') AS Mouvement ,CONVERT(nvarchar, ID_Regulation_Stock)+';2' AS Type
				FROM dbo.TP_Regulation_Stock Regulation_Stock INNER JOIN TP_Emballage Emballage
				ON Regulation_Stock.ID_Element = Emballage.ID_Emballage
				WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END) AND Type_Element = 1 AND (ID_Element = @ID_Element)

				UNION ALL
				
		  --- Regularisarion Stock Liquide
				SELECT     ID_Element ID_Element, 0 AS Plastique, 0 AS Plastique_Livrer, 
						   Quantite_Bouteille AS Bouteille, NUM_DOC Numero,
						   Quantite_Bouteille AS Bouteille_Livrer,
						   Quantite_Casier Casier, 
						   Quantite_Casier Casier_Livrer, Date as Date,
						   Emballage.Conditionnement Conditionnement,
				(N'Regularisation') AS Mouvement ,CONVERT(nvarchar, ID_Element)+';2' AS Type
				FROM  dbo.TP_Regulation_Stock Regulation_Stock INNER JOIN TP_Produit Produit 
				ON Regulation_Stock.ID_Element = Produit.ID_Produit INNER JOIN TP_Emballage Emballage
				ON Produit.ID_Emballage = Emballage.ID_Emballage
				WHERE Etat = (CASE @Type WHEN 0 THEN 1 ELSE 10 END) AND Type_Element = 0 AND (ID_Element = @ID_Element)		
				
				UNION ALL 
				
			--- Retour Emballage client 
				SELECT     Detail.ID_Element ID_Element, Quantite_Plastique AS Plastique, 
						   Quantite_Plastique AS Plastique_Livrer, 
						   Quantite_Bouteille AS Bouteille, NUM_DOC Numero,
						   Quantite_Bouteille AS Bouteille_Livrer,
						   Quantite_Casier Casier, 
						   Quantite_Casier Casier_Livrer, Date as Date,
						   Detail.Conditionnement Conditionnement,  
				(N'Retour emballage de ' + Nom) AS Mouvement ,CONVERT(nvarchar, Retour.ID_Retour_Emballage)+';3' AS Type
				FROM dbo.TP_Retour_Emballage AS Retour INNER JOIN (SELECT ID_Retour_Emballage, ID_Emballage AS ID_Element, Conditionnement, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
																	 FROM TP_Retour_Emballage_Detail
																	 GROUP BY ID_Retour_Emballage, ID_Emballage, Conditionnement) AS Detail
				ON Retour.ID_Retour_Emballage = Detail.ID_Retour_Emballage  INNER JOIN TP_Client Client 
				ON Retour.ID_Element = Client.ID_Client
				WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END) AND (Detail.ID_Element = @ID_Element) AND Type_Element = 1
				AND (Quantite_Plastique + Quantite_Bouteille + Quantite_Casier * Conditionnement ) <> 0
				
				UNION ALL 
				
			--- Retour emballage fournisseur
				SELECT     Detail.ID_Element ID_Element, - Quantite_Plastique AS Plastique, 
						  - Quantite_Plastique AS Plastique_Livrer, 
						  - Quantite_Bouteille AS Bouteille, NUM_DOC Numero,
						  - Quantite_Bouteille AS Bouteille_Livrer,
						  - Quantite_Casier Casier, 
						  - Quantite_Casier Casier_Livrer, Date as Date,
						  Detail.Conditionnement Conditionnement,  
				(N'Retour emballage de ' + Nom) AS Mouvement ,CONVERT(nvarchar, Retour.ID_Retour_Emballage)+';3' AS Type
				FROM dbo.TP_Retour_Emballage AS Retour INNER JOIN (SELECT ID_Retour_Emballage, ID_Emballage AS ID_Element, Conditionnement, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
					                                             FROM TP_Retour_Emballage_Detail
					                                             GROUP BY ID_Retour_Emballage, ID_Emballage, Conditionnement) AS Detail
				ON Retour.ID_Retour_Emballage = Detail.ID_Retour_Emballage  INNER JOIN TP_Fournisseur Fournisseur 
				ON Retour.ID_Element = Fournisseur.ID_Fournisseur
				WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END) AND Detail.ID_Element = @ID_Element AND Type_Element = 0
				AND (Quantite_Plastique + Quantite_Bouteille + Quantite_Casier * Conditionnement ) <> 0
				
				UNION ALL 
				
			--- Inventaire Liquide
				SELECT     Detail.ID_Element ID_Element,  Quantite_Plastique AS Plastique, 
						   Quantite_Plastique AS Plastique_Livrer, 
						   Quantite_Bouteille AS Bouteille, NUM_DOC Numero,
						   Quantite_Bouteille AS Bouteille_Livrer,
						   Quantite_Casier Casier, 
						   Quantite_Casier Casier_Livrer, Edit_Date as Date,
						   Detail.Conditionnement Conditionnement,  
				(N'Inventaire N° ' + NUM_DOC) AS Mouvement ,CONVERT(nvarchar, Inventaire.ID_Inventaire)+';4' AS Type
				FROM dbo.TP_Inventaire AS Inventaire INNER JOIN (SELECT ID_Inventaire, ID_Element, Emballage.Conditionnement Conditionnement, SUM(Quantite_Casier_Reel - Quantite_Casier_Theorique) Quantite_Casier, 
					                                                  0 Quantite_Plastique, SUM(Quantite_Bouteille_Reel - Quantite_Bouteille_Theorique) Quantite_Bouteille
					                                             FROM TP_Inventaire_Detail Inventaire_Detail INNER JOIN TP_Emballage Emballage
																 ON Inventaire_Detail.ID_Element = Emballage.ID_Emballage
					                                             GROUP BY ID_Inventaire, ID_Element, Emballage.Conditionnement) AS Detail
				ON Inventaire.ID_Inventaire = Detail.ID_Inventaire
				WHERE Etat = (CASE @Type WHEN 0 THEN 2 ELSE 10 END) AND Type = 0 AND ID_Element = @ID_Element

				UNION ALL 
				
			--- Inventaire Emballage
				SELECT     Detail.ID_Element ID_Element,  Quantite_Plastique AS Plastique, 
						   Quantite_Plastique AS Plastique_Livrer, 
						   Quantite_Bouteille AS Bouteille, NUM_DOC Numero,
						   Quantite_Bouteille AS Bouteille_Livrer,
						   Quantite_Casier Casier, 
						   Quantite_Casier Casier_Livrer, Edit_Date as Date,
						   Detail.Conditionnement Conditionnement,  
				(N'Inventaire N° ' + NUM_DOC) AS Mouvement ,CONVERT(nvarchar, Inventaire.ID_Inventaire)+';4' AS Type
				FROM dbo.TP_Inventaire AS Inventaire INNER JOIN (SELECT ID_Inventaire, ID_Element, Emballage.Conditionnement Conditionnement, SUM(Quantite_Casier_Reel - Quantite_Casier_Theorique) Quantite_Casier, 
					                                                  SUM(Quantite_Plastique_Reel - Quantite_Plastique_Theorique) Quantite_Plastique, 
																	  SUM(Quantite_Bouteille_Reel - Quantite_Bouteille_Theorique) Quantite_Bouteille
					                                             FROM TP_Inventaire_Detail Inventaire_Detail INNER JOIN TP_Emballage Emballage
																 ON Inventaire_Detail.ID_Element = Emballage.ID_Emballage
					                                             GROUP BY ID_Inventaire, ID_Element, Emballage.Conditionnement) AS Detail
				ON Inventaire.ID_Inventaire = Detail.ID_Inventaire
				WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 2 END)  AND Type = 1 AND ID_Element = @ID_Element
																															
	) AS derivedtbl_1) As EE order by Rang 

	DECLARE @Rang as INT, @Plastique_Livrer as float, @Bouteille_Livrer as float, 
			@Casier_Livrer as float, @ID as Nvarchar(10), @Casier_Porgressif as float,
			@Plastique_Porgressif as float, @Bouteille_Porgressif as float,
            @Casier AS INT, @Plastique AS INT, @Bouteille AS INT, @Contenance AS INT, 
			@Nombre_Total_Plastique AS INT, @NombrePlastique_Restant AS INT, @Nombre_Total_Bouteil AS INT,
			@NombreCasierPlein AS INT, @NombreBouteil_Restant AS INT
			                          
	DECLARE Historique_Cursor CURSOR FOR
	SELECT [Type], Rang, Bouteille_Livrer, Plastique_Livrer,  Casier_Livrer FROM @HistoriqueStock;
	OPEN Historique_Cursor;
	FETCH NEXT FROM Historique_Cursor into @ID, @Rang, @Bouteille_Livrer, @Plastique_Livrer, @Casier_Livrer;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
		if (@Rang = 1)
			update @HistoriqueStock set Bouteille_Porgressif = Bouteille_Livrer, 
										Casier_Porgressif = Casier_Livrer, 
										Plastique_Porgressif = Plastique_Livrer 
										where Rang = @Rang;
		else
		begin
			select @Bouteille_Porgressif = Bouteille_Porgressif,
				   @Plastique_Porgressif = Plastique_Porgressif,
				   @Casier_Porgressif = Casier_Porgressif 
			 from  @HistoriqueStock where Rang = @Rang - 1;
			update @HistoriqueStock set Casier_Porgressif = Casier_Livrer + @Casier_Porgressif,
										Bouteille_Porgressif = Bouteille_Livrer + @Bouteille_Porgressif, 
										Plastique_Porgressif = Plastique_Livrer + @Plastique_Porgressif
			 where Rang = @Rang ;
		end
		FETCH NEXT FROM Historique_Cursor into @ID, @Rang, @Bouteille_Livrer, @Plastique_Livrer, @Casier_Livrer;
	   END;
	CLOSE Historique_Cursor;
	DEALLOCATE Historique_Cursor; 

	IF(@Type = 0)
	BEGIN
		DECLARE Stock_Cursor CURSOR FOR
		SELECT Rang, Casier_Porgressif, Bouteille_Porgressif, Conditionnement FROM @HistoriqueStock;
		OPEN Stock_Cursor;
		FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
		WHILE @@FETCH_STATUS = 0
			BEGIN
			SET @Nombre_Total_Bouteil = @Casier * @Contenance + @Bouteille;
			SET @NombreCasierPlein = @Nombre_Total_Bouteil / @Contenance;
			SET @NombreBouteil_Restant = @Nombre_Total_Bouteil % @Contenance;
			UPDATE @HistoriqueStock set Casier_Porgressif = @NombreCasierPlein, Bouteille_Porgressif = @NombreBouteil_Restant   where Rang = @ID;		
			FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
			END;
		CLOSE Stock_Cursor;
		DEALLOCATE Stock_Cursor;   
	END
	ELSE
	BEGIN
		DECLARE Stock_Cursor CURSOR FOR
		SELECT Rang, Casier_Porgressif, Plastique_Porgressif, Bouteille_Porgressif, Conditionnement FROM @HistoriqueStock;
		OPEN Stock_Cursor;
		FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Plastique, @Bouteille, @Contenance;
		WHILE @@FETCH_STATUS = 0
			BEGIN
			SET @Nombre_Total_Bouteil = @Casier * @Contenance + @Bouteille;
			SET @Nombre_Total_Plastique = @Casier + @Plastique;
			SET @NombreCasierPlein = @Nombre_Total_Bouteil / @Contenance;
			SET @NombrePlastique_Restant = @Nombre_Total_Plastique - @NombreCasierPlein;
			IF(@NombreCasierPlein > @Nombre_Total_Plastique)
			BEGIN
				SET @NombreCasierPlein = @Nombre_Total_Plastique
				SET @NombrePlastique_Restant = 0;
			END				
			SET @NombreBouteil_Restant = @Nombre_Total_Bouteil % @Contenance;
			update @HistoriqueStock SET Casier_Porgressif = @NombreCasierPlein, Plastique_Porgressif = @NombrePlastique_Restant, Bouteille_Porgressif = @NombreBouteil_Restant   where Rang = @ID;		
			FETCH NEXT FROM Stock_Cursor into @ID, @Casier,@Plastique, @Bouteille, @Contenance;
			END;
		CLOSE Stock_Cursor;
		DEALLOCATE Stock_Cursor; 
	END           
RETURN ;
END









GO
/****** Object:  UserDefinedFunction [dbo].[FN_Montant_Commande]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Montant_Commande]
(	
	@ID_Commande BIGINT
)
RETURNS @Tab_Solde TABLE
(
	ID_Commande bigint NOT NULL,
	Montant_Liquide float NOT NULL,
    Montant_Emballage float NOT NULL,
    Montant_Retour_Emballage float NOT NULL
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (ID_Commande, Montant_Liquide, Montant_Emballage, Montant_Retour_Emballage)   
    SELECT ID_Commande, Montant_Liquide, Montant_Emballage, Montant_Retour_Emballage  
    FROM		
	(	
		SELECT TAB_MONTANT_LIQUIDE.ID_Commande as ID_Commande,	
		CASE WHEN (TAB_MONTANT_LIQUIDE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_LIQUIDE.Montant END AS Montant_Liquide,    
	    CASE WHEN (TAB_MONTANT_EMBALLAGE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_EMBALLAGE.Montant END AS Montant_Emballage,
	    CASE WHEN (TAB_MONTANT_RETOUR_EMBALLAGE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_RETOUR_EMBALLAGE.Montant END AS Montant_Retour_Emballage
		FROM 
		(
			SELECT ID_Commande ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Liquide  
				    SELECT       ID_Commande, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide,Conditionnement,PU_Achat, 0,0)  Montant
					FROM         TP_Commande_Liquide
					WHERE ID_Commande = @ID_Commande
				)AS derivedtbl_1 GROUP BY ID_Commande 
		) as TAB_MONTANT_LIQUIDE LEFT OUTER JOIN
		(
			SELECT ID_Commande ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Emballage  
				    SELECT       ID_Commande, dbo.FN_Prix_Emballage(Quantite_Casier_Emballage, Quantite_Plastique_Emballage, Quantite_Bouteille_Emballage,PU_Plastique,PU_Bouteille,Conditionnement) Montant
					FROM         TP_Commande_Liquide
					WHERE ID_Commande = @ID_Commande
				)AS derivedtbl_1 GROUP BY ID_Commande
		) AS TAB_MONTANT_EMBALLAGE on TAB_MONTANT_LIQUIDE.ID_Commande = TAB_MONTANT_EMBALLAGE.ID_Commande LEFT OUTER JOIN
		(
			SELECT ID_Commande ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Emballage  
				    SELECT       ID_Commande, dbo.FN_Prix_Emballage(Quantite_Casier, Quantite_Plastique, Quantite_Bouteille,PU_Plastique,PU_Bouteille,Conditionnement) Montant
					FROM         TP_Commande_Emballage
					WHERE ID_Commande = @ID_Commande
				)AS derivedtbl_1 GROUP BY ID_Commande
		) AS TAB_MONTANT_RETOUR_EMBALLAGE on TAB_MONTANT_LIQUIDE.ID_Commande = TAB_MONTANT_RETOUR_EMBALLAGE.ID_Commande		
   ) AS derivedtbl_2               
RETURN ;
END








GO
/****** Object:  UserDefinedFunction [dbo].[FN_Montant_Commande_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Montant_Commande_Temp]
(	
	@Code_User int
)
RETURNS @Tab_Solde TABLE
(
	Code_User int NOT NULL,
	Montant_Liquide float NOT NULL,
    Montant_Emballage float NOT NULL,
    Montant_Retour_Emballage float NOT NULL
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (Code_User, Montant_Liquide, Montant_Emballage, Montant_Retour_Emballage)   
    SELECT Code_User, Montant_Liquide, Montant_Emballage, Montant_Retour_Emballage  
    FROM		
	(	
		SELECT TAB_MONTANT_LIQUIDE.Create_Code_User as Code_User,	
		CASE WHEN (TAB_MONTANT_LIQUIDE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_LIQUIDE.Montant END AS Montant_Liquide,    
	    CASE WHEN (TAB_MONTANT_EMBALLAGE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_EMBALLAGE.Montant END AS Montant_Emballage,
	    CASE WHEN (TAB_MONTANT_RETOUR_EMBALLAGE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_RETOUR_EMBALLAGE.Montant END AS Montant_Retour_Emballage
		FROM 
		(
			SELECT Create_Code_User ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Liquide  
				    SELECT       Create_Code_User, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide,Conditionnement,PU_Achat, 0,0)  Montant
					FROM         TP_Commande_Liquide_Temp
					WHERE Create_Code_User = @Code_User
				)AS derivedtbl_1 GROUP BY Create_Code_User 
		) as TAB_MONTANT_LIQUIDE LEFT OUTER JOIN
		(
			SELECT Create_Code_User ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Emballage  
				    SELECT       Create_Code_User, dbo.FN_Prix_Emballage(Quantite_Casier_Emballage, Quantite_Plastique_Emballage, Quantite_Bouteille_Emballage,PU_Plastique,PU_Bouteille,Conditionnement) Montant
					FROM         TP_Commande_Liquide_Temp
					WHERE Create_Code_User = @Code_User
				)AS derivedtbl_1 GROUP BY Create_Code_User
		) AS TAB_MONTANT_EMBALLAGE on TAB_MONTANT_LIQUIDE.Create_Code_User = TAB_MONTANT_EMBALLAGE.Create_Code_User LEFT OUTER JOIN
		(
			SELECT Create_Code_User ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Emballage  
				    SELECT       Create_Code_User, dbo.FN_Prix_Emballage(Quantite_Casier, Quantite_Plastique, Quantite_Bouteille,PU_Plastique,PU_Bouteille,Conditionnement) Montant
					FROM         TP_Commande_Emballage_Temp
					WHERE Create_Code_User = @Code_User
				)AS derivedtbl_1 GROUP BY Create_Code_User
		) AS TAB_MONTANT_RETOUR_EMBALLAGE on TAB_MONTANT_LIQUIDE.Create_Code_User = TAB_MONTANT_RETOUR_EMBALLAGE.Create_Code_User		
   ) AS derivedtbl_2               
RETURN ;
END







GO
/****** Object:  UserDefinedFunction [dbo].[FN_Montant_Facture]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Montant_Facture]
(	
	@ID_Vente BIGINT
)
RETURNS @Tab_Solde TABLE
(
	ID_Vente bigint NOT NULL,
	Montant_Liquide float NOT NULL,
    Montant_Emballage float NOT NULL,
    Montant_Retour_Emballage float NOT NULL
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (ID_Vente, Montant_Liquide, Montant_Emballage, Montant_Retour_Emballage)   
    SELECT ID_Vente, Montant_Liquide, Montant_Emballage, Montant_Retour_Emballage  
    FROM		
	(	
		SELECT TAB_MONTANT_LIQUIDE.ID_Vente as ID_Vente,	
		CASE WHEN (TAB_MONTANT_LIQUIDE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_LIQUIDE.Montant END AS Montant_Liquide,    
	    CASE WHEN (TAB_MONTANT_EMBALLAGE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_EMBALLAGE.Montant END AS Montant_Emballage,
	    CASE WHEN (TAB_MONTANT_RETOUR_EMBALLAGE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_RETOUR_EMBALLAGE.Montant END AS Montant_Retour_Emballage
		FROM 
		(
			SELECT ID_Vente ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Liquide  
				    SELECT       ID_Vente, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide,Conditionnement,PU_Vente, Ristourne_Client,Cumuler_Ristourne)  Montant
					FROM         TP_Vente_Liquide
					WHERE ID_Vente = @ID_Vente
				)AS derivedtbl_1 GROUP BY ID_Vente 
		) as TAB_MONTANT_LIQUIDE LEFT OUTER JOIN
		(
			SELECT ID_Vente ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Emballage  
				    SELECT       ID_Vente, dbo.FN_Prix_Emballage(Quantite_Casier_Emballage, Quantite_Plastique_Emballage, Quantite_Bouteille_Emballage,PU_Plastique,PU_Bouteille,Conditionnement) Montant
					FROM         TP_Vente_Liquide
					WHERE ID_Vente = @ID_Vente
				)AS derivedtbl_1 GROUP BY ID_Vente
		) AS TAB_MONTANT_EMBALLAGE on TAB_MONTANT_LIQUIDE.ID_Vente = TAB_MONTANT_EMBALLAGE.ID_Vente LEFT OUTER JOIN
		(
			SELECT ID_Vente ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Emballage  
				    SELECT       ID_Vente, dbo.FN_Prix_Emballage(Quantite_Casier, Quantite_Plastique, Quantite_Bouteille,PU_Plastique,PU_Bouteille,Conditionnement) Montant
					FROM         TP_Vente_Emballage
					WHERE ID_Vente = @ID_Vente
				)AS derivedtbl_1 GROUP BY ID_Vente
		) AS TAB_MONTANT_RETOUR_EMBALLAGE on TAB_MONTANT_LIQUIDE.ID_Vente = TAB_MONTANT_RETOUR_EMBALLAGE.ID_Vente		
   ) AS derivedtbl_2               
RETURN ;
END







GO
/****** Object:  UserDefinedFunction [dbo].[FN_Montant_Facture_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Montant_Facture_Temp]
(	
	@Code_User int
)
RETURNS @Tab_Solde TABLE
(
	Code_User int NOT NULL,
	Montant_Liquide float NOT NULL,
    Montant_Emballage float NOT NULL,
    Montant_Retour_Emballage float NOT NULL
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (Code_User, Montant_Liquide, Montant_Emballage, Montant_Retour_Emballage)   
    SELECT Code_User, Montant_Liquide, Montant_Emballage, Montant_Retour_Emballage  
    FROM		
	(	
		SELECT TAB_MONTANT_LIQUIDE.Create_Code_User as Code_User,	
		CASE WHEN (TAB_MONTANT_LIQUIDE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_LIQUIDE.Montant END AS Montant_Liquide,    
	    CASE WHEN (TAB_MONTANT_EMBALLAGE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_EMBALLAGE.Montant END AS Montant_Emballage,
	    CASE WHEN (TAB_MONTANT_RETOUR_EMBALLAGE.Montant IS NULL) THEN 0 ELSE TAB_MONTANT_RETOUR_EMBALLAGE.Montant END AS Montant_Retour_Emballage
		FROM 
		(
			SELECT Create_Code_User ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Liquide  
				    SELECT       Create_Code_User, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide,Conditionnement,PU_Vente, Ristourne_Client,Cumuler_Ristourne)  Montant
					FROM         TP_Vente_Liquide_Temp
					WHERE Create_Code_User = @Code_User
				)AS derivedtbl_1 GROUP BY Create_Code_User 
		) as TAB_MONTANT_LIQUIDE LEFT OUTER JOIN
		(
			SELECT Create_Code_User ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Emballage  
				    SELECT       Create_Code_User, dbo.FN_Prix_Emballage(Quantite_Casier_Emballage, Quantite_Plastique_Emballage, Quantite_Bouteille_Emballage,PU_Plastique,PU_Bouteille,Conditionnement) Montant
					FROM         TP_Vente_Liquide_Temp
					WHERE Create_Code_User = @Code_User
				)AS derivedtbl_1 GROUP BY Create_Code_User
		) AS TAB_MONTANT_EMBALLAGE on TAB_MONTANT_LIQUIDE.Create_Code_User = TAB_MONTANT_EMBALLAGE.Create_Code_User LEFT OUTER JOIN
		(
			SELECT Create_Code_User ,SUM(Montant) AS Montant
			FROM (				 
				     --Montant Emballage  
				    SELECT       Create_Code_User, dbo.FN_Prix_Emballage(Quantite_Casier, Quantite_Plastique, Quantite_Bouteille,PU_Plastique,PU_Bouteille,Conditionnement) Montant
					FROM         TP_Vente_Emballage_Temp
					WHERE Create_Code_User = @Code_User
				)AS derivedtbl_1 GROUP BY Create_Code_User
		) AS TAB_MONTANT_RETOUR_EMBALLAGE on TAB_MONTANT_LIQUIDE.Create_Code_User = TAB_MONTANT_RETOUR_EMBALLAGE.Create_Code_User		
   ) AS derivedtbl_2               
RETURN ;
END






GO
/****** Object:  UserDefinedFunction [dbo].[FN_Parent_Commande]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TEG-FACTURATION
-- Create date: 19-11-2015
-- Description:	cette fontion renvoie toutes les commandes parent d'une commande passer en parametre
-- =============================================
CREATE FUNCTION [dbo].[FN_Parent_Commande]
(
	@Code_Commande int
)
RETURNS 
@Tab_Parent_Commande TABLE 
(
	Code_Commande int not null
)
AS
BEGIN
	declare @Code_Commande_Parent as int
	-- insertion de la commande elle meme comme etant son propore parent
	insert into @Tab_Parent_Commande values(@Code_Commande);
	
	select @Code_Commande_Parent = Code_Commande_Parent from TG_Commandes where Code_Commande = @Code_Commande;
	
	-- cette boucle permet de retrouver tous les parent des dans l'hierarchie de la commande
	-- si on retrouve un parent
	WHILE(EXISTS(SELECT Code_Commande_Parent FROM TG_Commandes WHERE Code_Commande = @Code_Commande_Parent))
	BEGIN
		-- inserer le parent retrouver dans la hierarchie
		insert into @Tab_Parent_Commande values(@Code_Commande_Parent);
		
		-- rechercher le parent du parent precedement retrouver
		SELECT @Code_Commande_Parent = Code_Commande_Parent FROM TG_Commandes WHERE Code_Commande = @Code_Commande_Parent
		----------------------------------------------------------------------------------------------------------------------------------
	END
	RETURN 
END







GO
/****** Object:  UserDefinedFunction [dbo].[FN_Prix_Achat]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TEG-FACTURATION
-- Create date: 15/11/2017
-- Description:	Retourne le prix d'achat du produit liquide
-- =============================================
CREATE FUNCTION [dbo].[FN_Prix_Achat]
(
	@ID_Produit int, @Quantite_Casier int, @Quantite_Bouteille int
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @PU_Achat float, @Montant float, @Conditionnement as int
	
	-- On recupère le prix d'achat et le conditionnement du produit
	select @PU_Achat = PU_Achat, @Conditionnement = Conditionnement from TP_Produit Produit
	inner join TP_Emballage Emballage on Produit.ID_Emballage = Emballage.ID_Emballage where ID_Produit = @ID_Produit
	
	set @Montant = (@Quantite_Casier * @PU_Achat) + (@PU_Achat / @Conditionnement) * @Quantite_Bouteille
	return @Montant
END









GO
/****** Object:  UserDefinedFunction [dbo].[FN_Prix_Emballage]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		TEG-FACTURATION
-- Create date: 15/11/2017
-- Description:	Calcul le prix d'un emballage(Platique + bouteille vide)
-- =============================================
CREATE FUNCTION [dbo].[FN_Prix_Emballage]
(
	@Quantite_Casier int, @Quantite_Plastique int, @Quantite_Bouteille int, @PU_Platique int, @PU_Bouteille int, @Conditionnement int
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE  @Montant int
	
	set @Montant = @Quantite_Casier*(@PU_Platique+(@PU_Bouteille*@Conditionnement)) + @PU_Platique*@Quantite_Plastique + @PU_Bouteille * @Quantite_Bouteille
	
	return @Montant
END











GO
/****** Object:  UserDefinedFunction [dbo].[FN_Prix_Vente]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		TEG-FACTURATION
-- Create date: 15/11/2017
-- Description:	Retourne le prix de vente du produit liquide pour un client
-- =============================================
CREATE FUNCTION [dbo].[FN_Prix_Vente]
(
	@ID_Client int, @ID_Produit int, @Quantite_Casier int, @Quantite_Bouteille int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @PU_Vente int, @Ristourne int, @Montant int, @PU_Vente_Avec_Ristourne int, @Conditionnement int
	
	-- On recupère le prix de vente du produit
	select @PU_Vente = PU_Vente, @Conditionnement = Conditionnement from TP_Produit Produit
	inner join TP_Emballage Emballage on Produit.ID_Emballage = Emballage.ID_Emballage where ID_Produit = @ID_Produit
	
	-- On recupère le montant des ristournes accordé au client sur le produit	
	select @Ristourne = ristourne.Ristourne from TP_Ristourne_Client ristourne inner join 
	TP_Client client on client.ID_Client = ristourne.ID_Client where client.ID_Client = @ID_Client and ristourne.ID_Produit = @ID_Produit and ristourne.Actif = 1 and client.cumuler_ristourne = 0
    if(@Ristourne is null) 
		set @Ristourne = 0;
		
	set @PU_Vente_Avec_Ristourne = @PU_Vente - @Ristourne
	
	set @Montant = (@Quantite_Casier * @PU_Vente_Avec_Ristourne) + (@PU_Vente_Avec_Ristourne / @Conditionnement) * @Quantite_Bouteille

	return @Montant
END










GO
/****** Object:  UserDefinedFunction [dbo].[FN_Prix_Vente_Achat_Detail]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		TEG-FACTURATION
-- Create date: 15/11/2017
-- Description:	Retourne le prix de vente du produit liquide
-- =============================================
CREATE FUNCTION [dbo].[FN_Prix_Vente_Achat_Detail]
(
	@Quantite_Casier int, @Quantite_Bouteille int, @Conditionnement int, @PU_Liquide float, @Ristourne float, @Cumuler_Ristourne bit
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Montant float, @Prix_Vente_Achat float
	
	set @Prix_Vente_Achat = @PU_Liquide
	if(@Cumuler_Ristourne = 0)
	begin
		set @Prix_Vente_Achat = @PU_Liquide - @Ristourne
	end
	set @Montant = ((@Quantite_Casier * @Prix_Vente_Achat) + ((@Prix_Vente_Achat / @Conditionnement) * @Quantite_Bouteille))
		
	return @Montant
END











GO
/****** Object:  UserDefinedFunction [dbo].[FN_Recapitulatif_Commande]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Recapitulatif_Commande]
(
	@Code_Commande  int
)
RETURNS @Tab_Recapitulatif_Commande TABLE 
(
	Code_Commande INT NOT NULL,
	Commande nvarchar(150) NOT NULL,
	Profil nvarchar(150) NOT NULL,
	Executer bit NOT NULL,
	Disponible bit NOT NULL
)
AS
BEGIN
	insert into @Tab_Recapitulatif_Commande
	select C.Code_Commande, C.Libelle,P.Libelle ,D.Executer,D.Disponible
	from TG_Commandes C inner join TG_Droits D on C.Code_Commande = D.Code_Commande 
	inner join TG_Profiles P on D.Code_Profile = P.Code_Profile
	where C.Code_Commande = @Code_Commande and p.Code_Profile <> 1
	
	RETURN ;
END







GO
/****** Object:  UserDefinedFunction [dbo].[FN_Recapitulatif_Commande_Ensemble]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Recapitulatif_Commande_Ensemble]
(
	@Code_Commande  int
)
RETURNS @Tab_Recapitulatif_Commande_Ensemble TABLE 
(
	Code_Commande INT NOT NULL,
	Commande nvarchar(150) NOT NULL,
	Commande_Parent nvarchar(150) NOT NULL,
	AD bit NOT NULL,--1
	RC bit NOT NULL,--2
	RS bit NOT NULL,--3
	CC bit NOT NULL,--4
	CL bit NOT NULL,--5
	CA bit NOT NULL,--6
	ComB bit NOT NULL,--7
	DG bit NOT NULL,--8
	ComK bit NOT NULL,--9
	ResCai bit NOT NULL,--10
	CT bit NOT NULL,--11
	RA bit NOT NULL--12
)
AS
BEGIN
	insert into @Tab_Recapitulatif_Commande_Ensemble
	select C.Code_Commande, C.Commandes,c.Commande_Parent,0,0,0,0,0,0,0,0,0,0,0,0
	from Vue_Commande C order by C.Code_Commande_Parent;
	-- AD
	update @Tab_Recapitulatif_Commande_Ensemble set AD = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 1 
	) A
	-- RC
	update @Tab_Recapitulatif_Commande_Ensemble set RC = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 2 
	) A
	-- RS
	update @Tab_Recapitulatif_Commande_Ensemble set RS = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 3 
	) A
	-- CC
	update @Tab_Recapitulatif_Commande_Ensemble set CC = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 4 
	) A
	-- CL
	update @Tab_Recapitulatif_Commande_Ensemble set CL = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 5 
	) A
	-- CA
	update @Tab_Recapitulatif_Commande_Ensemble set CA = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 6
	) A
	-- ComB
	update @Tab_Recapitulatif_Commande_Ensemble set ComB = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 7 
	) A
	-- DG
	update @Tab_Recapitulatif_Commande_Ensemble set DG = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande and A.Code_Profile = 8 
			where A.Code_Profile = 8 
	) A
	-- ComK
	update @Tab_Recapitulatif_Commande_Ensemble set ComK = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 9 
	) A
	-- ResCai
	update @Tab_Recapitulatif_Commande_Ensemble set ResCai = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 10 
	) A
	-- CT
	update @Tab_Recapitulatif_Commande_Ensemble set CT = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 11 
	) A
	-- RA
	update @Tab_Recapitulatif_Commande_Ensemble set RA = A.Executer
	from (
			select A.Executer from TG_Droits A inner join
			@Tab_Recapitulatif_Commande_Ensemble B on A.Code_Commande = B.Code_Commande
			where A.Code_Profile = 12
	) A
	
	RETURN ;
END







GO
/****** Object:  UserDefinedFunction [dbo].[FN_Retour_Emballage]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Retour_Emballage](	)
RETURNS @FN_Encaissement TABLE(
	ID_Retour_Emballage bigint NOT NULL,
	ID_Element int NOT NULL,
	Type_Element int not null,
	Code_User int not null,
	NUM_DOC nvarchar(50),
    Date  Datetime NOT NULL,
    Etat  int NOT NULL,
    Montant float NOT NULL
) AS BEGIN 
INSERT INTO @FN_Encaissement ( 
	ID_Retour_Emballage, 
	ID_Element, Type_Element, Code_User, 
	NUM_DOC, 
	Date, 
	Etat, 
	Montant	
)
    
SELECT ID_Retour_Emballage, ID_Element,Type_Element,Code_User,  NUM_DOC, Date, Etat, Montant FROM	(	
	SELECT StockReel.ID_Element ID_Element, 
	StockReel.ID_Retour_Emballage ID_Retour_Emballage,
	StockReel.Create_Code_User Code_User, 
	StockReel.Type_Element AS Type_Element, 
	StockReel.Date Date,
	StockReel.NUM_DOC NUM_DOC,
	StockReel.Etat Etat,   
	CASE WHEN (EMBALLAGE.EMBALLAGE IS NULL) THEN 0 ELSE EMBALLAGE.EMBALLAGE END AS Montant			
	FROM (
		SELECT ID_Element ,Type_Element,Create_Code_User, NUM_DOC, Date, Etat, ID_Retour_Emballage FROM (						
			SELECT  ID_Element,Type_Element,Create_Code_User,NUM_DOC, Date, Etat, ID_Retour_Emballage
			FROM  dbo.TP_Retour_Emballage
		) AS derivedtbl_1 ) AS StockReel	LEFT OUTER JOIN
		(  
			  SELECT ID_Retour_Emballage, EMBALLAGE
			   FROM
			     (
			       SELECT ID_Retour_Emballage ,SUM(Montant) AS EMBALLAGE	
						FROM (
						       SELECT      ID_Retour_Emballage, dbo.FN_Prix_emballage(Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) Montant
								FROM       dbo.TP_Retour_Emballage_Detail					        
							 ) AS EMBALLAGE_2
						GROUP BY  ID_Retour_Emballage
			      ) AS EMBALLAGE_1
			) AS EMBALLAGE ON StockReel.ID_Retour_Emballage  = EMBALLAGE.ID_Retour_Emballage																					
   ) AS derivedtbl_2               
RETURN ;
END











GO
/****** Object:  UserDefinedFunction [dbo].[FN_Stat_Client_Fournisseur]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Stat_Client_Fournisseur]
(
   -- 0 Client, 1 Fournisseur
   @Date_Debut date, @Date_Fin date, @Type int
)
RETURNS @Tab_Solde TABLE
(
	ID_Element int NOT NULL,
	Nom nvarchar(100) NOT NULL,
	Adresse nvarchar(100) NOT NULL,
	Telephone nvarchar(100) NOT NULL,
    Solde  as (- Reglement + Livraison),
	Part_Marche  as ( (Livraison / (CASE WHEN Part = 0 THEN 1 ELSE Part END) ) * 100 ),
    Livraison float NOT NULL,
    Reglement float NOT NULL,
	Part float NOT NULL
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (ID_Element, Nom, Adresse, Telephone, Livraison, Reglement, Part)   
    SELECT ID_Element, Nom, Adresse, Telephone, Livraison, Reglement, Part
    FROM		
		(	
		SELECT StockReel.ID_Element as ID_Element,

		 (CASE WHEN (@Type = 0) THEN (SELECT Nom FROM TP_Client WHERE ID_Client = StockReel.ID_Element) 
	     ELSE (SELECT Nom FROM TP_Fournisseur WHERE ID_Fournisseur = StockReel.ID_Element) END) AS Nom,   

		  (CASE WHEN (@Type = 0) THEN (SELECT (CASE WHEN Adresse IS NULL THEN '' ELSE Adresse END) AS Adresse  FROM TP_Client WHERE ID_Client = StockReel.ID_Element) 
	     ELSE (SELECT (CASE WHEN Adresse IS NULL THEN '' ELSE Adresse END) AS Adresse  FROM TP_Fournisseur WHERE ID_Fournisseur = StockReel.ID_Element) END) AS Adresse,  
		 
		  (CASE WHEN (@Type = 0) THEN (SELECT (CASE WHEN Telephone IS NULL THEN '' ELSE Telephone END) AS Telephone  FROM TP_Client WHERE ID_Client = StockReel.ID_Element) 
	     ELSE (SELECT (CASE WHEN Telephone IS NULL THEN '' ELSE Telephone END) AS Telephone  FROM TP_Fournisseur WHERE ID_Fournisseur = StockReel.ID_Element) END) AS Telephone,    
		  
	    CASE WHEN (StockReel.Montant IS NULL) THEN 0 ELSE StockReel.Montant END AS Livraison,

		CASE WHEN (ENCAISSEMENT.ENCAISSEMENT IS NULL) THEN 0 ELSE ENCAISSEMENT.ENCAISSEMENT END AS Reglement,
		
		CASE WHEN (@Type = 0 ) THEN (
										SELECT      (CASE WHEN SUM(dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Vente, Ristourne_Client, Cumuler_Ristourne))  IS NULL THEN 0 ELSE SUM(dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Vente, Ristourne_Client, Cumuler_Ristourne)) END) Montant
										FROM         TP_Vente Vente INNER JOIN TP_Vente_Liquide Vente_Detail 
										ON Vente.ID_Vente = Vente_Detail.ID_Vente
										WHERE Etat = 1
										AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
													AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))
								   ) ELSE (
								        SELECT        (CASE WHEN SUM(dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Achat, 0, 0)) IS NULL THEN 0 ELSE SUM(dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Achat, 0, 0)) END) Montant
										FROM         TP_Commande Commande INNER JOIN TP_Commande_Liquide Commande_Detail 
										ON Commande.ID_Commande = Commande_Detail.ID_Commande 
										WHERE Etat = 1
										AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
													AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))
								   ) END AS Part
		
		FROM
		(SELECT ID_Element, SUM(Montant) AS Montant
			FROM (				
					--COMMANDES
					SELECT       Commande.ID_Fournisseur ID_Element, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Achat, 0, 0) Montant
					FROM         TP_Commande Commande INNER JOIN TP_Commande_Liquide Commande_Detail 
					ON Commande.ID_Commande = Commande_Detail.ID_Commande 
					WHERE Etat = (CASE @Type WHEN 0 THEN  10  ELSE  1 END)
					AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))

					UNION ALL

					--VENTE
					SELECT       Vente.ID_Client ID_Element, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Vente, Ristourne_Client, Cumuler_Ristourne) Montant
					FROM         TP_Vente Vente INNER JOIN TP_Vente_Liquide Vente_Detail 
					ON Vente.ID_Vente = Vente_Detail.ID_Vente
					WHERE Etat = (CASE @Type WHEN 0 THEN  1  ELSE  10 END)
					AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))

					UNION ALL

					--Client
					SELECT       ID_Client ID_Element, 0 Montant
					FROM         TP_Client 
					WHERE Actif = (CASE @Type WHEN 0 THEN  1  ELSE  10 END)

					UNION ALL

					--Fournisseur
					SELECT       ID_Fournisseur ID_Element, 0 Montant
					FROM         TP_Fournisseur 
					WHERE Actif = (CASE @Type WHEN 0 THEN  10  ELSE  1 END)
				)
			AS derivedtbl_1
			GROUP BY ID_Element) AS StockReel LEFT OUTER JOIN		
			(  
			  SELECT ID_Element, ENCAISSEMENT
			   FROM
			     (
			       SELECT ID_Element, SUM(Montant) AS ENCAISSEMENT
			
						FROM (
								--DECAISSEMENT
					SELECT       ID_Element, Montant
					FROM         TP_Decaissement 
					WHERE Etat = (CASE @Type WHEN 0 THEN  10  ELSE  1 END)
					AND Type_Element = 0 AND (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))

					UNION ALL

					--ENCAISSEMENT
					SELECT       ID_Client ID_Element, Montant
					FROM         TP_Encaissement 
					WHERE Etat = (CASE @Type WHEN 0 THEN  1  ELSE  10 END)
					AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))
				    ) AS ENCAISSEMENT_2
						GROUP BY ID_Element
			      ) AS ENCAISSEMENT_1
			) AS ENCAISSEMENT ON StockReel.ID_Element  = ENCAISSEMENT.ID_Element
   ) AS derivedtbl_2               
RETURN ;
END








GO
/****** Object:  UserDefinedFunction [dbo].[FN_Stat_Commande_Produit]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Stat_Commande_Produit]
(
   -- Type == 0 Liquide, Type == 1 Emballage
   @Date_Debut date, @Date_Fin date, @Type int
)
RETURNS @FN_Stat_Vente_Produit TABLE
(
	ID_Produit int NOT NULL,
	Produit nvarchar(50) NOT NULL,
    Montant float  NULL,
	Quantite_Casier float  NOT NULL,
	Quantite_Plastique float  NOT NULL,
	Quantite_Bouteille float  NOT NULL,
	Quantite_Casier_Temp float NOT NULL,
	Quantite_Plastique_Temp float NOT NULL,
	Quantite_Bouteille_Temp float NOT NULL,
	Conditionnement int NOT NULL
)
AS
BEGIN 
    INSERT INTO @FN_Stat_Vente_Produit (ID_Produit, Produit, Montant, Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Plastique_Temp, Quantite_Bouteille_Temp)
    
    SELECT ID_Produit, Produit, Montant, Quantite_Casier,  Quantite_Plastique, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Plastique_Temp, Quantite_Bouteille_Temp        
    FROM		
		(	
		SELECT RESULTAT.ID_Produit ID_Produit, RESULTAT.Conditionnement Conditionnement,
		(CASE WHEN (@Type = 0) THEN (SELECT Libelle FROM TP_Produit WHERE ID_Produit = RESULTAT.ID_Produit) 
	     ELSE (SELECT Description FROM TP_Emballage WHERE ID_Emballage = RESULTAT.ID_Produit) END) AS Produit,
	    CASE WHEN (RESULTAT.Montant IS NULL) THEN 0 ELSE RESULTAT.Montant END AS Montant,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier,
		CASE WHEN (RESULTAT.Quantite_Plastique IS NULL) THEN 0 ELSE RESULTAT.Quantite_Plastique END AS Quantite_Plastique,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier_Temp,
		CASE WHEN (RESULTAT.Quantite_Plastique IS NULL) THEN 0 ELSE RESULTAT.Quantite_Plastique END AS Quantite_Plastique_Temp,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille_Temp
		FROM (SELECT ID_Produit, SUM(Montant) AS Montant, SUM(Quantite_Casier) AS Quantite_Casier, SUM(Quantite_Plastique) AS Quantite_Plastique, SUM(Quantite_Bouteille) AS Quantite_Bouteille, Conditionnement
			FROM (					
					--COMMANDE LIQUIDE
					SELECT       ID_Produit, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Achat, 0, 0) Montant,
					             Quantite_Casier_Liquide Quantite_Casier, 0 Quantite_Plastique, Quantite_Bouteille_Liquide Quantite_Bouteille, Conditionnement
					FROM         TP_Commande Commande INNER JOIN TP_Commande_Liquide Detail
					ON           Commande.ID_Commande = Detail.ID_Commande
					WHERE Etat = (CASE @Type WHEN 0 THEN 1 ELSE 10 END)
					AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))

				 --   UNION ALL
					----COMMANDE EMBALLAGE
					--SELECT       ID_Emballage ID_Produit, dbo.FN_Prix_Emballage(Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) Montant,
					--             Quantite_Casier, Quantite_Plastique, Quantite_Bouteille
					--FROM         TP_Commande Commande INNER JOIN TP_Commande_Emballage Detail
					--ON          Commande.ID_Commande = Detail.ID_Commande
					--WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END)
					--AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
					--	        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))
				)
			AS RESULTAT_1
			GROUP BY ID_Produit, Conditionnement) AS RESULTAT 
   ) AS derivedtbl_2
   
    DECLARE @ID AS INT, @Casier AS INT, @Bouteille AS INT, @Contenance AS INT, @Nombre_Total_Bouteil AS INT, @NombreCasierPlein AS INT, @NombreBouteil_Restant AS INT
    DECLARE Stock_Cursor CURSOR FOR
	SELECT ID_Produit, Quantite_Casier_Temp, Quantite_Bouteille_Temp, Conditionnement FROM @FN_Stat_Vente_Produit;
	OPEN Stock_Cursor;
	FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
	WHILE @@FETCH_STATUS = 0
		BEGIN
		SET @Nombre_Total_Bouteil = @Casier * @Contenance + @Bouteille;
		SET @NombreCasierPlein = @Nombre_Total_Bouteil / @Contenance;
		SET @NombreBouteil_Restant = @Nombre_Total_Bouteil % @Contenance;
		update @FN_Stat_Vente_Produit set Quantite_Casier = @NombreCasierPlein, Quantite_Bouteille = @NombreBouteil_Restant   where ID_Produit = @ID;		
		FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
		END;
	CLOSE Stock_Cursor;
	DEALLOCATE Stock_Cursor;   
                 
RETURN ;
END









GO
/****** Object:  UserDefinedFunction [dbo].[FN_Stat_EvolutionCapital]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Stat_EvolutionCapital] ()
RETURNS @Tab_Solde TABLE
(
	Date date NOT NULL,    
    Montant float NOT NULL,
	Entree float NOT NULL,
	Sortie float NOT NULL,
	QT_Montant float NOT NULL,
    Montant_Porgressif float NOT NULL,
	Rang int NOT NULL
)
AS
BEGIN 
    INSERT INTO @Tab_Solde (Date, Montant, Entree, Sortie, QT_Montant, Montant_Porgressif, Rang)   
    SELECT Date, Montant, Entree, Sortie, QT_Montant, Montant_Porgressif, Rang   
    FROM		
		(	
		SELECT StockReel.Date Date,	    
	    CASE WHEN (StockReel.Montant IS NULL) THEN 0 ELSE StockReel.Montant END AS Montant,
	    CASE WHEN (StockReel.QT_Montant IS NULL) THEN 0 ELSE StockReel.QT_Montant END AS QT_Montant,
	    CASE WHEN (StockReel.QT_Montant IS NULL) THEN 0 ELSE StockReel.QT_Montant END AS Montant_Porgressif,
		CASE WHEN (StockReel.Rang IS NULL) THEN 0 ELSE StockReel.Rang END AS Rang,
		CASE WHEN (ENTREE.ENTREE IS NULL) THEN 0 ELSE ENTREE.ENTREE END AS Entree,
		CASE WHEN (SORTIES.SORTIES IS NULL) THEN 0 ELSE SORTIES.SORTIES END AS Sortie
		FROM 
		(SELECT Date, SUM(Montant) Montant, SUM(Montant) QT_Montant, ROW_NUMBER() OVER (ORDER BY Date) AS Rang
			FROM (				
					--- Commande
					SELECT  CONVERT(DATE, Date, 102) Date,  Montant
					FROM FN_Commande() 
					WHERE Etat =  1							 									
				 				
					UNION ALL 
				
			    	--- Vente
					SELECT   CONVERT(DATE, Date, 102) Date, - Montant_Facture Montant
					FROM FN_Vente() 
					WHERE Etat = 1

					UNION ALL 
				
				    --- Reguarisation Stock Emballage
					SELECT   CONVERT(DATE, Date, 102)  Date, [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) Montant
					FROM dbo.TP_Regulation_Stock Regulation_Stock INNER JOIN TP_Emballage Emballage
					ON Regulation_Stock.ID_Element = Emballage.ID_Emballage
					WHERE Etat = 1 AND Type_Element = 1

					UNION ALL
				
			        --- Regularisarion Stock Liquide
					SELECT CONVERT(DATE, Date, 102) Date, [dbo].[FN_Prix_Vente_Achat_Detail](Quantite_Casier, Quantite_Bouteille, Conditionnement, PU_Vente, 0, 0) Montant
					FROM  dbo.TP_Regulation_Stock Regulation_Stock INNER JOIN TP_Produit Produit
					ON Regulation_Stock.ID_Element = Produit.ID_Produit INNER JOIN TP_Emballage Emballage
					ON Produit.ID_Emballage = Emballage.ID_Emballage
					WHERE Etat = 1 AND Type_Element = 0
				
					UNION ALL 
				
				--- Retour Emballage client 
					SELECT   CONVERT(DATE, Date, 102)  Date, [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) Montant
					FROM dbo.TP_Retour_Emballage AS Retour INNER JOIN (SELECT ID_Retour_Emballage, PU_Plastique, PU_Bouteille, Conditionnement, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
																		 FROM TP_Retour_Emballage_Detail
																		 GROUP BY ID_Retour_Emballage, PU_Plastique, PU_Bouteille, Conditionnement) AS Detail
					ON Retour.ID_Retour_Emballage = Detail.ID_Retour_Emballage  
					WHERE Etat = 1 AND Type_Element = 1
				
					UNION ALL 
				
				--- Retour emballage fournisseur
					SELECT   CONVERT(DATE, Date, 102)  Date, - [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) Montant
					FROM dbo.TP_Retour_Emballage AS Retour INNER JOIN (SELECT ID_Retour_Emballage, Conditionnement, PU_Plastique, PU_Bouteille, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
																	 FROM TP_Retour_Emballage_Detail
																	 GROUP BY ID_Retour_Emballage, PU_Plastique, PU_Bouteille, Conditionnement) AS Detail
					ON Retour.ID_Retour_Emballage = Detail.ID_Retour_Emballage  INNER JOIN TP_Fournisseur Fournisseur 
					ON Retour.ID_Element = Fournisseur.ID_Fournisseur
					WHERE Etat = 1 AND Type_Element = 0
				
					UNION ALL 
				
				--- Inventaire Liquide
					SELECT  CONVERT(DATE, Date_Debut, 102)  Date, [dbo].[FN_Prix_Vente_Achat_Detail](Quantite_Casier, Quantite_Bouteille, Conditionnement, PU_Casier_Bouteille, 0, 0) Montant
					FROM dbo.TP_Inventaire AS Inventaire INNER JOIN (SELECT ID_Inventaire, PU_Casier_Bouteille, Emballage.Conditionnement Conditionnement, SUM(Quantite_Casier_Reel - Quantite_Casier_Theorique) Quantite_Casier, 
																		  0 Quantite_Plastique, SUM(Quantite_Bouteille_Reel - Quantite_Bouteille_Theorique) Quantite_Bouteille
																	 FROM TP_Inventaire_Detail Inventaire_Detail INNER JOIN TP_Produit Produit
																	 ON Inventaire_Detail.ID_Element = Produit.ID_Produit INNER JOIN TP_Emballage Emballage
																	 ON Produit.ID_Emballage = Emballage.ID_Emballage
																	 GROUP BY ID_Inventaire, PU_Casier_Bouteille, Emballage.Conditionnement) AS Detail
					ON Inventaire.ID_Inventaire = Detail.ID_Inventaire
					WHERE Etat = 2 AND Type = 0 

					UNION ALL 
				
				--- Inventaire Emballage
					SELECT  CONVERT(DATE, Date_Debut, 102) Date, [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Casier_Bouteille, Conditionnement) Montant
					FROM dbo.TP_Inventaire AS Inventaire INNER JOIN (SELECT ID_Inventaire, Inventaire_Detail.PU_Plastique PU_Plastique, PU_Casier_Bouteille, Emballage.Conditionnement Conditionnement, SUM(Quantite_Casier_Reel - Quantite_Casier_Theorique) Quantite_Casier, 
																		  SUM(Quantite_Plastique_Reel - Quantite_Plastique_Theorique) Quantite_Plastique, 
																		  SUM(Quantite_Bouteille_Reel - Quantite_Bouteille_Theorique) Quantite_Bouteille
																	 FROM TP_Inventaire_Detail Inventaire_Detail INNER JOIN TP_Emballage Emballage
																	 ON Inventaire_Detail.ID_Element = Emballage.ID_Emballage
																	 GROUP BY ID_Inventaire, Inventaire_Detail.PU_Plastique, PU_Casier_Bouteille, Emballage.Conditionnement) AS Detail
					ON Inventaire.ID_Inventaire = Detail.ID_Inventaire
					WHERE Etat = 2 AND Type = 1

					UNION ALL 
					--Depense
					SELECT   CONVERT(DATE, Date, 102)  Date , - Montant  Montant
					FROM         dbo.TP_Decaissement
					WHERE (Etat = 1)
					
					UNION ALL
					--Encaissement
					SELECT    CONVERT(DATE, Date, 102)   Date,  Montant
					FROM         dbo.TP_Encaissement 
					WHERE (Etat = 1) 
					
					UNION ALL
					--Regulation
					SELECT     CONVERT(DATE, Date, 102)  Date,  Montant
					FROM         dbo.TP_Regulation_Solde 
					WHERE (Etat = 1) AND Type_Element = 2 
									 
					UNION ALL 
						--Ristourne 
					SELECT   CONVERT(DATE, Date, 102)  Date, - Montant Montant
					FROM         dbo.TP_Decaissement
					WHERE (Etat = 1) AND Type_Element = 1
					
					UNION ALL 
						--Decaissement 
					SELECT   CONVERT(DATE, Date, 102)  Date, - Montant Montant
					FROM         dbo.TP_Decaissement
					WHERE (Etat = 1) AND Type_Element = 0 

					UNION ALL 
						--Vente  
					SELECT    CONVERT(DATE, Date, 102)  Date, Montant_Verser Montant
					FROM         dbo.FN_Vente()
					WHERE (Etat = 1) 
				)
			AS derivedtbl_1
			GROUP BY CONVERT(DATE, Date, 102)) AS StockReel LEFT OUTER JOIN		
			(  
			  SELECT Date, SORTIES
			   FROM
			     (
			       SELECT Date ,SUM(Montant) AS SORTIES
			     --SORTIES
						FROM (
								 			
							--- Vente
								SELECT   CONVERT(DATE, Date, 102) Date, Montant_Facture Montant
								FROM FN_Vente() 
								WHERE Etat = 1

								UNION ALL 

							--- Retour emballage fournisseur
								SELECT   CONVERT(DATE, Date, 102)  Date, [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) Montant
								FROM dbo.TP_Retour_Emballage AS Retour INNER JOIN (SELECT ID_Retour_Emballage, Conditionnement, PU_Plastique, PU_Bouteille, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
																				 FROM TP_Retour_Emballage_Detail
																				 GROUP BY ID_Retour_Emballage, PU_Plastique, PU_Bouteille, Conditionnement) AS Detail
								ON Retour.ID_Retour_Emballage = Detail.ID_Retour_Emballage  INNER JOIN TP_Fournisseur Fournisseur 
								ON Retour.ID_Element = Fournisseur.ID_Fournisseur
								WHERE Etat = 1 AND Type_Element = 0

								UNION ALL 
								--Depense
								SELECT   CONVERT(DATE, Date, 102)  Date , Montant  Montant
								FROM         dbo.TP_Decaissement
								WHERE (Etat = 1)

								UNION ALL 
								--Ristourne 
								SELECT   CONVERT(DATE, Date, 102)  Date, Montant Montant
								FROM         dbo.TP_Decaissement
								WHERE (Etat = 1) AND Type_Element = 1

								UNION ALL 
								--Decaissement 
								SELECT   CONVERT(DATE, Date, 102)  Date, Montant Montant
								FROM         dbo.TP_Decaissement
								WHERE (Etat = 1) AND Type_Element = 0 

								UNION ALL 
								 --- Reguarisation Stock Emballage
								SELECT   CONVERT(DATE, Date, 102)  Date, -[dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) Montant
								FROM dbo.TP_Regulation_Stock Regulation_Stock INNER JOIN TP_Emballage Emballage
								ON Regulation_Stock.ID_Element = Emballage.ID_Emballage
								WHERE Etat = 1 AND Type_Element = 1 AND [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) < 0

								UNION ALL
				
								--- Regularisarion Stock Liquide
								SELECT CONVERT(DATE, Date, 102) Date, -[dbo].[FN_Prix_Vente_Achat_Detail](Quantite_Casier, Quantite_Bouteille, Conditionnement, PU_Vente, 0, 0) Montant
								FROM  dbo.TP_Regulation_Stock Regulation_Stock INNER JOIN TP_Produit Produit
								ON Regulation_Stock.ID_Element = Produit.ID_Produit INNER JOIN TP_Emballage Emballage
								ON Produit.ID_Emballage = Emballage.ID_Emballage
								WHERE Etat = 1 AND Type_Element = 0 AND [dbo].[FN_Prix_Vente_Achat_Detail](Quantite_Casier, Quantite_Bouteille, Conditionnement, PU_Vente, 0, 0) < 0

							   UNION ALL 
				
							--- Inventaire Liquide
								SELECT  CONVERT(DATE, Date_Debut, 102)  Date, -[dbo].[FN_Prix_Vente_Achat_Detail](Quantite_Casier, Quantite_Bouteille, Conditionnement, PU_Casier_Bouteille, 0, 0) Montant
								FROM dbo.TP_Inventaire AS Inventaire INNER JOIN (SELECT ID_Inventaire, PU_Casier_Bouteille, Emballage.Conditionnement Conditionnement, SUM(Quantite_Casier_Reel - Quantite_Casier_Theorique) Quantite_Casier, 
																					  0 Quantite_Plastique, SUM(Quantite_Bouteille_Reel - Quantite_Bouteille_Theorique) Quantite_Bouteille
																				 FROM TP_Inventaire_Detail Inventaire_Detail INNER JOIN TP_Produit Produit
																				 ON Produit.ID_Produit = Inventaire_Detail.ID_Element INNER JOIN TP_Emballage Emballage
																				 ON  Produit.ID_Emballage = Emballage.ID_Emballage
																				 GROUP BY ID_Inventaire, PU_Casier_Bouteille, Emballage.Conditionnement) AS Detail
								ON Inventaire.ID_Inventaire = Detail.ID_Inventaire
								WHERE Etat = 2 AND Type = 0 AND [dbo].[FN_Prix_Vente_Achat_Detail](Quantite_Casier, Quantite_Bouteille, Conditionnement, PU_Casier_Bouteille, 0, 0) < 0

								UNION ALL 
				
							--- Inventaire Emballage
								SELECT  CONVERT(DATE, Date_Debut, 102) Date, -[dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Casier_Bouteille, Conditionnement) Montant
								FROM dbo.TP_Inventaire AS Inventaire INNER JOIN (SELECT ID_Inventaire, Inventaire_Detail.PU_Plastique PU_Plastique, PU_Casier_Bouteille, Emballage.Conditionnement Conditionnement, SUM(Quantite_Casier_Reel - Quantite_Casier_Theorique) Quantite_Casier, 
																					  SUM(Quantite_Plastique_Reel - Quantite_Plastique_Theorique) Quantite_Plastique, 
																					  SUM(Quantite_Bouteille_Reel - Quantite_Bouteille_Theorique) Quantite_Bouteille
																				 FROM TP_Inventaire_Detail Inventaire_Detail INNER JOIN TP_Emballage Emballage
																				 ON Inventaire_Detail.ID_Element = Emballage.ID_Emballage
																				 GROUP BY ID_Inventaire, Inventaire_Detail.PU_Plastique, PU_Casier_Bouteille, Emballage.Conditionnement) AS Detail
								ON Inventaire.ID_Inventaire = Detail.ID_Inventaire
								WHERE Etat = 2 AND Type = 1 AND [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Casier_Bouteille, Conditionnement) < 0

								UNION ALL
								--Regulation
								SELECT     CONVERT(DATE, Date, 102)  Date,  -Montant
								FROM         dbo.TP_Regulation_Solde 
								WHERE (Etat = 1) AND Type_Element = 2 AND Montant < 0

							 ) AS SORTIES_2
						GROUP BY Date
			      ) AS SORTIES_1
			) AS SORTIES ON StockReel.Date  = SORTIES.Date LEFT OUTER JOIN
			(  
			  SELECT Date, ENTREE
			   FROM
			     (
			       SELECT Date ,SUM(Montant) AS ENTREE
			     --ENTREE
						FROM (
							    --- Commande
								SELECT  CONVERT(DATE, Date, 102) Date,  Montant
								FROM FN_Commande() 
								WHERE Etat =  1	

								UNION ALL 
				
							--- Retour Emballage client 
								SELECT   CONVERT(DATE, Date, 102)  Date, [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) Montant
								FROM dbo.TP_Retour_Emballage AS Retour INNER JOIN (SELECT ID_Retour_Emballage, PU_Plastique, PU_Bouteille, Conditionnement, SUM(Quantite_Casier) Quantite_Casier, SUM(Quantite_Plastique) Quantite_Plastique, SUM(Quantite_Bouteille) Quantite_Bouteille
																					 FROM TP_Retour_Emballage_Detail
																					 GROUP BY ID_Retour_Emballage, PU_Plastique, PU_Bouteille, Conditionnement) AS Detail
								ON Retour.ID_Retour_Emballage = Detail.ID_Retour_Emballage  
								WHERE Etat = 1 AND Type_Element = 1
				             	
								UNION ALL

								--Encaissement
								SELECT    CONVERT(DATE, Date, 102)   Date,  Montant
								FROM         dbo.TP_Encaissement 
								WHERE (Etat = 1) 

								UNION ALL 

								--Vente  
								SELECT    CONVERT(DATE, Date, 102)  Date, Montant_Verser Montant
								FROM         dbo.FN_Vente()
								WHERE (Etat = 1) 

								UNION ALL 
								 --- Reguarisation Stock Emballage
								SELECT   CONVERT(DATE, Date, 102)  Date, [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) Montant
								FROM dbo.TP_Regulation_Stock Regulation_Stock INNER JOIN TP_Emballage Emballage
								ON Regulation_Stock.ID_Element = Emballage.ID_Emballage
								WHERE Etat = 1 AND Type_Element = 1 AND [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) > 0

								UNION ALL
				
								--- Regularisarion Stock Liquide
								SELECT CONVERT(DATE, Date, 102) Date, [dbo].[FN_Prix_Vente_Achat_Detail](Quantite_Casier, Quantite_Bouteille, Conditionnement, PU_Vente, 0, 0) Montant
								FROM  dbo.TP_Regulation_Stock Regulation_Stock INNER JOIN TP_Produit Produit
								ON Regulation_Stock.ID_Element = Produit.ID_Produit INNER JOIN TP_Emballage Emballage
								ON Produit.ID_Emballage = Emballage.ID_Emballage
								WHERE Etat = 1 AND Type_Element = 0 AND [dbo].[FN_Prix_Vente_Achat_Detail](Quantite_Casier, Quantite_Bouteille, Conditionnement, PU_Vente, 0, 0) > 0

							   UNION ALL 
				
							--- Inventaire Liquide
								SELECT  CONVERT(DATE, Date_Debut, 102)  Date, [dbo].[FN_Prix_Vente_Achat_Detail](Quantite_Casier, Quantite_Bouteille, Conditionnement, PU_Casier_Bouteille, 0, 0) Montant
								FROM dbo.TP_Inventaire AS Inventaire INNER JOIN (SELECT ID_Inventaire, PU_Casier_Bouteille, Emballage.Conditionnement Conditionnement, SUM(Quantite_Casier_Reel - Quantite_Casier_Theorique) Quantite_Casier, 
																					  0 Quantite_Plastique, SUM(Quantite_Bouteille_Reel - Quantite_Bouteille_Theorique) Quantite_Bouteille
																				 FROM TP_Inventaire_Detail Inventaire_Detail INNER JOIN TP_Produit Produit
																				 ON Produit.ID_Produit = Inventaire_Detail.ID_Element INNER JOIN TP_Emballage Emballage
																				 ON  Produit.ID_Emballage = Emballage.ID_Emballage
																				 GROUP BY ID_Inventaire, PU_Casier_Bouteille, Emballage.Conditionnement) AS Detail
								ON Inventaire.ID_Inventaire = Detail.ID_Inventaire
								WHERE Etat = 2 AND Type = 0 AND [dbo].[FN_Prix_Vente_Achat_Detail](Quantite_Casier, Quantite_Bouteille, Conditionnement, PU_Casier_Bouteille, 0, 0) > 0

								UNION ALL 
				
							--- Inventaire Emballage
								SELECT  CONVERT(DATE, Date_Debut, 102) Date, [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Casier_Bouteille, Conditionnement) Montant
								FROM dbo.TP_Inventaire AS Inventaire INNER JOIN (SELECT ID_Inventaire, Inventaire_Detail.PU_Plastique PU_Plastique, PU_Casier_Bouteille, Emballage.Conditionnement Conditionnement, SUM(Quantite_Casier_Reel - Quantite_Casier_Theorique) Quantite_Casier, 
																					  SUM(Quantite_Plastique_Reel - Quantite_Plastique_Theorique) Quantite_Plastique, 
																					  SUM(Quantite_Bouteille_Reel - Quantite_Bouteille_Theorique) Quantite_Bouteille
																				 FROM TP_Inventaire_Detail Inventaire_Detail INNER JOIN TP_Emballage Emballage
																				 ON Inventaire_Detail.ID_Element = Emballage.ID_Emballage
																				 GROUP BY ID_Inventaire, Inventaire_Detail.PU_Plastique, PU_Casier_Bouteille, Emballage.Conditionnement) AS Detail
								ON Inventaire.ID_Inventaire = Detail.ID_Inventaire
								WHERE Etat = 2 AND Type = 1 AND [dbo].[FN_Prix_Emballage](Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Casier_Bouteille, Conditionnement) > 0

								UNION ALL
								--Regulation
								SELECT     CONVERT(DATE, Date, 102)  Date,  Montant
								FROM         dbo.TP_Regulation_Solde 
								WHERE (Etat = 1) AND Type_Element = 2 AND Montant > 0

							 ) AS ENTREE_2
						GROUP BY Date
			      ) AS ENTREE_1
			) AS ENTREE ON StockReel.Date  = ENTREE.Date 			
   ) AS derivedtbl_2 
   
   DECLARE @Rang as int, @QT_Livrer as float ,@ID as Nvarchar(10), @Montant_Porgressif as float
                                             
	DECLARE Historique_Cursor CURSOR FOR
	SELECT Rang, QT_Montant FROM @Tab_Solde;

	OPEN Historique_Cursor;
	FETCH NEXT FROM Historique_Cursor into @Rang, @QT_Livrer;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
		if (@Rang = 1)
			update @Tab_Solde set Montant_Porgressif = QT_Montant  where Rang = @Rang ;
		else
		begin
			select @Montant_Porgressif = Montant_Porgressif from @Tab_Solde where Rang = @Rang - 1;
			update @Tab_Solde set Montant_Porgressif = QT_Montant + @Montant_Porgressif where Rang = @Rang ;
		end
		FETCH NEXT FROM Historique_Cursor into  @Rang, @QT_Livrer;
	   END;
	CLOSE Historique_Cursor;
	DEALLOCATE Historique_Cursor;
	           
RETURN ;
END











GO
/****** Object:  UserDefinedFunction [dbo].[FN_Stat_Mouvement_Stock]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Stat_Mouvement_Stock]
(
   @Date_Debut date, @Date_Fin date
)
RETURNS @FN_Stat_Vente_Produit TABLE
(
	ID_Produit int NOT NULL,
	Produit nvarchar(50) NOT NULL,
	Code nvarchar(50) NOT NULL,
    PU_Achat float  NULL,
	PU_Vente float  NULL,

	Quantite_Casier_Cmd int  NOT NULL,
	Quantite_Bouteille_Cmd int  NOT NULL,
	Valeur_Cmd float  NOT NULL,

	Quantite_Casier_Vnt int  NOT NULL,
	Quantite_Bouteille_Vnt int  NOT NULL,
	Valeur_Vnt float  NOT NULL,

	Innitial_Quantite_Casier_Stk int  NOT NULL,
	Innitial_Quantite_Bouteille_Stk int  NOT NULL,
	Innitial_Valeur_Stk float  NOT NULL,

	Quantite_Casier_Stk int  NOT NULL,
	Quantite_Bouteille_Stk int  NOT NULL,
	Valeur_Stk float  NOT NULL,

	Conditionnement int  NOT NULL
)
AS
BEGIN 
    INSERT INTO @FN_Stat_Vente_Produit (ID_Produit, Produit, Code, Conditionnement, PU_Achat, PU_Vente, Quantite_Casier_Vnt, Quantite_Bouteille_Vnt,  Valeur_Vnt, Quantite_Casier_Cmd, Quantite_Bouteille_Cmd, Valeur_Cmd, Quantite_Casier_Stk, Quantite_Bouteille_Stk, Valeur_Stk, Innitial_Quantite_Casier_Stk, Innitial_Quantite_Bouteille_Stk, Innitial_Valeur_Stk)
    
    SELECT 
		  ID_Produit, Produit, Code, Conditionnement, PU_Achat, PU_Vente, Quantite_Casier_Vnt, Quantite_Bouteille_Vnt,  Valeur_Vnt, Quantite_Casier_Cmd, Quantite_Bouteille_Cmd, Valeur_Cmd, Quantite_Casier_Stk, Quantite_Bouteille_Stk, Valeur_Stk,  Innitial_Quantite_Casier_Stk, Innitial_Quantite_Bouteille_Stk, Innitial_Valeur_Stk 
    FROM		
		(	
		SELECT STOCK.ID_Produit ID_Produit, 
		(SELECT Libelle FROM TP_Produit WHERE ID_Produit = STOCK.ID_Produit) AS Produit,
		(SELECT Conditionnement FROM TP_Produit Produit INNER JOIN TP_Emballage Emballage ON Produit.ID_Emballage = Emballage.ID_Emballage WHERE ID_Produit = STOCK.ID_Produit) AS Conditionnement,
		(SELECT Code FROM TP_Produit WHERE ID_Produit = STOCK.ID_Produit) AS Code,
		(SELECT PU_Achat FROM TP_Produit WHERE ID_Produit = STOCK.ID_Produit) AS PU_Achat,
		(SELECT PU_Vente FROM TP_Produit WHERE ID_Produit = STOCK.ID_Produit) AS PU_Vente,
	   
		CASE WHEN (VENTE.Quantite_Casier IS NULL) THEN 0 ELSE VENTE.Quantite_Casier END AS Quantite_Casier_Vnt,
		CASE WHEN (VENTE.Quantite_Bouteille IS NULL) THEN 0 ELSE VENTE.Quantite_Bouteille END AS Quantite_Bouteille_Vnt,
	    CASE WHEN (VENTE.Montant IS NULL) THEN 0 ELSE VENTE.Montant  END AS Valeur_Vnt,

		CASE WHEN (COMMANDE.Quantite_Casier IS NULL) THEN 0 ELSE COMMANDE.Quantite_Casier END AS Quantite_Casier_Cmd,
		CASE WHEN (COMMANDE.Quantite_Bouteille IS NULL) THEN 0 ELSE COMMANDE.Quantite_Bouteille END AS Quantite_Bouteille_Cmd,
	    CASE WHEN (COMMANDE.Montant IS NULL) THEN 0 ELSE COMMANDE.Montant END AS Valeur_Cmd,

		CASE WHEN (STOCK.Quantite_Casier IS NULL) THEN 0 ELSE STOCK.Quantite_Casier END AS Quantite_Casier_Stk,
		CASE WHEN (STOCK.Quantite_Bouteille IS NULL) THEN 0 ELSE STOCK.Quantite_Bouteille END AS Quantite_Bouteille_Stk,
	    CASE WHEN (STOCK.Montant IS NULL) THEN 0 ELSE STOCK.Montant END AS Valeur_Stk,

		CASE WHEN (INNITIAL.Quantite_Casier IS NULL) THEN 0 ELSE INNITIAL.Quantite_Casier END AS Innitial_Quantite_Casier_Stk,
		CASE WHEN (INNITIAL.Quantite_Bouteille IS NULL) THEN 0 ELSE INNITIAL.Quantite_Bouteille END AS Innitial_Quantite_Bouteille_Stk,
	    CASE WHEN (INNITIAL.Montant IS NULL) THEN 0 ELSE INNITIAL.Montant END AS Innitial_Valeur_Stk

		FROM (SELECT ID_Produit, Quantite_Casier, Quantite_Bouteille, Montant
			FROM 
			 (		
			        ---STOCK FINAL
			        SELECT    ID_Element ID_Produit, Stock_Casier Quantite_Casier, Stock_Bouteille Quantite_Bouteille, Valeur_Final_Vente Montant
					FROM          dbo.FN_Etat_Stock(0, @Date_Fin, @Date_Fin)							
			)
			AS STOCK_1) AS STOCK LEFT OUTER JOIN
			(  
			  SELECT ID_Produit, Quantite_Casier, Quantite_Bouteille, Montant
			   FROM
			     (
			       SELECT ID_Produit, Quantite_Casier, Quantite_Bouteille, Montant
			     ---VENTE
						FROM (
								SELECT       ID_Produit, Quantite_Casier, Quantite_Bouteille, Montant
					            FROM         dbo.FN_Stat_Vente_Produit(@Date_Debut, @Date_Fin, 0)
							 )	AS VENTE_2
				 ) AS VENTE_1
			) AS VENTE ON STOCK.ID_Produit  = VENTE.ID_Produit LEFT OUTER JOIN
			(  
			  SELECT ID_Produit, Quantite_Casier, Quantite_Bouteille, Montant
			   FROM
			     (
			       SELECT ID_Produit, Quantite_Casier, Quantite_Bouteille, Montant
			     ---STOCK INNITIAL
						FROM (
								SELECT       ID_Element ID_Produit, Stock_Casier Quantite_Casier, Stock_Bouteille Quantite_Bouteille, Valeur_Final_Vente Montant
					            FROM         dbo.FN_Etat_Stock(0, @Date_Debut, @Date_Debut)
							 )	AS INNITIAL_2
				 ) AS INNITIAL_1
			) AS INNITIAL ON STOCK.ID_Produit  = INNITIAL.ID_Produit LEFT OUTER JOIN
			(  
			  SELECT ID_Produit, Quantite_Casier, Quantite_Bouteille, Montant
			   FROM
			     (
			       SELECT ID_Produit, Quantite_Casier, Quantite_Bouteille, Montant
			     --COMMANDE LIQUIDE 
						FROM (
								SELECT       ID_Produit, Quantite_Casier, Quantite_Bouteille, Montant
				            	FROM         dbo.FN_Stat_Commande_Produit(@Date_Debut, @Date_Fin, 0)
				   			 )	AS COMMANDE_2
			      ) AS STOCK_1
			) AS COMMANDE ON COMMANDE.ID_Produit  = STOCK.ID_Produit 
   ) AS derivedtbl_2
                
RETURN ;
END













GO
/****** Object:  UserDefinedFunction [dbo].[FN_Stat_Mouvement_Stock_Emballage]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Stat_Mouvement_Stock_Emballage]
(
   @Date_Debut date, @Date_Fin date
)
RETURNS @FN_Stat_Vente_Produit TABLE
(
	ID_Element int NOT NULL,
	Element nvarchar(50) NOT NULL,

	Innitial_Quantite_Casier_Stk int  NOT NULL,
	Innitial_Quantite_Plastique_Stk int  NOT NULL,
	Innitial_Quantite_Bouteille_Stk int  NOT NULL,
	Innitial_Valeur_Stk float  NOT NULL,

	Quantite_Casier_Stk int  NOT NULL,
	Quantite_Plastique_Stk int  NOT NULL,
	Quantite_Bouteille_Stk int  NOT NULL,
	Valeur_Stk float  NOT NULL
)
AS
BEGIN 
    INSERT INTO @FN_Stat_Vente_Produit (ID_Element, Element, Quantite_Casier_Stk, Quantite_Plastique_Stk, Quantite_Bouteille_Stk, Valeur_Stk, Innitial_Quantite_Casier_Stk, Innitial_Quantite_Plastique_Stk, Innitial_Quantite_Bouteille_Stk, Innitial_Valeur_Stk)
    
    SELECT 
		   ID_Element, Element, Quantite_Casier_Stk, Quantite_Plastique_Stk, Quantite_Bouteille_Stk, Valeur_Stk,  Innitial_Quantite_Casier_Stk, Innitial_Quantite_Plastique_Stk, Innitial_Quantite_Bouteille_Stk, Innitial_Valeur_Stk 
    FROM		
		(	
		SELECT STOCK.ID_Element ID_Element, 
		(SELECT Code FROM TP_Emballage WHERE ID_Emballage = STOCK.ID_Element) AS Element,

	    CASE WHEN (STOCK.Quantite_Casier IS NULL) THEN 0 ELSE STOCK.Quantite_Casier END AS Quantite_Casier_Stk,
		CASE WHEN (STOCK.Quantite_Plastique IS NULL) THEN 0 ELSE STOCK.Quantite_Plastique END AS Quantite_Plastique_Stk,
		CASE WHEN (STOCK.Quantite_Bouteille IS NULL) THEN 0 ELSE STOCK.Quantite_Bouteille END AS Quantite_Bouteille_Stk,
	    CASE WHEN (STOCK.Montant IS NULL) THEN 0 ELSE STOCK.Montant END AS Valeur_Stk,

		CASE WHEN (INNITIAL.Quantite_Casier IS NULL) THEN 0 ELSE INNITIAL.Quantite_Casier END AS Innitial_Quantite_Casier_Stk,
		CASE WHEN (INNITIAL.Quantite_Plastique IS NULL) THEN 0 ELSE INNITIAL.Quantite_Plastique END AS Innitial_Quantite_Plastique_Stk,
		CASE WHEN (INNITIAL.Quantite_Bouteille IS NULL) THEN 0 ELSE INNITIAL.Quantite_Bouteille END AS Innitial_Quantite_Bouteille_Stk,
	    CASE WHEN (INNITIAL.Montant IS NULL) THEN 0 ELSE INNITIAL.Montant END AS Innitial_Valeur_Stk

		FROM (SELECT ID_Element, Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, Montant
			FROM 
			 (		
			        ---STOCK FINAL
			        SELECT    ID_Element, Stock_Casier Quantite_Casier,  Stock_Plastique Quantite_Plastique, Stock_Bouteille Quantite_Bouteille, Valeur_Final_Emballage Montant
					FROM          dbo.FN_Etat_Stock(1, @Date_Fin, @Date_Fin)
					WHERE ID_Element <> 4							
			)
			AS STOCK_1) AS STOCK LEFT OUTER JOIN
			
			(  
			  SELECT ID_Element, Quantite_Casier, Quantite_Plastique,  Quantite_Bouteille, Montant
			   FROM
			     (
			       SELECT ID_Element, Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, Montant
			     ---STOCK INNITIAL
						FROM (
								SELECT       ID_Element , Stock_Casier Quantite_Casier, Stock_Plastique Quantite_Plastique, Stock_Bouteille Quantite_Bouteille, Valeur_Final_Emballage Montant
					            FROM         dbo.FN_Etat_Stock(1, @Date_Debut, @Date_Debut)
								WHERE ID_Element <> 4
							 )	AS INNITIAL_2
				 ) AS INNITIAL_1
			) AS INNITIAL ON STOCK.ID_Element  = INNITIAL.ID_Element
   ) AS derivedtbl_2
                
RETURN ;
END













GO
/****** Object:  UserDefinedFunction [dbo].[FN_Stat_Vente_Produit]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Stat_Vente_Produit]
(
   -- Type == 0 Liquide, Type == 1 Emballage
   @Date_Debut date, @Date_Fin date, @Type int
)
RETURNS @FN_Stat_Vente_Produit TABLE
(
	ID_Produit int NOT NULL,
	Produit nvarchar(50) NOT NULL,
    Montant float  NULL,
	Quantite_Casier float  NOT NULL,
	Quantite_Plastique float  NOT NULL,
	Quantite_Bouteille float  NOT NULL,
	Quantite_Casier_Temp float NOT NULL,
	Quantite_Plastique_Temp float NOT NULL,
	Quantite_Bouteille_Temp float NOT NULL,
	Conditionnement int NOT NULL
)
AS
BEGIN 
    INSERT INTO @FN_Stat_Vente_Produit (ID_Produit, Produit, Montant, Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Plastique_Temp, Quantite_Bouteille_Temp)
    
    SELECT ID_Produit, Produit, Montant, Quantite_Casier,  Quantite_Plastique, Quantite_Bouteille, Conditionnement, Quantite_Casier_Temp, Quantite_Plastique_Temp, Quantite_Bouteille_Temp        
    FROM		
		(	
		SELECT RESULTAT.ID_Produit ID_Produit, RESULTAT.Conditionnement Conditionnement,
		(CASE WHEN (@Type = 0) THEN (SELECT Libelle FROM TP_Produit WHERE ID_Produit = RESULTAT.ID_Produit) 
	     ELSE (SELECT Description FROM TP_Emballage WHERE ID_Emballage = RESULTAT.ID_Produit) END) AS Produit,
	    CASE WHEN (RESULTAT.Montant IS NULL) THEN 0 ELSE RESULTAT.Montant END AS Montant,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier,
		CASE WHEN (RESULTAT.Quantite_Plastique IS NULL) THEN 0 ELSE RESULTAT.Quantite_Plastique END AS Quantite_Plastique,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille,
		CASE WHEN (RESULTAT.Quantite_Casier IS NULL) THEN 0 ELSE RESULTAT.Quantite_Casier END AS Quantite_Casier_Temp,
		CASE WHEN (RESULTAT.Quantite_Plastique IS NULL) THEN 0 ELSE RESULTAT.Quantite_Plastique END AS Quantite_Plastique_Temp,
	    CASE WHEN (RESULTAT.Quantite_Bouteille IS NULL) THEN 0 ELSE RESULTAT.Quantite_Bouteille END AS Quantite_Bouteille_Temp
		FROM (SELECT ID_Produit, SUM(Montant) AS Montant, SUM(Quantite_Casier) AS Quantite_Casier, SUM(Quantite_Plastique) AS Quantite_Plastique, SUM(Quantite_Bouteille) AS Quantite_Bouteille, Conditionnement
			FROM (					
					--VENTE LIQUIDE
					SELECT       ID_Produit, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide, Conditionnement, PU_Vente, Detail.Ristourne_Client, Detail.Cumuler_Ristourne) Montant,
					             Quantite_Casier_Liquide Quantite_Casier, 0 Quantite_Plastique, Quantite_Bouteille_Liquide Quantite_Bouteille, Conditionnement
					FROM         TP_Vente Vente INNER JOIN TP_Vente_Liquide Detail
					ON           Vente.ID_Vente = Detail.ID_Vente
					WHERE Etat = (CASE @Type WHEN 0 THEN 1 ELSE 10 END)
					AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
						        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))

				 --   UNION ALL
					----COMMANDE EMBALLAGE
					--SELECT       ID_Emballage ID_Produit, dbo.FN_Prix_Emballage(Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille, Conditionnement) Montant,
					--             Quantite_Casier, Quantite_Plastique, Quantite_Bouteille
					--FROM         TP_Vente Commande INNER JOIN TP_Vente_Emballage Detail
					--ON          Commande.ID_Commande = Detail.ID_Commande
					--WHERE Etat = (CASE @Type WHEN 0 THEN 10 ELSE 1 END)
					--AND   (CAST([Date]  AS DATE) <= CONVERT(DATE, @Date_Fin, 102)) 
					--	        AND (CAST(Date AS DATE) >= CONVERT(DATE, @Date_Debut, 102))
				)
			AS RESULTAT_1
			GROUP BY ID_Produit, Conditionnement) AS RESULTAT 
   ) AS derivedtbl_2
   
    DECLARE @ID AS INT, @Casier AS INT, @Bouteille AS INT, @Contenance AS INT, @Nombre_Total_Bouteil AS INT, @NombreCasierPlein AS INT, @NombreBouteil_Restant AS INT
    DECLARE Stock_Cursor CURSOR FOR
	SELECT ID_Produit, Quantite_Casier_Temp, Quantite_Bouteille_Temp, Conditionnement FROM @FN_Stat_Vente_Produit;
	OPEN Stock_Cursor;
	FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
	WHILE @@FETCH_STATUS = 0
		BEGIN
		SET @Nombre_Total_Bouteil = @Casier * @Contenance + @Bouteille;
		SET @NombreCasierPlein = @Nombre_Total_Bouteil / @Contenance;
		SET @NombreBouteil_Restant = @Nombre_Total_Bouteil % @Contenance;
		update @FN_Stat_Vente_Produit set Quantite_Casier = @NombreCasierPlein, Quantite_Bouteille = @NombreBouteil_Restant   where ID_Produit = @ID;		
		FETCH NEXT FROM Stock_Cursor into @ID, @Casier, @Bouteille, @Contenance;
		END;
	CLOSE Stock_Cursor;
	DEALLOCATE Stock_Cursor;   
                 
RETURN ;
END









GO
/****** Object:  UserDefinedFunction [dbo].[FN_Vente]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Author:		TEG-FACTURATION
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FN_Vente]
(	
)
RETURNS @FN_Vente TABLE
(
	ID_Vente bigint NOT NULL,
	ID_Client int NOT NULL,
	Client nvarchar(200),
	ID_Caisse int NOT NULL,
	NUM_DOC nvarchar(50),
    Date  Datetime NOT NULL,
    Etat  int NOT NULL,
	Solde_Liquide float not null,
	Solde_Emballage float not null,
    Montant_Liquide int NOT NULL,
	Montant_Liquide_PU_Achat float not null,
    Montant_Emballage int NOT NULL,
    Montant_Retour_Emballage int NOT NULL,
	Montant_Verser int NOT NULL,
	Montant_Consigne_Emballage int NOT NULL,
	Quantite_Casier int not null,
	Quantite_Bouteille int not null,
	Montant_Facture as (Montant_Consigne_Emballage + Montant_Liquide + Montant_Emballage - Montant_Retour_Emballage)
)
AS
BEGIN 
    INSERT INTO @FN_Vente (ID_Vente, ID_Caisse, NUM_DOC, Date, Etat,Solde_Liquide,Solde_Emballage, Montant_Liquide,Montant_Liquide_PU_Achat, Montant_Emballage, Montant_Retour_Emballage, ID_Client, Client,Montant_Verser,Montant_Consigne_Emballage,Quantite_Casier,Quantite_Bouteille)
    
    SELECT 
          ID_Vente,
          ID_Caisse,   
          NUM_DOC,
          Date,
          Etat,
		  Solde_Liquide,
		  Solde_Emballage,
          Montant_Liquide,
		  Montant_Liquide_PU_Achat,
          Montant_Emballage,
          Montant_Retour_Emballage, 
		  ID_Client,
		  Client,
		  Montant_Verser,
		  Montant_Consigne_Emballage,
		  0,0
    FROM		
		(	
		SELECT StockReel.ID_Vente ID_Vente, 
		StockReel.ID_Client as ID_Client,
		StockReel.ID_Caisse ID_Caisse,
		StockReel.Nom Client,
		StockReel.Solde_Liquide Solde_Liquide,
		StockReel.Solde_Emballage Solde_Emballage, 
		StockReel.Montant Montant_Verser, 
		StockReel.Montant_Consigne_Emballage Montant_Consigne_Emballage, 
		StockReel.NUM_DOC NUM_DOC, 
		StockReel.Date Date, 
		StockReel.Etat Etat,		    
	    CASE WHEN (EMBALLAGE.EMBALLAGE IS NULL) THEN 0 ELSE EMBALLAGE.EMBALLAGE END AS Montant_Emballage,
	    CASE WHEN (LIQUIDE.LIQUIDE IS NULL) THEN 0 ELSE LIQUIDE.LIQUIDE END AS Montant_Liquide,
		CASE WHEN (LIQUIDE_PU_ACHAT.LIQUIDE IS NULL) THEN 0 ELSE LIQUIDE_PU_ACHAT.LIQUIDE END AS Montant_Liquide_PU_Achat,
	    CASE WHEN (RETOUR_EMBALLAGE.RETOUR_EMBALLAGE IS NULL) THEN 0 ELSE RETOUR_EMBALLAGE.RETOUR_EMBALLAGE END AS Montant_Retour_Emballage	    	
		FROM (SELECT ID_Vente , Date, Etat, ID_Client,Nom, ID_Caisse,NUM_DOC,Solde_Liquide, Solde_Emballage,Montant_Consigne_Emballage,Montant,Create_Code_User
		FROM (--Vente
			  select ID_Vente, vente.ID_Client ID_Client, Nom, ID_Caisse, [Date], NUM_DOC, Nom_Client,Solde_Liquide, Solde_Emballage, Montant_Consigne_Emballage, Montant, Etat, vente.Create_Code_User Create_Code_User
			  from TP_Vente vente inner join TP_Client client on vente.ID_Client = client.ID_Client 
			 )AS derivedtbl_1) AS StockReel LEFT OUTER JOIN
			(  
			  SELECT ID_Vente, EMBALLAGE
			   FROM
			     (
			       SELECT ID_Vente ,SUM(Montant) AS EMBALLAGE
			       --VENTE EMBALLAGE 
						FROM (
						       SELECT      vente.ID_Vente ID_Vente, dbo.FN_Prix_Emballage(Quantite_Casier_Emballage, Quantite_Plastique_Emballage, Quantite_Bouteille_Emballage, PU_Plastique, PU_Bouteille, Conditionnement)  Montant
								FROM       dbo.TP_Vente vente inner join dbo.TP_Vente_Liquide emballage on vente.ID_Vente = emballage.ID_Vente					        
							 ) AS EMBALLAGE_2
						GROUP BY ID_Vente
			      ) AS EMBALLAGE_1
			) AS EMBALLAGE ON StockReel.ID_Vente  = EMBALLAGE.ID_Vente LEFT OUTER JOIN					
			(  
			  SELECT ID_Vente, LIQUIDE
			   FROM
			     (
			       SELECT ID_Vente ,SUM(Montant) AS LIQUIDE
			     --VENTE LIQUIDE 
						FROM (
						       SELECT       vente.ID_Vente ID_Vente, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide,Conditionnement, PU_Vente, Ristourne_Client, Cumuler_Ristourne) Montant
								FROM        TP_Vente vente inner join TP_Vente_Liquide liquide
								on vente.ID_Vente = liquide.ID_Vente    
								) AS LIQUIDE_2
						GROUP BY ID_Vente
			      ) AS LIQUIDE_1
			) AS LIQUIDE ON StockReel.ID_Vente  = LIQUIDE.ID_Vente LEFT OUTER JOIN
			(  
			  SELECT ID_Vente, LIQUIDE
			   FROM
			     (
			       SELECT ID_Vente ,SUM(Montant) AS LIQUIDE
			     --VENTE LIQUIDE 
						FROM (
						       SELECT       vente.ID_Vente ID_Vente, dbo.FN_Prix_Vente_Achat_Detail(Quantite_Casier_Liquide, Quantite_Bouteille_Liquide,Conditionnement, PU_Achat, 0, 0) Montant
								FROM        TP_Vente vente inner join TP_Vente_Liquide liquide
								on vente.ID_Vente = liquide.ID_Vente    
								) AS LIQUIDE_PU_ACHAT_2
						GROUP BY ID_Vente
			      ) AS LIQUIDE_1
			) AS LIQUIDE_PU_ACHAT ON StockReel.ID_Vente  = LIQUIDE_PU_ACHAT.ID_Vente LEFT OUTER JOIN
			(  
			  SELECT ID_Vente, RETOUR_EMBALLAGE
			   FROM
			     (
			       SELECT ID_Vente ,SUM(Montant) AS RETOUR_EMBALLAGE
			     --RETOUR_EMBALLAGE 
						FROM (
						       SELECT      Vente.ID_Vente ID_Vente ,dbo.FN_Prix_Emballage(Quantite_Casier, Quantite_Plastique, Quantite_Bouteille, PU_Plastique, PU_Bouteille,Conditionnement) Montant
								FROM       dbo.TP_Vente vente inner join dbo.TP_Vente_Emballage emballage on vente.ID_Vente = emballage.ID_Vente        
								) AS RETOUR_EMBALLAGE_2
						GROUP BY ID_Vente
			      ) AS RETOUR_EMBALLAGE_1
			) AS RETOUR_EMBALLAGE ON StockReel.ID_Vente  = RETOUR_EMBALLAGE.ID_Vente
   ) AS derivedtbl_2     
   
   DECLARE @ID AS INT, @ID_Vente as bigint, @Casier AS INT ,@Plastique AS INT, @Bouteille AS INT, @Contenance AS INT, @C AS INT, @Temp_1 AS INT, @Temp AS INT, @Div AS INT, @Mod AS INT
   DECLARE Stock_Cursor CURSOR FOR
	   SELECT ID_Vente FROM @FN_Vente;
	   OPEN Stock_Cursor;
	   FETCH NEXT FROM Stock_Cursor into @ID_Vente;
		WHILE @@FETCH_STATUS = 0
		   BEGIN
			SELECT @Casier = SUM(Quantite_Casier_Liquide), @Bouteille = SUM(Quantite_Bouteille_Liquide) FROM TP_Vente_Liquide  WHERE ID_Vente = @ID_Vente GROUP BY ID_Vente
			
			update @FN_Vente set Quantite_Casier = @Casier, Quantite_Bouteille = @Bouteille where ID_Vente = @ID_Vente;		
			FETCH NEXT FROM Stock_Cursor into @ID_Vente;
		   END;
		CLOSE Stock_Cursor;
		DEALLOCATE Stock_Cursor;           
RETURN ;
END








GO
/****** Object:  Table [dbo].[TG_Commandes]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Commandes](
	[Code_Commande] [int] IDENTITY(1,1) NOT NULL,
	[Code_Commande_Parent] [int] NOT NULL,
	[Libelle] [nvarchar](50) NOT NULL,
	[ID_Control] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Ordre] [int] NOT NULL,
	[url] [nvarchar](50) NULL,
 CONSTRAINT [TG_Commandes_PK] PRIMARY KEY CLUSTERED 
(
	[Code_Commande] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Corbeille]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Corbeille](
	[ID_Corbeille] [int] IDENTITY(1,1) NOT NULL,
	[ID_Table] [int] NOT NULL,
	[ID_Element] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL,
	[Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
 CONSTRAINT [PK_TG_Corbeille] PRIMARY KEY CLUSTERED 
(
	[ID_Corbeille] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Droits]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Droits](
	[ID_Droit] [int] IDENTITY(1,1) NOT NULL,
	[Code_Profile] [int] NOT NULL,
	[Code_Commande] [int] NOT NULL,
	[Executer] [bit] NOT NULL,
	[Disponible] [bit] NOT NULL,
 CONSTRAINT [PK_TG_Droits] PRIMARY KEY CLUSTERED 
(
	[ID_Droit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Hierarchie_Commande]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Hierarchie_Commande](
	[ID_Hierarchie] [int] IDENTITY(1,1) NOT NULL,
	[Code_Commande] [int] NOT NULL,
	[Code_Commande_Parent] [int] NOT NULL,
 CONSTRAINT [PK_TG_Hierarchie_Commande] PRIMARY KEY CLUSTERED 
(
	[ID_Hierarchie] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Locked_Records]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Locked_Records](
	[ID_Locked] [int] IDENTITY(1,1) NOT NULL,
	[Table_Name] [nvarchar](50) NOT NULL,
	[Record_ID] [int] NOT NULL,
	[Computer_Name] [nvarchar](50) NOT NULL,
	[Lock_Date_Time] [datetime] NOT NULL,
	[Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TG_Locked_Records] PRIMARY KEY CLUSTERED 
(
	[ID_Locked] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Logs]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Logs](
	[Id_Error] [int] IDENTITY(1,1) NOT NULL,
	[Error_value] [nvarchar](max) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Code_user] [int] NOT NULL,
	[Nom_Poste_Travail] [nvarchar](50) NOT NULL,
	[Fonction] [nvarchar](50) NOT NULL,
 CONSTRAINT [TG_Log_PK] PRIMARY KEY CLUSTERED 
(
	[Id_Error] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Personnels]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Personnels](
	[ID_Personnel] [int] IDENTITY(1,1) NOT NULL,
	[ID_Caisse] [int] NOT NULL,
	[Nom] [nvarchar](150) NOT NULL,
	[Prenom] [nvarchar](150) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
	[Display] [bit] NOT NULL,
 CONSTRAINT [TG_Personnels_PK] PRIMARY KEY CLUSTERED 
(
	[ID_Personnel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Profiles]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Profiles](
	[Code_Profile] [int] IDENTITY(1,1) NOT NULL,
	[Libelle] [nvarchar](50) NOT NULL,
	[Display] [bit] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
 CONSTRAINT [TG_Profiles_PK] PRIMARY KEY CLUSTERED 
(
	[Code_Profile] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Profiles_Utilisateurs]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Profiles_Utilisateurs](
	[ID_Profiles_Utilisateur] [int] IDENTITY(1,1) NOT NULL,
	[Code_User] [int] NOT NULL,
	[Code_Profile] [int] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
 CONSTRAINT [TG_Profiles_Utilisateurs_PK] PRIMARY KEY CLUSTERED 
(
	[ID_Profiles_Utilisateur] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Sentinelles]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Sentinelles](
	[ID_Sentinelle] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Code_User] [int] NOT NULL,
	[Action] [int] NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
 CONSTRAINT [TG_Sentinelles_PK] PRIMARY KEY CLUSTERED 
(
	[ID_Sentinelle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Sentinelles_Options]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Sentinelles_Options](
	[ID_Sentinelle] [int] IDENTITY(1,1) NOT NULL,
	[Sentinelle] [bit] NOT NULL,
	[SentinelleConnexion] [bit] NOT NULL,
	[SentinelleEndConnexion] [bit] NOT NULL,
	[SentinelleInsert] [bit] NOT NULL,
	[SentinelleEdit] [bit] NOT NULL,
	[SentinelleDelete] [bit] NOT NULL,
	[SentinellePrint] [bit] NOT NULL,
	[SentinelleLife] [int] NOT NULL,
 CONSTRAINT [TG_Options_Sentinelle_PK] PRIMARY KEY CLUSTERED 
(
	[ID_Sentinelle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Utilisateurs]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TG_Utilisateurs](
	[Code_User] [int] IDENTITY(1,1) NOT NULL,
	[ID_Personnel] [int] NOT NULL,
	[Login] [nvarchar](50) NOT NULL,
	[Somme_Ctrl] [nvarchar](50) NOT NULL,
	[Display] [bit] NOT NULL,
	[Last_Connexion] [datetime] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
 CONSTRAINT [TG_Utilisateurs_PK] PRIMARY KEY CLUSTERED 
(
	[Code_User] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TG_Variable]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TG_Variable](
	[ID_Variable] [int] IDENTITY(1,1) NOT NULL,
	[Nom_Societe] [nvarchar](150) NOT NULL,
	[Numero_Contribuable] [nvarchar](50) NULL,
	[Registre_Commerce] [nvarchar](50) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Adresse] [nvarchar](50) NULL,
	[Description] [nvarchar](150) NULL,
	[Ville] [nvarchar](50) NULL,
	[Logo] [varbinary](max) NULL,
	[Appliquer_TVA] [bit] NULL,
	[TVA] [float] NULL,
	[Gestion_Emballage] [bit] NULL,
	[Verifier_Etat_Stock] [bit] NULL,
	[Cle0] [nvarchar](100) NULL,
	[Cle1] [varbinary](100) NULL,
	[Cle2] [varbinary](100) NULL,
	[Dossier_Sauvegarde] [nvarchar](150) NULL,
	[Password_Sauvegarde] [nvarchar](50) NULL,
 CONSTRAINT [PK_TG_Variable] PRIMARY KEY CLUSTERED 
(
	[ID_Variable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TP_Caisse]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Caisse](
	[ID_Caisse] [int] IDENTITY(1,1) NOT NULL,
	[Libelle] [nvarchar](150) NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
 CONSTRAINT [PK_TP_Caisse] PRIMARY KEY CLUSTERED 
(
	[ID_Caisse] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Client]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Client](
	[ID_Client] [int] IDENTITY(1,1) NOT NULL,
	[Nom] [nvarchar](150) NOT NULL,
	[Adresse] [nvarchar](50) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
 CONSTRAINT [PK_TP_Client] PRIMARY KEY CLUSTERED 
(
	[ID_Client] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Commande]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Commande](
	[ID_Commande] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Fournisseur] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Montant] [float] NULL,
	[NUM_DOC] [nvarchar](50) NULL,
	[Etat] [tinyint] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Commande] PRIMARY KEY CLUSTERED 
(
	[ID_Commande] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Commande_Emballage]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Commande_Emballage](
	[ID_Commande_Emballage] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Commande] [bigint] NOT NULL,
	[ID_Emballage] [int] NOT NULL,
	[PU_Plastique] [float] NULL,
	[PU_Bouteille] [float] NULL,
	[Conditionnement] [int] NULL,
	[Quantite_Casier] [int] NOT NULL,
	[Quantite_Plastique] [int] NOT NULL,
	[Quantite_Bouteille] [int] NOT NULL,
 CONSTRAINT [PK_TP_Commande_Emballage] PRIMARY KEY CLUSTERED 
(
	[ID_Commande_Emballage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Commande_Emballage_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Commande_Emballage_Temp](
	[ID_Commande_Emballage_Temp] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Emballage] [int] NOT NULL,
	[PU_Plastique] [float] NULL,
	[PU_Bouteille] [float] NULL,
	[Conditionnement] [int] NULL,
	[Quantite_Casier] [int] NOT NULL,
	[Quantite_Plastique] [int] NOT NULL,
	[Quantite_Bouteille] [int] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Commande_Emballage_Temp] PRIMARY KEY CLUSTERED 
(
	[ID_Commande_Emballage_Temp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Commande_Liquide]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Commande_Liquide](
	[ID_Commande_Liquide] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Commande] [bigint] NOT NULL,
	[ID_Produit] [int] NOT NULL,
	[PU_Achat] [float] NULL,
	[Conditionnement] [int] NULL,
	[PU_Plastique] [float] NULL,
	[PU_Bouteille] [float] NULL,
	[Quantite_Casier_Liquide] [int] NOT NULL,
	[Quantite_Bouteille_Liquide] [int] NOT NULL,
	[Quantite_Casier_Emballage] [int] NOT NULL,
	[Quantite_Plastique_Emballage] [int] NOT NULL,
	[Quantite_Bouteille_Emballage] [int] NOT NULL,
 CONSTRAINT [PK_TP_Commande_Liquide] PRIMARY KEY CLUSTERED 
(
	[ID_Commande_Liquide] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Commande_Liquide_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Commande_Liquide_Temp](
	[ID_Commande_Liquide_Temp] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Produit] [int] NOT NULL,
	[PU_Achat] [float] NULL,
	[Conditionnement] [int] NULL,
	[PU_Plastique] [float] NULL,
	[PU_Bouteille] [float] NULL,
	[Quantite_Casier_Liquide] [int] NOT NULL,
	[Quantite_Bouteille_Liquide] [int] NOT NULL,
	[Quantite_Casier_Emballage] [int] NOT NULL,
	[Quantite_Plastique_Emballage] [int] NOT NULL,
	[Quantite_Bouteille_Emballage] [int] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Commande_Liquide_Temp] PRIMARY KEY CLUSTERED 
(
	[ID_Commande_Liquide_Temp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Decaissement]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Decaissement](
	[ID_Decaissement] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Caisse] [int] NOT NULL,
	[Type_Element] [tinyint] NOT NULL,
	[ID_Element] [int] NOT NULL,
	[NUM_DOC] [nvarchar](50) NULL,
	[Date] [datetime] NOT NULL,
	[Montant] [float] NOT NULL,
	[Motif] [nvarchar](500) NULL,
	[Etat] [tinyint] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Decaissement] PRIMARY KEY CLUSTERED 
(
	[ID_Decaissement] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Emballage]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Emballage](
	[ID_Emballage] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](150) NOT NULL,
	[Conditionnement] [int] NOT NULL,
	[PU_Plastique] [float] NOT NULL,
	[PU_Bouteille] [float] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
 CONSTRAINT [PK_TP_Emballage] PRIMARY KEY CLUSTERED 
(
	[ID_Emballage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Encaissement]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Encaissement](
	[ID_Encaissement] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Client] [int] NOT NULL,
	[ID_Caissse] [int] NOT NULL,
	[NUM_DOC] [nvarchar](50) NULL,
	[Date] [datetime] NOT NULL,
	[Montant] [float] NOT NULL,
	[Motif] [nvarchar](500) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Etat] [tinyint] NOT NULL,
 CONSTRAINT [PK_TP_Encaissement] PRIMARY KEY CLUSTERED 
(
	[ID_Encaissement] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Famille]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Famille](
	[ID_Famille] [int] IDENTITY(1,1) NOT NULL,
	[Libelle] [nvarchar](50) NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
 CONSTRAINT [PK_TP_Producteur] PRIMARY KEY CLUSTERED 
(
	[ID_Famille] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Fournisseur]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Fournisseur](
	[ID_Fournisseur] [int] IDENTITY(1,1) NOT NULL,
	[Nom] [nvarchar](150) NOT NULL,
	[Adresse] [nvarchar](50) NULL,
	[Telephone] [nvarchar](50) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
 CONSTRAINT [PK_TP_Fournisseur] PRIMARY KEY CLUSTERED 
(
	[ID_Fournisseur] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Inventaire]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Inventaire](
	[ID_Inventaire] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [tinyint] NOT NULL,
	[NUM_DOC] [nvarchar](50) NULL,
	[Date_Debut] [datetime] NOT NULL,
	[Date_Fin] [datetime] NULL,
	[Etat] [tinyint] NOT NULL,
	[Motif] [nvarchar](500) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Inventaire] PRIMARY KEY CLUSTERED 
(
	[ID_Inventaire] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Inventaire_Detail]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Inventaire_Detail](
	[ID_Inventaire_Detail] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Inventaire] [bigint] NOT NULL,
	[ID_Element] [int] NOT NULL,
	[Quantite_Casier_Theorique] [int] NOT NULL,
	[Quantite_Plastique_Theorique] [int] NOT NULL,
	[Quantite_Bouteille_Theorique] [int] NOT NULL,
	[Quantite_Casier_Reel] [int] NOT NULL,
	[Quantite_Plastique_Reel] [int] NOT NULL,
	[Quantite_Bouteille_Reel] [int] NOT NULL,
	[PU_Casier_Bouteille] [float] NOT NULL,
	[PU_Plastique] [float] NOT NULL,
	[Conditionnement] [int] NOT NULL,
 CONSTRAINT [PK_TP_Inventaire_Detail] PRIMARY KEY CLUSTERED 
(
	[ID_Inventaire_Detail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Ouverture_Cloture_Caisse]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Ouverture_Cloture_Caisse](
	[ID_Ouverture_Cloture_Caisse] [int] IDENTITY(1,1) NOT NULL,
	[ID_Caisse] [int] NOT NULL,
	[Date_Ouverture] [datetime] NOT NULL,
	[Date_Cloture] [datetime] NULL,
	[Solde_Caisse] [float] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Etat] [tinyint] NOT NULL,
	[ID_Personnel] [int] NOT NULL,
 CONSTRAINT [PK_TP_Ouverture_Cloture_Caisse] PRIMARY KEY CLUSTERED 
(
	[ID_Ouverture_Cloture_Caisse] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Produit]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Produit](
	[ID_Produit] [int] IDENTITY(1,1) NOT NULL,
	[ID_Produit_Base] [int] NULL,
	[ID_Famille] [int] NOT NULL,
	[ID_Emballage] [int] NOT NULL,
	[Code] [nvarchar](50) NULL,
	[Libelle] [nvarchar](150) NOT NULL,
	[PU_Achat] [float] NOT NULL,
	[PU_Vente] [float] NOT NULL,
	[Valeur_Emballage] [bit] NOT NULL,
	[Produit_Decomposer] [bit] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
	[ID_Plastique_Base] [int] NULL,
	[ID_Bouteille_Base] [int] NULL,
 CONSTRAINT [PK_TP_Produit] PRIMARY KEY CLUSTERED 
(
	[ID_Produit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Regulation_Solde]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Regulation_Solde](
	[ID_Regulation_Solde] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Element] [int] NOT NULL,
	[Type_Element] [tinyint] NOT NULL,
	[NUM_DOC] [nvarchar](50) NULL,
	[Date] [datetime] NOT NULL,
	[Montant] [float] NOT NULL,
	[Motif] [nvarchar](500) NULL,
	[Etat] [tinyint] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Regulation_Solde] PRIMARY KEY CLUSTERED 
(
	[ID_Regulation_Solde] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Regulation_Stock]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Regulation_Stock](
	[ID_Regulation_Stock] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Element] [int] NOT NULL,
	[Type_Element] [tinyint] NOT NULL,
	[NUM_DOC] [nvarchar](50) NULL,
	[Date] [datetime] NOT NULL,
	[Quantite_Casier] [int] NOT NULL,
	[Quantite_Plastique] [int] NOT NULL,
	[Quantite_Bouteille] [int] NOT NULL,
	[Motif] [nvarchar](500) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Etat] [tinyint] NOT NULL,
 CONSTRAINT [PK_TP_Regulation_Stock] PRIMARY KEY CLUSTERED 
(
	[ID_Regulation_Stock] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Retour_Emballage]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Retour_Emballage](
	[ID_Retour_Emballage] [bigint] IDENTITY(1,1) NOT NULL,
	[Type_Element] [tinyint] NOT NULL,
	[ID_Element] [int] NOT NULL,
	[NUM_DOC] [nvarchar](50) NULL,
	[Date] [datetime] NOT NULL,
	[Etat] [tinyint] NOT NULL,
	[Motif] [nvarchar](500) NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Retour_Emballage_Fournisseur] PRIMARY KEY CLUSTERED 
(
	[ID_Retour_Emballage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Retour_Emballage_Detail]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Retour_Emballage_Detail](
	[ID_Retour_Emballage_Detail] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Retour_Emballage] [bigint] NOT NULL,
	[ID_Emballage] [int] NOT NULL,
	[PU_Plastique] [float] NOT NULL,
	[PU_Bouteille] [float] NOT NULL,
	[Conditionnement] [int] NOT NULL,
	[Quantite_Casier] [int] NOT NULL,
	[Quantite_Plastique] [int] NOT NULL,
	[Quantite_Bouteille] [int] NOT NULL,
 CONSTRAINT [PK_TP_Retour_Emballage_Fournisseur_Detail] PRIMARY KEY CLUSTERED 
(
	[ID_Retour_Emballage_Detail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Retour_Emballage_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Retour_Emballage_Temp](
	[ID_Retour_Emballage_Temp] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Emballage] [int] NOT NULL,
	[PU_Plastique] [float] NOT NULL,
	[PU_Bouteille] [float] NOT NULL,
	[Conditionnement] [int] NOT NULL,
	[Quantite_Casier] [int] NOT NULL,
	[Quantite_Plastique] [int] NOT NULL,
	[Quantite_Bouteille] [int] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Retour_Emballage_Fournisseur_Temp] PRIMARY KEY CLUSTERED 
(
	[ID_Retour_Emballage_Temp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Ristourne_Client]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Ristourne_Client](
	[ID_Ristourne_Client] [int] IDENTITY(1,1) NOT NULL,
	[ID_Client] [int] NOT NULL,
	[ID_Produit] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Ristourne] [float] NOT NULL,
	[Cumuler_Ristourne] [bit] NOT NULL,
	[Actif] [bit] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Ristourne_Client] PRIMARY KEY CLUSTERED 
(
	[ID_Ristourne_Client] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Type_Depense]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Type_Depense](
	[ID_Type_Depense] [int] IDENTITY(1,1) NOT NULL,
	[Libelle] [nvarchar](50) NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
	[Actif] [bit] NOT NULL,
 CONSTRAINT [PK_TP_Type_Decaissement] PRIMARY KEY CLUSTERED 
(
	[ID_Type_Depense] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Vente]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Vente](
	[ID_Vente] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Client] [int] NOT NULL,
	[ID_Caisse] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[NUM_DOC] [nvarchar](50) NULL,
	[Nom_Client] [nvarchar](150) NOT NULL,
	[Montant_Consigne_Emballage] [float] NOT NULL,
	[Montant] [float] NOT NULL,
	[Solde_Liquide] [float] NULL,
	[Solde_Emballage] [float] NULL,
	[Etat] [tinyint] NOT NULL,
	[Create_Date] [datetime] NOT NULL,
	[Edit_Date] [datetime] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
	[Edit_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Vente] PRIMARY KEY CLUSTERED 
(
	[ID_Vente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Vente_Emballage]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Vente_Emballage](
	[ID_Vente_Emballage] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Vente] [bigint] NOT NULL,
	[ID_Emballage] [int] NOT NULL,
	[Quantite_Casier] [int] NOT NULL,
	[Quantite_Plastique] [int] NOT NULL,
	[Quantite_Bouteille] [int] NOT NULL,
	[Conditionnement] [int] NOT NULL,
	[PU_Plastique] [float] NOT NULL,
	[PU_Bouteille] [float] NOT NULL,
 CONSTRAINT [PK_TP_Vente_Emballage] PRIMARY KEY CLUSTERED 
(
	[ID_Vente_Emballage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Vente_Emballage_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Vente_Emballage_Temp](
	[ID_Vente_Emballage_Temp] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Emballage] [int] NOT NULL,
	[Quantite_Casier] [int] NOT NULL,
	[Quantite_Plastique] [int] NOT NULL,
	[Quantite_Bouteille] [int] NOT NULL,
	[Conditionnement] [int] NOT NULL,
	[PU_Plastique] [float] NOT NULL,
	[PU_Bouteille] [float] NOT NULL,
	[Create_Code_User] [int] NOT NULL,
 CONSTRAINT [PK_TP_Vente_Emballage_Temp] PRIMARY KEY CLUSTERED 
(
	[ID_Vente_Emballage_Temp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Vente_Liquide]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Vente_Liquide](
	[ID_Vente_Liquide] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Vente] [bigint] NOT NULL,
	[ID_Produit] [int] NOT NULL,
	[PU_Vente] [float] NULL,
	[PU_Achat] [float] NULL,
	[Conditionnement] [int] NULL,
	[PU_Plastique] [float] NULL,
	[PU_Bouteille] [float] NULL,
	[Quantite_Casier_Liquide] [int] NOT NULL,
	[Quantite_Bouteille_Liquide] [int] NOT NULL,
	[Quantite_Plastique_Emballage] [int] NOT NULL,
	[Quantite_Bouteille_Emballage] [int] NOT NULL,
	[Quantite_Casier_Emballage] [int] NOT NULL,
	[Ristourne_Client] [int] NULL,
	[Produit_Decomposer] [bit] NULL,
	[Cumuler_Ristourne] [bit] NULL,
 CONSTRAINT [PK_TP_Vente_Liquide] PRIMARY KEY CLUSTERED 
(
	[ID_Vente_Liquide] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TP_Vente_Liquide_Temp]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TP_Vente_Liquide_Temp](
	[ID_Vente_Liquide_Temp] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_Client] [int] NOT NULL,
	[ID_Produit] [int] NOT NULL,
	[PU_Vente] [float] NULL,
	[PU_Achat] [float] NULL,
	[Conditionnement] [int] NULL,
	[PU_Plastique] [float] NULL,
	[PU_Bouteille] [float] NULL,
	[Quantite_Casier_Liquide] [int] NOT NULL,
	[Quantite_Bouteille_Liquide] [int] NOT NULL,
	[Quantite_Casier_Emballage] [int] NOT NULL,
	[Quantite_Plastique_Emballage] [int] NOT NULL,
	[Quantite_Bouteille_Emballage] [int] NOT NULL,
	[Ristourne_Client] [float] NULL,
	[Create_Code_User] [int] NOT NULL,
	[Produit_Decomposer] [bit] NULL,
	[Cumuler_Ristourne] [bit] NULL,
 CONSTRAINT [PK_TP_Vente_Liquide_Temp] PRIMARY KEY CLUSTERED 
(
	[ID_Vente_Liquide_Temp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[Vue_Personnel]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vue_Personnel]
AS
SELECT     ID_Personnel, Actif, Nom + CASE WHEN (Prenom IS NULL) THEN '' ELSE '  ' + Prenom END AS Libelle
FROM         dbo.TG_Personnels






GO
SET IDENTITY_INSERT [dbo].[TG_Commandes] ON 

INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (1, 0, N'Application', N'0', N'Connexion à l''application', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (2, 1, N'Administration', N'Administration', N'Administration de l''application', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (3, 2, N'Utilisateurs', N'Gestion_des_utilisateurs', N'Gestion des utilisateurs', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (4, 2, N'Profiles', N'Gestion_des_profiles', N'Gestion des profiles', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (5, 2, N'Habilitations', N'Gestion_des_droits', N'Gestion des habalitation', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (6, 1, N'Donnée De Base', N'Gestion_des_Donnee_de_base', N'ras', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (7, 6, N'Famille', N'Gestion_des_familles', N'ras', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (8, 7, N'Ajouter', N'Ajouter_famille', N'ras', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (9, 7, N'Modifier', N'Modifier_famille', N'ras', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (10, 7, N'Activer/Suspendre', N'Activer_suspendre_famille', N'ras', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (11, 6, N'Emballage', N'Gestion_des_emballage', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (12, 11, N'Ajouter', N'Ajouter_emballage', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (13, 11, N'Modifier', N'Modifier_emballage', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (14, 11, N'Activer/Suspendre', N'Activer_suspendre_emballage', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (15, 6, N'Produit', N'Gestion_des_produits', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (16, 15, N'Ajouter', N'Ajouter_Produit', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (17, 15, N'Modifier', N'Modifier_Produit', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (18, 15, N'Activer/Suspendre', N'Activer_suspendre_Produit', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (19, 6, N'Fournisseur', N'Gestion_des_fournisseur', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (20, 19, N'Ajouter', N'Ajouter_Fournisseur', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (21, 19, N'Modifier', N'Modifier_Fournisseur', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (22, 19, N'Activer/Suspendre', N'Activer_suspendre_Fournisseur', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (23, 6, N'Client', N'Gestion_des_client', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (24, 23, N'Ajouter', N'Ajouter_Client', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (25, 23, N'Modifier', N'Modifier_Client', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (26, 23, N'Activer/Suspendre', N'Activer_suspendre_Client', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (27, 6, N'Type Dépense', N'Gestion_des_type_depense', N'RAS', 5, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (28, 27, N'Ajouter', N'Ajouter_type_depense', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (29, 27, N'Modifier', N'Modifier_type_depense', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (30, 27, N'Activer/Suspendre', N'Activer_suspendre_type_depense', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (31, 6, N'Caisse', N'Gestion_des_caisse', N'RAS', 6, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (32, 31, N'Ajouter', N'Ajouter_Caisse', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (33, 31, N'Modifier', N'Modifier_Caisse', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (34, 31, N'Activer/Suspendre', N'Activer_suspendre_Caisse', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (35, 1, N'Opérations', N'Gestion_des_operations', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (36, 35, N'Vente', N'Gestion_des_ventes', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (37, 36, N'Ajouter', N'Ajouter_Vente', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (38, 36, N'Modifier', N'Modifier_Vente', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (39, 36, N'Valider', N'Valider_Vente', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (40, 36, N'Annuler', N'Annuler_Vente', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (41, 36, N'Imprimer', N'Imprimer_Vente', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (42, 35, N'Commande', N'Gestion_des_commandes', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (43, 42, N'Ajouter', N'Ajouter_Commande', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (44, 42, N'Modifier', N'Modifier_Commande', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (45, 42, N'Valider', N'Valider_Commande', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (46, 42, N'Annuler', N'Annuler_Commande', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (47, 42, N'Imprimer', N'Imprimer_Commande', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (48, 35, N'Règlement Fournisseur', N'Gestion_des_reglement_fournisseur', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (49, 35, N'Règlement Ristourne', N'Gestion_des_reglement_ristourne', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (50, 42, N'Dépense', N'Gestion_des_depenses', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (51, 35, N'Dépense', N'Gestion_des_depenses', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (52, 48, N'Ajouter', N'Ajouter_reglement_fournisseur', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (53, 49, N'Ajouter', N'Ajouter_reglement_ristourne', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (54, 51, N'Ajouter', N'Ajouter_depense', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (55, 48, N'Modifier', N'Modifier_reglement_fournisseur', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (56, 49, N'Modifier', N'Modifier_reglement_ristourne', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (57, 51, N'Modifier', N'Modifier_depense', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (58, 48, N'Valider', N'Valider_reglement_fournisseur', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (59, 49, N'Valider', N'Valider_reglement_ristourne', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (60, 51, N'Valider', N'Valider_depense', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (61, 48, N'Annuler', N'Annuler_reglement_fournisseur', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (62, 49, N'Annuler', N'Annuler_reglement_ristourne', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (63, 51, N'Annuler', N'Annuler_depense', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (64, 35, N'Règlement Client', N'Gestion_des_reglements_clients', N'RAS', 5, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (65, 64, N'Ajouter', N'Ajouter_reglement_client', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (66, 64, N'Modifier', N'Modifier_reglement_client', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (67, 64, N'Valider', N'Valider_reglement_client', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (68, 64, N'Annuler', N'Annuler_reglement_client', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (69, 64, N'Imprimer', N'Imprimer_reglement_client', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (70, 35, N'Régulation Solde Fournisseur', N'Gestion_des_regulations_solde_fournisseur', N'RAS', 6, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (71, 35, N'Régulation Solde Client', N'Gestion_des_regulations_solde_client', N'RAS', 7, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (72, 35, N'Régulation Solde Caisse', N'Gestion_des_regulations_solde_caisse', N'RAS', 8, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (73, 70, N'Ajouter', N'Ajouter_regulations_solde_fournisseur', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (74, 71, N'Ajouter', N'Ajouter_regulations_solde_client', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (75, 72, N'Ajouter', N'Ajouter_regulations_solde_caisse', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (76, 70, N'Modifier', N'Modifier_regulations_solde_fournisseur', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (77, 71, N'Modifier', N'Modifier_regulations_solde_client', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (78, 72, N'Modifier', N'Modifier_regulations_solde_caisse', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (79, 70, N'Valider', N'Valider_regulations_solde_fournisseur', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (80, 71, N'Valider', N'Valider_regulations_solde_client', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (81, 72, N'Valider', N'Valider_regulations_solde_caisse', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (82, 70, N'Annuler', N'Annuler_regulations_solde_fournisseur', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (83, 71, N'Annuler', N'Annuler_regulations_solde_client', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (84, 72, N'Annuler', N'Annuler_regulations_solde_caisse', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (85, 70, N'Imprimer', N'Imprimer_regulations_solde_fournisseur', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (86, 71, N'Imprimer', N'Imprimer_regulations_solde_client', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (87, 72, N'Imprimer', N'Imprimer_regulations_solde_caisse', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (88, 35, N'Inventaire Liquide', N'Gestion_des_inventaire_liquide', N'RAS', 9, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (89, 88, N'Ajouter', N'Ajouter_inventaire_liquide', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (90, 35, N'Retour Emballage Fournisseur', N'Gestion_des_retour_emballage_fournisseur', N'RAS', 10, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (91, 35, N'Retour Emballage Client', N'Gestion_des_retour_emballage_client', N'RAS', 11, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (92, 90, N'Ajouter', N'Ajouter_des_retour_emballage_fournisseur', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (93, 91, N'Ajouter', N'Ajouter_des_retour_emballage_client', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (94, 90, N'Modifier', N'Modifier_des_retour_emballage_fournisseur', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (95, 91, N'Modifier', N'Modifier_des_retour_emballage_client', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (96, 90, N'Vailder', N'Valider_des_retour_emballage_fournisseur', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (97, 91, N'Vailder', N'Valider_des_retour_emballage_client', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (98, 90, N'Annuler', N'Annuler_des_retour_emballage_fournisseur', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (99, 91, N'Annuler', N'Annuler_des_retour_emballage_client', N'RAS', 3, NULL)
GO
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (100, 91, N'Imprimer', N'Imprimer_des_retour_emballage_client', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (101, 90, N'Imprimer', N'Imprimer_des_retour_emballage_fournisseur', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (102, 48, N'Imprimer', N'Imprimer_reglement_fournisseur', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (103, 49, N'Imprimer', N'Imprimer_reglement_ristourne', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (104, 51, N'Imprimer', N'Imprimer_depense', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (105, 3, N'Ajouter', N'Ajouter_Utilisateur', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (106, 3, N'Modifier', N'Modifier_Utilisateur', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (107, 3, N'Reinitaliser', N'Reinitaliser_Utilisateur', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (108, 3, N'Activer/Suspendre', N'Activer_suspendre_Utilisateur', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (109, 4, N'Ajouter', N'Ajouter_Profile', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (110, 4, N'Modifier', N'Modifier_Profile', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (111, 4, N'Activer/Suspendre', N'Activer_suspendre_Profile', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (112, 2, N'Variable', N'Gestion_des_variables', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (113, 35, N'Régulation Stock Liquide', N'Gestion_des_regulations_stock_liquide', N'RAS', 12, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (114, 35, N'Régulation Stock Emballage', N'Gestion_des_regulations_stock_emballage', N'RAS', 13, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (115, 113, N'Ajouter', N'Ajouter_regulations_stock_liquide', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (116, 114, N'Ajouter', N'Ajouter_regulations_stock_emballage', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (117, 113, N'Modifier', N'Modifier_regulations_stock_liquide', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (118, 114, N'Modifier', N'Modifier_regulations_stock_emballage', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (119, 113, N'Vailder', N'Valider_regulations_stock_liquide', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (120, 114, N'Vailder', N'Valider_regulations_stock_emballage', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (121, 113, N'Annuler', N'Annuler_regulations_stock_liquide', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (122, 114, N'Annuler', N'Annuler_regulations_stock_emballage', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (123, 114, N'Imprimer', N'Imprimer_regulations_stock_emballage', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (124, 113, N'Imprimer', N'Imprimer_regulations_stock_liquide', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (125, 35, N'Inventaire Emballage', N'Gestion_des_inventaire_emballage', N'RAS', 14, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (126, 125, N'Ajouter', N'Ajouter_inventaire_emballage', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (127, 1, N'Etat', N'Gestion_des_etat', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (128, 127, N'Etat Solde Client', N'Gestion_des_etat_solde_client', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (129, 128, N'Historique', N'Historique_Etat_Solde_Client', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (130, 127, N'Etat Solde Fournisseur', N'Gestion_des_etat_solde_Fournisseur', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (131, 130, N'Historique', N'Historique_Etat_Solde_Fournisseur', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (132, 127, N'Etat Stock', N'Gestion_des_etat_stock', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (133, 132, N'Historique', N'Historique_Etat_Stock', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (134, 127, N'Etat Solde Caisse', N'Gestion_des_etat_caisse', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (135, 134, N'Historique', N'Historique_Etat_Solde_Caisse', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (136, 127, N'Etat Soolde Ristourne_client', N'Gestion_des_etat_ristourne_client', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (137, 23, N'Ristourne', N'Gestion_des_ristourne_client', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (138, 137, N'Ajouter', N'Ajouter_ristourne_client', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (139, 137, N'Modifier', N'Modifier_ristourne_client', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (140, 137, N'Activer/Suspendre', N'Activer_suspendre_ristourne_client', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (141, 137, N'Supprimer', N'Supprimer_ristourne_client', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (142, 35, N'Ouverture/Clôture Caisse', N'Gestion_des_Ouverture_Cloture_Caisse', N'RAS', 15, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (143, 142, N'Ajouter', N'Ajouter_Ouverture_Cloture_Caisse', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (144, 142, N'Modifier', N'Modifier_Ouverture_Cloture_Caisse', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (145, 142, N'Vailder', N'Valider_Ouverture_Cloture_Caisse', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (146, 142, N'Annuler', N'Annuler_Ouverture_Cloture_Caisse', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (147, 142, N'Imprimer', N'Imprimer_Ouverture_Cloture_Caisse', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (148, 88, N'Modifier', N'Modifier_inventaire_liquide', N'ras', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (149, 125, N'Modifier', N'Modifier_inventaire_emballage', N'ras', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (150, 125, N'Valider', N'Valider_inventaire_emballage', N'ras', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (151, 125, N'Réguler Stock', N'Reguler_inventaire_emballage', N'ras', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (152, 88, N'Réguler Stock', N'Reguler_inventaire_liquide', N'ras', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (153, 88, N'Valider', N'Valider_inventaire_liquide', N'ras', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (154, 88, N'Annuler', N'Annuler_inventaire_liquide', N'ras', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (155, 88, N'Imprimer', N'Imprimer_inventaire_liquide', N'ras', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (156, 125, N'Imprimer', N'Imprimer_inventaire_emballage', N'ras', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (157, 125, N'Annuler', N'Annuler_inventaire_emballage', N'ras', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (158, 1, N'Statistiques', N'Gestion_des_statistique', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (159, 158, N'Client & Fournisseur', N'Gestion_des_Stat_Client_Fournisseur', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (160, 158, N'Evolution Boutique', N'Visualiser_Evolution_Btq', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (161, 136, N'Historique', N'Historique_Etat_Solde_Ristourne', N'RAS', 0, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (162, 158, N'Chiffre d''Affaire', N'Rapport_Chiffre_Affaire', N'RAS', 2, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (163, 158, N'Chiffre des Commandes', N'Rapport_Chiffre_Commande', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (164, 2, N'Sauvegarde Donnée', N'Gestion_des_sauvergarde_donnee', N'RAS', 4, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (165, 15, N'Exporter', N'Exporter_Produit', N'RAS', 3, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (166, 132, N'Exporter', N'Export_Etat_Stock', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (167, 128, N'Exporter', N'Export_Etat_Solde_Client', N'RAS', 1, NULL)
INSERT [dbo].[TG_Commandes] ([Code_Commande], [Code_Commande_Parent], [Libelle], [ID_Control], [Description], [Ordre], [url]) VALUES (168, 2, N'Reset BD', N'Reset_BD', N'RAS', 5, NULL)
SET IDENTITY_INSERT [dbo].[TG_Commandes] OFF
SET IDENTITY_INSERT [dbo].[TG_Droits] ON 

INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (1, 1, 1, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (2, 1, 2, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (3, 1, 3, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (4, 1, 4, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (5, 1, 5, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (6, 2, 1, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (7, 2, 2, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (8, 2, 3, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (9, 2, 4, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (10, 2, 5, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (11, 1, 6, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (12, 2, 6, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (13, 1, 7, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (14, 2, 7, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (15, 1, 8, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (16, 2, 8, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (17, 1, 9, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (18, 2, 9, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (19, 1, 10, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (20, 2, 10, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (21, 1, 11, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (22, 2, 11, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (23, 1, 12, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (24, 2, 12, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (25, 1, 13, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (26, 2, 13, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (27, 1, 14, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (28, 2, 14, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (29, 1, 15, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (30, 2, 15, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (31, 1, 16, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (32, 2, 16, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (33, 1, 17, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (34, 2, 17, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (35, 1, 18, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (36, 2, 18, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (37, 1, 19, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (38, 2, 19, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (39, 1, 20, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (40, 2, 20, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (41, 1, 21, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (42, 2, 21, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (43, 1, 22, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (44, 2, 22, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (45, 1, 23, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (46, 2, 23, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (47, 1, 24, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (48, 2, 24, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (49, 1, 25, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (50, 2, 25, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (51, 1, 26, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (52, 2, 26, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (53, 1, 27, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (54, 2, 27, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (55, 1, 28, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (56, 2, 28, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (57, 1, 29, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (58, 2, 29, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (59, 1, 30, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (60, 2, 30, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (61, 1, 31, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (62, 2, 31, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (63, 1, 32, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (64, 2, 32, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (65, 1, 33, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (66, 2, 33, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (67, 1, 34, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (68, 2, 34, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (69, 1, 35, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (70, 2, 35, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (71, 1, 36, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (72, 2, 36, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (73, 1, 37, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (74, 2, 37, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (75, 1, 38, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (76, 2, 38, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (77, 1, 39, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (78, 2, 39, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (79, 1, 40, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (80, 2, 40, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (81, 1, 41, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (82, 2, 41, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (83, 1, 42, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (84, 2, 42, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (85, 1, 43, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (86, 2, 43, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (87, 1, 44, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (88, 2, 44, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (89, 1, 45, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (90, 2, 45, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (91, 1, 46, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (92, 2, 46, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (93, 1, 47, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (94, 2, 47, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (95, 1, 48, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (96, 2, 48, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (97, 1, 49, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (98, 2, 49, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (99, 1, 50, 1, 1)
GO
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (100, 2, 50, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (101, 1, 51, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (102, 2, 51, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (103, 1, 52, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (104, 2, 52, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (105, 1, 53, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (106, 2, 53, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (107, 1, 54, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (108, 2, 54, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (109, 1, 55, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (110, 2, 55, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (111, 1, 56, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (112, 2, 56, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (113, 1, 57, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (114, 2, 57, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (115, 1, 58, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (116, 2, 58, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (117, 1, 59, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (118, 2, 59, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (119, 1, 60, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (120, 2, 60, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (121, 1, 61, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (122, 2, 61, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (123, 1, 62, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (124, 2, 62, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (125, 1, 63, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (126, 2, 63, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (127, 1, 64, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (128, 2, 64, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (129, 1, 65, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (130, 2, 65, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (131, 1, 66, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (132, 2, 66, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (133, 1, 67, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (134, 2, 67, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (135, 1, 68, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (136, 2, 68, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (137, 1, 69, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (138, 2, 69, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (139, 1, 70, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (140, 2, 70, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (141, 1, 71, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (142, 2, 71, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (143, 1, 72, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (144, 2, 72, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (145, 1, 73, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (146, 2, 73, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (147, 1, 74, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (148, 2, 74, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (149, 1, 75, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (150, 2, 75, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (151, 1, 76, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (152, 2, 76, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (153, 1, 77, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (154, 2, 77, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (155, 1, 78, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (156, 2, 78, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (157, 1, 79, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (158, 2, 79, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (159, 1, 80, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (160, 2, 80, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (161, 1, 81, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (162, 2, 81, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (163, 1, 82, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (164, 2, 82, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (165, 1, 83, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (166, 2, 83, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (167, 1, 84, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (168, 2, 84, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (169, 1, 85, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (170, 2, 85, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (171, 1, 86, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (172, 2, 86, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (173, 1, 87, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (174, 2, 87, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (175, 1, 88, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (176, 2, 88, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (177, 1, 89, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (178, 2, 89, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (179, 1, 90, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (180, 2, 90, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (181, 1, 91, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (182, 2, 91, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (183, 1, 92, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (184, 2, 92, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (185, 1, 93, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (186, 2, 93, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (187, 1, 94, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (188, 2, 94, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (189, 1, 95, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (190, 2, 95, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (191, 1, 96, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (192, 2, 96, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (193, 1, 97, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (194, 2, 97, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (195, 1, 98, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (196, 2, 98, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (197, 1, 99, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (198, 2, 99, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (199, 1, 100, 1, 1)
GO
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (200, 2, 100, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (201, 1, 101, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (202, 2, 101, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (203, 1, 102, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (204, 2, 102, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (205, 1, 103, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (206, 2, 103, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (207, 1, 104, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (208, 2, 104, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (209, 1, 105, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (210, 2, 105, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (211, 1, 106, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (212, 2, 106, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (213, 1, 107, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (214, 2, 107, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (215, 1, 108, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (216, 2, 108, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (217, 1, 109, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (218, 2, 109, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (219, 1, 110, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (220, 2, 110, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (221, 1, 111, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (222, 2, 111, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (223, 1, 112, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (224, 2, 112, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (225, 1, 113, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (226, 2, 113, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (227, 1, 114, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (228, 2, 114, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (229, 1, 115, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (230, 2, 115, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (231, 1, 116, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (232, 2, 116, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (233, 1, 117, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (234, 2, 117, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (235, 1, 118, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (236, 2, 118, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (237, 1, 119, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (238, 2, 119, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (239, 1, 120, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (240, 2, 120, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (241, 1, 121, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (242, 2, 121, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (243, 1, 122, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (244, 2, 122, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (245, 1, 123, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (246, 2, 123, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (247, 1, 124, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (248, 2, 124, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (249, 1, 125, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (250, 2, 125, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (251, 1, 126, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (252, 2, 126, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (253, 1, 127, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (254, 2, 127, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (255, 1, 128, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (256, 2, 128, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (257, 1, 129, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (258, 2, 129, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (259, 1, 130, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (260, 2, 130, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (261, 1, 131, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (262, 2, 131, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (263, 1, 132, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (264, 2, 132, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (265, 1, 133, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (266, 2, 133, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (267, 1, 134, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (268, 2, 134, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (269, 1, 135, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (270, 2, 135, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (271, 1, 136, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (272, 2, 136, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (273, 1, 137, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (274, 2, 137, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (275, 1, 138, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (276, 2, 138, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (277, 1, 139, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (278, 2, 139, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (279, 1, 140, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (280, 2, 140, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (281, 1, 141, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (282, 2, 141, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (283, 1, 142, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (284, 2, 142, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (285, 1, 143, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (286, 2, 143, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (287, 1, 144, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (288, 2, 144, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (289, 1, 145, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (290, 2, 145, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (291, 1, 146, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (292, 2, 146, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (293, 1, 147, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (294, 2, 147, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (295, 1, 148, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (296, 2, 148, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (297, 1, 149, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (298, 2, 149, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (299, 1, 150, 1, 1)
GO
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (300, 2, 150, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (301, 1, 151, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (302, 2, 151, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (303, 1, 152, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (304, 2, 152, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (305, 1, 153, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (306, 2, 153, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (307, 1, 154, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (308, 2, 154, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (309, 1, 155, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (310, 2, 155, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (311, 1, 156, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (312, 2, 156, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (313, 1, 157, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (314, 2, 157, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (315, 1, 158, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (316, 2, 158, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (317, 1, 159, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (318, 2, 159, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (319, 1, 160, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (320, 2, 160, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (321, 1, 161, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (322, 2, 161, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (323, 1, 162, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (324, 2, 162, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (325, 1, 163, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (326, 2, 163, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (327, 1, 164, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (328, 2, 164, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (329, 1, 165, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (330, 2, 165, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (331, 1, 166, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (332, 2, 166, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (333, 1, 167, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (334, 2, 167, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (335, 3, 1, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (336, 3, 2, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (337, 3, 3, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (338, 3, 4, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (339, 3, 5, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (340, 3, 6, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (341, 3, 7, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (342, 3, 8, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (343, 3, 9, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (344, 3, 10, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (345, 3, 11, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (346, 3, 12, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (347, 3, 13, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (348, 3, 14, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (349, 3, 15, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (350, 3, 16, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (351, 3, 17, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (352, 3, 18, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (353, 3, 19, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (354, 3, 20, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (355, 3, 21, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (356, 3, 22, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (357, 3, 23, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (358, 3, 24, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (359, 3, 25, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (360, 3, 26, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (361, 3, 27, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (362, 3, 28, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (363, 3, 29, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (364, 3, 30, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (365, 3, 31, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (366, 3, 32, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (367, 3, 33, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (368, 3, 34, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (369, 3, 35, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (370, 3, 36, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (371, 3, 37, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (372, 3, 38, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (373, 3, 39, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (374, 3, 40, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (375, 3, 41, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (376, 3, 42, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (377, 3, 43, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (378, 3, 44, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (379, 3, 45, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (380, 3, 46, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (381, 3, 47, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (382, 3, 48, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (383, 3, 49, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (384, 3, 50, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (385, 3, 51, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (386, 3, 52, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (387, 3, 53, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (388, 3, 54, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (389, 3, 55, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (390, 3, 56, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (391, 3, 57, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (392, 3, 58, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (393, 3, 59, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (394, 3, 60, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (395, 3, 61, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (396, 3, 62, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (397, 3, 63, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (398, 3, 64, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (399, 3, 65, 0, 1)
GO
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (400, 3, 66, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (401, 3, 67, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (402, 3, 68, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (403, 3, 69, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (404, 3, 70, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (405, 3, 71, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (406, 3, 72, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (407, 3, 73, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (408, 3, 74, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (409, 3, 75, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (410, 3, 76, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (411, 3, 77, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (412, 3, 78, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (413, 3, 79, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (414, 3, 80, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (415, 3, 81, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (416, 3, 82, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (417, 3, 83, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (418, 3, 84, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (419, 3, 85, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (420, 3, 86, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (421, 3, 87, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (422, 3, 88, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (423, 3, 89, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (424, 3, 90, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (425, 3, 91, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (426, 3, 92, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (427, 3, 93, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (428, 3, 94, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (429, 3, 95, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (430, 3, 96, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (431, 3, 97, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (432, 3, 98, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (433, 3, 99, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (434, 3, 100, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (435, 3, 101, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (436, 3, 102, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (437, 3, 103, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (438, 3, 104, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (439, 3, 105, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (440, 3, 106, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (441, 3, 107, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (442, 3, 108, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (443, 3, 109, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (444, 3, 110, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (445, 3, 111, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (446, 3, 112, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (447, 3, 113, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (448, 3, 114, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (449, 3, 115, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (450, 3, 116, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (451, 3, 117, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (452, 3, 118, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (453, 3, 119, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (454, 3, 120, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (455, 3, 121, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (456, 3, 122, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (457, 3, 123, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (458, 3, 124, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (459, 3, 125, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (460, 3, 126, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (461, 3, 127, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (462, 3, 128, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (463, 3, 129, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (464, 3, 130, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (465, 3, 131, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (466, 3, 132, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (467, 3, 133, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (468, 3, 134, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (469, 3, 135, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (470, 3, 136, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (471, 3, 137, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (472, 3, 138, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (473, 3, 139, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (474, 3, 140, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (475, 3, 141, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (476, 3, 142, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (477, 3, 143, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (478, 3, 144, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (479, 3, 145, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (480, 3, 146, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (481, 3, 147, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (482, 3, 148, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (483, 3, 149, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (484, 3, 150, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (485, 3, 151, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (486, 3, 152, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (487, 3, 153, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (488, 3, 154, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (489, 3, 155, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (490, 3, 156, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (491, 3, 157, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (492, 3, 158, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (493, 3, 159, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (494, 3, 160, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (495, 3, 161, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (496, 3, 162, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (497, 3, 163, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (498, 3, 164, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (499, 3, 165, 1, 1)
GO
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (500, 3, 166, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (501, 3, 167, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (502, 1, 168, 1, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (503, 2, 168, 0, 1)
INSERT [dbo].[TG_Droits] ([ID_Droit], [Code_Profile], [Code_Commande], [Executer], [Disponible]) VALUES (504, 3, 168, 0, 1)
SET IDENTITY_INSERT [dbo].[TG_Droits] OFF
SET IDENTITY_INSERT [dbo].[TG_Logs] ON 

INSERT [dbo].[TG_Logs] ([Id_Error], [Error_value], [Date], [Code_user], [Nom_Poste_Travail], [Fonction]) VALUES (1, N'Une ou plusieurs options (password) ne sont pas prises en charge par cette instruction. Pour plus d''informations, consultez la documentation.', CAST(0x0000A94A01246571 AS DateTime), 1, N'DESKTOP-P7RPO7P', N'POST: /Sauvegarde/Ajouter/')
INSERT [dbo].[TG_Logs] ([Id_Error], [Error_value], [Date], [Code_user], [Nom_Poste_Travail], [Fonction]) VALUES (2, N'Syntaxe incorrecte vers ''?''.
Ouvrez les guillemets après la chaîne de caractères ''''.', CAST(0x0000A94A012A4B55 AS DateTime), 1, N'DESKTOP-P7RPO7P', N'POST: /Sauvegarde/Ajouter/')
INSERT [dbo].[TG_Logs] ([Id_Error], [Error_value], [Date], [Code_user], [Nom_Poste_Travail], [Fonction]) VALUES (3, N'Ouvrez les guillemets après la chaîne de caractères ''''.', CAST(0x0000A94A012ABBED AS DateTime), 1, N'DESKTOP-P7RPO7P', N'POST: /Sauvegarde/Ajouter/')
INSERT [dbo].[TG_Logs] ([Id_Error], [Error_value], [Date], [Code_user], [Nom_Poste_Travail], [Fonction]) VALUES (4, N'Ouvrez les guillemets après la chaîne de caractères ''''.', CAST(0x0000A94A01310D23 AS DateTime), 1, N'DESKTOP-P7RPO7P', N'POST: /Sauvegarde/Ajouter/')
INSERT [dbo].[TG_Logs] ([Id_Error], [Error_value], [Date], [Code_user], [Nom_Poste_Travail], [Fonction]) VALUES (5, N'Ouvrez les guillemets après la chaîne de caractères ''''.', CAST(0x0000A94A0135A3E2 AS DateTime), 1, N'DESKTOP-P7RPO7P', N'POST: /Sauvegarde/Ajouter/')
INSERT [dbo].[TG_Logs] ([Id_Error], [Error_value], [Date], [Code_user], [Nom_Poste_Travail], [Fonction]) VALUES (6, N'L''instruction BACKUP DATABASE WITH COMPRESSION n''est pas prise en charge sur Express Edition.
BACKUP DATABASE s''est terminé anormalement.', CAST(0x0000A94A0136291F AS DateTime), 1, N'DESKTOP-P7RPO7P', N'POST: /Sauvegarde/Ajouter/')
SET IDENTITY_INSERT [dbo].[TG_Logs] OFF
SET IDENTITY_INSERT [dbo].[TG_Personnels] ON 

INSERT [dbo].[TG_Personnels] ([ID_Personnel], [ID_Caisse], [Nom], [Prenom], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [Display]) VALUES (1, 1, N'É-DÉPÔT', NULL, CAST(0x0000A7800169E01F AS DateTime), CAST(0x0000A780016A47E7 AS DateTime), 2, 2, 1, 1)
INSERT [dbo].[TG_Personnels] ([ID_Personnel], [ID_Caisse], [Nom], [Prenom], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [Display]) VALUES (2, 1, N'admin', NULL, CAST(0x0000A94900678A0E AS DateTime), CAST(0x0000A9490078B00F AS DateTime), 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[TG_Personnels] OFF
SET IDENTITY_INSERT [dbo].[TG_Profiles] ON 

INSERT [dbo].[TG_Profiles] ([Code_Profile], [Libelle], [Display], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (1, N'SUPER ADMIN', 1, CAST(0x0000A89800000000 AS DateTime), CAST(0x0000A89800000000 AS DateTime), 2, 2, 1)
INSERT [dbo].[TG_Profiles] ([Code_Profile], [Libelle], [Display], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (2, N'ADMINISTRATEUR', 1, CAST(0x0000A89800000000 AS DateTime), CAST(0x0000A89800000000 AS DateTime), 2, 2, 1)
INSERT [dbo].[TG_Profiles] ([Code_Profile], [Libelle], [Display], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (3, N'RESPONSABLE', 1, CAST(0x0000A93F011A18AD AS DateTime), CAST(0x0000A93F011A18AD AS DateTime), 2, 2, 1)
SET IDENTITY_INSERT [dbo].[TG_Profiles] OFF
SET IDENTITY_INSERT [dbo].[TG_Profiles_Utilisateurs] ON 

INSERT [dbo].[TG_Profiles_Utilisateurs] ([ID_Profiles_Utilisateur], [Code_User], [Code_Profile], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (1, 1, 1, CAST(0x0000A89800000000 AS DateTime), CAST(0x0000A89800000000 AS DateTime), 1, 1, 1)
INSERT [dbo].[TG_Profiles_Utilisateurs] ([ID_Profiles_Utilisateur], [Code_User], [Code_Profile], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (2, 2, 2, CAST(0x0000A9490078B013 AS DateTime), CAST(0x0000A9490078B013 AS DateTime), 1, 1, 1)
SET IDENTITY_INSERT [dbo].[TG_Profiles_Utilisateurs] OFF
SET IDENTITY_INSERT [dbo].[TG_Sentinelles_Options] ON 

INSERT [dbo].[TG_Sentinelles_Options] ([ID_Sentinelle], [Sentinelle], [SentinelleConnexion], [SentinelleEndConnexion], [SentinelleInsert], [SentinelleEdit], [SentinelleDelete], [SentinellePrint], [SentinelleLife]) VALUES (1, 1, 1, 1, 1, 1, 1, 1, 100)
SET IDENTITY_INSERT [dbo].[TG_Sentinelles_Options] OFF
SET IDENTITY_INSERT [dbo].[TG_Utilisateurs] ON 

INSERT [dbo].[TG_Utilisateurs] ([Code_User], [ID_Personnel], [Login], [Somme_Ctrl], [Display], [Last_Connexion], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (1, 1, N'admin10', N'4FBD41A36DAC3CD79AA1041C9648AB89', 1, CAST(0x0000A9520172FB14 AS DateTime), CAST(0x0000A7800169E037 AS DateTime), CAST(0x0000A9520172FB14 AS DateTime), 1, 1, 1)
INSERT [dbo].[TG_Utilisateurs] ([Code_User], [ID_Personnel], [Login], [Somme_Ctrl], [Display], [Last_Connexion], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (2, 2, N'admin', N'21232F297A57A5A743894A0E4A801FC3', 1, CAST(0x0000A9F2016298D2 AS DateTime), CAST(0x0000A94900678A18 AS DateTime), CAST(0x0000A9F2016298D2 AS DateTime), 1, 1, 1)
SET IDENTITY_INSERT [dbo].[TG_Utilisateurs] OFF
SET IDENTITY_INSERT [dbo].[TG_Variable] ON 

INSERT [dbo].[TG_Variable] ([ID_Variable], [Nom_Societe], [Numero_Contribuable], [Registre_Commerce], [Telephone], [Adresse], [Description], [Ville], [Logo], [Appliquer_TVA], [TVA], [Gestion_Emballage], [Verifier_Etat_Stock], [Cle0], [Cle1], [Cle2], [Dossier_Sauvegarde], [Password_Sauvegarde]) VALUES (1, N'DÉPÔT STADE ABEGA', N'P048012118150L ', NULL, N'67000 7000', N'STADE ABEGA', N'COMMERCE GENERAL - DÉPÔT DE BOISSON', N'Yaoundé', NULL, 0, 0, 1, 1, N'f272d25a6a23c5ae98e1246d6d2aa536', 0x5CCC6E37C9FA924C19817BC89BB9EDF6, 0xC17B21C737CFACAB8CA63AD55984B161, N'C:\DATA\', N'admin')
SET IDENTITY_INSERT [dbo].[TG_Variable] OFF
SET IDENTITY_INSERT [dbo].[TP_Caisse] ON 

INSERT [dbo].[TP_Caisse] ([ID_Caisse], [Libelle], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (1, N'CAISSE PRINCIPAL', CAST(0x0000A76200000000 AS DateTime), CAST(0x0000A89801265325 AS DateTime), 1, 1, 1)
SET IDENTITY_INSERT [dbo].[TP_Caisse] OFF
SET IDENTITY_INSERT [dbo].[TP_Client] ON 

INSERT [dbo].[TP_Client] ([ID_Client], [Nom], [Adresse], [Telephone], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (1, N'CLIENT DIVERS', NULL, NULL, CAST(0x0000A89900C666F6 AS DateTime), CAST(0x0000A89900C666F6 AS DateTime), 1, 1, 1)
SET IDENTITY_INSERT [dbo].[TP_Client] OFF
SET IDENTITY_INSERT [dbo].[TP_Emballage] ON 

INSERT [dbo].[TP_Emballage] ([ID_Emballage], [Code], [Description], [Conditionnement], [PU_Plastique], [PU_Bouteille], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (1, N'C12', N'Casier de 12 bouteilles', 12, 1200, 150, CAST(0x0000A7620183ADE2 AS DateTime), CAST(0x0000A8390158181E AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Emballage] ([ID_Emballage], [Code], [Description], [Conditionnement], [PU_Plastique], [PU_Bouteille], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (2, N'C15', N'Casier de 15 bouteilles', 15, 1200, 120, CAST(0x0000A76201874F79 AS DateTime), CAST(0x0000A83901581231 AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Emballage] ([ID_Emballage], [Code], [Description], [Conditionnement], [PU_Plastique], [PU_Bouteille], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (3, N'C24', N'Casier de 24 bouteilles', 24, 1200, 75, CAST(0x0000A76201874F79 AS DateTime), CAST(0x0000A83901581233 AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Emballage] ([ID_Emballage], [Code], [Description], [Conditionnement], [PU_Plastique], [PU_Bouteille], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (4, N'C12 SMOOTH', N'Casie de 12 bouteille', 12, 1200, 120, CAST(0x0000A83901584BB0 AS DateTime), CAST(0x0000A83901584BB0 AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Emballage] ([ID_Emballage], [Code], [Description], [Conditionnement], [PU_Plastique], [PU_Bouteille], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (5, N'CVIDE 12', N'Casie de 12 bouteille de 15', 12, 0, 0, CAST(0x0000A875002A4DAC AS DateTime), CAST(0x0000A875002A4DAC AS DateTime), 2, 2, 1)
SET IDENTITY_INSERT [dbo].[TP_Emballage] OFF
SET IDENTITY_INSERT [dbo].[TP_Famille] ON 

INSERT [dbo].[TP_Famille] ([ID_Famille], [Libelle], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (1, N'BRASSERIE', CAST(0x0000A762016F5566 AS DateTime), CAST(0x0000A83901560378 AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Famille] ([ID_Famille], [Libelle], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (2, N'GUINESS', CAST(0x0000A762016F6B16 AS DateTime), CAST(0x0000A8390156037A AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Famille] ([ID_Famille], [Libelle], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (3, N'KADJI', CAST(0x0000A762016F728B AS DateTime), CAST(0x0000A8390156037C AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Famille] ([ID_Famille], [Libelle], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (5, N'EAU', CAST(0x0000A8390155D4CA AS DateTime), CAST(0x0000A89800EEC28C AS DateTime), 2, 2, 0)
SET IDENTITY_INSERT [dbo].[TP_Famille] OFF
SET IDENTITY_INSERT [dbo].[TP_Fournisseur] ON 

INSERT [dbo].[TP_Fournisseur] ([ID_Fournisseur], [Nom], [Adresse], [Telephone], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (7, N'TURBO DISTRIBUTION', N'NGOUSSO', N'679799607', CAST(0x0000A89900C6A3B3 AS DateTime), CAST(0x0000A89900C6A3B3 AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Fournisseur] ([ID_Fournisseur], [Nom], [Adresse], [Telephone], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (8, N'SAGEM SARL', N'EMANA', N'679799607', CAST(0x0000A89900C6E8F3 AS DateTime), CAST(0x0000A89900C6FADB AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Fournisseur] ([ID_Fournisseur], [Nom], [Adresse], [Telephone], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (9, N'SOURCE DU PAYS', NULL, NULL, CAST(0x0000A8A601751C2F AS DateTime), CAST(0x0000A8A601751C2F AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Fournisseur] ([ID_Fournisseur], [Nom], [Adresse], [Telephone], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (10, N'GUINNESS SA', NULL, NULL, CAST(0x0000A93E00E5B976 AS DateTime), CAST(0x0000A93E00E5B976 AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Fournisseur] ([ID_Fournisseur], [Nom], [Adresse], [Telephone], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (11, N'BRASSERIE SA', NULL, NULL, CAST(0x0000A93E00E5F637 AS DateTime), CAST(0x0000A93E00E5FAB8 AS DateTime), 2, 2, 0)
SET IDENTITY_INSERT [dbo].[TP_Fournisseur] OFF
SET IDENTITY_INSERT [dbo].[TP_Ouverture_Cloture_Caisse] ON 

INSERT [dbo].[TP_Ouverture_Cloture_Caisse] ([ID_Ouverture_Cloture_Caisse], [ID_Caisse], [Date_Ouverture], [Date_Cloture], [Solde_Caisse], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Etat], [ID_Personnel]) VALUES (1, 1, CAST(0x0000A98300D2198C AS DateTime), NULL, 0, CAST(0x0000A98300D21CE9 AS DateTime), CAST(0x0000A98300D21CE9 AS DateTime), 2, 2, 0, 2)
SET IDENTITY_INSERT [dbo].[TP_Ouverture_Cloture_Caisse] OFF
SET IDENTITY_INSERT [dbo].[TP_Produit] ON 

INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (1, NULL, 1, 1, N'33EXP65', N'"33" EXPORT 65 CL', 6325, 6500, 1, 0, CAST(0x0000A76300067667 AS DateTime), CAST(0x0000A83901680D9C AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (3, NULL, 1, 1, N'AM65', N'AMSTEL 65 CL', 6325, 6500, 1, 0, CAST(0x0000A7630006E727 AS DateTime), CAST(0x0000A83901680D9F AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (4, NULL, 1, 1, N'BEAU2', N'BEAUFORT LIGHT', 6150, 6500, 1, 0, CAST(0x0000A76300070C77 AS DateTime), CAST(0x0000A83901680DA0 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (5, NULL, 1, 1, N'BEAU1', N'BEAUFORT ORDINAIRE', 6150, 6500, 1, 0, CAST(0x0000A76300073833 AS DateTime), CAST(0x0000A83901680DA3 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (6, NULL, 1, 1, N'BOST50', N'BOOSTER COLA 50 CL', 6125, 6500, 1, 0, CAST(0x0000A76300076B44 AS DateTime), CAST(0x0000A83901680DA6 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (7, NULL, 1, 1, N'CAST65', N'CASTEL 65 CL', 6325, 6500, 1, 0, CAST(0x0000A763000790CA AS DateTime), CAST(0x0000A83901680DAA AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (8, NULL, 1, 1, N'DJINO65', N'DJINO 65 CL', 4525, 4800, 1, 0, CAST(0x0000A7630007DFE1 AS DateTime), CAST(0x0000A83901680DAF AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (9, NULL, 1, 1, N'DOPEL65', N'DOPEL 65 CL', 6325, 6500, 1, 0, CAST(0x0000A76300081B7D AS DateTime), CAST(0x0000A88400E8DBB3 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (10, NULL, 2, 1, N'GUIN65', N'GUINNESS 65 CL', 10359, 11000, 1, 0, CAST(0x0000A76300086B4F AS DateTime), CAST(0x0000A83901680DDD AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (11, NULL, 2, 3, N'GUIN33', N'GUINNESS 33 CL', 12159, 12900, 1, 0, CAST(0x0000A7630008C897 AS DateTime), CAST(0x0000A88400E7D7C0 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (12, NULL, 2, 1, N'HARP65', N'HARP 65 CL', 6305, 6900, 1, 0, CAST(0x0000A7630009099A AS DateTime), CAST(0x0000A88400E92CC8 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (13, NULL, 1, 1, N'ISEM65', N'ISEMBECK 65 CL', 7335, 7500, 1, 0, CAST(0x0000A76300095929 AS DateTime), CAST(0x0000A83901680DBF AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (15, NULL, 1, 1, N'MAN65', N'MANYANG 65 CL', 5125, 5400, 1, 0, CAST(0x0000A7630009C449 AS DateTime), CAST(0x0000A88400E8EC23 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (16, NULL, 2, 3, N'MALTAG', N'MALTA GUINNESS 33 CL', 10216, 10900, 1, 0, CAST(0x0000A7630009F727 AS DateTime), CAST(0x0000A88400E83753 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (17, NULL, 1, 1, N'MUTZ65', N'MUTZIG 65 CL', 6325, 6500, 1, 0, CAST(0x0000A763000A1E60 AS DateTime), CAST(0x0000A83901680DCC AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (18, NULL, 1, 1, N'ORAN65', N'ORANGINA 65 CL', 4500, 4800, 1, 0, CAST(0x0000A763000A51FA AS DateTime), CAST(0x0000A89900C876EA AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (19, NULL, 2, 1, N'ORIJ65CL', N'ORIGIN', 5945, 6400, 1, 0, CAST(0x0000A763000A74DD AS DateTime), CAST(0x0000A83901680DEE AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (20, NULL, 1, 5, N'JUSP15', N'JUS PLASTIQUE 1.5 L', 5250, 5300, 0, 0, CAST(0x0000A763000AE7FC AS DateTime), CAST(0x0000A89900C946A0 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (21, NULL, 1, 5, N'TOPP1', N'TOP PLASTIQUE 1 L', 2500, 2600, 0, 0, CAST(0x0000A763000B105C AS DateTime), CAST(0x0000A89900C9541A AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (22, NULL, 1, 5, N'JUSP1', N'JUS PLASTIQUE 1 L', 2950, 3100, 0, 0, CAST(0x0000A763000B4E53 AS DateTime), CAST(0x0000A89900C93D86 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (23, NULL, 1, 5, N'JUSP5', N'JUS PLASTIQUE 0.5L', 2950, 3100, 0, 0, CAST(0x0000A763000B6B05 AS DateTime), CAST(0x0000A89900C9355A AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (24, NULL, 1, 1, N'TOP65', N'TOP 65 CL', 3650, 3950, 1, 0, CAST(0x0000A763000B8A3B AS DateTime), CAST(0x0000A89900C743C0 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (26, NULL, 1, 1, N'BEAU3', N'BEAUFORT TANGO', 6150, 6500, 1, 0, CAST(0x0000A76500A715C3 AS DateTime), CAST(0x0000A88400E9B900 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (27, NULL, 1, 1, N'JUS65', N'JUS 65 CL', 4500, 4900, 1, 0, CAST(0x0000A76500A79DC8 AS DateTime), CAST(0x0000A89900C78C59 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (28, NULL, 1, 1, N'VIMTO', N'VIMTO', 4525, 4800, 1, 0, CAST(0x0000A76500A80F36 AS DateTime), CAST(0x0000A88400E9A8EE AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (29, NULL, 1, 1, N'SODA', N'SODA', 2325, 2600, 1, 0, CAST(0x0000A76500A88711 AS DateTime), CAST(0x0000A83901680DD0 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (30, NULL, 1, 3, N'TONIC', N'TONIC', 4875, 5200, 1, 0, CAST(0x0000A76500A8C105 AS DateTime), CAST(0x0000A89900C921C4 AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (31, NULL, 2, 3, N'ICE', N'ICE BLACK', 11163, 11900, 1, 0, CAST(0x0000A76500A984C0 AS DateTime), CAST(0x0000A88400E9E13D AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (32, NULL, 2, 2, N'SMOOTH', N'SMOOTH', 8723, 9300, 1, 0, CAST(0x0000A76500AA2952 AS DateTime), CAST(0x0000A83901680DEF AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (34, NULL, 3, 1, N'KADJI', N'KADJI', 6600, 6600, 1, 0, CAST(0x0000A76500000000 AS DateTime), CAST(0x0000A88400EA05EC AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (35, NULL, 1, 1, N'COCA', N'COCA COLA', 3700, 3950, 1, 0, CAST(0x0000A777014D1EFA AS DateTime), CAST(0x0000A88400E8C71F AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (37, NULL, 3, 1, N'K44', N'k44', 5400, 5400, 1, 0, CAST(0x0000A79300A12FBF AS DateTime), CAST(0x0000A88400E9F9DC AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (38, NULL, 3, 1, N'UCBPAM', N'UCB PAMPLEMOUSE', 3950, 3950, 1, 0, CAST(0x0000A79300A15D9C AS DateTime), CAST(0x0000A88400EA10F0 AS DateTime), 2, 2, 0, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (39, NULL, 1, 5, N'TOPP15', N'TOP PLASTIQUE 1.5 L', 2950, 3100, 0, 0, CAST(0x0000A79C0101EC1C AS DateTime), CAST(0x0000A89900C960CF AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (40, NULL, 2, 3, N'HARP33', N'HARP 33cl', 6989, 7500, 1, 0, CAST(0x0000A7AB00FC04F6 AS DateTime), CAST(0x0000A88400E91F1B AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (41, NULL, 2, 3, N'ICE', N'ICE ELETRIC', 10260, 10900, 1, 0, CAST(0x0000A7E50089FBE9 AS DateTime), CAST(0x0000A88400E9EB4B AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (42, NULL, 1, 1, N'BOST65', N'BOOSTER GIN 65 CL', 6125, 6500, 1, 0, CAST(0x0000A83100A84F2F AS DateTime), CAST(0x0000A88400E9C57F AS DateTime), 2, 2, 1, NULL, NULL)
INSERT [dbo].[TP_Produit] ([ID_Produit], [ID_Produit_Base], [ID_Famille], [ID_Emballage], [Code], [Libelle], [PU_Achat], [PU_Vente], [Valeur_Emballage], [Produit_Decomposer], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif], [ID_Plastique_Base], [ID_Bouteille_Base]) VALUES (45, 32, 2, 4, N'SMOOTH12', N'SMOOTH 12', 7000, 7500, 1, 1, CAST(0x0000A875002A8AB1 AS DateTime), CAST(0x0000A8B5015DC17B AS DateTime), 2, 2, 1, 1, 2)
SET IDENTITY_INSERT [dbo].[TP_Produit] OFF
SET IDENTITY_INSERT [dbo].[TP_Regulation_Stock] ON 

INSERT [dbo].[TP_Regulation_Stock] ([ID_Regulation_Stock], [ID_Element], [Type_Element], [NUM_DOC], [Date], [Quantite_Casier], [Quantite_Plastique], [Quantite_Bouteille], [Motif], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Etat]) VALUES (1, 1, 0, N'000001', CAST(0x0000A98300D25965 AS DateTime), 200, 0, 0, N'ras', CAST(0x0000A98300D25965 AS DateTime), CAST(0x0000A98300D25F39 AS DateTime), 2, 2, 1)
SET IDENTITY_INSERT [dbo].[TP_Regulation_Stock] OFF
SET IDENTITY_INSERT [dbo].[TP_Type_Depense] ON 

INSERT [dbo].[TP_Type_Depense] ([ID_Type_Depense], [Libelle], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (1, N'LOYER, EAU, LUMIERE', CAST(0x0000A83600000000 AS DateTime), CAST(0x0000A83600000000 AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Type_Depense] ([ID_Type_Depense], [Libelle], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (2, N'DEPENSE DIVERSE', CAST(0x0000A83600000000 AS DateTime), CAST(0x0000A8B300D73B0D AS DateTime), 2, 2, 0)
INSERT [dbo].[TP_Type_Depense] ([ID_Type_Depense], [Libelle], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (3, N'SALAIRE', CAST(0x0000A83600000000 AS DateTime), CAST(0x0000A8B300D716CC AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Type_Depense] ([ID_Type_Depense], [Libelle], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (5, N'CARBURANT', CAST(0x0000A83600000000 AS DateTime), CAST(0x0000A8B300D73B10 AS DateTime), 2, 2, 1)
INSERT [dbo].[TP_Type_Depense] ([ID_Type_Depense], [Libelle], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User], [Actif]) VALUES (6, N'GARAGE', CAST(0x0000A83600000000 AS DateTime), CAST(0x0000A83600000000 AS DateTime), 2, 2, 1)
SET IDENTITY_INSERT [dbo].[TP_Type_Depense] OFF
SET IDENTITY_INSERT [dbo].[TP_Vente] ON 

INSERT [dbo].[TP_Vente] ([ID_Vente], [ID_Client], [ID_Caisse], [Date], [NUM_DOC], [Nom_Client], [Montant_Consigne_Emballage], [Montant], [Solde_Liquide], [Solde_Emballage], [Etat], [Create_Date], [Edit_Date], [Create_Code_User], [Edit_Code_User]) VALUES (1, 1, 1, CAST(0x0000A98300D2A19A AS DateTime), N'000001', N'CLIENT DIVERS', 0, 32500, 0, 0, 1, CAST(0x0000A98300D2A19A AS DateTime), CAST(0x0000A98300D2A19A AS DateTime), 2, 2)
SET IDENTITY_INSERT [dbo].[TP_Vente] OFF
SET IDENTITY_INSERT [dbo].[TP_Vente_Liquide] ON 

INSERT [dbo].[TP_Vente_Liquide] ([ID_Vente_Liquide], [ID_Vente], [ID_Produit], [PU_Vente], [PU_Achat], [Conditionnement], [PU_Plastique], [PU_Bouteille], [Quantite_Casier_Liquide], [Quantite_Bouteille_Liquide], [Quantite_Plastique_Emballage], [Quantite_Bouteille_Emballage], [Quantite_Casier_Emballage], [Ristourne_Client], [Produit_Decomposer], [Cumuler_Ristourne]) VALUES (1, 1, 1, 6500, 6325, 12, 1200, 150, 5, 0, 0, 0, 5, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[TP_Vente_Liquide] OFF
ALTER TABLE [dbo].[TG_Corbeille] ADD  CONSTRAINT [DF_TG_Corbeille_Date]  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[TG_Corbeille] ADD  CONSTRAINT [DF_TG_Corbeille_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TG_Droits] ADD  CONSTRAINT [DF_TG_Droits_Executer]  DEFAULT ((0)) FOR [Executer]
GO
ALTER TABLE [dbo].[TG_Droits] ADD  CONSTRAINT [DF_TG_Droits_Disponible]  DEFAULT ((1)) FOR [Disponible]
GO
ALTER TABLE [dbo].[TG_Locked_Records] ADD  CONSTRAINT [DF_TG_Locked_Records_Lock_Date_Time]  DEFAULT (getdate()) FOR [Lock_Date_Time]
GO
ALTER TABLE [dbo].[TG_Personnels] ADD  CONSTRAINT [DF_TG_Personnels_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TG_Profiles] ADD  CONSTRAINT [DF_TG_Profiles_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TG_Profiles_Utilisateurs] ADD  CONSTRAINT [DF_TG_Profiles_Utilisateurs_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TG_Utilisateurs] ADD  CONSTRAINT [DF_TG_Utilisateurs_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TP_Caisse] ADD  CONSTRAINT [DF_TP_Caisse_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TP_Client] ADD  CONSTRAINT [DF_TP_Client_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TP_Decaissement] ADD  CONSTRAINT [DF_TP_Decaissement_Etat]  DEFAULT ((1)) FOR [Etat]
GO
ALTER TABLE [dbo].[TP_Emballage] ADD  CONSTRAINT [DF_TP_Emballage_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TP_Encaissement] ADD  CONSTRAINT [DF_Table_1_Actif]  DEFAULT ((1)) FOR [Etat]
GO
ALTER TABLE [dbo].[TP_Famille] ADD  CONSTRAINT [DF_TP_Producteur_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TP_Fournisseur] ADD  CONSTRAINT [DF_TP_Fournisseur_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TP_Ouverture_Cloture_Caisse] ADD  CONSTRAINT [DF_TP_Ouverture_Cloture_Caisse_Actif]  DEFAULT ((1)) FOR [Etat]
GO
ALTER TABLE [dbo].[TP_Produit] ADD  CONSTRAINT [DF_TP_Produit_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TP_Type_Depense] ADD  CONSTRAINT [DF_TP_Type_Decaissement_Actif]  DEFAULT ((1)) FOR [Actif]
GO
ALTER TABLE [dbo].[TG_Droits]  WITH CHECK ADD  CONSTRAINT [TG_Commandes_TG_Droits_FK1] FOREIGN KEY([Code_Commande])
REFERENCES [dbo].[TG_Commandes] ([Code_Commande])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TG_Droits] CHECK CONSTRAINT [TG_Commandes_TG_Droits_FK1]
GO
ALTER TABLE [dbo].[TG_Droits]  WITH CHECK ADD  CONSTRAINT [TG_Profiles_TG_Droits_FK1] FOREIGN KEY([Code_Profile])
REFERENCES [dbo].[TG_Profiles] ([Code_Profile])
GO
ALTER TABLE [dbo].[TG_Droits] CHECK CONSTRAINT [TG_Profiles_TG_Droits_FK1]
GO
ALTER TABLE [dbo].[TG_Hierarchie_Commande]  WITH CHECK ADD  CONSTRAINT [FK_TG_Hierarchie_Commande_TG_Commandes1] FOREIGN KEY([Code_Commande_Parent])
REFERENCES [dbo].[TG_Commandes] ([Code_Commande])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TG_Hierarchie_Commande] CHECK CONSTRAINT [FK_TG_Hierarchie_Commande_TG_Commandes1]
GO
ALTER TABLE [dbo].[TG_Personnels]  WITH CHECK ADD  CONSTRAINT [FK_TG_Personnels_TP_Caisse] FOREIGN KEY([ID_Caisse])
REFERENCES [dbo].[TP_Caisse] ([ID_Caisse])
GO
ALTER TABLE [dbo].[TG_Personnels] CHECK CONSTRAINT [FK_TG_Personnels_TP_Caisse]
GO
ALTER TABLE [dbo].[TG_Profiles_Utilisateurs]  WITH CHECK ADD  CONSTRAINT [TG_Profiles_TG_Profiles_Utilisateurs_FK1] FOREIGN KEY([Code_Profile])
REFERENCES [dbo].[TG_Profiles] ([Code_Profile])
GO
ALTER TABLE [dbo].[TG_Profiles_Utilisateurs] CHECK CONSTRAINT [TG_Profiles_TG_Profiles_Utilisateurs_FK1]
GO
ALTER TABLE [dbo].[TG_Profiles_Utilisateurs]  WITH CHECK ADD  CONSTRAINT [TG_Utilisateurs_TG_Profiles_Utilisateurs_FK1] FOREIGN KEY([Code_User])
REFERENCES [dbo].[TG_Utilisateurs] ([Code_User])
GO
ALTER TABLE [dbo].[TG_Profiles_Utilisateurs] CHECK CONSTRAINT [TG_Utilisateurs_TG_Profiles_Utilisateurs_FK1]
GO
ALTER TABLE [dbo].[TG_Utilisateurs]  WITH CHECK ADD  CONSTRAINT [TG_Personnels_TG_Utilisateurs_FK1] FOREIGN KEY([ID_Personnel])
REFERENCES [dbo].[TG_Personnels] ([ID_Personnel])
GO
ALTER TABLE [dbo].[TG_Utilisateurs] CHECK CONSTRAINT [TG_Personnels_TG_Utilisateurs_FK1]
GO
ALTER TABLE [dbo].[TP_Commande]  WITH CHECK ADD  CONSTRAINT [FK_TP_Commande_TP_Fournisseur] FOREIGN KEY([ID_Fournisseur])
REFERENCES [dbo].[TP_Fournisseur] ([ID_Fournisseur])
GO
ALTER TABLE [dbo].[TP_Commande] CHECK CONSTRAINT [FK_TP_Commande_TP_Fournisseur]
GO
ALTER TABLE [dbo].[TP_Commande_Emballage]  WITH CHECK ADD  CONSTRAINT [FK_TP_Commande_Emballage_TP_Commande] FOREIGN KEY([ID_Commande])
REFERENCES [dbo].[TP_Commande] ([ID_Commande])
GO
ALTER TABLE [dbo].[TP_Commande_Emballage] CHECK CONSTRAINT [FK_TP_Commande_Emballage_TP_Commande]
GO
ALTER TABLE [dbo].[TP_Commande_Emballage]  WITH CHECK ADD  CONSTRAINT [FK_TP_Commande_Emballage_TP_Emballage] FOREIGN KEY([ID_Emballage])
REFERENCES [dbo].[TP_Emballage] ([ID_Emballage])
GO
ALTER TABLE [dbo].[TP_Commande_Emballage] CHECK CONSTRAINT [FK_TP_Commande_Emballage_TP_Emballage]
GO
ALTER TABLE [dbo].[TP_Commande_Emballage_Temp]  WITH CHECK ADD  CONSTRAINT [FK_TP_Commande_Emballage_Temp_TP_Emballage] FOREIGN KEY([ID_Emballage])
REFERENCES [dbo].[TP_Emballage] ([ID_Emballage])
GO
ALTER TABLE [dbo].[TP_Commande_Emballage_Temp] CHECK CONSTRAINT [FK_TP_Commande_Emballage_Temp_TP_Emballage]
GO
ALTER TABLE [dbo].[TP_Commande_Liquide]  WITH CHECK ADD  CONSTRAINT [FK_TP_Commande_Liquide_TP_Commande] FOREIGN KEY([ID_Commande])
REFERENCES [dbo].[TP_Commande] ([ID_Commande])
GO
ALTER TABLE [dbo].[TP_Commande_Liquide] CHECK CONSTRAINT [FK_TP_Commande_Liquide_TP_Commande]
GO
ALTER TABLE [dbo].[TP_Commande_Liquide]  WITH CHECK ADD  CONSTRAINT [FK_TP_Commande_Liquide_TP_Produit] FOREIGN KEY([ID_Produit])
REFERENCES [dbo].[TP_Produit] ([ID_Produit])
GO
ALTER TABLE [dbo].[TP_Commande_Liquide] CHECK CONSTRAINT [FK_TP_Commande_Liquide_TP_Produit]
GO
ALTER TABLE [dbo].[TP_Commande_Liquide_Temp]  WITH CHECK ADD  CONSTRAINT [FK_TP_Commande_Liquide_Temp_TP_Produit] FOREIGN KEY([ID_Produit])
REFERENCES [dbo].[TP_Produit] ([ID_Produit])
GO
ALTER TABLE [dbo].[TP_Commande_Liquide_Temp] CHECK CONSTRAINT [FK_TP_Commande_Liquide_Temp_TP_Produit]
GO
ALTER TABLE [dbo].[TP_Decaissement]  WITH CHECK ADD  CONSTRAINT [FK_TP_Decaissement_TP_Caisse] FOREIGN KEY([ID_Caisse])
REFERENCES [dbo].[TP_Caisse] ([ID_Caisse])
GO
ALTER TABLE [dbo].[TP_Decaissement] CHECK CONSTRAINT [FK_TP_Decaissement_TP_Caisse]
GO
ALTER TABLE [dbo].[TP_Encaissement]  WITH CHECK ADD  CONSTRAINT [FK_TP_Encaissement_TP_Caisse] FOREIGN KEY([ID_Caissse])
REFERENCES [dbo].[TP_Caisse] ([ID_Caisse])
GO
ALTER TABLE [dbo].[TP_Encaissement] CHECK CONSTRAINT [FK_TP_Encaissement_TP_Caisse]
GO
ALTER TABLE [dbo].[TP_Encaissement]  WITH CHECK ADD  CONSTRAINT [FK_TP_Encaissement_TP_Client] FOREIGN KEY([ID_Client])
REFERENCES [dbo].[TP_Client] ([ID_Client])
GO
ALTER TABLE [dbo].[TP_Encaissement] CHECK CONSTRAINT [FK_TP_Encaissement_TP_Client]
GO
ALTER TABLE [dbo].[TP_Inventaire_Detail]  WITH CHECK ADD  CONSTRAINT [FK_TP_Inventaire_Detail_TP_Inventaire] FOREIGN KEY([ID_Inventaire])
REFERENCES [dbo].[TP_Inventaire] ([ID_Inventaire])
GO
ALTER TABLE [dbo].[TP_Inventaire_Detail] CHECK CONSTRAINT [FK_TP_Inventaire_Detail_TP_Inventaire]
GO
ALTER TABLE [dbo].[TP_Ouverture_Cloture_Caisse]  WITH CHECK ADD  CONSTRAINT [FK_TP_Ouverture_Cloture_Caisse_TG_Personnels] FOREIGN KEY([ID_Personnel])
REFERENCES [dbo].[TG_Personnels] ([ID_Personnel])
GO
ALTER TABLE [dbo].[TP_Ouverture_Cloture_Caisse] CHECK CONSTRAINT [FK_TP_Ouverture_Cloture_Caisse_TG_Personnels]
GO
ALTER TABLE [dbo].[TP_Ouverture_Cloture_Caisse]  WITH CHECK ADD  CONSTRAINT [FK_TP_Ouverture_Cloture_Caisse_TP_Caisse] FOREIGN KEY([ID_Caisse])
REFERENCES [dbo].[TP_Caisse] ([ID_Caisse])
GO
ALTER TABLE [dbo].[TP_Ouverture_Cloture_Caisse] CHECK CONSTRAINT [FK_TP_Ouverture_Cloture_Caisse_TP_Caisse]
GO
ALTER TABLE [dbo].[TP_Produit]  WITH CHECK ADD  CONSTRAINT [FK_TP_Produit_TP_Emballage] FOREIGN KEY([ID_Emballage])
REFERENCES [dbo].[TP_Emballage] ([ID_Emballage])
GO
ALTER TABLE [dbo].[TP_Produit] CHECK CONSTRAINT [FK_TP_Produit_TP_Emballage]
GO
ALTER TABLE [dbo].[TP_Produit]  WITH CHECK ADD  CONSTRAINT [FK_TP_Produit_TP_Emballage1] FOREIGN KEY([ID_Plastique_Base])
REFERENCES [dbo].[TP_Emballage] ([ID_Emballage])
GO
ALTER TABLE [dbo].[TP_Produit] CHECK CONSTRAINT [FK_TP_Produit_TP_Emballage1]
GO
ALTER TABLE [dbo].[TP_Produit]  WITH CHECK ADD  CONSTRAINT [FK_TP_Produit_TP_Emballage2] FOREIGN KEY([ID_Bouteille_Base])
REFERENCES [dbo].[TP_Emballage] ([ID_Emballage])
GO
ALTER TABLE [dbo].[TP_Produit] CHECK CONSTRAINT [FK_TP_Produit_TP_Emballage2]
GO
ALTER TABLE [dbo].[TP_Produit]  WITH CHECK ADD  CONSTRAINT [FK_TP_Produit_TP_Producteur] FOREIGN KEY([ID_Famille])
REFERENCES [dbo].[TP_Famille] ([ID_Famille])
GO
ALTER TABLE [dbo].[TP_Produit] CHECK CONSTRAINT [FK_TP_Produit_TP_Producteur]
GO
ALTER TABLE [dbo].[TP_Produit]  WITH CHECK ADD  CONSTRAINT [FK_TP_Produit_TP_Produit] FOREIGN KEY([ID_Produit_Base])
REFERENCES [dbo].[TP_Produit] ([ID_Produit])
GO
ALTER TABLE [dbo].[TP_Produit] CHECK CONSTRAINT [FK_TP_Produit_TP_Produit]
GO
ALTER TABLE [dbo].[TP_Retour_Emballage_Detail]  WITH CHECK ADD  CONSTRAINT [FK_TP_Retour_Emballage_Detail_TP_Emballage] FOREIGN KEY([ID_Emballage])
REFERENCES [dbo].[TP_Emballage] ([ID_Emballage])
GO
ALTER TABLE [dbo].[TP_Retour_Emballage_Detail] CHECK CONSTRAINT [FK_TP_Retour_Emballage_Detail_TP_Emballage]
GO
ALTER TABLE [dbo].[TP_Retour_Emballage_Detail]  WITH CHECK ADD  CONSTRAINT [FK_TP_Retour_Emballage_Detail_TP_Retour_Emballage] FOREIGN KEY([ID_Retour_Emballage])
REFERENCES [dbo].[TP_Retour_Emballage] ([ID_Retour_Emballage])
GO
ALTER TABLE [dbo].[TP_Retour_Emballage_Detail] CHECK CONSTRAINT [FK_TP_Retour_Emballage_Detail_TP_Retour_Emballage]
GO
ALTER TABLE [dbo].[TP_Retour_Emballage_Temp]  WITH CHECK ADD  CONSTRAINT [FK_TP_Retour_Emballage_Fournisseur_Temp_TP_Emballage] FOREIGN KEY([ID_Emballage])
REFERENCES [dbo].[TP_Emballage] ([ID_Emballage])
GO
ALTER TABLE [dbo].[TP_Retour_Emballage_Temp] CHECK CONSTRAINT [FK_TP_Retour_Emballage_Fournisseur_Temp_TP_Emballage]
GO
ALTER TABLE [dbo].[TP_Ristourne_Client]  WITH CHECK ADD  CONSTRAINT [FK_TP_Ristourne_Client_TP_Client] FOREIGN KEY([ID_Client])
REFERENCES [dbo].[TP_Client] ([ID_Client])
GO
ALTER TABLE [dbo].[TP_Ristourne_Client] CHECK CONSTRAINT [FK_TP_Ristourne_Client_TP_Client]
GO
ALTER TABLE [dbo].[TP_Ristourne_Client]  WITH CHECK ADD  CONSTRAINT [FK_TP_Ristourne_Client_TP_Produit] FOREIGN KEY([ID_Produit])
REFERENCES [dbo].[TP_Produit] ([ID_Produit])
GO
ALTER TABLE [dbo].[TP_Ristourne_Client] CHECK CONSTRAINT [FK_TP_Ristourne_Client_TP_Produit]
GO
ALTER TABLE [dbo].[TP_Vente]  WITH CHECK ADD  CONSTRAINT [FK_TP_Vente_TP_Caisse] FOREIGN KEY([ID_Caisse])
REFERENCES [dbo].[TP_Caisse] ([ID_Caisse])
GO
ALTER TABLE [dbo].[TP_Vente] CHECK CONSTRAINT [FK_TP_Vente_TP_Caisse]
GO
ALTER TABLE [dbo].[TP_Vente]  WITH CHECK ADD  CONSTRAINT [FK_TP_Vente_TP_Client] FOREIGN KEY([ID_Client])
REFERENCES [dbo].[TP_Client] ([ID_Client])
GO
ALTER TABLE [dbo].[TP_Vente] CHECK CONSTRAINT [FK_TP_Vente_TP_Client]
GO
ALTER TABLE [dbo].[TP_Vente_Emballage]  WITH CHECK ADD  CONSTRAINT [FK_TP_Vente_Emballage_TP_Emballage] FOREIGN KEY([ID_Emballage])
REFERENCES [dbo].[TP_Emballage] ([ID_Emballage])
GO
ALTER TABLE [dbo].[TP_Vente_Emballage] CHECK CONSTRAINT [FK_TP_Vente_Emballage_TP_Emballage]
GO
ALTER TABLE [dbo].[TP_Vente_Emballage]  WITH CHECK ADD  CONSTRAINT [FK_TP_Vente_Emballage_TP_Vente] FOREIGN KEY([ID_Vente])
REFERENCES [dbo].[TP_Vente] ([ID_Vente])
GO
ALTER TABLE [dbo].[TP_Vente_Emballage] CHECK CONSTRAINT [FK_TP_Vente_Emballage_TP_Vente]
GO
ALTER TABLE [dbo].[TP_Vente_Emballage_Temp]  WITH CHECK ADD  CONSTRAINT [FK_TP_Vente_Emballage_Temp_TP_Emballage] FOREIGN KEY([ID_Emballage])
REFERENCES [dbo].[TP_Emballage] ([ID_Emballage])
GO
ALTER TABLE [dbo].[TP_Vente_Emballage_Temp] CHECK CONSTRAINT [FK_TP_Vente_Emballage_Temp_TP_Emballage]
GO
ALTER TABLE [dbo].[TP_Vente_Liquide]  WITH CHECK ADD  CONSTRAINT [FK_TP_Vente_Liquide_TP_Produit] FOREIGN KEY([ID_Produit])
REFERENCES [dbo].[TP_Produit] ([ID_Produit])
GO
ALTER TABLE [dbo].[TP_Vente_Liquide] CHECK CONSTRAINT [FK_TP_Vente_Liquide_TP_Produit]
GO
ALTER TABLE [dbo].[TP_Vente_Liquide]  WITH CHECK ADD  CONSTRAINT [FK_TP_Vente_Liquide_TP_Vente] FOREIGN KEY([ID_Vente])
REFERENCES [dbo].[TP_Vente] ([ID_Vente])
GO
ALTER TABLE [dbo].[TP_Vente_Liquide] CHECK CONSTRAINT [FK_TP_Vente_Liquide_TP_Vente]
GO
ALTER TABLE [dbo].[TP_Vente_Liquide_Temp]  WITH CHECK ADD  CONSTRAINT [FK_TP_Vente_Liquide_Temp_TP_Client] FOREIGN KEY([ID_Client])
REFERENCES [dbo].[TP_Client] ([ID_Client])
GO
ALTER TABLE [dbo].[TP_Vente_Liquide_Temp] CHECK CONSTRAINT [FK_TP_Vente_Liquide_Temp_TP_Client]
GO
ALTER TABLE [dbo].[TP_Vente_Liquide_Temp]  WITH CHECK ADD  CONSTRAINT [FK_TP_Vente_Liquide_Temp_TP_Produit] FOREIGN KEY([ID_Produit])
REFERENCES [dbo].[TP_Produit] ([ID_Produit])
GO
ALTER TABLE [dbo].[TP_Vente_Liquide_Temp] CHECK CONSTRAINT [FK_TP_Vente_Liquide_Temp_TP_Produit]
GO
/****** Object:  Trigger [dbo].[After_Insert_Commandes]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
CREATE TRIGGER [dbo].[After_Insert_Commandes]
   ON  [dbo].[TG_Commandes]
   AFTER INSERT
AS 
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 DECLARE @Code_Commande as int ,@Code_Commande_Parent as int
    -- Insert statements for trigger here
    
    SELECT @Code_Commande = Code_Commande , @Code_Commande_Parent = Code_Commande_Parent FROM inserted
    
    DELETE FROM [TG_Droits] WHERE Code_Commande = @Code_Commande
    
    INSERT INTO [dbo].[TG_Droits] ([Code_Profile],[Code_Commande] ,[Executer] ,[Disponible])
  SELECT Code_Profile ,@Code_Commande, '0','1' FROM [dbo].[TG_Profiles]
 
END






GO
/****** Object:  Trigger [dbo].[Corbeille_Profile]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TEG-FACTURATION
-- Create date: 02 02 2014
-- Description:	JOURNALISE LES SUPRESSIONS
-- =============================================
CREATE TRIGGER [dbo].[Corbeille_Profile] 
   ON  [dbo].[TG_Profiles]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Actif as bit, @ID_Element as int,@Code_User as int, @ID_Table as int ,@Description as nvarchar(500)
	IF UPDATE(Actif)
	BEGIN
		SELECT @Actif =Actif,@Code_User = Edit_Code_User ,@ID_Element = Code_Profile , @Description = Libelle  from inserted;
		
		-- recuperation de l'identifiant de la table
		select @ID_Table = ID_Table from TG_Tables where LibelleReel=N'TG_Profiles';
		if (@Actif = 0)
		begin			
			insert into TG_Corbeille (ID_Table,ID_Element,[Date],Code_User, [Description]) values (@ID_Table,@ID_Element,GETDATE(),@Code_User , N'Libelle du Profile = ' + @Description);
		end
	END
    -- Insert statements for trigger here

END






GO
/****** Object:  Trigger [dbo].[Creer_Droit]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TEG-FACTURATION
-- Create date: 19/05/2013
-- Description:	Lorsqu'on creer un profil on lui attibut toutes les commande avec par defaut aucun droit
-- =============================================
CREATE TRIGGER [dbo].[Creer_Droit] 
   ON  [dbo].[TG_Profiles]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Code_Profile as int
    -- Insert statements for trigger here
    SELECT @Code_Profile = [Code_Profile] from inserted
	INSERT INTO [dbo].[TG_Droits] ([Code_Profile],[Code_Commande] ,[Executer] ,[Disponible])
		SELECT @Code_Profile ,[Code_Commande], '0','1' FROM [dbo].[TG_Commandes]
END






GO
/****** Object:  Trigger [dbo].[AFTER_SAVE_COMMANDE]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_SAVE_COMMANDE] ON  [dbo].[TP_Commande] AFTER INSERT
AS 
BEGIN
	 DECLARE @ID_Commande as bigint, @NUM_DOC as bigint, @Create_Code_User as int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	select @ID_Commande = ID_Commande, @Create_Code_User = Create_Code_User from inserted
	
	-- enregistrement des produits liquide de la commande
	 INSERT INTO [TP_Commande_Liquide]([ID_Commande],[ID_Produit],[Quantite_Casier_Liquide],[Quantite_Bouteille_Liquide],
	 [PU_Achat],[Conditionnement],[Quantite_Casier_Emballage],[Quantite_Plastique_Emballage],
	 [Quantite_Bouteille_Emballage],[PU_Plastique],[PU_Bouteille])
		SELECT @ID_Commande, Temp.[ID_Produit] ,Temp.[Quantite_Casier_Liquide],Temp.[Quantite_Bouteille_Liquide] ,
		Temp.[PU_Achat],Temp.[Conditionnement],Temp.[Quantite_Casier_Emballage],Temp.[Quantite_Plastique_Emballage],
		Temp.[Quantite_Bouteille_Emballage],Temp.[PU_Plastique],Temp.[PU_Bouteille]
			FROM TP_Commande_Liquide_Temp Temp WHERE Temp.Create_Code_User = @Create_Code_User
			
	-- enregistrement des emballage de la commande
	 INSERT INTO [TP_Commande_Emballage]([ID_Commande],[ID_Emballage],[Quantite_Casier],[Quantite_Plastique],[Quantite_Bouteille],
	 [Conditionnement],[PU_Plastique],[PU_Bouteille])
		SELECT @ID_Commande, Temp.[ID_Emballage] , Temp.[Quantite_Casier],Temp.[Quantite_Plastique],Temp.[Quantite_Bouteille] ,
		Temp.[Conditionnement], Temp.[PU_Plastique],Temp.[PU_Bouteille]
			FROM TP_Commande_Emballage_Temp Temp WHERE Temp.Create_Code_User = @Create_Code_User
	
	--mise a jour du numero de la commande			
	select @NUM_DOC = COUNT(ID_Commande) from [TP_Commande];
	UPDATE TP_Commande SET NUM_DOC =
		CASE 
			WHEN (@NUM_DOC < 10) THEN REPLACE (N'00000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100) THEN REPLACE (N'0000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 1000) THEN REPLACE (N'000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 10000) THEN REPLACE (N'00' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100000) THEN REPLACE (N'0' + STR(@NUM_DOC), ' ','')
			ELSE STR(@NUM_DOC)
		END 
	WHERE ID_Commande = @ID_Commande;
	--vide les table temporaire liée a la commande
	DELETE FROM [TP_Commande_Liquide_Temp] WHERE Create_Code_User = @Create_Code_User
    DELETE FROM [TP_Commande_Emballage_Temp] WHERE Create_Code_User = @Create_Code_User
    -- FIN --
END







GO
/****** Object:  Trigger [dbo].[AFTER_INSERT_COMMANDE_EMBALLAGE_TEMP]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_INSERT_COMMANDE_EMBALLAGE_TEMP] 
   ON  [dbo].[TP_Commande_Emballage_Temp] 
   AFTER INSERT
AS 
BEGIN
	declare @ID as bigint, @Conditionnement as int, @PU_Bouteille as float, @PU_Plastique as int, @ID_Emballage as int 
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	select @ID = ID_Commande_Emballage_Temp, @ID_Emballage = ID_Emballage from inserted
	
	SELECT @Conditionnement = Conditionnement,
	@PU_Plastique = PU_Plastique, @PU_Bouteille = PU_Bouteille FROM TP_Emballage WHERE ID_Emballage = @ID_Emballage 

	UPDATE TP_Commande_Emballage_Temp SET
	PU_Bouteille = @PU_Bouteille,
	PU_Plastique = @PU_Plastique,
	Conditionnement = @Conditionnement
	WHERE ID_Commande_Emballage_Temp = @ID
END






GO
/****** Object:  Trigger [dbo].[AFTER_INSERT_COMMANDE_LIQUIDE]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_INSERT_COMMANDE_LIQUIDE] 
   ON  [dbo].[TP_Commande_Liquide]
   AFTER INSERT
AS 
BEGIN
	DECLARE @NUM_DOC as bigint, @ID as bigint, @ID_Produit as int, @ID_Commande as bigint, @ID_Fournisseur as int, @Create_Code_User as int,
	@PU_Achat as float, @Conditionnement as int, @PU_Plastique as float,
	@PU_Bouteille as float, @Ristourne_Client as float, @Cumuler_Ristourne as bit, @Produit_Decomposer as bit
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	SELECT @ID = ID_Commande_Liquide,  @ID_Produit = ID_Produit, @ID_Commande = ID_Commande FROM INSERTED
	SELECT @ID_Fournisseur = ID_Fournisseur FROM TP_Commande WHERE ID_Commande = @ID_Commande

	SELECT @PU_Achat = PU_Achat, @Conditionnement = Conditionnement,
	@PU_Plastique = PU_Plastique, @PU_Bouteille = PU_Bouteille FROM TP_Produit produit INNER JOIN TP_Emballage
	emballage ON produit.ID_Emballage = emballage.ID_Emballage WHERE ID_Produit = @ID_Produit 

	UPDATE TP_Commande_Liquide SET 	
	PU_Achat = @PU_Achat, 
	Conditionnement = @Conditionnement,
	PU_Plastique = @PU_Plastique, 
	PU_Bouteille = @PU_Bouteille
	WHERE ID_Commande_Liquide = @ID
END






GO
/****** Object:  Trigger [dbo].[AFTER_INSERT_COMMANDE_LIQUIDE_TEMP]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_INSERT_COMMANDE_LIQUIDE_TEMP] 
   ON  [dbo].[TP_Commande_Liquide_Temp]
   AFTER INSERT
AS 
BEGIN
	DECLARE @NUM_DOC as bigint, @ID as bigint, @ID_Produit as int, @ID_Commande as bigint, @ID_Fournisseur as int, @Create_Code_User as int,
	@PU_Achat as float, @Conditionnement as int, @PU_Plastique as float,
	@PU_Bouteille as float, @Ristourne_Client as float, @Cumuler_Ristourne as bit, @Produit_Decomposer as bit
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	SELECT @ID = ID_Commande_Liquide_Temp,  @ID_Produit = ID_Produit FROM INSERTED

	SELECT @PU_Achat = PU_Achat, @Conditionnement = Conditionnement,
	@PU_Plastique = PU_Plastique, @PU_Bouteille = PU_Bouteille FROM TP_Produit produit INNER JOIN TP_Emballage
	emballage ON produit.ID_Emballage = emballage.ID_Emballage WHERE ID_Produit = @ID_Produit

	UPDATE TP_Commande_Liquide_Temp SET 	
	PU_Achat = @PU_Achat, 
	Conditionnement = @Conditionnement,
	PU_Plastique = @PU_Plastique, 
	PU_Bouteille = @PU_Bouteille
	WHERE ID_Commande_Liquide_Temp = @ID

END






GO
/****** Object:  Trigger [dbo].[AFTER_SAVE_DECAISSEMENT]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_SAVE_DECAISSEMENT] ON  [dbo].[TP_Decaissement] AFTER INSERT
AS 
BEGIN
	 DECLARE @ID_Decaissement as bigint, @NUM_DOC as bigint, @Create_Code_User as int, @type as int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for trigger here
	select @ID_Decaissement = ID_Decaissement, @Create_Code_User = Create_Code_User, @type = Type_Element from inserted
			
	--mise a jour du numero de la commande			
	select @NUM_DOC = COUNT(ID_Decaissement) from [TP_Decaissement];
	UPDATE TP_Decaissement SET NUM_DOC =
		CASE 
			WHEN (@NUM_DOC < 10) THEN REPLACE (N'00000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100) THEN REPLACE (N'0000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 1000) THEN REPLACE (N'000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 10000) THEN REPLACE (N'00' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100000) THEN REPLACE (N'0' + STR(@NUM_DOC), ' ','')
			ELSE STR(@NUM_DOC)
		END 
	WHERE ID_Decaissement = @ID_Decaissement;

    -- FIN --
END







GO
/****** Object:  Trigger [dbo].[AFTER_SAVE_ENCAISSEMENT]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_SAVE_ENCAISSEMENT] ON  [dbo].[TP_Encaissement] AFTER INSERT
AS 
BEGIN
	 DECLARE @ID_Encaissement as bigint, @NUM_DOC as bigint, @Create_Code_User as int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for trigger here
	select @ID_Encaissement = ID_Encaissement, @Create_Code_User = Create_Code_User from inserted
			
	--mise a jour du numero de la commande			
	select @NUM_DOC = COUNT(ID_Encaissement) from [TP_Encaissement];
	UPDATE TP_Encaissement SET NUM_DOC =
		CASE 
			WHEN (@NUM_DOC < 10) THEN REPLACE (N'00000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100) THEN REPLACE (N'0000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 1000) THEN REPLACE (N'000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 10000) THEN REPLACE (N'00' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100000) THEN REPLACE (N'0' + STR(@NUM_DOC), ' ','')
			ELSE STR(@NUM_DOC)
		END 
	WHERE ID_Encaissement = @ID_Encaissement;

    -- FIN --
END







GO
/****** Object:  Trigger [dbo].[AFTER_SAVE_INVENTAIRE]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_SAVE_INVENTAIRE] ON [dbo].[TP_Inventaire] AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @NUM_DOC as BIGINT, @ID_Inventaire as bigint, @num as nvarchar(50), @type as int
	SET NOCOUNT ON;

    -- Insert statements for trigger here
     SELECT @ID_Inventaire = ID_Inventaire, @type = [Type] FROM inserted;
	
	if(@type = 0)
	begin
	--insertion des detail de l'inventaire
	 INSERT INTO [TP_Inventaire_Detail]([ID_Inventaire],[ID_Element],[Quantite_Casier_Theorique],[Quantite_Plastique_Theorique],[Quantite_Bouteille_Theorique],[Quantite_Casier_Reel],[Quantite_Plastique_Reel],[Quantite_Bouteille_Reel],[PU_Casier_Bouteille],[PU_Plastique],[Conditionnement])
	 SELECT @ID_Inventaire, Temp.[ID_Element] ,Temp.[Stock_Casier],0,Temp.[Stock_Bouteille] ,0,0,0, PU_Bouteille_Vente,PU_Plastique,Conditionnement
	 FROM FN_Etat_Stock(0, GETDATE(), GETDATE()) Temp
	end
	else
	begin
	--insertion des detail de l'inventaire
	 INSERT INTO [TP_Inventaire_Detail]([ID_Inventaire],[ID_Element],[Quantite_Casier_Theorique],[Quantite_Plastique_Theorique],[Quantite_Bouteille_Theorique],[Quantite_Casier_Reel],[Quantite_Plastique_Reel],[Quantite_Bouteille_Reel],[PU_Casier_Bouteille],[PU_Plastique],[Conditionnement])
	 SELECT @ID_Inventaire, Temp.[ID_Element] ,Temp.[Stock_Casier],Temp.[Stock_Plastique],Temp.[Stock_Bouteille] ,0,0,0, PU_Bouteille_Achat,PU_Plastique,Conditionnement
	 FROM FN_Etat_Stock(1, GETDATE(), GETDATE()) Temp Where ID_Element in(1,2,3)
	end
	 
			
	 SELECT @NUM_DOC = COUNT(ID_Inventaire) from [TP_Inventaire] where [Type] = @type;
	 UPDATE TP_Inventaire SET NUM_DOC =
		CASE 
			WHEN (@NUM_DOC < 10) THEN REPLACE (N'00000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100) THEN REPLACE (N'0000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 1000) THEN REPLACE (N'000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 10000) THEN REPLACE (N'00' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100000) THEN REPLACE (N'0' + STR(@NUM_DOC), ' ','')
			ELSE STR(@NUM_DOC)
		END 
	WHERE ID_Inventaire = @ID_Inventaire;
END








GO
/****** Object:  Trigger [dbo].[AFTER_SAVE_REGULATION_SOLDE]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_SAVE_REGULATION_SOLDE] ON  [dbo].[TP_Regulation_Solde] AFTER INSERT
AS 
BEGIN
	 DECLARE @ID as bigint, @NUM_DOC as bigint, @Create_Code_User as int, @type as int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for trigger here
	select @ID = ID_Regulation_Solde, @Create_Code_User = Create_Code_User, @type = Type_Element from inserted
			
	--mise a jour du numero de la commande			
	select @NUM_DOC = COUNT(ID_Regulation_Solde) from [TP_Regulation_Solde] where Type_Element = @type;
	UPDATE TP_Regulation_Solde SET NUM_DOC =
		CASE 
			WHEN (@NUM_DOC < 10) THEN REPLACE (N'00000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100) THEN REPLACE (N'0000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 1000) THEN REPLACE (N'000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 10000) THEN REPLACE (N'00' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100000) THEN REPLACE (N'0' + STR(@NUM_DOC), ' ','')
			ELSE STR(@NUM_DOC)
		END 
	WHERE ID_Regulation_Solde = @ID;

    -- FIN --
END








GO
/****** Object:  Trigger [dbo].[AFTER_SAVE_REGULATION_STOCK]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_SAVE_REGULATION_STOCK] ON  [dbo].[TP_Regulation_Stock] AFTER INSERT
AS 
BEGIN
	 DECLARE @ID as bigint, @NUM_DOC as bigint, @Create_Code_User as int, @type as int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for trigger here
	select @ID = ID_Regulation_Stock, @Create_Code_User = Create_Code_User, @type = Type_Element from inserted
			
	--mise a jour du numero de la commande			
	select @NUM_DOC = COUNT(ID_Regulation_Stock) from [TP_Regulation_Stock] WHERE Type_Element = @type;
	UPDATE TP_Regulation_Stock SET NUM_DOC =
		CASE 
			WHEN (@NUM_DOC < 10) THEN REPLACE (N'00000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100) THEN REPLACE (N'0000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 1000) THEN REPLACE (N'000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 10000) THEN REPLACE (N'00' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100000) THEN REPLACE (N'0' + STR(@NUM_DOC), ' ','')
			ELSE STR(@NUM_DOC)
		END 
	WHERE ID_Regulation_Stock = @ID;

    -- FIN --
END









GO
/****** Object:  Trigger [dbo].[AFTER_SAVE_RETOUR_EMBALLAGE]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_SAVE_RETOUR_EMBALLAGE] 
   ON  [dbo].[TP_Retour_Emballage] 
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @NUM_DOC as BIGINT, @ID_Retour_Emballage as BIGINT,@Create_Code_User as int, @Type as int
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	SELECT @Create_Code_User = Create_Code_User  ,@ID_Retour_Emballage = ID_Retour_Emballage, @Type = Type_Element FROM inserted
	
	
	select @NUM_DOC = COUNT(ID_Retour_Emballage) from [TP_Retour_Emballage] where Type_Element = @Type;
	
	UPDATE TP_Retour_Emballage SET NUM_DOC =
		CASE 
			WHEN (@NUM_DOC < 10) THEN REPLACE (N'00000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100) THEN REPLACE (N'0000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 1000) THEN REPLACE (N'000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 10000) THEN REPLACE (N'00' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100000) THEN REPLACE (N'0' + STR(@NUM_DOC), ' ','')
			ELSE STR(@NUM_DOC)
		END 
	WHERE ID_Retour_Emballage = @ID_Retour_Emballage;
	
	DELETE FROM [TP_Retour_Emballage_Temp] WHERE Create_Code_User = @Create_Code_User
			
END








GO
/****** Object:  Trigger [dbo].[AFTER_SAVE_VENTE]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_SAVE_VENTE]
   ON  [dbo].[TP_Vente] 
   AFTER INSERT
AS 
BEGIN
 DECLARE @NUM_DOC as BIGINT, @ID_Vente as BIGINT, @Create_Code_User AS INT, @Solde_Liquide as float, @Solde_Emballage as float,@ID_Client as int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	SELECT @ID_Vente = ID_Vente, @Create_Code_User = Create_Code_User, @ID_Client = ID_Client FROM inserted
	
	--SELECT @Solde_Liquide =  Solde FROM FN_Etat_Solde_Client(0,GETDATE(), GETDATE()) WHERE ID_Client = @ID_Client
	--SELECT @Solde_Emballage =  Solde FROM FN_Etat_Solde_Client(1,GETDATE(), GETDATE()) WHERE ID_Client = @ID_Client
	-- enregistrement des details de la vente
	 INSERT INTO [TP_Vente_Liquide]([ID_Vente],[ID_Produit],[Quantite_Casier_Liquide],[Quantite_Bouteille_Liquide],
	 [PU_Vente],[PU_Achat],[Conditionnement],[Quantite_Casier_Emballage],[Quantite_Plastique_Emballage],
	 [Quantite_Bouteille_Emballage],[PU_Plastique],[PU_Bouteille],[Ristourne_Client],[Cumuler_Ristourne],[Produit_Decomposer])
		SELECT @ID_Vente, Temp.[ID_Produit] ,Temp.[Quantite_Casier_Liquide],Temp.[Quantite_Bouteille_Liquide] ,
		Temp.[PU_Vente],Temp.[PU_Achat],Temp.[Conditionnement],Temp.[Quantite_Casier_Emballage],Temp.[Quantite_Plastique_Emballage],
		Temp.[Quantite_Bouteille_Emballage],Temp.[PU_Plastique],Temp.[PU_Bouteille], Temp.[Ristourne_Client], Temp.[Cumuler_Ristourne],Temp.[Produit_Decomposer]
			FROM TP_Vente_Liquide_Temp Temp WHERE Temp.Create_Code_User = @Create_Code_User
	
	
	-- enregistrement des details de la vente
	 INSERT INTO [TP_Vente_Emballage]([ID_Vente],[ID_Emballage],[Quantite_Casier],[Quantite_Plastique],[Quantite_Bouteille],
	 [Conditionnement],[PU_Plastique],[PU_Bouteille])
		SELECT @ID_Vente, Temp.[ID_Emballage] , Temp.[Quantite_Casier],Temp.[Quantite_Plastique],Temp.[Quantite_Bouteille] ,
		Temp.[Conditionnement], Temp.[PU_Plastique],Temp.[PU_Bouteille]
			FROM TP_Vente_Emballage_Temp Temp WHERE Temp.Create_Code_User = @Create_Code_User
	
	SELECT @NUM_DOC = count(ID_Vente) FROM TP_Vente
	 UPDATE TP_Vente SET NUM_DOC =
		CASE 
			WHEN (@NUM_DOC < 10) THEN REPLACE (N'00000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100) THEN REPLACE (N'0000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 1000) THEN REPLACE (N'000' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 10000) THEN REPLACE (N'00' + STR(@NUM_DOC), ' ','')
			WHEN (@NUM_DOC < 100000) THEN REPLACE (N'0' + STR(@NUM_DOC), ' ','')
			ELSE STR(@NUM_DOC)
		END--, Solde_Liquide = @Solde_Liquide, Solde_Emballage = @Solde_Emballage
	WHERE ID_Vente = @ID_Vente;
	
	DELETE FROM [TP_Vente_Liquide_Temp] WHERE Create_Code_User = @Create_Code_User
    DELETE FROM [TP_Vente_Emballage_Temp] WHERE Create_Code_User = @Create_Code_User
   
END







GO
/****** Object:  Trigger [dbo].[AFTER_INSERT_VENTE_EMBALLAGE_TEMP]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_INSERT_VENTE_EMBALLAGE_TEMP] 
   ON  [dbo].[TP_Vente_Emballage_Temp] 
   AFTER INSERT
AS 
BEGIN
	declare @ID as bigint, @Conditionnement as int, @PU_Bouteille as float, @PU_Plastique as int, @ID_Emballage as int 
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	select @ID = ID_Vente_Emballage_Temp, @ID_Emballage = ID_Emballage from inserted
	
	SELECT @Conditionnement = Conditionnement,
	@PU_Plastique = PU_Plastique, @PU_Bouteille = PU_Bouteille FROM TP_Emballage WHERE ID_Emballage = @ID_Emballage 

	UPDATE TP_Vente_Emballage_Temp SET
	PU_Bouteille = @PU_Bouteille,
	PU_Plastique = @PU_Plastique,
	Conditionnement = @Conditionnement
	WHERE ID_Vente_Emballage_Temp = @ID
END






GO
/****** Object:  Trigger [dbo].[AFTER_INSERT_VENTE_LIQUIDE]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_INSERT_VENTE_LIQUIDE] 
   ON [dbo].[TP_Vente_Liquide] AFTER INSERT
AS 
BEGIN
	DECLARE @NUM_DOC as bigint, @ID as bigint, @ID_Produit as int, @ID_Vente as bigint, @ID_Client as int, @Create_Code_User as int,
	@PU_Vente as float, @PU_Achat as float, @Conditionnement as int, @PU_Plastique as float,
	@PU_Bouteille as float, @Ristourne_Client as float, @Cumuler_Ristourne as bit, @Produit_Decomposer as bit
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	SELECT @ID = ID_Vente_Liquide,  @ID_Produit = ID_Produit, @ID_Vente = ID_Vente FROM INSERTED
	SELECT @ID_Client = ID_Client FROM TP_Vente WHERE ID_Vente = @ID_Vente
	
	SELECT @PU_Achat = PU_Achat, @PU_Vente = PU_Vente, @Conditionnement = Conditionnement,
	@PU_Plastique = PU_Plastique, @PU_Bouteille = PU_Bouteille, @Produit_Decomposer = Produit_Decomposer  FROM TP_Produit produit INNER JOIN TP_Emballage
	emballage ON produit.ID_Emballage = emballage.ID_Emballage WHERE ID_Produit = @ID_Produit 
	
	SELECT @Ristourne_Client = Ristourne, @Cumuler_Ristourne = Cumuler_Ristourne FROM TP_Ristourne_Client WHERE ID_Client = @ID_Client AND ID_Produit = @ID_Produit AND Actif = 1
    if(@Ristourne_Client is null) 
	begin
		set @Ristourne_Client = 0;
		set @Cumuler_Ristourne = 0
	end
		
		
	UPDATE TP_Vente_Liquide SET 
	PU_Vente = @PU_Vente, 
	PU_Achat = @PU_Achat, 
	Conditionnement = @Conditionnement,
	PU_Plastique = @PU_Plastique, 
	PU_Bouteille = @PU_Bouteille,
	Produit_Decomposer = @Produit_Decomposer,
	Ristourne_Client = @Ristourne_Client,
	Cumuler_Ristourne = @Cumuler_Ristourne
	WHERE ID_Vente_Liquide = @ID
	
END






GO
/****** Object:  Trigger [dbo].[AFTER_INSERT_VENTE_LIQUIDE_TEMP]    Script Date: 12/02/2019 21:40:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[AFTER_INSERT_VENTE_LIQUIDE_TEMP] 
   ON [dbo].[TP_Vente_Liquide_Temp] AFTER INSERT
AS 
BEGIN
	DECLARE @NUM_DOC as bigint, @ID as bigint, @ID_Produit as int, @ID_Vente as bigint, @ID_Client as int, @Create_Code_User as int,
	@PU_Vente as float, @PU_Achat as float, @Conditionnement as int, @PU_Plastique as float,
	@PU_Bouteille as float, @Ristourne_Client as float, @Cumuler_Ristourne as bit, @Produit_Decomposer as bit
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	SELECT @ID = ID_Vente_Liquide_Temp,  @ID_Produit = ID_Produit, @ID_Client = ID_Client FROM INSERTED
	
	
	SELECT @PU_Achat = PU_Achat, @PU_Vente = PU_Vente, @Conditionnement = Conditionnement,
	@PU_Plastique = PU_Plastique, @PU_Bouteille = PU_Bouteille, @Produit_Decomposer = Produit_Decomposer  FROM TP_Produit produit INNER JOIN TP_Emballage
	emballage ON produit.ID_Emballage = emballage.ID_Emballage WHERE ID_Produit = @ID_Produit 
	
	SELECT @Ristourne_Client = Ristourne, @Cumuler_Ristourne = Cumuler_Ristourne FROM TP_Ristourne_Client WHERE ID_Client = @ID_Client AND ID_Produit = @ID_Produit AND Actif = 1
    if(@Ristourne_Client is null) 
	begin
		set @Ristourne_Client = 0;
		set @Cumuler_Ristourne = 0
	end
		
		
	UPDATE TP_Vente_Liquide_Temp SET 
	PU_Vente = @PU_Vente, 
	PU_Achat = @PU_Achat, 
	Conditionnement = @Conditionnement,
	PU_Plastique = @PU_Plastique, 
	PU_Bouteille = @PU_Bouteille,
	Produit_Decomposer = @Produit_Decomposer,
	Ristourne_Client = @Ristourne_Client,
	Cumuler_Ristourne = @Cumuler_Ristourne
	WHERE ID_Vente_Liquide_Temp = @ID
	
END






GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Reglement Fournisseur, 1 = Payement Ristourne Client, 2 = Depense' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TP_Decaissement', @level2type=N'COLUMN',@level2name=N'Type_Element'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = En Attente de validation, 1 = Valider, 2 = Regularisation du stock, 3 = Annuler' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TP_Inventaire', @level2type=N'COLUMN',@level2name=N'Etat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TG_Personnels"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vue_Personnel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vue_Personnel'
GO
