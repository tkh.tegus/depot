﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using PROJETMODELE.Models.DAL;
using PROJETMODELE.Tools;
using System.Threading;
using System.Globalization;

namespace PROJETMODELE
{
    // Remarque : pour obtenir des instructions sur l'activation du mode classique IIS6 ou IIS7, 
    // visitez http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {

        public static int m_handle { get; set; }
        public static DateTime DateEnregistrement { get; set; }
        private Thread tread { get; set; }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            DateEnregistrement = DateTime.Now;
           
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session.Timeout = 60;
            Response.Redirect("/Login/Login");
        }
        //protected void Application_Error(Object sender, EventArgs e)
        //{
        //    Exception ex = Server.GetLastError().GetBaseException();
        //    //using (MailMessage message = new MailMessage())
        //    //{
        //    //    message.IsBodyHtml = true;
        //    //    message.Priority = MailPriority.High;
        //    //    MailAddress from = new MailAddress("ton_email_from@ton_domaine.com");
        //    //    MailAddress to = new MailAddress("ton_email_to@ton_domaine.com");
        //    //    MailAddress BoiteBug = new MailAddress("boite_bug@ton_domaine.com");
        //    //    message.Sender = from;
        //    //    message.To.Add(to);
        //    //    message.To.Add(BoiteBug);
        //    //    message.Subject = ex.Message;
        //    //    StringBuilder chaine = new StringBuilder();
        //    //    chaine.Append("<b>Erreur sur le site</b><br><br>");
        //    //    chaine.Append("Détails : <br><br>");
        //    //    chaine.Append(string.Format("Date : {0}<br><br>", DateTime.Now));
        //    //    chaine.Append(string.Format("Pile : {0}<br><br>", ex.StackTrace));
        //    //    chaine.Append(string.Format("Source : {0}<br><br>", ex.Source));
        //    //    chaine.Append(string.Format("Url :", Request.Url));

        //    //    message.Body = Server.HtmlEncode(chaine.ToString());

        //    //    SmtpClient smtp = new SmtpClient("localhost");
        //    //    smtp.Send(message);
        //    //}

        //}

        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    CultureInfo CurrentCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
        //    CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";

        //    System.Threading.Thread.CurrentThread.CurrentCulture = CurrentCulture;
        //    System.Threading.Thread.CurrentThread.CurrentUICulture = CurrentCulture;
        //}
    }
}