﻿function insertion(repdeb, repfin) {
    var input = document.forms[0].elements['Libelle'] || document.forms[1].elements['Libelle'];
    console.log(input);
    input.focus();
    /* pour l'Explorer Internet */
    if (typeof document.selection != 'undefined') {
        /* Insertion du code de formatage */
        var range = document.selection.createRange();
        var insText = range.text;
        range.text = repdeb + insText + repfin;
        /* Ajustement de la position du curseur */
        range = document.selection.createRange();
        if (insText.length == 0) {
            range.move('character', -repfin.length);
        } else {
            range.moveStart('character', repdeb.length + insText.length + repfin.length);
        }
        range.select();
    }
        /* pour navigateurs plus récents basés sur Gecko*/
    else if (typeof input.selectionStart != 'undefined') {
        /* Insertion du code de formatage */
        var start = input.selectionStart;
        var end = input.selectionEnd;
        var insText = input.value.substring(start, end);
        input.value = input.value.substr(0, start) + repdeb + insText + repfin + input.value.substr(end);
        /* Ajustement de la position du curseur */
        var pos;
        if (insText.length == 0) {
            pos = start + repdeb.length;
        } else {
            pos = start + repdeb.length + insText.length + repfin.length;
        }
        input.selectionStart = pos;
        input.selectionEnd = pos;
    }
        /* pour les autres navigateurs */
    else {
        /* requête de la position d'insertion */
        var pos;
        var re = new RegExp('^[0-9]{0,3}$');
        while (!re.test(pos)) {
            pos = prompt("Insertion à la position (0.." + input.value.length + "):", "0");
        }
        if (pos > input.value.length) {
            pos = input.value.length;
        }
        /* Insertion du code de formatage */
        var insText = prompt("Veuillez entrer le texte à formater:");
        input.value = input.value.substr(0, pos) + repdeb + insText + repfin + input.value.substr(pos);
    }
}

$("#formulaire #Libelle").off('keyup');
$("#formulaire #Libelle").on('keyup', function () {
   
    nbr1 = $(this).val().split('^').length - 1;
    nbr2 = $(this).val().split('|').length - 1;
    nbr3 = $(this).val().split('€').length - 1;
    nbr4 = $(this).val().split('}').length - 1;
    nbr5 = $(this).val().split('{').length - 1;
    nbr6 = $(this).val().split('[').length - 1;
    nbr7 = $(this).val().split('~').length - 1;
    nbr8 = $(this).val().split(']').length - 1;
    nbr9 = $(this).val().split('\\').length - 1;
    $("#formulaire .labelss").text($(this).val().length + nbr1 + nbr2 + nbr3 + nbr4 + nbr5 + nbr6 + nbr7 + nbr8 + nbr9);
   
});
$(".detailsms").toggle();
$("#formulaire .explication").off('click');
$("#formulaire .explication").on('click', function () {
    $(".detailsms").toggle();
});