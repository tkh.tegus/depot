﻿$("button").jqxButton({ width: 'auto', theme: "ui-redmond" });
$('#jqxButton').on('click', function () {
    $('#formulaire').jqxValidator('validate')
});
$('#jqxAnnuler').off('click');
$('#jqxAnnuler').on('click',function () {
        $("#loaderSpace,#loader").show();
        $('#formulaire').jqxValidator('hide');
        $('#formulaire').jqxWindow("destroy");
        $("#grades").jqxGrid('updatebounddata', 'cells');
        $("#grades").jqxGrid('clearselection');
        $("#loaderSpace,#loader").hide()
    });
$('#formulaire').on('validationSuccess',function (b) {
        $('form').on('submit', function () {
            $('form').off('submit');
            $("#loaderSpace,#loader").show();
            $('#formulaire').jqxValidator('hide');
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: $(this).serialize(),
                dataType: 'json'
            }).success(function (a) {
                $(".toast-item").remove();
                if (a.length == 2) {
                    var url = $("#OpenconsulterUrl").attr('value');
                    $('#formulaire').jqxWindow("destroy");
                    $("#grades").jqxGrid('updatebounddata', 'cells');
                    $("#grades").jqxGrid('clearselection');
                    $('body').append(a[0]);
                    if (a[1] != null) {
                        if ($("#OpenconsulterUrl").length != 0) { // si on doit ouvrir une page en popup
                            OpenConsulter(url + a[1]);
                        }
                        else if ($("#RedirectUrl").length != 0) {  //si on doit rediriger l'utilisateur
                            var redirectionurl = $("#RedirectUrl").attr('value');
                            Afficher(redirectionurl + a[1]);
                        }
                    }
                    
                } else {
                    $('body').append(a)
                }
                $("#loaderSpace,#loader").hide();
            }).error(function () {
                $("#loaderSpace,#loader").hide();
                alert('echek')
            }); return false
        })
    });

$('#formulaire').on('validationError', function (a) {
    $('form').on('submit', function () {
       return false;
    })
});

function OpenConsulter(url) {
    $.ajax({
        type: 'GET',
        url: url,
        success: function (response) {
            $(".toast-item").remove();
            $("#loaderSpace,#loader").hide();
            $.each(response, function (i, item) {
                $("#popupform").html(item);
            });
            $('#formulaire').jqxWindow({ height: "95%", width: "95%", isModal: true, minHeight: "20%", showCloseButton: true, theme: "ui-redmond" });
        },
        error: function (jqXHR, exception) {
            $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });

}

function Redirection(url) {
    $.ajax({
        type: 'GET',
        url: url,
        success: function (response) {
            $(".toast-item").remove();
            $.each(response, function (i, item) {
                if (i == 0) {
                    $('#contentajax').html(item);
                }
                else if (i == 1)
                    $('#main header h3').html(item);
                else
                    $(".breadcrumbs").html(item);
            });
            $("#loaderSpace,#loader").hide();
        },
        error: function (jqXHR, exception) {
            $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });

}