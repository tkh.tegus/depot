﻿/*!
 * Component Integrity Constrainst
 * OUFAREZ
 * Date: 16/08/2017
 */


/*!
 * Indique si une chaîne spécifiée est nulle, vide ou se compose uniquement de caractères blancs
 *
 */
IsNullOrWhiteSpace_OUF = function (Libelle) {
    if (jQuery.trim(Libelle) == "") {
        return true;
    } else {
        return false;
    }
}




/*!
 * indique si une chaîne est un nombre
 *
 */
IsNumber_OUF = function (Libelle) {
    var zippattern = /^[0-9]\d*(,\d+)?$/;
    return (zippattern.test(Libelle) == true);
}

/*!
 * indique si une chaîne est un nombre
 *
 */
IsNumberWithSigne_OUF = function (Libelle) {
    var zippattern = /[-+]\d{1,}/;
    return (zippattern.test(Libelle) == true);
}

/*!
 * indique si une chaîne est un nombre mais avec le point utiliser dans les notes
 *
 */
IsNumberWhithPoint_OUF = function (Libelle) {
    var zippattern = /^[0-9]\d*(.\d+)?$/;
    return (zippattern.test(Libelle) == true);
}
/*!
 * Perlet d'echapper une chaine en javascript
 *
 */
function EchappeString(ch) {
ch = ch.replace(/\\/g,"\\\\")
ch = ch.replace(/\'/g,"\\'")
ch = ch.replace(/\"/g,"\\\"")
return ch
}

