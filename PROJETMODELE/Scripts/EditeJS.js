﻿$('#jqxButton').on('click', function () { $('#formulaire').jqxValidator('validate') });

$('#formulaire').on('close', function (event) {
    $("#loaderSpace,#loader").show();
    $.ajax({
        url: '/grades/deverouiller', type: 'GET',
        success: function (a) {
            $('#formulaire').jqxValidator('hide');
            $('#formulaire').jqxWindow("destroy");
            $("#grades").jqxGrid('updatebounddata', 'cells');
            $("#grades").jqxGrid('clearselection');
        }
    });
    $("#loaderSpace,#loader").hide();
    return false;
});

$('#jqxAnnuler').off('click'); $('#jqxAnnuler').on('click', function () {
    $("#loaderSpace,#loader").show();
    $.ajax({
        url: '/grades/deverouiller',
        type: 'GET',
        success: function (a) {
            $('#formulaire').jqxValidator('hide');
            $('#formulaire').jqxWindow("destroy");
            $("#grades").jqxGrid('updatebounddata', 'cells');
            $("#grades").jqxGrid('clearselection');
            $("#loaderSpace,#loader").hide()
        }
    }); return false
});
$('#formulaire').on('validationSuccess', function (b) {
    $('form').on('submit', function () {
        $('form').off('submit');
        $("#loaderSpace,#loader").show();
        $('#formulaire').jqxValidator('hide');
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json'
        }).success(function (a) {
            $(".toast-item").remove();
            if (a.length == 2) {
                $('#formulaire').jqxWindow("destroy");
                $("#grades").jqxGrid('updatebounddata', 'cells');
                $("#grades").jqxGrid('clearselection'); $('body').append(a)
            } else { $('body').append(a) } $("#loaderSpace,#loader").hide()
        }).error(function () { $("#loaderSpace,#loader").hide(); alert('echek') }); return false
    })
});
$('#formulaire').on('validationError', function (a) { $('form').on('submit', function () { return false; }) });