﻿var tabPDF = "";
$('#Confirmpdf').css("display", "none");
$('#Confirmpdf').jqxWindow({ height: "120px", width: "350px", isModal: true, theme: "ui-redmond" });
$('#Confirmpdf').jqxWindow("close");
$("#okpdf").off('click');
$("#okpdf").on('click', function () {
    $("#loaderSpace,#loader").show();
    $('#Confirmpdf').jqxWindow("close");
    if ($("#wfile").is(':checked')) {
        url = '/Base/AllFiche/0';
    } else {
        url = '/Base/AllFiche/1';
    }
    DoawlaodFile(url, tabPDF);
});

$("#annulerpdf").off('click');
$("#annulerpdf").on('click', function () {
    $('#Confirmpdf').jqxWindow("close");
    id = 0;
});


$(".AvecIdentifiantEnvoieFichier").off('click');
function DoawlaodFile(url, param) {
    $.ajax({
        url: '/Base/SetTitre',
        type: 'POST',
        data: 'q=' + param,
        dataType: 'json',
        success: function (response) {
            $(".toast-item").remove();
            $("#loaderSpace,#loader").hide();
            window.location = url;
        },
        error: function (jqXHR, exception) {
            $("#loaderSpace,#loader").hide();
            if (jqXHR.status === 0) {
                alert('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });
    return false;
}

$(".AvecIdentifiantEnvoieFichier").on('click', function () {
    var selectedRows = $("#grades").jqxGrid('getselectedrowindexes');
    var tab = '';
    $.each(selectedRows, function (index, row) {
        var ligne = $("#grades").jqxGrid('getrowdata', row);
        if (tab == '')
            tab = ligne.id;
        else
            tab = tab + ';' + ligne.id;
    });
    if (tab == '') {
        $('#messages').jqxWindow("open");
        return;
    }
    tabPDF = tab;
    $('#Confirmpdf').jqxWindow("open");
    return false;
});