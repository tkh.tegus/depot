﻿
function InitialiseFiltre() {

    if ($("#DateDebut").attr('value') != null && $("#DateFin").attr('value') != null) {
        var DateDebut = $("#DateDebut").attr('value').split(' ')[0].split('/');

        var DateFin = $("#DateFin").attr('value').split(' ')[0].split('/');

        $.getScript('/Content/jqwidgets/globalization/globalize.culture.fr-FR.js', function () {
            $("#startdate").jqxDateTimeInput({ readonly: true, width: '120px', height: '30px', theme: "ui-redmond", culture: 'fr-FR', formatString: 'd/M/yyyy' });
            $("#enddate").jqxDateTimeInput({ readonly: true, width: '120px', height: '30px', theme: "ui-redmond", culture: 'fr-FR', formatString: 'd/M/yyyy' });
        });
        if (DateDebut.length >= 2) {
            $("#startdate").val(DateDebut[2] + "-" + DateDebut[1] + "-" + DateDebut[0]);
        }

        if (DateFin.length >= 2) {
            $("#enddate").val(DateFin[2] + "/" + DateFin[1] + "/" + DateFin[0]);
        }
        $("#DateDebut").css('display', 'none');
        $("#DateDebut").css('visibility', 'hidden');
        $("#DateFin").css('display', 'none');
        $("#DateFin").css('visibility', 'hidden');
    }
}

$('#startdate').on('valueChanged', function (event) {
    var textstart = $('#startdate').jqxDateTimeInput('getText');
    $('#DateDebut').attr('value', textstart);
});

$('#enddate').on('valueChanged', function (event) {
    var textend = $('#enddate').jqxDateTimeInput('getText');
    $('#DateFin').attr('value', textend);
});

$("#filtrer").on("click", function (event) {
    FormatDateFiltre($(this));
    return false;
});
function FormatDateFiltre(event) {
    // appel Ajax
    $("#loaderSpace,#loader").show();
    $.ajax({
        url: "/Base/BuildFiltre", // le nom du fichier indiqué dans le formulaire
        type: 'POST', // la méthode indiquée dans le formulaire (get ou post)
        data: $('form').serialize(), // je sérialise les données (voir plus loin), ici les $_POST             
        dataType: 'json'
    }).success(function (html) { // je récupère la réponse de l'action
        $(".toast-item").remove();
        // si le boutton a la classe css EditUrlFiltre alors il faut modofier le paramettre de l'url
        if ($(event).hasClass('EditUrlFiltre'))
        {
            var TempSource = $("#grades").jqxGrid('source');// recuperer la source
            var OldUrl = TempSource._source.url;// recuperer l'url de la source
            var RecupereUrl = OldUrl.split("/");
            OldUrl = "/" + RecupereUrl[1] + "/" + RecupereUrl[2];
            TempSource._source.url = OldUrl + "/1";
            $("#grades").jqxGrid('source', TempSource);
        }
        $("#grades").jqxGrid('updatebounddata', 'cells');
        $("#grades").jqxGrid('clearselection');
        $('body').append(html);
        $("#loaderSpace,#loader").hide();
    }).error(function () { $("#loaderSpace,#loader").hide(); alert('echek'); });
    return false; // j'empêche le navigateur de soumettre lui-même le formulaire
}

function replaceUrlParam(url, paramName, paramValue) {
    var pattern = new RegExp('(' + paramName + '=).*?(&|$)')
    var newUrl = url
    if (url.search(pattern) >= 0) {
        newUrl = url.replace(pattern, '$1' + paramValue + '$2');
    }
    else {
        newUrl = newUrl + (newUrl.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue
    }
    return newUrl
}

