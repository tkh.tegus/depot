﻿using System.Web;
using System.Web.Optimization;

namespace PROJETMODELE
{
    public class BundleConfig
    {
        // Pour plus d’informations sur le Bundling, accédez à l’adresse http://go.microsoft.com/fwlink/?LinkId=254725 (en anglais) , "~/Content/scripts/jsdesign.js"
        public static void RegisterBundles ( BundleCollection bundles )
        {
            bundles.Add ( new ScriptBundle ( "~/bundles/jquery" ).Include (
                        "~/Scripts/jquery-{version}.js" ));

            bundles.Add(new ScriptBundle("~/bundles/design").Include(
                        "~/Content/scripts/jquery.js",
                        "~/Content/scripts/hideshow.js",
                        "~/Content/scripts/switcher.js",
                        "~/Content/scripts/jquerytablesortermin.js",
                        "~/Content/scripts/jquery.equalHeight.js",
                        "~/Content/scripts/ribbon.js",
                        "~/Content/scripts/jquerytooltipmin.js",
                        "~/Content/scripts/jsdesign.js",
                        "~/Content/scripts/jquery.toastmessage.js" ));



            bundles.Add(new ScriptBundle("~/bundles/jqwidgets/js").Include(
                         "~/Content/jqwidgets/jqxcore.js",
                         "~/Content/jqwidgets/jqxdata.js",
                         "~/Content/jqwidgets/jqxdata.export.js",
                         "~/Content/jqwidgets/jqxbuttons.js",
                         "~/Content/jqwidgets/jqxscrollbar.js",
                         "~/Content/jqwidgets/jqxcheckbox.js",
                         "~/Content/jqwidgets/jqxdropdownlist.js",
                         "~/Content/jqwidgets/jqxlistbox.js",
                         "~/Content/jqwidgets/jqxgrid.js",
                         "~/Content/jqwidgets/jqxgrid.columnsresize.js",
                         "~/Content/jqwidgets/jqxgrid.filter.js",
                         "~/Content/jqwidgets/jqxgrid.edit.js",
                         "~/Content/jqwidgets/jqxgrid.grouping.js",
                         "~/Content/jqwidgets/jqxgrid.aggregates.js",
                         "~/Content/jqwidgets/jqxmenu.js",
                         "~/Content/jqwidgets/jqxinput.js",
                         "~/Content/jqwidgets/jqxgrid.sort.js",
                         "~/Content/jqwidgets/jqxgrid.pager.js",
                         "~/Content/jqwidgets/jqxgrid.selection.js",
                         "~/Content/jqwidgets/jqxgrid.export.js",
                         "~/Content/jqwidgets/jqxexpander.js",
                         "~/Content/jqwidgets/jqxtabs.js",
                         "~/Content/jqwidgets/jqxtooltip.js",
                         "~/Content/jqwidgets/jqxchart.js",
                         "~/Content/jqwidgets/jqxpanel.js",
                         "~/Content/jqwidgets/jqxtree.js",
                         "~/Content/jqwidgets/jqxvalidator.js",
                         "~/Content/jqwidgets/jqxwindow.js",
                         "~/Content/jqwidgets/jqxcalendar.js",
                         "~/Content/jqwidgets/jqxnavigationbar.js",
                         "~/Content/jqwidgets/jqxdatetimeinput.js",
                         "~/Content/jqwidgets/globalization/globalize.js"
                        ));

            bundles.Add ( new StyleBundle ( "~/bundles/jqwidgets/css" ).Include ( 
                "~/Content/jqwidgets/styles/jqx.base.css",
                "~/Content/jqwidgets/styles/jqx.ui-redmond.css"
                ) );
            
            // script de validation cote client à introduire dans le master page 
            bundles.Add ( new ScriptBundle ( "~/bundles/jquery" ).Include (
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*" ) );
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/site.css",
                "~/Content/css/layout.css",
                "~/Content/css/ribbon.css",
                "~/Content/css/soft_button.css",
                "~/Content/css/FiltreCSS.css",
                "~/Content/jquery.toastmessage.css"));

        }
    }
}